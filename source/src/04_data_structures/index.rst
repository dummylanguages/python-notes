###############
DATA STRUCTURES
###############

.. toctree::
	:maxdepth: 3
	:hidden:

	strings_toc
	lists_toc
	tuples
	dictionaries
	sets

Here we are gonna be seeing more Python's **core types**, more specifically those who can be grouped under the notion of **data structures**. Several Python's **core types** fit into this category. In computer science, a **data structure** is a unit of data that consists of several smaller pieces of data. Or in other words, a data structure is an object that contains a collection of smaller objects. 

.. note:: A data structure is a **collection** of objects

For example, the **integer** ``32`` is an object with just one item, the number ``32``. On the other hand, the **string** ``'hello'`` is a data structure: an object that contains a collection of smaller objects. These smaller objects are the individual characters of ``'hello'``. 

Data structures can be **classified** according to different criteria:

1. Depending on the position of the elements inside a data structure, we have two kind of data structures:

	* **Ordered** collection of objects linearly ordered also known as **Sequences**. For example, a string is a sequence, the order of the characters in ``'hello'`` matters, otherwise it wouldn't say ``'hello'``.
	
	* **Unordered** collection of objects, such as **dictionaries** or **sets**.

2. Regarding its **content**, a collection can be:
	* **Homogeneous**, meaning that all items in the collection belong to the same type (in **strings**, all of its elements are characters)
	* **Heterogeneous**, meaning the items can be of different types (**lists**).

3. Another important characteristic is **mutability**. Most data structures can be modified(lists), but other ones, like **strings** or **tuples**, are inmutable.

.. note:: A **sequence** is a data structure, in which all its elements are linearly ordered, left to right.

We are gonna start the study of data structures with **sequences**. In Python we have three **core types** that fall into this category:

  * Strings
  * Lists
  * Tuples 
