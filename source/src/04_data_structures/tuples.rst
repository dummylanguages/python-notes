******
Tuples
******
On top of **strings** and **lists** Python has another built-in sequence: the tuple. As sequences, tuples support common sequence operations like indexing and slicing. But they also have features that make them unique:

	* Tuples are **heterogeneous**, like lists. They can contain any kind of object, including other compound objects (e.g., lists, dictionaries, other tuples).
	* They also are **inmutable**, like strings. They don’t support any of the in-place change operations applied to lists. Tuples have fixed-length, you cannot change the size of a tuple without making a copy.
	* Like lists, tuples are **arrays of object references**.

Tuple literals
==============
A tuple is written as a series of objects (technically, expressions that generate objects), separated by commas and normally enclosed in parentheses::

	>>> my_tpl = ('Ralph', 123, 3.14, ['another', 'list'])

Although parentheses are the common  way or write tuples, if you separate some values with commas, you automatically have a tuple::

	>>> another_tpl = 1, 2, 3
	type(another_tpl)
	<class 'tuple'>

An **empty tuple** is just a parentheses pair with nothing inside::

	>>> empty_tpl = ()
	>>> type(empty_tpl)
	<class 'tuple'>

To write a tuple containing a **single value**, you have to include a **comma**, even though there is only one value (the parentheses are optional)::

	>>> your_tpl = 42,
	>>> type(your_tpl)
	<class 'tuple'>
	>>> your_tpl
	(42,)

The comma is crucial. Simply adding parentheses won’t help: ``(42)`` is exactly the same as ``42``::

	>>> not_tpl = (42)
	>>> type(not_tpl)
	<class 'int'>

.. note::
	The **comma** is the **tuple constructor**, not the parentheses.

+----------------------------+---------------------------------------+
| Some tuple literals        |                                       |
+============================+=======================================+
| ()                         | An empty tuple                        |
+----------------------------+---------------------------------------+
| tpl = ('spam',)            | A one-item tuple (not an expression)  |
+----------------------------+---------------------------------------+
| tpl = ('A', 'Ni', 3.14, 7) | A four-item tuple                     |
+----------------------------+---------------------------------------+
| tpl = 'A', 'Ni', 3.14, 7   | Same as previous line, but without () |
+----------------------------+---------------------------------------+
| tpl = ('Bob', (1, 4.5))    | Nested tuples                         |
+----------------------------+---------------------------------------+

Tuple syntax peculiarities: Commas and parentheses
==================================================
Python allows us to **omit the enclosing parentheses** for a tuple in contexts where it isn’t syntactically ambiguous to do so. But because parentheses can also enclose **expressions**, we need to do something special to tell Python when a single object in parentheses is a tuple object and not a simple expression.

The most common places where the parentheses are required for tuple literals are those where:

* Within a function call, or nested in a larger expression.
* Embedded in the literal of a larger data structure like a **list** or **dictionary**, or listed in a Python 2.X print statement.

In most other contexts, the enclosing parentheses are optional. That being said, parentheses improve readability making the tuples more explicit. 

Operations with tuples
======================
Tuples support the usual sequence operations that we saw for both strings and lists:

* Concatenation::

	>>> (1, 2) + (3, 4) 
	(1, 2, 3, 4)

* Repetition::

	>>> (1, 2) * 4
	(1, 2, 1, 2, 1, 2, 1, 2)

* Indexing, slicing

	>>> tpl = (1, 2, 3, 4) 
	>>> tpl[0]
	1
	>>> tpl[1:3]
	(2, 3)
	>>> tpl_2 = tpl[0], tpl[1:3]  # Creating another tuple.
	(1, (2, 3))

Being tuples inmutable objects, all of these operations return new tuples.

.. note::
	Tuples are inmutable.

Tuples methods
--------------
Tuples do not have all the methods that strings or lists have: for instance, due to their inmutability, they don't have an ``append`` method. In older versions of Python (prior to 2.6 and 3.0), tuples did not have methods at all. This was an old Python convention for immutable types, but now they do have some like::

	>>> tpl = ('a', 'b', 'c')
	>>> tpl.index(1)
	'b'
	>>> tpl.count('b')
	1

Conversions
-----------
Given the fact that tuples don't have the same methods as lists, if we ever need to sort a tuple, we would have to convert it to a list first, to **sort** it::

	>>> tpl = ('e', 'c', 'a', 'b', 'd')
	>>> temp_li = list(tpl)
	>>> temp_li.sort()
	>>> temp_li
	['a', 'b', 'c', 'd', 'e']

And then back to a tuple::

	>>> tpl = tuple(temp_li)

.. tip::
	We can also use the ``sorted()`` **built-in function**, that returns a sorted list, and convert back to a tuple after::

		>>> temp_li = sorted(tu)
		>>> tu = tuple(temp_li) 

Actually, when we use the functions ``list()`` and ``tuple()``, we are creating new objects in memory, but the practical result is a conversion.

**List comprehensions** can also be used to convert tuples::

	>>> tpl = ('e', 'c', 'a', 'b', 'd')
	>>> li = [x for x in tpl]
	>>> li
	['e', 'c', 'a', 'b', 'd']

.. todo::	Named tuples. Mark Lutz.

