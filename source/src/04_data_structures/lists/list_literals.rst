*************
List literals
*************
Literally, a list may be coded as a series of items enclosed in square brackets and separated by commas::

    >>> my_list = ['Ralph', 123, 3.14, ['another', 'list']]

We can also make empty lists::

    >>> empty_list = []
    >>> type(empty_list)
    <class 'list'>

Each of these items is an **object** (strictly speaking a reference to the object), or an **expression** that evaluates to an object::

    >>> li = [3 * 2, 1.4 + 3.6, 'hello' + ' world']
    >>> li
    [6, 5.0, 'hello world']

In real world programs, we are not gonna see much many written down as a literal expression. Most likely we are gonna find code that processes lists constructed dynamically (at runtime), from user inputs, file contents, etc. Although in practice, many data structures in Python are built by running program code at runtime, it’s still important to master literal syntax.