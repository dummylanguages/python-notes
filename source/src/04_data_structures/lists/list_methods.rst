************
List methods
************
Like strings, Python list objects also support **type-specific** methods. Methods are functions that are associated with and act upon particular objects. 

Changing lists
==============
We have already seen ways for changing lists using slice operations, but some programmers prefer to do it using list methods such as ``insert``, ``pop``, and ``remove``, since it's a more straightforward and mnemonic way to do it.

* If we want to insert **only one item** at the end of the list, we would use ``append``::
  
    >>> li = [1, 2, 3]
    >>> li.append(4)
    >>> li
    [1, 2, 3, 4]

We could have done that using **concatenation**, ``li + [4]`` but this way we are generating a copy of the list, while the ``append`` method doesn’t generate a new list, it's faster. 

.. tip::
    Another way of adding one (or several) element at the **end** is using **slicing**:: 

        li[len(li):] = [4]

    But this way also changes the list in place. Adding element(s) at the **beginning** it's just a matter of::

        li[:0] = [4]

* If we want to append **several items** at the end, the ``extend`` method does it::

    >>> li = [1, 2, 3]
    >>> li.extend(4, 5, 6)
    >>> li
    [1, 2, 3, 4, 5, 6]

* To delete **only one item** at the end of the list we would use, try ``pop`` ::
  
    >>> li = [1, 2, 3]
    >>> li.pop()
    3
    >>> li
    [1, 2]

We can pass an optional index position, so we can delete an item at any position, for example::

    >>> li = [1, 2, 3]
    >>> li.pop(1)
    2
    >>> li
    [1, 3]

Sorting lists
=============


Other methods
=============
There are methods that don't change lists, but return some information about its contents. For example, the ``count`` method returns the number of ocurrences of an item::

    >>> li = ['John', 'Brad', 'Edward', 'Brad']
    >>> li.count('Brad')
    2

The ``index`` method returns the first index position (starting from the left) of an item in the list::
    
    >>> li = ['John', 'Brad', 'Edward', 'Brad']
    >>> li.index('Brad')
    1

Remember that lists are zero-based indexed.

Copying lists
=============
When a variable is assigned to another variable referencing a list, both of them become references to the same list object in memory. For example::

    >>> li_1 = ['apple', 'strawberry', 'watermelon', 'orange']
    >>> li_2 = li_1
    >>> li_1[3] = 'ferrari'
    >>> li_1
    ['apple', 'strawberry', 'watermelon', 'ferrari']
    >>> li_2
    ['apple', 'strawberry', 'watermelon', 'ferrari']

Thus, any changes to the elements of list_1 results in changes to list_2, and viceversa. This is because we didn't make any copy of the list, we just assigned 2 names to the same list::

    >>> id(li_1)
    4362418120
    >>> id(li_2)
    4362418120

Both names are pointing to the same memory location, the same list object. Bottom line, we haven't made any copy.

Shallow copy
------------
A way of making a copy of a list is using the ``list()`` constructor. For example::

>>> li_a = [1, 2, 3, ['a', 'b', 'c']]
>>> li_b = list(li_a)

A copy of ``li_a`` has been made to ``li_b``, and both names are referencing different list objects::

    >>> id(li_a)
    4362418632
    >>> id(li_b)
    4362419208

Therefore, changes make to ``li_a`` will not affect to ``li_b``, for example::

    >>> li_a[1] = 'two'
    >>> li_a
    [1, 'two', 3, ['a', 'b', 'c']]
    >>> li_b
    [1, 2, 3, ['a', 'b', 'c']]

So far so good, but both lists contain nested list. What happens when we make a change on some item of the nested lists? Let's see it::

    >>> li_a[3][1] = 'X'
    >>> li_a
    [1, 'two', 3, ['a', 'X', 'c']]
    >>> li_b
    [1, 2, 3, ['a', 'X', 'c']]

Oops, every change we make to any element of the nested list in ``li_a``, is made also to the nested list in ``li_b``. This is because we made what is known a **top level copy**, which only makes a copy of the top level elements in the list, but does not create a new copy of the elements in deeper levels::

    >>> id(li_a[3])
    4362417992
    >>> id(li_b[3])
    4362417992

The ``list()`` constructor does not copy sublists, these remain as references to the same object. That's why copies made this way are known as **shallow copies**. The list constructor ``list()`` makes a copy of the top level of a list, in which the sublist (lower-level) structures are shared.

Deep copies
-----------
Let's start again with the same list, and try to make a copy, this time of all of its elements, including nested lists. In order to do that we need to import a module named ``copy``, what is easily done with the ``import`` statement::

    >>> import copy
    >>> li_c = [1, 2, 3, ['a', 'b', 'c']]
    >>> li_d = copy.deepcopy(li_a)

A deep copy has been made. Now if we check the ``id`` of the sublists, we'll see they are pointing to different objects in memory::

    >>> id(li_c[3])
    4362741448
    >>> id(li_d[3])
    4362983624

If we make a change on one element of a sublist in ``li_a`` this change won't be reflected on ``li_b`` ::

    >>> li_c[3][1] = 'O'
    >>> li_c
    [1, 2, 3, ['a', 'O', 'c']]
    >>> li_d
    [1, 2, 3, ['a', 'b', 'c']]

A deep copy operation makes a complete copy of a list. A deep copy operator is provided by the ``deepcopy`` **method** which is available when we import the ``copy`` **module**, included in the **Python Standard Library**.
