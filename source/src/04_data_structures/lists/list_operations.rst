***************
List operations
***************
Here we are gonna see some manipulations we can perform on lists using **operators**, such us indexing, slicing, multiplication, membership, length, minimum and maximum. We have already seen most of these operations applied to **strings** because they are standard sequence operations.

Sequence operations
===================
Since **lists** are also **sequences**, they support many of the same operations **strings** do. The most basic one may be finding out how many itmes the list contains, what it's known as its **length**. For example::

    >>> li = [1, 2, 3]
    >>> len(li)
    3

Membership
----------
We can also check whether an element is present in a list using the ``in`` operator. This operation exists for other **sequences** (such strings or tuples) and other data structures such as **dictionaries**. For example:
::

    >>> disney = ['Mickey', 'Goofy', 'Donald']
    >>> 'Goofy' in disney
    True

The above example checks if the string ``'Goofy'`` is a **member** of the list ``disney``. The answer it's yes, so the expression evaluates to ``True``.

List iteration and comprehensions
---------------------------------
We haven't seen **statements** yet, but the following example is a glimpse at the ``for`` loop.

.. code-block:: python
    :emphasize-lines: 2

    >>> disney = ['Mickey', 'Goofy', 'Donald']
    >>> for x in disney:
    ...    print(x + " is a Disney's character")
    ...
    Mickey is a Disney's character
    Goofy is a Disney's character
    Donald is a Disney's character

We are using a for loop for iterating over each item of the ``disney`` list, and printing a line for each of them.

List comprehensions are a way to build a new list by applying an expression to each item in a sequence (really, in any iterable), and are close relatives to for loops:

.. todo:: 
    the ``map()`` function, applies a function to items in a sequence and collects all the results in a new list

Concatenation and repetition
----------------------------
Lists respond to the ``+`` and ``*`` operators much like strings. They mean concatenation and repetition here too, except that the result is a new list, not a string::

    >>> [1, 2, 3] + [4, 5, 6] 
    [1, 2, 3, 4, 5, 6]

    >>> ['Ni!'] * 4
    ['Ni!', 'Ni!', 'Ni!', 'Ni!']

We can use the ``+`` operator for concatenate **iterable objects**, but it expects the same type of sequence on both sides, otherwise, you get a ``type error`` when the code runs. For instance::

    >>> [1, 2] + '3.14'
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: can only concatenate list (not "str") to list

You cannot concatenate a list and a string unless you first convert it to a list. The ``list()`` function explodes a string and creates a new list with the resulting characters::

    >>> [1, 2] + list('3.14')
    [1, 2, '3', '.', '1', '4']

Or convert the list to a string using the ``str()`` function. Even the square brackets and commas of the **list literal** are converted::

    >>> str([1, 2]) + '3.14'
    '[1, 2]3.14'

Indexing and slicing lists
--------------------------
Because lists are sequences, indexing and slicing work the same way for lists as they do for strings. The result of **indexing** a list is whatever type of object lives at the index we specify. For example::

    >>> disney = ['Mickey', 'Goofy', 'Donald'] 
    >>> disney[2]
    'Donald'
    >>> type(disney[2])
    <class 'str'>

However, **slicing** a list always returns a **new list**::

    >>> disney = ['Mickey', 'Goofy', 'Donald'] 
    >>> disney[2:]
    ['Donald']
    >>> type(disney[2:])
    <class 'list'>

Matrixes
========
Because you can nest lists and other object types within lists, one of the simplest ways to represent **matrixes** (multidimensional arrays) in Python it's by using lists with nested sublists. For example, here’s a basic **3 × 3** (two-dimensional) list-based array::


    >>> matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

With one index, you get an entire row (really, a nested sublist), and with two, you get an item within the row::

    >>> matrix[1] 
    [4, 5, 6]
    >>> matrix[1][1] 
    5

Problem is, that writing matrixes linearly is a bit confusing. Thanks to one python's feature named **Implicit Line Joining**, we can use several lines for expressions enclosed in brackets and other matching symbols, Python will treat it as one logical line, no syntax errors. This comes in handy for writing literal matrixes::

    >>> matrix = [[1, 2, 3],
    ...           [4, 5, 6],
    ...           [7, 8, 9]]

Changing Lists in Place
=======================
Because lists are collections of names that point to objects outside the list, they are **mutable**. That means that they support operations that change a list object **in place**. That is, we are gonna be modifying elements in the list directly by pointing its elements to new values, without having to create new copies of the list in memory, as we had to for strings.

* We can change lists contents, one item at a time using **index assignment** statements::

    >>> disney = ['Mickey', 'Goofy', 'Donald'] 
    >>> disney[1] = 'Pluto'
    >>> disney
    ['Mickey', 'Pluto', 'Donald']

* When can even **insert a nested list** at one item position, remember that lists are heterogeneous sequences::

    >>> disney
    ['Mickey', 'Pluto', 'Donald']
    >>> disney[1] = ['Mick Jagger', 'Bruce Springteen', 'Axel Rose']
    >>> disney
    ['Mickey', 'Goofy', ['Mick Jagger', 'Bruce Springteen', 'Axel Rose']]

We are just changing one item in the list, before it was a **string**, now is a **list**.


* It's possible to change several items at once using **slice assignments**::

    >>> disney = ['Mickey', 'Goofy', 'Donald'] 
    >>> disney[1:3] = ['Chip', 'Dale']  # Replaces last 2 items
    >>> disney
    ['Mickey', 'Chip', 'Dale']

The slice assignment in the example above can be seen as a 2 step process:

    1. Deletion of the ``disney[1:3]`` slice.
    2. Insertion of the list ``['Chip', 'Dale']``
   
* When using slice assignments, the number of items inserted doesn’t have to match the number of items deleted. For instance::

    >>> disney = ['Mickey', 'Goofy', 'Donald']
    >>> disney[2:] = ['Snoop Doggy Dog', '2Pac', '50 Cent']
    >>> disney
    ['Mickey', 'Goofy', 'Snoop Doggy Dog', '2Pac', '50 Cent']

* We can even use slice assignments for **deletions**, when we insert nothing::
  
    >>> disney = ['Mickey', 'Goofy', 'Donald']
    >>> disney[1:2] = []
    >>> disney
    ['Mickey', 'Donald']

* And also do **insertions**, extending a list with new elements, without changing any of the previous ones::
  
    >>> disney = ['Mickey', 'Goofy', 'Donald']
    >>> disney[1:1] = ['Clinton', 'Bush', 'Obama']
    >>> disney
    ['Mickey', 'Clinton', 'Bush', 'Obama', 'Goofy', 'Donald']
