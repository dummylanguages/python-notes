*******************
List comprehensions
*******************
It's very common to create lists based upon some other existing sequences. For example, we may create a new list in which each item is the square of each number in another list::

    >>> orig_li = [5, 3, 2, 10]
    >>> new_li = []
    >>> for x in orig_li:
    ...    new_li.append(x ** 2)  # append the square of the item to the new list.
    >>> new_li
    [25, 9, 4, 100]

A **list comprehension** can do all of this work in a **single expression** that returns the new list::

    [output for target in input]

where each item of ``input`` (a sequence) gets assigned to the ``target`` (a variable), and then the items of the new list are produced from ``output`` (an expression). For example, we can get the same new list as in the above example with this list comprehension::

    >>> li = [x ** 2 for x in [5, 3, 2, 10]]
    >>> li
    [25, 9, 4, 100]

However, there is one subtle semantic difference between using **for-in statements** and **list comprehensions**. With a for-in statement, the target variable is a regular variable of the current scope::

    >>> x = ‘hi’
    >>> for x in [5, 3, 2, 10]:
    ...    print(x)
    ...
    5
    3
    2
    10
    >>> x
    10

We can see how the **for-in statement** overwrote the variable ``x``, it doesn't point to the string ``'hi'``, but to the last element of the list, the integer ``10``.

List comprehensions have their own local scope
==============================================
A list comprehension, in contrast, creates its own **local scope**, and variables inside this local scope are different even from same named variables in the outer scope (aka containing scope). In other words the list comprehension uses its own local variables and does not use variables of the scope containing it::

    >>> x = ‘hello there’
    >>> li = [x ** 2 for x in [5, 3, 2, 10]]
    >>> li
    [25, 9, 4, 100]
    >>> x
    'hello there'

``if`` clauses
==============
A list comprehension may optionally include an **if clause** which can filter out iterations form the resultant list::

    [output for target in input if condition]

For each iteration, the condition is evaluated, and if false, the output is neither evaluated nor added to the new list::

    >>> li = [x ** 2 for x in [5, 3, 2, 10] if x < 9]
    >>> li
    [25, 9, 4]

Above, because the condition tested false in the last iteration, no output was generated for that iteration. We get the same effect writing::

    >>> new = []
    >>> for x in [5, 3, 2, 10]:
    ...    if x < 9:
    ...        new.append(x ** 2)
    ...
    >>> new
    [25, 9, 4]

Unreadable list comprehensions
==============================
A list comprehension can get even more busy with the addition of any number of additional for-in clauses (each optionally with their own if clauses), e.g.::

    [output for target in sequence if condition for target in sequence if condition for target in sequence if condition]

To picture what such complicated comprehensions do, simply imagine them written out as a series of nested for-in and if statements, left-to-right with the output in the most inner block. For example::

    >>> li = [(x + y + z) ** 2 for x in [5, 2] if x < 4 
    ... for y in [7, 3, -1] for z in [2, 9, 15] if z <= 12]
    ...
    >>> li
    [121, 324, 49, 196, 9, 100]

…would be written out in statements as::

    >>> new = []
    >>> for x in [5, 2]:
    ...    if x < 4:
    ...        for y in [7, 3, -1]:
    ...            for z in [2, 9, 15]:
    ...                if z <= 12:
    ...                    new.append((x + y + z) ** 2)
    ...
    >>> new
    [121, 324, 49, 196, 9, 100]

Complicated comprehensions can be quite hard to read, so it’s often best to favor the more verbose but clearer form of a series of nested for-in’s and if’s. `From codeschool.com <http://codeschool.org/python-strings-and-collections/>`__
