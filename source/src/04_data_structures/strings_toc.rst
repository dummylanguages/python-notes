*******
Strings
*******

.. toctree::
	:maxdepth: 3
	
	The string type <strings/string_type>
	String literals <strings/string_literals>
	String expressions <strings/string_expressions>
	Converting data types <strings/string_convertions>
	String operations <strings/string_operations>
	String methods <strings/string_methods>
	Working with string the easy way <strings/some_methods>
	Formating methods <strings/format_method>