The ``format`` method
=====================
We have already explored :ref:`string formating using expressions <formatting-expressions>`. This technique is based upon the **C language**’s ``printf`` model, and sees widespread use in much existing code. But a new technique was added in Python **2.6** and **3.0**, derived in part from a same-named tool in **C#** and the **.NET framework**, and overlaps with string formatting expression functionality.

Now we have 2 overlapping ways for formatting strings: 

* The old way make use of **expressions**, and it probably will be deprecated in the future. But that still hasn't happened and the reality is that has wide use in a lot of existing code, and appears in the Python's standard library thousands of times.
* Using the relatively new ``format`` **method**, which is the new way of formatting strings in Python and probably is here to stay. It adds some features that the old expressions don't have.

Syntax of the ``format`` method
-------------------------------
The string object’s ``format`` method, available in Python **2.6**, **2.7**, and **3.X**, is based on normal method call syntax. The syntax is as follows:

	``template.format(arguments)``

1. The **template** is just a string literal that contains what is known as **replacement fields**, and which is surrounded by curly braces ``{ replacement fields go here }``. Anything that is not contained in braces is considered **literal text**, and it's copied unchanged to the output. 

.. tip:: If you need to include a **curly brace** character in the literal text, it can be escaped by doubling it: ``{{`` and ``}}``.
   
2. Inside the parenthesis we pass the **arguments** to the ``format`` method. In one of the subsections about :ref:`functions`, we speak about the two ways of passing arguments to **functions**, by **position** and by **name**. This is true for **methods** too:

	1. We can pass the arguments **by relative position**. For example::
	   
		>>> s = "Hello, my name is {}, and I'm {} years old."
		>>> s.format('Boris', 33)
		"Hello, my name is Boris, and I'm 33 years old."

	.. note:: This is new in Python **3.1** and **2.7**
	
	2. Passing the arguments **by position**::
	   
		>>> s = "Hello, my name is {0}, and I'm {1} years old."
		>>> s.format('Boris', 33)
		"Hello, my name is Boris, and I'm 33 years old."

	Notice that we are passing the arguments as a **tuple**, which is another sequence core type, also zero-base indexed, that's why the numbers in the **replacement fields** have to start with ``0``. You can use the indexes in any order you want in the **replacement fields**::

		>>> s = "Hello, my name is {1}, and I'm {0} years old."
		>>> s.format('Boris', 33)
		"Hello, my name is 33, and I'm Boris years old."

	3. We can use also **names** (keywords)::
	   
		>>> s = "Hello, my name is {name}, and I'm {age} years old."
		>>> s.format(age = 33, name = 'Boris')
		"Hello, my name is Boris, and I'm 33 years old."

	We can even mix **names** and **positions** in the replacement fields::

		>>> s = "Hello, my name is {name}, and I'm {0} years old."
		>>> s.format(33, name='Boris')
		"Hello, my name is Boris, and I'm 33 years old."

	.. error:: 
		If we mix keywords and positions, it's important that the keywords go after the positions **inside the tuple**, otherwise we'll see a syntaxerror: ``SyntaxError: non-keyword arg after keyword arg``

.. todo:: Not finished.


