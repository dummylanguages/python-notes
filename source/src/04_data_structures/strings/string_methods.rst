String methods
==============
So far, we have seen built-in **functions** ``len()``, and **operators** for indexing and slicing. These are operations that work across a range of types, but **methods** are specific to object types. Many types, have methods in common, like the ``count()`` and ``copy()`` methods, but they are still more type-specific than other tools.

.. note::
	**Methods** are simply functions that are associated with and act upon particular objects.

Method calls
------------
Most objects have callable methods, and all are accessed using the same syntax. The **syntax** of a 
method call combines 2 operations at once:

1. An **attribute fetch**. An expression of the form ``object.attribute`` means *“fetch the value of 
   attribute in object”*.
2. A **call expression**. An expression of the form ``function(arguments)`` means *“invoke the code of 
   function, passing zero or more comma-separated argument objects to it, and return function’s 
   result value.”*

Putting these two together allows us to call a method of an object: 
	
	``object.method(arguments)``

The **method call expression** is evaluated from left to right. Python will first fetch the method of the object and then call it, passing in both, the object and the argument or arguments. Or, in plain words, *"Call method to process object with arguments."*

For example::

	>>> s = 'spam'
	>>> s.swapcase()
	'SPAM'

Here we have the string object ``'spam'`` assigned to the variable ``s``, and we call a string method on it. The ``s.swapcase()`` method change the case of each character of the string. No argument is given.

.. note::
	To call an object method, you need an existing object; methods cannot be run without a subject.

Again, ``str`` objects are inmutable, we are not changing the value of the string. 

.. code-block:: python
	:linenos:
	:emphasize-lines: 4-

	>>> s = 'spam'
	>>> s.swapcase()
	'SPAM'
	>>> s
	'spam'

The variable ``s`` is still pointing to the same object. The method call created a new string object. We could have assigned a name to the new object. For example:

.. code-block:: python
	:linenos:
	:emphasize-lines: 2

	>>> s = 'spam'
	>>> a = s.swapcase()
	>>> s
	'spam'
	>>> a
	'SPAM'

All available methods for strings
---------------------------------
Python includes quite a few methods for string objects, and these change frequently, so at any time 
is a good idea to do ``dir()`` on some string object to check the methods available. Some of them:

+--------------------------------------+----------------------------------------+
| **Some string methods**                                                       |
+======================================+========================================+
| S.capitalize()                       | S.rpartition(sep)                      |
+--------------------------------------+----------------------------------------+
| S.casefold()                         | S.rsplit([sep[, maxsplit]])            |
+--------------------------------------+----------------------------------------+
| S.center(width [, fill])             | S.rstrip([chars])                      |
+--------------------------------------+----------------------------------------+
| S.count(sub [, start [, end]])       | S.split([sep [,maxsplit]])             |
+--------------------------------------+----------------------------------------+
| S.encode([encoding [,errors]])       | S.isdigit()                            |
+--------------------------------------+----------------------------------------+
| S.endswith(suffix [, start [, end]]) | S.isidentifier()                       |
+--------------------------------------+----------------------------------------+
| S.expandtabs([tabsize])              | S.islower()                            |
+--------------------------------------+----------------------------------------+
| S.find(sub [, start [, end]])        | S.isnumeric()                          |
+--------------------------------------+----------------------------------------+
| S.format(fmtstr, \*args, \*\*kwargs) | S.isprintable()                        |
+--------------------------------------+----------------------------------------+
| S.index(sub [, start [, end]])       | S.isspace()                            |
+--------------------------------------+----------------------------------------+
| S.isalnum()                          | S.istitle()                            |
+--------------------------------------+----------------------------------------+
| S.isalpha()                          | S.isupper()                            |
+--------------------------------------+----------------------------------------+
| S.isdecimal()                        | S.join(iterable)                       |
+--------------------------------------+----------------------------------------+
| S.ljust(width [, fill])              | S.splitlines([keepends])               |
+--------------------------------------+----------------------------------------+
| S.lower()                            | S.startswith(prefix [, start [, end]]) |
+--------------------------------------+----------------------------------------+
| S.lstrip([chars])                    | S.strip([chars])                       |
+--------------------------------------+----------------------------------------+
| S.maketrans(x[, y[, z]])             | S.swapcase()                           |
+--------------------------------------+----------------------------------------+
| S.partition(sep)                     | S.title()                              |
+--------------------------------------+----------------------------------------+
| S.replace(old, new [, count])        | S.translate(map)                       |
+--------------------------------------+----------------------------------------+
| S.rfind(sub [,start [,end]])         | S.upper()                              |
+--------------------------------------+----------------------------------------+
| S.rindex(sub [, start [, end]])      | S.zfill(width)                         |
+--------------------------------------+----------------------------------------+
| S.rjust(width [, fill])              |                                        |
+--------------------------------------+----------------------------------------+

Next, we are gonna explain explain how some of them work.
