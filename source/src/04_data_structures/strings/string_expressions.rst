String expressions
==================
The same way we used numbers and operators to create arithmetic expressions, we can also use strings 
and operators to create string expressions. 

String operators
----------------
For example, check this :download:`script <../../../code/chap_4/string_expressions.py>` :

.. literalinclude:: ../../../code/chap_4/string_expressions.py
   :language: python
   :linenos:

* In **line 1** we ask the user for a name, and we store it in the variable ``name``.
* In **line 2** we print a welcome message. The argument we use for ``print()`` is a *string expression*.
  In this expression we use the ``+`` operator to concatenate 3 string objects.

This in an example of **operator overloading**: when the same operator performs different operations on 
different types. 

* When we use the ``+`` operator with numbers, we are **adding** the values of the operands.
* When used on strings, it's called the **concatenation operator**, it joins together the strings, 
  and creates a new string object.
  
Another example of operator overload on strings is the **repetition operator**, ``*``. For example:

.. literalinclude:: ../../../code/chap_4/string_expressions_2.py
   :language: python
   :linenos:

* In line **1** we are using the ``*`` operator to repeat the string ``< =^= >`` 8 times, without having to type it. Very useful when we want to repeat a string any number of times, without having to phisically type it. Check the output:

.. literalinclude:: ../../../code/chap_4/output
   :language: bash

Bottom line, we can use some math operators to do some string operations:

* The **addition operator**, ``+``, for **concatenating** strings. 
* The **multiplication operator**, ``*`` for **repeating** strings. 

.. warning:: 
	If we try to use any other operator, such as the substraction (-) or division operators (/) the 
	interpreter will throw an error.

In the above example, the expression ``'\nHello ' + name + ', welcome to the game.\n'``, python creates a 
new string object out of 3 string objects:

* The literal ``'\nHello '``
* The variable ``name`` 
* The literal ``', welcome to the game.\n'``
  
.. note:: String expressions create string objects.

.. _formatting-expressions:
	
String formatting expressions
-----------------------------
String formatting expressions allow us to perform multiple type-specific substitutions on a string in a single step. For example check this code:

	.. literalinclude:: ../../../code/chap_4/formatting_expressions.py
		:language: python
		:linenos:

We have used concatenation for strings, and the comma to separate different types. Using **formatting expressions** we can rewrite the code above as follows:

	.. literalinclude:: ../../../code/chap_4/formatting_expressions_2.py
		:language: python
		:linenos:
		:emphasize-lines: 4

Using formatting expressions it’s never strictly required, but it can be very convenient.

Formatting expressions syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In this expressions we are also gonna use an operator used with numbers, the **modulo operator**, ``%`` (used with numbers to get the remainder of a division). When used on strings, it's known as the **string formatting operator**, and is used to format values as strings according to a format definition. It's used in two ways:

* In the above script, inside the string ``'Hello, my name is %s, and I am %d years old.'`` we find the expressions ``%s`` and ``%d``. These are called **conversion specifiers**, and are used as placeholders for the values that are gonna be inserted as strings. To the right of the ``%`` operator we have to specify the type of object we are gonna insert (%s for strings, %d for integers)

* To the left of the string we use the ``%`` operator by itself, and after it, we provide the object or objects (inside parenthesis if there's more than one) we want to insert. For example:
  
.. code-block:: python
	:linenos:
	:emphasize-lines: 3
  
	>>> 'Give me %d mangos.' % 3
	'Give me 3 mangos.'
	>>> 'Welcome %s, you have an average of %s points.' % ('Bob', 43.7)
	'Welcome Bob, you have an average of 43.7 points.'

Notice that in line **3** we use the ``%s`` for inserting both a **string** as well as a **float**. That's because every type of object can be converted to a string, so unless you will be doing some special formatting, ``%s`` is often the only code you need to remember for the formatting expression.

Advanced formatting expressions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
As our formatting needs become more demanding, our conversion specifiers become quite sophisticated. This is the general structure of the syntax for advanced formatting expressions:

	``[(keyname)][flags][width][.precision]typecode``

* An optional ``keyname``, is the key name for indexing the dictionary on the right side of the expression.
* Optional ``flags`` specify things like left justification(−), numeric sign(+), a blank before positive numbers and a – for negatives (a space), and zero fills (0)
* Also optional ``width`` is the total minimum space for the inserted text.
* Optional ``.precision`` sets the number of digits to display after a decimal point for **floats**
* And finally, a ``typecode`` which is not optional. The following table list all of the formatting type codes:
  
+------------+---------------------------------------------------------------------------------+
| Type codes | Code Meaning                                                                    |
+============+=================================================================================+
| ``s``      | String (or any object’s str(X) string)                                          |
+------------+---------------------------------------------------------------------------------+
| ``r``      | Same as ``s``, but uses ``repr``, not ``str``                                   |
+------------+---------------------------------------------------------------------------------+
| ``c``      | Character (``int`` or ``str``)                                                  |
+------------+---------------------------------------------------------------------------------+
| ``d``      | Decimal (base-10 integer)                                                       |
+------------+---------------------------------------------------------------------------------+
| ``i``      | Integer                                                                         |
+------------+---------------------------------------------------------------------------------+
| ``u``      | Same as ``d`` (obsolete: no longer unsigned)                                    |
+------------+---------------------------------------------------------------------------------+
| ``o``      | ``o`` Octal integer (base 8)                                                    |
+------------+---------------------------------------------------------------------------------+
| ``x``      | Hex integer (base 16)                                                           |
+------------+---------------------------------------------------------------------------------+
| ``X``      | Same as ``x``, but with uppercase letters                                       |
+------------+---------------------------------------------------------------------------------+
| ``e``      | Floating point with exponent, **lowercase**                                     |
+------------+---------------------------------------------------------------------------------+
| ``E``      | Same as ``e``, but uses **uppercase** letters                                   |
+------------+---------------------------------------------------------------------------------+
| ``f``      | Floating-point decimal                                                          |
+------------+---------------------------------------------------------------------------------+
| ``F``      | Same as ``f``, but uses uppercase letters                                       |
+------------+---------------------------------------------------------------------------------+
| ``g``      | Floating-point ``e`` or ``f``                                                   |
+------------+---------------------------------------------------------------------------------+
| ``G``      | Floating-point ``E`` or ``F``                                                   |
+------------+---------------------------------------------------------------------------------+
| ``%``      | Literal % (coded as %%)                                                         |
+------------+---------------------------------------------------------------------------------+

Let's see some examples:

	>>> pi = 3.14159
	>>> print("pi = %.2f " % pi)
	pi = 3.14

Here we are using the ``%.2f`` to specify the ``.precision`` we want.

.. todo:: More examples. Dictionary-Based Formatting Expressions

.. note:: 
	Python **3.0** threatened to deprecate string formatting expressions in version **3.1**, but as of version **3.3**, that hasn't happened yet. Thing is, all that can be done with formatting expressions, can also be accomplished with formatting methods. And since the expressions may be deprecated in the future, probably it's a good idea to focus in formatting string using methods.
	That being said, we are gonna be dealing with other people's code where formatting expressions may be widely used, so familiarity with them won't hurt.
