String literals
===============
String literals, commonly known just as **strings**, are used for representing text. In Python, a string 
literal is a sequence of characters enclosed in:
 
* Single quotation marks, like ``'John'`` 
* Double quotation marks ``"Laura"``
* Or even triple quotation marks, for several lines of text.

+----------------------------------------------------+----------------------------------------------------+
| Some string literals                               |                                                    |
+=================================================+=======================================================+
| 'A'                                                | Single character                                   |
+----------------------------------------------------+----------------------------------------------------+
| "Hello world"                                      | Multiple characters                                |
+----------------------------------------------------+----------------------------------------------------+
| '234'                                              | Numeric string                                     |
+----------------------------------------------------+----------------------------------------------------+
| 'johnny69\@gmail.com'                              | Containing non-letter characters                   |
+----------------------------------------------------+----------------------------------------------------+
| ' '                                                | Containing a single blank space character          |
+----------------------------------------------------+----------------------------------------------------+
| ''                                                 | **Empty string**. Containing no characters at all. |
+----------------------------------------------------+----------------------------------------------------+
| .. literalinclude:: ../../../code/chap_4/multiline | Multiline strings (Single or double quotes)        |
+----------------------------------------------------+----------------------------------------------------+

.. note::
	If we surround numbers with **matching** double or single quotes, they are still considered as 
	strings. Only when numbers without are not enclosed in quotes, they are considered **numbers**. 

Single and double quotes
------------------------
String literals can be written enclosed in either two single or two double quotes. The two forms work 
the same and return the same type of object. The reason for supporting both is that it allows you to 
embed single-quote character in a string enclosed in double-quote characters, and viceversa::

	"John's dog"
	'Have you read "Romeo and Juliet"?'

In the first case it's better to use **double quotes** since we are gonna use a single quote inside. 
In the second one though, it's more convenient to use **single quotes** to embed the string.

.. note:: 
	Most python programmers prefer the use of **single quotes** around strings whenever it's possible, 
	just because they are a bit more readable.

Multiline block strings
-----------------------
Python also has a *triple-quoted string literal format*, sometimes called a **block string**, that is a 
syntactic convenience for coding multiline text data. This form begins with three quotes (of either the 
single or double variety), is followed by any number of lines of text, and is closed with the same 
triple-quote sequence that opened it.

For example::

	>>> mantra = """Always look
	... on the bright
	... side of life.""" 
	>>> print(mantra)
	Always look
	on the bright
	side of life.

.. note::
	While using the **interactive mode**, when the **triple quotes** are open, the prompt starts 
	every new line with ``...``, until you close the quotes.
	In **IDLE** this triple dotted prompt is not shown.

While writing inside the triple quotes, **single and double quotes** embedded in the string’s text 
may, but **do not have to, be escaped**. For example::

	>>> a = '''Have you seen a Quentin Tarantino's 
	... movie named "Pulp fiction"?
	... It's very good!'''
	>>> a
	'Have you seen Quentin Tarantino\'s\nmovie named "Pulp fiction"?\nIt's very good!'

Triple-quoted strings are useful anytime you need multiline text in your program:

* Anytime you need multiline text in your program; for example, to embed multiline error messages or 
  HTML, XML, or JSON code in your Python source code files. 

* They are also commonly used for documentation strings, which are string literals that are taken as 
  comments when they appear at specific points in your file. **Docstrings** don’t have to be 
  triple-quoted blocks, but they usually are to allow for multiline comments.

* Finally, triple-quoted strings are also sometimes used as a way to temporarily disable lines of code 
  during development. For large sections of code, it’s also easier than manually adding hash marks before 
  each line and later removing them.

Control characters
------------------
Control characters are special characters that are not displayed on the screen. They are represented by 
a combination of characters called an **escape sequence**.
An escape sequence begins with an escape character that causes the sequence of characters following it 
to “escape” their normal meaning. The **backslash**, ``\`` serves as the **escape character** in Python.

+-------------------------+-------------------------------+
| Some control characters | Meaning                       |
+=========================+===============================+
| ``\``                   | Ignored. For continuing lines |
+-------------------------+-------------------------------+
| ``\\``                  | The backslash character       |
+-------------------------+-------------------------------+
| ``\n``                  | Newline character             |
+-------------------------+-------------------------------+
| ``\t``                  | Horizontal tab                |
+-------------------------+-------------------------------+
| ``\v``                  | Vertical tab                  |
+-------------------------+-------------------------------+

Every escape sequence represent only 1 character, even when they contain 2 characters (``\t``), for 
example::

	>>> s = 'A\tB\tC'
	>>> len(s)
	5			# 5 characters, the 3 letters plus the 2 tabs.
	>>> s
	'A    B    C'

Raw strings
-----------
Escape sequences are handy for embedding special character codes within strings. Sometimes though, 
the special treatment of backslashes for introducing escapes can lead to trouble, especially in 
Microsoft Windows® systems, where the backslash is used for paths. For example::

	>>> path = 'C:\new\text.dat'
	>>> print(path)
	C:
	ew	ext.dat

The problem here is that ``\n`` is taken to stand for a newline character, and ``\t`` is replaced with 
a tab.

We can avoid that problem using **raw strings**. We just have to add the character ``r`` *right before 
the opening quote of the string*, to turn off the escape mechanism. The result is that Python retains 
your backslashes literally, exactly as you type them. For example::

	>>> path = r'C:\new\text.dat'
	>>> print(path)
	'C:\new\text.dat'

Besides directory paths on Windows, raw strings are also commonly used 
for **regular expressions**.
