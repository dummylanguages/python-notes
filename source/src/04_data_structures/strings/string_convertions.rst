Converting data types
=====================

Converting to strings
---------------------
Consider :download:`the following script <../../../code/chap_4/mixed_types.py>`:

.. literalinclude:: ../../../code/chap_4/mixed_types.py
   :language: python
   :linenos:

If we try to run this script, the interpreter will throw the following error:

.. error::
	``TypeError: Can't convert 'int' object to str implicitly``

The reason is because in **line 3**, we used the ``+`` operator to concatenate the ``int`` variable ``num_of_cats`` with two literal strings. We can't do that, because Python gets confused and doesn't know what version of the ``+`` use, the one for strings or the one for numbers.

So if Python is not gonna do the conversion for us, we are gonna have to do it explicitely using the built-in ``str`` function:

.. literalinclude:: ../../../code/chap_4/types_conversion.py
   :language: python
   :linenos:

The non ``str`` objects are converted to strings and printed to the screen.

.. admonition:: A way around
	:class: btw

	In this case, another option, instead of doing type conversion, would have been simply using 
	the ``,`` to separate the ``str`` and ``int`` objects inside the ``print()`` argument:

	.. literalinclude:: ../../../code/chap_4/types_separation.py
	   :language: python
	   :linenos:

	Internally, ``print()`` convert the non string objects to ``str``. When printed, a space will 
	be automatically inserted between objects.::

		My name is Doris and I have 14 beautiful cats at home.

Converting from strings
-----------------------
We can also find ourselves in a situation where we have ``str`` objects, when what we need is numbers.
For example, consider a :download:`script <../../../code/chap_4/your_age.py>` that ask the user for the current year, and the year she was born, 
in order to compute her age and print it. It would look something like this:

.. literalinclude:: ../../../code/chap_4/your_age.py
   :language: python
   :linenos:

* In line **1**, we ask for the current year and we store the value in the variable ``now``.
* Line **3** does the same with the year of birth. 
* In Line **5** we try to substract these values and store the result in the variable ``age``. It 
  sounds reasonable, but if you try to run this program, the interpreter is gonna throw this 
  error:

.. code-block:: bash

	Traceback (most recent call last):
	File "your_age.py", line 5, in <module>
	age = now - born
	TypeError: unsupported operand type(s) for -: 'str' and 'str'

We have a ``TypeError`` in our code, because we were trying to make a mathematical operation with 
two strings (the variables ``now`` and ``born``). As we said in the beginning, the ``input()`` 
function always returns string types, so even when the user introduced numbers, they were 
automatically transformed into strings. When we try to rest them and assign the value to the 
variable ``age``, python throws an error.

To fix the situation, we are gonna use a built-in function named ``int()``. What this function does, 
is transforming **numeric strings** (numbers that are stored as strings), into actual integer 
numbers, so we can do math with them:

.. literalinclude:: ../../../code/chap_4/your_age_2.py
   :language: python
   :linenos:

* In Line **5**, we use ``int()`` to convert string variables to integer numbers, and then we can
  do the substraction, no error this time.

Now our :download:`code <../../../code/chap_4/your_age_2.py>` runs smoothly, and we have a program that 
helps the user remember his age.

A similar ``float()`` built-in function does the conversiont from ``str`` to ``float``::

	>>> a = '33'
	>>> float(a)

Character code conversions
--------------------------
It is also possible to convert a single character to its underlying integer code, meaning the 
decimal value of the unicode code point used to represent the corresponding character in memory. 
We use the built-in function ``ord()`` for that. For example::

	>>> ord('A')
	65

It returns an integer number, the position of the character ``A`` in the Unicode set. Useful to check 
if it really is the same position is ASCII.

.. todo::
	Extend the topic below.

.. admonition:: Underlying byte.
	:class: btw

	And if we want the ordinal value in binary numbers (base 2), we can use another built-in function::

		>>> bin(ord('A'))
		'0b1000001'

	``bin()`` returns the binary representation of any number as a string, ``''0b1000001''``. If we
	want this value as an integer, we'll just use::

		>>> int(bin(ord('a'))[2:])
		1100001
		>>> int(bin(ord('ы'))[2:])
		10001001011


If we want to know the unicode character corresponds to a certain position in the unicode set, we 
can use the function ``chr()``::

	>>> chr(209)
	'Ñ'

The argument is an integer number, ``chr()`` returns the unicode character ``Ñ``, stored in memory 
as the value ``209``.