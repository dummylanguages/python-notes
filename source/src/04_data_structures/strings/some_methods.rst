Working with strings the easy way
=================================
We have already been working with strings using operators. We can achieve the same results in an easier way using methods. Let's 
compare both ways:

.. code-block:: python

	>>> s = "It's six o'clock. I have six tickets."
	>>> s = s[:5] + 'seven' + s[8:]
	>>> s
	"It's seven o'clock. I have six tickets."

Problem with this way is that is a bit of an overkill having to count index positions. Let's try with 
methods. 

The ``replace`` method
----------------------
We can replace substrings using the replace method::

	>>> s = "It's six o'clock. I have six tickets."
	>>> s = s.replace('six', 'seven')
	"It's seven o'clock. I have seven tickets."

We have passed 2 arguments:

* The string to replace, in this case ``'six'`` 
* The string to replace with, in our example ``'seven'``
  
and performs a **global** search and replace. But what happens when we want to change a substring 
in only one place? We can pass an additional argument that tells how many sustitutions we want to
perform::

	>>> s = "It's six o'clock. I have six tickets."
	>>> s = s.replace('six', 'seven', 1)
	"It's seven o'clock. I have six tickets."

We have change the time, not the number of tickets.

The ``find`` method
-------------------
The find method returns the first index where the substring appears, starting from the left, for 
example::

	>>> a = 'hello spam'
	>>> a.find('spam')
	6

We can use this method in combination with slicing expressions:

.. code-block:: python
	:linenos:
	:emphasize-lines: 5

	>>> a = "Monty Java's flying circus"
	>>> i = a.find('Java')
	>>> i
	6
	>>> a = a[:i] + 'Python' + a[(i + 4):]
	>>> a
	"Monty Python's flying circus"

The expression in line 5 is quite interesting:

* The slice ``a[:i]`` is from the beginning until (not included) the first character of the word 'Java'.
* The slice ``a[(i + 4):]`` is the rest of the string after the word 'Java'.
* In the middle we concatenate the word ``'Python'``, like a sandwich.

Exploding and imploding strings
-------------------------------
Manipulating strings either with **operators** (slicing and concatenation), or **methods** (such as ``replace``), generate new string objects each and every time. That may be expensive in terms of computer memory, especially if we are working with big chunks of text. If we want to improve the performance of our scripts, we may have to convert our string objects to other types that support **in-place changes**. This way there's no need of creating new objects in memory every time we want to change strings.

The **built-in function** ``list()`` (not a method) allows us to convert strings to **lists**. Lists are another Python’s core type, that support indeed changes in-place, without having to create new objects in memory. Let's see how it works::

	>>> s = 'Chump!'
	>>> l = list(s)
	>>> l
	['C', 'h', 'u', 'm', 'p', '!']
	>>> l[2] = 'a'

Here ``list()`` has converted our original string into a **list** (another core type, also a sequence). The effect is that we have **exploded** our string into a list of characters, which we can change without having to generate a copy for each change.

After our changes, we can convert back the list to a string object using the **string method** ``join``::

	>>> s = ''.join(l)
	>>> s
	'Champ!'

We are using the ``join`` method on a string object, an empty string ``''``, being the **argument** of this method *the list* we want to convert. 

We can also use the join method to concatenate elements of a list, inserting a **separator** in between the elements::

	>>> l ['spam', 'eggs', 'bacon']
	>>> s = ' - '.join(l)
	>>> s
	'spam - eggs - bacon'

Parsing text
------------
When we need to parse some text (analyze it) to extract some substrings, string methods are very useful. Of course we could do that with slicing operations::

	>>> line = 'John Laura   Ralph'
	>>> col1 = line[:4]
	>>> col2 = line[5:10]
	>>> col3 = line[13:]
	>>> col1
	'Johh'
	>>> col2
	'Laura'
	>>> col3
	'Ralph'

But having to count indexes, especially when there is undefined amount of space in between, is an utter overkill. Again, string methods save the day::

	>>> line = 'John Laura   Ralph'
	>>> cols = line.split()
	>>> cols
	['John', 'Laura', 'Ralph']

The string ``split`` method chops up a string into a **list** of substrings, around a **delimiter** string. We didn’t pass a delimiter in the prior example, so it defaults to whitespace. The string is split at groups of one or more spaces, tabs, and newlines, and we get back a list of the resulting substrings.

This method is great for working with **CSV** (Comma Separated Values) files. This is a simple file format, used widely to import/export **tabular data** (values separated by commas or another separator) between aplications such as databases or spreadsheets. 

.. tip:: See the ``csv`` module in the Python’s standard library.
