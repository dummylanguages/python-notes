.. sidebar:: Table of contents.

	.. contents::
		:depth: 3
		:backlinks: top
		:local:

The ``str`` type
================
Generally speaking, in computer science a string is a **sequence** of homogeneusly typed data. They can 
be used for storing **text** (readable and not readable characters) but also the **raw bytes** used in media 
files and network transfers.

In **Python 3**, there are three string types:

* ``str`` used for **unicode text**.
* ``bytes``, literally sequence of bytes for **binary data** (images, audio, but also non-unicode text)
* ``bytearrays`` a mutable variant of ``bytes``.
  
.. note:: 
	**Files** work in two modes: **text**, which represents content as str and implements Unicode encodings, 
	and **binary**, which deals in raw bytes and does no data translation.

Here we are just gonna take care of the ``str`` type. This type is used for representing **text** encoded is **Unicode**. 
In fact, **Python 3** defines strings formally as sequences of unicode code points, not bytes, to make 
this clear.

.. note:: In Python 3, a ``str`` object is a sequence of **unicode code points**.

If you’re accustomed to all-ASCII text, it’s tempting to think of ``str`` as sequences of bytes, since 
in ASCII every character is stored as a single byte in memory. But “bytes” have no meaning in the Unicode 
world, a single character does not necessarily map directly to a single byte, they may be 1, 2, or 4 
bytes in memory. For now, to be safest, think of ``str`` strings as **sequences of characters** 
instead of bytes.

.. note:: 
	**UTF-8** encodes each of its more than one million **code points** using 1 to 4 bytes (8 bits). 
	The first 128 characters of Unicode, which correspond one-to-one with ASCII, are encoded using 
	a single byte with the same binary value as ASCII, making valid ASCII text valid UTF-8-encoded 
	Unicode as well.