Operations on strings
=====================
We have already been doing some operations on strings using some built-in functions. Here we are gonna see:

1. **Sequence operations**: Operations that are common to any sequence.
2. **Methods**: Operations that are specific to strings.

Sequence operations
-------------------
Here we are gonna be take care of operations that are appliable to sequences in general. We are 
gonna see them applied to strings, but they will work the same on other sequence types we’ll explore 
later, such as **lists** and **tuples**.

Length of a string
^^^^^^^^^^^^^^^^^^
As sequences, strings support some of the typical operations that we can apply to any sequence. One 
of the most useful operations is finding out how many elements contains a sequence. When we apply 
python's **built-in function** ``len()`` to a string, it returns the number of characters:
::

  >>> s = 'hello world'
  >>> len(s)
  11

Why 11 elements if we have only 10 letters? Easy, the space is also a character.

Can we create an empty string? Sure we can:
::

  >>> s = ''
  >>> len(s)
  0
  >>> type(s)
  <class 'str'>

Strictly speaking, a string is a **sequence** of zero or more characters.

Membership
^^^^^^^^^^
To check whether an element can be found in a **sequence** (list, tuple, string) or a dictionary, we can use the ``in`` operator. Let's see an extremely simple example with a string::

	>>> name = 'Paul'
	>>> 'u' in name
	True
	
The ``in`` operator, checks if the character ``'u'`` is a member of the string ``'Paul'``. If it exists, the expression ``'u' in name``  evaluates to ``True``. We haven't seen statements yet, but we will see that this operator is very useful inside tests.

Indexing
^^^^^^^^
We have access to any element in a sequence. It's easier to understand how this works, if we 
remember that all the objects contained in a sequence, are ordered **left to right**. So in a string, 
every character has its own position, marked with a number, called its **index**. 

The subscript operator
""""""""""""""""""""""
In order to access a given character, we just have to write its index inside the **subscript operator**:
::

  >>> a = 'Spam'
  >>> a[1]
  'p'

Wait a minute, this may not be what we were expecting. The first element of ``'Spam'`` is the ``'S'`` character, why the output is the second one, ``'p'``? Easy, because in Python, as in many other programming languages, sequences use **zero-based indexing**. The following table should elp us understand how sequences are indexed:

+----------+-------------+-------------+-------------+-------------+
| String:  | S           | p           | a           | m           |
+==========+=============+=============+=============+=============+
| Posit.   | 1 :sup:`st` | 2 :sup:`nd` | 3 :sup:`rd` | 4 :sup:`th` |
+----------+-------------+-------------+-------------+-------------+
| Index    | 0           | 1           | 2           | 3           |
+----------+-------------+-------------+-------------+-------------+

According to this table, in a string with 4 elements, the first one has the index 0, and the last 
one has the index 3. If we try to access the last element with:
::

  >>> a = 'Spam'
  >>> a[len(a)]
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  IndexError: string index out of range

This is because ``len(a) == 4``, but the last index is 3, so we get an error because our index is 
out of range. 

Negative indexes
""""""""""""""""
A way of accessing the last element would be with the expression:
::

  >>> a = 'Spam'
  >>> a[len(a) - 1]
  'm'

Another way of writing the above expression, would be omiting the `len(a)` piece:
::

  >>> a = 'Spam'
  >>> a[-1]
  'm'

We still get the last element this way. This is how **negative index values** work. Check the following table:

+----------+-------------+-------------+-------------+-------------+
| String:  | S           | p           | a           | m           |
+==========+=============+=============+=============+=============+
| Neg. ind | -4          | -3          | -2          | -1          |
+----------+-------------+-------------+-------------+-------------+

Negative indexing allows us to access characters from right to left. Try to guess the result of the following example:
::

  >>> s = 'Monty'
  >>> s[-2] 		# This is equivalent to s[len(s) - 2]
  't'

Slicing
^^^^^^^
Python’s **subscript operator** can also be used to obtain a **substring** (slice), through a process called slicing. 

Slicing operator
""""""""""""""""
To extract a substring, we use two indexes separated by a colon ``string_name[x:y]`` inside the subscript. The resulting expression is also known as the **slicing operator**.

* The element at the **first index**, it's **included** in the slice.
* The element at the **second index** it's **not included** in the slice. 
  
So every **slice expresion** returns a new string object that starts on ``x`` (inclusive), and ends one position before ``y`` (non-inclusive).
 
For example:
::

  >>> s = 'Hello spam'
  >>> a = s[0:5] 		# The first 5 characters (from 0 to 4, not including index 5)
  >>> a
  'Hello'
  >>> s
  'Hello spam'

We start creating a string ``s``, then create another variable, ``a`` and assign it a slice of ``s``, the first 5 characters. Notice that after the slicing, ``s``, the original string is still the same. This is because Python considers strings are **inmutable** objects, and as such can never be overwriten. For example:
::
	
	>>> s = 'Monty'
	>>> s[1] = 'a'
	Traceback (most recent call last):F
	File "<stdin>", line 1, in <module>
	TypeError: 'str' object does not support item assignment

Here we were trying to modify the second element in the string ``'Monty'`` by assigning it a new value. We can not do that, but don't worry, a bit further we'll se how we can change strings.

.. tip:: Slicing is a very important operation when dealing with strings, so it's very important to get a 
	good grasp of how it works. 

Here are some more examples:
::

  >>> s[0:len(s)] 	# The whole string (from 0 to len(s))
  'Hello spam'
  >>> s[:] 			# The whole string (from 0 to len(s))
  'Hello spam'

+----------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+--------------+
| String:  | H           | e           | l           | l           | o           |             | s           | p           | a           | m            |
+==========+=============+=============+=============+=============+=============+=============+=============+=============+=============+==============+
| Posit.   | 1 :sup:`st` | 2 :sup:`nd` | 3 :sup:`rd` | 4 :sup:`th` | 5 :sup:`th` | 6 :sup:`th` | 7 :sup:`th` | 8 :sup:`th` | 9 :sup:`th` | 10 :sup:`th` |
+----------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+--------------+
| Index    | 0           | 1           | 2           | 3           | 4           | 5           | 6           | 7           | 8           | 9            |
+----------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+--------------+
| Neg. ind | -10         | -9          | -8          | -7          | -6          | -5          | -4          | -3          | -2          | -1           |
+----------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+--------------+

* When we omit the first index, ``s[:len(s)]``, is like writing ``s[0:len(s)]``. 
* And when we omit the last index, ``s[0:]``, is like writing ``s[0:len(s)]``. 
* If we omit both indexes, is also like writing ``s[0:len(s)]``.

If we want to extract the **last 4 characters** of the string above, in order to get the substring ``'spam'``, we can make use of negative indexing:
::

  >>> s = 'Hello spam'
  >>> s[-4:]
  'spam'

Another couple of slicing tricks:

* ``s[1:]`` gets all items beyond the first element.
* ``s[:-1]`` gets all items except the last element.

The third index
"""""""""""""""
Since Python 2.3, slice expressions can use an optional 3 :sup:`rd` index called **step**. This index it's used to skip characters, and by default is set to ``1``, which is why if we don't use it no character is skipped. For example::

	>>> s = 'Hello spam'
	>>> s[6::2]		# Start in the 6th character, until the end, skip the odd numbered.
	'hlosa'

We can also use this index to slice in reverse, for example, let's slice the word ``'spam'`` and reverse it::

	>>> s[-1:-5:-1]
	'maps'

Or we can reverse the whole string doing::

	>>> s[::-1]
	'maps olleh'
	
Changing strings
^^^^^^^^^^^^^^^^
If strings are **inmutable** and they cannot be changed in place, how to modify text information in 
Python, then? For example, we have a string that says ``'hello spam'``, and we want to change it, so it 
says ``'hello world'``.

.. code-block:: python
	:linenos:
	:emphasize-lines: 2
   
	>>> s = 'hello spam'
	>>> s = s[:6] + 'world'
	>>> s
	'hello world'

What it's really happenning in line **2**, is that the expression to the right of the assignment operator ``s[:6] + 'world'`` is creating a new string object and assigning this new value to the name ``s`` (remember, we can assign names new values, otherwise variables would be useless). We can think of this as “changing” the string, but it's important not to forget that strings are inmutable. By they way, we can achieve similar effects with string method calls like ``replace()``.

.. admonition:: Garbage collector
	:class: btw

	Generating a new string object for each string change may sound inefficient, but it's not. Thanks 
	to one feature named **garbage collector**, Python automatically garbage-collects (reclaims the 
	space of) old unused string objects as you go, so newer objects reuse the space held by prior 
	values. 


If we wanted to retain the original string object, we can create variables for the new objects.

.. code-block:: python
	:linenos:
	:emphasize-lines: 2
   
	>>> s = 'hello spam'
	>>> a = s[:6] + 'world'
	>>> s
	'hello spam'
	>>> a
	'hello world'
