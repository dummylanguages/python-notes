************
Dictionaries
************
So far we've taken care of **strings**, **lists** and **tuples**. These core types were classified as **sequences**, linearly ordered collections of items. But not all data structures in Python are sequences. Along with lists, **dictionaries** are one of the most flexible built-in data types in Python. They can be classified as **mappings**, another type of data structure, in which *their elements are not stored following a linear order*. Dictionaries are the only built-in mapping type in Python, but other mappings may be created by importing some modules.

Some of their characteristics are:

* Unlike sequences, dictionaries are **unordered** collections of objects. That's why items can't be indexed by position. Items are **indexed** through their **keys**.

* Dictionaries are **heterogeneous**, they can contain objects of any type, and they support nesting to any depth (they can contain lists, other dictionaries, and so on)
  
* They are **mutable**, dictionaries can grow and shrink in place (without new copies being made)

Dictionary literals
===================
Each **item** in a dictionary consist of a **key** and their corresponding **value**. Each key is separated from its value by a colon (``:``), and the items are separated by commas. The whole thing is enclosed in curly braces, for example::

    >>> phonebook = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258'}

In the example above, a dictionary is assigned to the variable ``phonebook``. Its **first item** is ``'Alice': '2341'``, where ``'Alice'`` is the **key**, and ``'2341'`` is the **value**.

**Keys** not always have to be strings, we can use any **immutable type** as a key:

* **Strings** and numbers can always be keys.
* We can use **integers** as keys, which makes the dictionary look much like a list (when indexing, at least).
* **Tuples** can be used as keys if they contain only strings, numbers, or tuples; if a tuple contains any mutable object either directly or indirectly, it cannot be used as a key. 
* You can’t use **lists** or other dictionaries as keys, because they are mutable.

.. note:: **Keys** are **unique** within a dictionary (and any other kind of mapping). **Values** do not need to be unique within a dictionary.

An **empty dictionary** can be created just by writing an empty pair of curly braces::

    >>> my_dict = {}

We might not see dictionaries coded in full using literals very often. Programs rarely know all their data before they are run, and more typically extract it dynamically from users, files, and so on.

Basic dictionary operations
===========================
Operations that assume a fixed left-to-right order (e.g., slicing, concatenation) do not apply to dictionaries. 

* We can fetch values only **by key**, not by position. Same as in sequences, we also use the **subscript operator**, but inside we write a key instead of an index number. For example::

    >>> phonebook = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258'}
    >>> phonebook['Beth']
    '9102'

* The ``len()`` **built-in function** works as well on dictionaries, it returns the number of items stored in the dictionary or, equivalently, the length of its keys list::
  
    >>> len(phonebook)
    3

* In dictionaries, the ``in`` **operator** allows us to test for the existence of any key::

    >>> 'Angelina' in phonebook
    False
    >>> 'Beth' in phonebook
    True

Changing dictionaries
---------------------
Dictionaries, like lists, are **mutable**, so you can change, expand, and shrink them in place *without making new dictionaries*. 

* To **create a new item**, we simply assign a value to a **new key** (one that hasn’t been assigned before), for instance::
  
    >>> phonebook['Cindy'] = '3181'
    >>> phonebook
    {'Alice': '2341', 'Beth': '9102', 'Cindy': '3181', 'Cecil': '3258'}

* To **change a value**, we just have to assign to an **existing key** another value ::
  
    >>> phonebook['Beth'] = '3318', '1010'

* The ``del`` **statement** works here, too. We have to pass the key specified as an index::
  
    >>> del phonebook['Alice']
    >>> phonebook
    {'Beth': '9102', 'Cindy': '3181', 'Cecil': '3258'}


Dictionary methods
==================
Dictionary methods provide a variety of type-specific tools.

* The ``items`` **method** returns all of the dictionary’s **keys and values** in tuples like (key,value) ::
  
    >>> phonebook.items()
    dict_items([('Beth', '9102'), ('Cindy', '3181'), ('Cecil', '3258')])

* The ``values`` **method** returns all of the **values**::
  
    >>> phonebook.values()
    dict_values(['9102', '3181', '3258'])

* The ``keys`` **method** returns all the **keys** in the dictionary::
  
    >>> phonebook.keys()
    dict_keys(['Alice', 'Beth', 'Cecil'])

As we will see in the ... section, these 2 last methods are useful in loops that need to step through dictionary entries one by one.

.. todo:: What section?

* The ``update`` **method** provides something similar to concatenation for dictionaries. It merges the keys and values of one dictionary into another, blindly overwriting values of the same key if there’s a clash::
  
    >>> birthdays = {'Beth': '10 of July', 'Pops': '9 of January'}
    >>> phonebook
    {'Beth': '9102', 'Cindy': '3181', 'Cecil': '3258'}
    >>> phonebook.update(birthdays)
    >>> phonebook
    {'Beth': '10 of July', 'Pops': '9 of January', 'Cindy': '3181', 'Cecil': '3258'}

As a result, we lost ``'Beth'`` number.

* The dictionary ``pop`` **method** deletes a key from a dictionary and returns the value it had. It’s similar to the list pop method, but it takes a key instead of an optional position::
  
    >>> phonebook.pop('Pops')
    >>> '9 of January'
    >>> phonebook
    {'Cindy': '3181', 'Cecil': '3258'}


* Dictionaries also provide a ``copy`` **method**::
  
    >>> pb = phonebook.copy()
    >>> pb
    {'Cindy': '3181', 'Cecil': '3258'}

These were just some of the methods available for dictionaries. To check for a complete list of them::

>>> dir(phonebook)

.. todo:: Complete this section with more info about dictionaries.


