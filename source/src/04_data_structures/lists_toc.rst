*****
Lists
*****
In Python, lists are also a **core type**, meaning a type of object built-in into the language. Same as strings, lists can also be classified as **sequences**, collections of objects linearly ordered (left to right), which allow them to be accessed using indexes. But apart from that, they don't have anything else in common:

* Lists are **heterogeneous** data structures. We saw that strings are homogeneous collections of one-character strings. Lists, on the other hand, can contain any sort of objects: numbers, strings, and even other lists(nestable). 

* Lists are **mutable** sequences. Unlike a string, a list can be **changed in place** (without having to make a copy in memory) by index assignment operations, deletions statements, etc. We can also make them grow and shrink (their lengths can vary).
 
These characteristics make Python's lists the most flexible sequence type. Technically, lists are **arrays of object references** inside the standard Python interpreter. Whenever you assign an object to an element of the list (a variable name), the list stores a reference to that same object, not a copy of it. 

.. note:: Lists contain **zero** (empty list) or **more references** to other objects.

.. toctree::
	:maxdepth: 3
	:hidden:
	
	lists/list_literals
	lists/list_comprehensions
	lists/list_operations
	lists/list_methods