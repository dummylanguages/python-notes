****
Sets
****
What is a set?
==============
**Python 2.6** introduced a new built-in type known as **set**. Sets, are neither mappings nor sequences. They are:

	* Unordered collections
	* And its elements are **unique** and **inmutable** objects

Currently there are two built-in set types, ``set`` and ``frozenset``. The difference between them:

	* The ``set`` type is **mutable**, meaning that its contents can be changed using methods like ``add()`` and ``remove()``. 
	* The ``frozenset`` type is **immutable**, so its contents cannot be altered after it is created.

Both types have in common:

	* They are **unordered collections** (like dictionaries), meaning that sets are not sequences.
	* The elements in a set are **unique**, so an element of set cannot be duplicated.
	* The elements in a set are **inmutable**, which means that a set cannot contain mutable objects like lists.

Creating sets
=============
We have two ways of creating sets:

1. Using the **constructor functions** ``set()`` and ``frozenset()``. The **arguments** we pass to these functions have to be **iterable objects**:
   
   >>> my_set = set( ('jack', 'sjoerd') )
   >>> my_set
   {'jack', 'sjoerd'}

We can create sets out of strings or tuples.

2. As of **Python 2.7**, we have a literal way of creating **non-empty** sets (but not frozensets) by placing a comma-separated list of elements within braces, for example: 

	>>> my_set = {'jack', 'sjoerd'}

Supported operations by both types
==================================
Sets do not support indexing, slicing, or other sequence-like behavior

.. todo:: 
	Check this `website <http://www.itmaybeahack.com/book/python-2.6/html/p02/p02c06_sets.html#data-set>`__