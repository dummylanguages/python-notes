############
Introduction
############

.. toctree::
	:maxdepth: 3
	:hidden:

	hardware_software
	programming_languages
	about_python
	interactive_mode
	hello_world

When learning a programming language it's very useful from time to time to take a step back and look at the big picture, learn about:

* What is a computer.
* The history of systems, how old machines used to do things, which explains a lot of their modern behaviour.
* The **jargon** of the `Information Technology`_ world. For example:

A computer program is a set of instructions that tell a computer what to do. People who write programs, are called **programmers**. The process of writing programs is also known as **Software development**, and the people with this mission are called **software developers** or just **developers**. The content of a program is also known as **Source code**, that's why programmers are also called **coders**.

So, we have different words for people with the same job:
	* programmer.
	* Software developer.
	* Coder.

And different words for writing programs:
	* Programming.
	* Developing software.
	* Coding.

So it's useful to realize from the beginning that the same thing may have several names, which often sound scary. But most of the times, behind scary words are really simple concepts which have an intrinsic logic. Once we understand this logic, the concepts are not scary at all and they become super fun to play with, and build things with them.

To write programs not much is needed, with just a computer we are good to go. But since the computer is gonna be mostly our only tool, it's neccessary a minimum familiarity with basic computer concepts. 

.. _`Information Technology`: https://en.wikipedia.org/wiki/Information_technology
