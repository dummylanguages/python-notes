*********************
Programming languages
*********************
Depending on what kind of program we are writting, there's always a language suitable for the task. It's not the same programming a driver, that writting a text editor. According to its proximity to the hardware, we can make a distinction between low and high level programming languages.

Low-level programming languages
===============================

.. sidebar:: INTEL® Core i5
   :subtitle: Intel 64

   This processor implements a microarchitecture named *Ivy Bridge*. This microarchitecture uses a set of instructions named **Intel 64**.

The **CPU** is the brain of the computer, but the only language it understands is called **Numerical machine code**, also known just as **machine code**. This is the lowest-level language, directly understood by the processor but almost unreadable for humans, and while it is possible to write programs directly in plain machine code it is very slow, tedious and error prone.

Another option would be to use **assembly language**, a low level programming language, also especific for the microarchitecture of a given processor. This language uses **mnemonic codes**, making it much more readable for humans, but still, only few applications are  writen using assembly. 

.. note:: 
	The programs written in **assembly**, are not directly readable by the computer, they need to be converted in executable **machine code** by an utility program called **assembler**. This conversion process is referred to as **assembly** or *assembling the code*.

High-level languages
====================

.. sidebar:: Fortran

	For example, Fortran was one of the first high-level languages. It was born around 1950 as a generic purpose language, but especially suited for numeric computation and scientific computing (its name derived from Formula Translating System). 

High-level languages are perfectly readable by people, at least for programmers. There are a lot of high-level languages and there is no better language, each and every one of them has its weak and strong points. Some of them are better designed for specific purposes, while others have a wider range of uses. 
Some examples of modern high-level languages are: Python, Java, Ruby, C, C++, etc. 
	
.. note:: 
	Python can be also classified as a **high-level, general-purpose** programming language.

Differences between compiled and interpreted languages
======================================================
We saw that **machine code programs** are specific for each microarchitecture. One advantage of programs written in high-level languages is its **portability**. The source code of any of these programs is platform-independent, which means that you can run it in different types of machines.

But since these programs are not **machine code**, in order to run them, we have to translate these instructions to make them understandable by the computer. There are two main ways of accomplish that:

* **Compiled languages**: These are the languages that using an utility program known as a **compiler**, translate all its source code into an **executable** file. The compiling stage is done once, and the executable can be run as many times as we wish. Every platform has to use a suitable compiler for that platform. The resulting executable files, contain **machine code** ready to run in that platform. Examples of languages that have to be compiled are C and C++.

* **Interpreted languages** are those that, instead of a compiler, use another program called **interpreter** to execute the source code of another program. Instead of compile all the source code into an executable, the interpreter executes the source code line by line. Examples of interpreted languages are python, ruby and perl.