************
About Python
************
It is said, that python is an excellent language for beginners. Also, that once you learn to program using one language, you should find it easy to pick up other languages. The key is to learn how to solve problems using a coder's approach.

Let's mention again some characteristics of Python:

* High-level
* General-purpose
* Interpreted

Before going on and start with the technical aspects, let's talk about a bit of history.

A bit of history: The two branches of python
============================================

.. sidebar:: About the name of Python.
   :subtitle: Nothing to do with the snake.

   Guido was a big fan of a british tv-show from the early seventies named the `"Monty Python's Flying Circus" <http://en.wikipedia.org/wiki/Monty_Python%27s_Flying_Circus>`_
   
   "I chose Python as a working title for the project, being in a slightly irreverent mood."
	   -- Van Rossum, 1996

Python was created by **Guido van Rossum**, he is the principal author of python, and his continuing central role in deciding the direction of the language is reflected in the title given to him by the Python community, **Benevolent Dictator for Life** (BDFL)

February 1991
	Python 0.9.0 was created and first released to the public.

January 1994
	Python reached version **1.0**.

16 October 2000
	Python **2.0** was released.

3 December, 2008
	Python **3.0** was released. 

**Python 3** was a major, **backwards-incompatible** release, designed to rectify certain fundamental design flaws in the language (the required changes could not be implemented while retaining full backwards compatibility with the 2.x series)

Although Python 3.X was both an improvement and the future of Python, the majority of Python code in use was 2.X, and its migration to 3.X is gonna be a slow process. As a consequence, Python 2.X is going to be supported in parallel with Python 3.X for years to come. Python 2.7 is gonna be the last 2.X, while 3.4.1 is, at the moment of writing this, the latest in the 3.X line’s continuing evolution.

So yes, Python is a *dual-version world*, with many users running both 2.X and 3.X according to their software goals. While the two versions are largely similar, they diverge in some important ways. 
