.. sidebar:: Table of contents.

	.. contents::
		:depth: 3
		:backlinks: top
		:local:

***************************
The python interactive mode
***************************
In addition to running scripts, we can also write and run python code using its interactive mode. The problem with this approach, is that when we finish, we don't have a file containing the source code. Still, it's pretty useful to try stuff, like for example arithmetic operations.

.. warning:: In order to follow along with this section, you need the **Python interpreter** installed in your system. The :ref:`installing Python` appendix explains in details how to do so.

To run the **python interactive mode** write in your terminal: ``$ python3``

.. note::
	In order to run python 2.X, you would write just: ``$ python``

The screen of your terminal will show something like this:

.. code-block:: python
	:emphasize-lines: 4

	Python 3.4.1 (default, May 19 2014, 13:10:29)
	[GCC 4.2.1 Compatible Apple LLVM 5.1 (clang-503.0.40)] on darwin
	Type "help", "copyright", "credits" or "license" for more information.
	>>>

The last line is known as the **prompt**. 

Quiting the interactive mode
============================
You can type ``exit()`` or ``quit()``. Or just use the shortcut ``CONTROL + D``. This is the code for **EOF** (end-of-file) or **EOT** (end-of-transmission) in UNIX systems.

.. tip::
	In **DOS** systems (Windows®) we would use ``CONTROL + W``

Clearing the screen
-------------------
For clearing the screen we can use the shortcut: ``CONTROL + L``

.. note::
	Another way would be importing the module os with:
		``>>> import os``
	And every time we wanted to clear the screen we would do:
		``>>> os.system('clear')``

Checking the help
=================
We can write ``help()`` for interactive help, or ``help(object)`` for help about object. For quiting the help,  ``CONTROL + D``


A very powerful calculator
==========================
The python interactive mode also comes in handy when we need to do complex arithmetic operations. So we can use the interactive mode as a powerful calculator. This may sound as a simplistic use for a computer, but computers usually have much more bigger screens and keyboards than calculators, they are more comfortable to use.

.. todo::	
	Put some examples of arithmetic and boolean operations, and references to both kind of operators in their respective sections.

When one line is not enough
===========================
According to the `PEP 8 - the Style guide for Python Code <https://www.python.org/dev/peps/pep-0008/#maximum-line-length>`__, it's recommended to limit the length of the **lines** to **79 characters**, including spaces, tabs, and other whitespace characters, and that’s a common limit throughout the programming world. But what happens when lines get too long or when you want to split it up for clarity.
In order to split up a statement into more than one line, you have 2 options:

1. Make sure your line break occurs inside parentheses.
2. Use the line-continuation character, which is a backslash, ``\``.

.. note:: 
	The line-continuation character is a **backslash** ``\``, not the division symbol ``/``.

Here are examples of both::

	>>> (2 + 
	... 3)
	5
	>>> 2 + \ 
	... 3
	5

Notice how we don’t get a SyntaxError. Each **triple-dot prompt** in our examples indicates that we are in the middle of entering an expression, its purpose is to make the code line up nicely. If you are using IDLE, you won’t see them at all.

.. warning::
	You do **not** have to type the dots. They appear automatically when you press :kbd:`Enter` after the ``+`` sign.


.. note::
	When using the interactive mode, python prints automatically the results of expressions you type. We don't need to use ``print()`` explicitly.::

		>>> name = 'John'
		>>> name
		John