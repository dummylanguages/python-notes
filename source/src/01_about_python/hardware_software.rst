*********************
Hardware and software
*********************
A computer includes both hardware and software. In general, hardware comprises the visible, physical elements of the computer, and software provides the invisible instructions that control the hardware and make it perform specific tasks. You don't have to be an expert in hardware for learning a programming language, but it is convenient a minimum understanding of the hardware to better grasp the effects that a program’s instructions have on the computer and its components. 

Hardware
========

The processor
-------------

.. sidebar:: My CPU

   For example, I'm writing this text in MacBook Air, 2013 model, a computer with an **Intel Core i5**. This processor has 2 cores.

Processors were originally developed with only one **CPU** (Central Processing Unit) also called **core**. The core is the part of the processor that performs the *reading and executing of program instructions*. In order to increase processing power, chip manufacturers are now producing processors that contain multiple cores. A **multicore processor** is a single component with two or more independent processors. Today’s consumer computers typically have two, four, and even ten separate cores. 

The Main memory (The RAM memory)
--------------------------------
A program and its data must be moved into the computer’s memory before they can be executed by the CPU. All this data is stored as an ordered sequence of bytes. But phisically, the memory of a computer is nothing more than a large amount of really small switches. Each switch can be in two positions: on or off. If the switch is on, its value is 1. If the switch is off, its value is 0. These 0s and 1s are interpreted as digits in the **binary number system** and called **bits** (binary digits).
The minimum storage unit in a computer is a **byte**. A byte is composed of eight bits. A small number such as 3 can be stored as a single byte. To store a number that cannot fit into a single byte, the computer uses several bytes.
Data of various kinds, such as numbers and characters, are encoded as a series of bytes. 
A program and its data must be moved into the computer’s memory before they can be executed by the CPU. The computer’s memory consists of an ordered sequence of bytes for storing programs as well as data that the program is working with.
Storing information in a computer is simply a matter of setting a sequence of switches on or off. 

+----------------+----------------+
| Memory address | Memory Content |
+================+================+
| ...            | ...            |
+----------------+----------------+
| 1331           | 01010110       |
+----------------+----------------+
| 1332           | 11101010       |
+----------------+----------------+
| 1333           | 01011011       |
+----------------+----------------+
| ...            | ...            |
+----------------+----------------+

Every byte in the memory has a unique address. The address is used to locate the byte for storing and retrieving the data. Since the bytes in the memory can be accessed in any order, the memory is also referred to as random-access memory (RAM).

Secondary memory
----------------
A computer’s memory is a volatile form of data storage, when the system’s power is turned off, all information that hasn’t been stored in the hard drive is lost. Storage devices such as hard drives, pen drives, CDs or DVDs, are used to permanently hold information. Data is stored in units called **files**.


Peripherals
-----------
Under this category, we group devices such as:

* Input devices (such as the mouse and keyboard)

* Output devices (such as monitors and printers)

* Communication devices (such as modems and network interface cards)
 
Software
========
Hardware without software, it's useless. No matter how technologically advance are digital electronics, we need some logic to control it. We can divide software into 2 big categories:

1. **System software**: These are the kind of programs with direct access to the hardware, and are in charge of managing the access to the hardware by other programs. **Drivers** and **Operating systems** belong to this category.

The first layer of software every computer has it's known as the ​**operating system**​. When any other application wants to draw on the screen, find out what key was just pressed on the keyboard, or fetch data from the hard drive, it sends a request to the OS.

.. note::
	**OS** is short for *Operating System*. 
	Some examples of OS are:

	* Microsoft Windows.
	* Linux 
	* Mac OS X

2. As opossed to the **system sofware**, we have the **application software**. These are the programs used for common tasks, such as photography editing, word proccesing or videogames. Some real life examples would be Adobe® Photoshop, Microsoft® Word, or World of Warcraft®.
