***********
Hello world
***********
In every programming language, it's a tradition for beginners to start learning to code, by writing a simple program that tells the computer to print some words on the screen. These words are *"Hello, world."*

Writing our first script
========================
In python, programs, especially the short ones, are also called **scripts**. A script is just a common text file finished with the extension ``.py``. Open your **code editor** and create a **new file**, write this line inside:

.. literalinclude:: ../../code/chap_2/first.py
   :language: python
   :linenos:
   :tab-width: 4

Save it as :download:`first.py <../../code/chap_2/first.py>`. 

How to run scripts
==================
We already mention that Python is an **interpreted language**, meaning that we have to use a program known as the **Python interpreter** to run our scripts. Assuming Python is already installed in our system, we have two ways of calling the interpreter:

	* Using **IDLE**
	* Using the **command line**, our preferred way to do it

IDLE
----
IDLE stands for **Integrated Development and Learning Environment** and is intended to help beginners running their code. Depending on how you installed **Python**, IDLE can be found in a couple of places.

To start IDLE we first must know where is located, which will depend on how you installed **Python**. 

1. If you installed Python using the installer (as we advised), IDLE can be found using macOS **Launchpad**. Every time you launch IDLE, a shell window will appear.

To run any script, on the **IDLE** menu bar, choose ``File > Open`` and navigate to your script. The script will open in a new window.
Now again on the **IDLE** menu bar, choose ``Run`` or press ``F5``. The window shell will show you the output.

2. If you installed python using **homebrew**, it's time to navigate to the directory where **homebrew** installs all the packages, aka the **Cellar**.

	a. Use finder to navigate to the **Cellar** (Cmd + Shift + G). It's located in ``/usr/local/Cellar``

	b. Once there, find the Python's directory, and inside you'll find an app named **IDLE**. Right-click on it, and choose ``Make Alias``. 
	c. Then you can move the new alias to your ``Applications`` folder. 

Now, every time you want to use **IDLE**, you can access it through macOS Launchpad, like any other app. 	

Running Python scripts from the command line
--------------------------------------------
The command line, also known as **shell**, it's just a **text interface** to interact with your system using commands. The shell is accessed using a **terminal emulator**, which is an app that in **macOS** can be launched like any other app, using the **OS X launchpad** (check inside **other**) or through **Finder**, inside a folder named **Utilities**.
 
	a. Double click to open a terminal window.
	b. Navigate to the directory where you have the script (Using the ``cd`` command).
	c. Once there, we just have to call the python interpreter using the ``python3`` command, and pass the **name of the script** (``first.py``) as an argument::
			
		$ python3 first.py
		Hello World!

	.. warning::
		You have to be in the same directory where the script is, to run it.

Congratulations, you run your :download:`first program <../../code/chap_2/first.py>` in Python. 

How the python interpreter works
================================

.. sidebar:: Python implementations.
   :subtitle: The Cpython interpreter is the default, most-widely used implementation of the Python programming language. It is written in C, hence its name..

   In addition to CPython, there are other "production-quality" Python implementations:

   * **Jython**, written in Java. 
   * **PyPy**, much faster than CPython.
   * **IronPython**, which is written for the Common Language Infrastructure.

When we run our **first script** above, what the **interpreter** has done is executing our program *one statement at a time*, in this case we only have one statement. 

.. note::
	Here we are gonna be talking about the **CPython** interpreter all the time.

Internally, the interpreter first compiles the source code that our file contains, into a format known as **byte code**. This translation of the statements of our original source code to byte code, is performed to improve the speed of execution of our code, since the byte code format, can be run much more quickly than the original code statements in our text file.

.. warning::
	This doesn't make python a compiled language. The byte code format is **NOT machine code**, and after the byte code is created, it has to be interpreted.

Usually, the intermediate step to *byte code* is invisible to us when we are running a single script. But when we run a **main script** that **imports** other scripts, the first time we run it, the interpreter will save the byte code files in a subdirectory named ``__pycache__``, located in the same directory where our source files reside. The next time we run our program, Python will load the ``.pyc`` files inside this subdirectory and skip the compilation into **byte code** step, as long as we haven’t changed your source code since the byte code was last save.

.. tip::
	Byte code files are also **one way to distribute Python programs**. Python is happy to run a program if all it can find are ``.pyc`` files, even if the original ``.py`` source files are absent. 

The Python Virtual Machine
==========================
Once our program has been compiled to *byte code* (or the byte code has been loaded from existing ``.pyc`` files), our code is executed by the **python virtual machine** (from now on, PVM). This is not a separate part of the interpreter, but it's included in it. The **PVM** take our source code (previously converted to *byte code*) and translate to machine code and executes, one by one, all of its statements.

Syntax errors
=============
Every programming language has a set of rules that specify how to combine the symbols to create a correctly structured document in that language. Every time we run a script, the interpreter looks for syntax errors in our code and inform us about them so we can fix them.

For example:

.. literalinclude:: ../../code/chap_2/bad_syntax.py
   :language: python
   :linenos:

Let's try to run the the above :download:`code <../../code/chap_2/bad_syntax.py>` code and... oops! the interpreter threw you the following error:

.. literalinclude:: ../../code/chap_2/syntax_error.txt
   :language: python
   :linenos:

The problem is that this time we made a **syntax error** in the last line. We wrote ``prin`` instead of `print`, and that's exactly what the interpreter is telling you ``NameError: name 'prin' is not defined``, python is not smart enough to know that you meant to write ``print``, so it just doesn't know what ``prin`` is. 

Ok, once corrected, our code should look like :download:`this <../../code/chap_2/first.py>`:

.. literalinclude:: ../../code/chap_2/first.py
   :language: python
   :linenos:

And it runs just fine.