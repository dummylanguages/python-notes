************************************
Creating a context manager for files
************************************
Let's remember the recommended way of working with files so we don't have to worry about leaking file descriptors:

.. literalinclude:: ../../code/chap_05/basic_with.py
    :language: python
    :linenos:
    :tab-width: 4

And that's it, we don't have to close the file, the ``while`` statement takes care of it for us. This is because the ``open`` BIF returns a file object, which class (``_io.TextIOWrapper``) supports the **context management protocol**.

Writing our own class
=====================
Let's create a totally redundant ``File`` class, compliant with the context management protocol, so we can hook up with the ``with`` statement:

.. literalinclude:: ../../code/chap_05/file_class.py
    :caption: file_class.py
    :language: python
    :linenos:
    :tab-width: 4

Using it
--------
Let's import our class and use it:

.. literalinclude:: ../../code/chap_05/using_file_class.py
    :language: python
    :linenos:
    :tab-width: 4

Once we run this file, we don't have any leaks whatsoever, so our **context manager** worked.

How does everything work out?
=============================

Dunder ``init`` initializes
---------------------------
If you create a class that defines ``__enter__`` and ``__exit__``, the class is automatically regarded as a **context manager** by the interpreter and can, as a consequence, hook into the ``with`` statement. In other words, such a class conforms to the **context management protocol**, and implements a **context manager**.

But that doesn’t mean that our class, in addition, cannot implement other methods. And sometimes define an ``__init__`` method can be useful because allows us to separate any initialization activity from any setup activity.

Dunder ``enter`` performs setup
-------------------------------
When an object is used with a with statement, the interpreter invokes the object’s ``__enter__`` method before the ``with`` statement’s suite starts. This provides an opportunity for you to perform any required setup code within dunder enter.

The protocol further states that dunder enter can (but doesn’t have to) return a value to the with statement

Dunder ``exit`` performs release
--------------------------------
As soon as the ``with`` statement’s suite ends, the interpreter always invokes the object’s ``__exit__`` method. This occurs after the ``with``’s suite terminates, and it provides an opportunity for you to perform any required release of the resource. (closing a file, teardown of a connection, etc).

As the code in the ``with`` statement’s suite may fail and raise an **exception**, dunder exit has to be ready to handle this if it happens.
