*************************************
A database connection context manager
*************************************
Let's create a context manager to **acquire**, **use** and **release** a database resource. Check this code:

.. literalinclude:: ../../code/cm_chap/db_context_manager.py
    :caption: db_context_manager.py
    :language: python
    :linenos:
    :tab-width: 4

Dunder ``init`` initializes
===========================
Apart from the mandatory ``self`` argument, this **dunder init** method accepts a single argument, which is a dictionary called ``config`` that contains the configuration options we need to connect to the database.

.. note:: We've added annotations for the ``config`` parameter and the return value.

Dunder ``enter`` performs setup
===============================
The dunder enter method provides a place for you to execute the setup code that needs to be executed before the suite in your ``with`` statement runs. The **dunder enter** method unpacks the configuration characteristics stored in ``self.configuration`` to:

    * create a **connection to the database** (``self.conn``) 
    * and create a ``cursor`` based on that connection. 

Other than the mandatory ``self`` argument, dunder enter takes no other arguments, but needs to **return the cursor**.

Dunder ``exit`` performs release
================================
The same way files need to be close, once we are done consuming a database resource, the connection need to be **teardown**. The **dunder exit** method provides a place for you to execute the **teardown code** that needs to be run when your with statement terminates. This teardown operation does 3 things:

    * Flush any cached data to the database.
    * Closes the **cursor**.
    * Closes the **connection**.

Next to the mandatory ``self`` parameter, this dunder exit contains several extra parameters. These ones have to do with **handling any exceptions** that might occur within the ``with``’s suite. When something goes wrong, the interpreter always notifies ``__exit__`` by passing three arguments into the method: 

    * ``exec_type``
    * ``exc_value``
    * ``exc_trace``

Using the context manager
=========================
To use our context manager we would have to provide the database configuration options in a dictionary.

.. literalinclude:: ../../code/cm_chap/using_db.py
    :caption: db_context_manager.py
    :language: python
    :linenos:
    :tab-width: 4

.. note:: We assume that the database ``testing`` exists, and that the user **bob** has the proper privileges to run queries.

The ``_SQL`` variable contains a string with the simplest query.