###########################
Context management protocol
###########################
We saw in the section about :ref:`files` how the ``with`` statement is used to avoid **file descriptors leaking**. Truth is that this statement can be used for avoid leaking of other type of resources too.

Everytime our code acquires some external resource, that needs to be released in order to avoid memory leaks, the ``with`` statement can help. But it's not enough to wrap that type of code inside a ``with`` statement, we need to to encapsulate our external resource inside a **context manager**. 

A **context manager** is an instance of a class that supports the **Context management protocol**. This protocol needs that the class we create contains at least two **magic methods**:

    * ``__enter__`` 
    * ``__exit__`` 
    
That's it, if we include these 2 methods, our class can hook into the ``with`` statement.

.. toctree::
    :maxdepth: 3
    :hidden:

    01_file_context_manager
    02_db_connection_context_manager
    