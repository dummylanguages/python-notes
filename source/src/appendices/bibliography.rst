************
Bibliography
************

While I was creating these notes, these are the main books I consulted:

.. _learning-python:

#. Learning Python. 
	* Mark Lutz.
	* 1500 pages.
	* O'reilly.
	* Python 2x and 3x.
	* It's like a bible, also meant to be read as the I part of **Programming python**. So I don't think is a good one for starting coding fast. Maybe as a 4th or further option.

.. _computer-science:

#. Introduction to computer science using python: A computational problem-solving focus.
	* Charles Dierbach.
	* 610 pages.
	* All 5 Stars review in Amazon (One by Guido van Rossum)
	* Great book. Very serious.


#. Fundamentals of python. From first programs to data structures.
	* Keneth A. Lambert. 
	* 877 pages.
	* The data structures part is very interesting.

#. Practical programming. An introduction to computer science using python 3.
	* 2nd Ed. 2013
	* Paul Gries, Jennifer Campbell, Jason Montojo.
	* 367 pages.
	  
#. Introduction to programming using python.
	* Y. Daniel Liang
	* 2013 
      
#. Python Cookbook.
	* 667 pages.
	* O'reilly.
	* Python 3x.
	* Author: David M. Beazley
	* Not for beginners, but it looks pretty good. I saw the author in youtube and he looks not boring. 
	* Probably I should check first his other book: "Python essential reference (2009)"

#. Think Python.
	* Subtitle: How to Think Like a Computer Scientist 
	* 265 pages.
	* Author: Allen Downey
	* O'reilly.
	* Python 2x and 3x.
	* Stopped at chapter 6. TOO MUCH FUCKING MATHS, ALMOST IN EVERY EXERCISE!! To be honest, I don't remember so much trigonometry.

#. Learning python the hard way.
	* 279 pages
	* 2013
	* Python 2x
	* Almost everything is exercises, so probably it should follow some other basic one.
	* Not a good book at all. At ex43, objects and classes, the step is huge. It also has some erratas, like for example, in several pages, it creates 'dicts' with '[ ]' instead of '{ }'.

#. Beginning Python: From Novice to Professional
	* 640 pages.
	* 2008. 2 Ed.
	* Magnus Lie Hetland (Same guy from Python Algorithms)

#. Python for kids.
	* 313 pages
	* 2013
	* A lot of work with turtle

#. Dive into python.
	* 327 pages.
	* Author: 
	* 2004
	* Python 2.2
	* For experienced programmers, but I'm gonna give it a try anyway because is a short one.
	* Yeah, it's not for beginners. It contains a lot of info, condensed, it goes to the point.
	* Good as a 3rd book.