virtualenv
==========
.. sidebar:: Who is Ian Bicking?

   `Ian Bicking`_ used to be a big name in the Python community. He also created `pip`_), which is now a core part of Python. Pip is now maintained by a `group of developers`_.

In order to start creating virtual environments we are gonna need a **Python package** named 
**Virtualenv**. This package was written by `Ian Bicking`_ , and you can check the project `here`_ and its  `documentation`_.

Installing virtualenv
---------------------
Since I'm gonna be working mostly with **Python 3**, I'm gonna install **virtualenv** using the command `pip3`, which will install the packages for **Python 3**. Just type in your terminal:

	$ pip3 install --user virtualenv

.. note::
    
    I didn't have to use `sudo`, for two reasons:
    1. Because the **Python3** installer places the interpreter inside `/Library/Frameworks`. We don't need admin permissions to write to this directory.
    2. Because we are using the :ref:`user-scheme`, which means installing packages to a user writeable location, not to site-packages.

Another way to install a package is to use the **versioned Python commands** in combination with the `-m` switch to run the appropriate copy of pip::
    
    $ python3 -m pip install --user virtualenv

Creating a virtual environment
------------------------------
To create a virtual environment using `virtualenv` we'll change to the directory of our project, and from there we'll do::

    $ py3 -m virtualenv my_new_env

This will create the directory `my_new_env` which will contain the new virtual environment::

    my_new_env/
        ├── bin
        │   ├── activate
        │   ├── activate.csh
        │   ├── activate.fish
        │   ├── easy_install
        │   ├── easy_install-3.6
        │   ├── pip
        │   ├── pip3
        │   ├── pip3.6
        │   ├── python -> python3
        │   └── python3 -> /usr/local/bin/python3
        ├── include
        ├── lib
        │   └── python3.6
        └── pyvenv.cfg


If you check the contents of your environment's directory, there's a folder name `bin`. Inside this folder live the scripts to activate/deactivate the environment, as well as a copy of `easy_install` and `pip`. Every time we create an environment we'll have available a copy of `pip` which we'll use to install packages inside the environment. 
Also, notice that by default, the environments are created **by default** with a copy of `python3`, since we used `pip3` to install `virtualenv` as a package of **Python 3**.

Enabling/disabling the environment
----------------------------------
To **enable** the environment we'll use::

    $ source my_new_env/bin/activate

And to **deactivate** the environment we'll simply run::

    $ deactivate

Installing packages in our virtual environment
----------------------------------------------
The whole point of virtual environments is installing packages in it. Every virtual environment is installed with a copy of ``pip`` in it, so installing new packages as simple as typing::

    $ pip install a_package

All the packages will be installed locally in the directory of our virtual environment, without interfering with other enviroments.

.. warning:: Do not forget to **enable your environment** before using pip.

Best practice: One directory for the enviroments
------------------------------------------------
Every time we create an environment, a small structure of directories under our project's root is also created (basically the folders ``bin``, ``include`` and ``lib``). It's not a good idea to have all this stuff messed up with our code, and in case of using version control, we have to tell the version control tools to ignore the virtual environment files.

To avoid all that, it’s a best practice to keep the stuff related to the virtual environment apart from our code base, centralizing the location of all our virtualenvs in one place. A common place for that is at `$HOME/.virtualenvs/`. 

.. code-block:: bash

    $ mkdir ~/.virtualenvs

After that, every time we create a new virtual environment we have to remember to place it in that centralize folder specifying its path. For example, if we are creating a virtual environment named `myenv` we would do::

    $ python3 -m venv ~/.virtualenvs/myvenv

Also, we have to remember that path again when **enabling** the environment::

    $ source ~/.virtualenvs/myvenv/bin/activate
    (myvenv) $

But having to take these 2 steps every time we want to create a virtual environment is a pain, ain't it? Wouldn't be awesome if all our environments were automatically created in one directory without us having to specify it? Now you understand how much easier is our Pythonista life using `virtualenvwrapper`.

Command not found
-----------------
If when you try to run ``virtualenv`` you run into this::

    $ virtualenv --help
    virtualenv: command not found

That's because the script is not in your `PATH`, the list of locations where your **shell** looks there before running any command. To solve this problem the first thing you need to know is where is ``virtualenv`` located, which depends on how did you install it:

* If you install it to the **global site-packages** directory, ``virtualenv`` will be installed in::

    /Library/Frameworks/Python.framework/Versions/3.6/bin/virtualenv

* If you installing using the ``--user`` option, the script will be place in the ``userbase/bin`` directory, which in my case is located in::

    $HOME/Library/Python/3.7/bin

.. note:: You can get the location of ``userbase`` directory running the command::
    
    $ python3 -m site --user-base

So to solve the problem you have to add one of these 2 locations to the ``$PATH``, I added the second one::

    export="${HOME}/Library/Python/3.7/bin:${PATH}"

* A **hacky** solution to that problem would bet to create a symbolic link to the `virtualenv` script in a place that is included in your `PATH`::

	$ ln -s /Library/Frameworks/Python.framework/Versions/3.6/bin/virtualenv /usr/local/bin/virtualenv

.. _`Ian Bicking`: http://www.ianbicking.org/
.. _`pip`: https://pip.pypa.io/en/latest/
.. _`group of developers`: https://github.com/pypa/pip
.. _`here`: https://github.com/pypa/pip
.. _`documentation`: https://virtualenv.readthedocs.org/en/latest/index.html