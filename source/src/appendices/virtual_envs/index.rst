.. _virtual environments:

####################
Virtual environments
####################

.. toctree::
    :maxdepth: 3
    :hidden:

    venv
    virtualenv
    virtualenvwrapper/index
    recipes

Imagine the following scenario. You have work to do on two projects:

* **Project A**, uses **Django 1.6.5**, that you already have installed.
* **Project B**, you need **Django 1.7**. in this one.

Updating to the more recent version of **Django 1.7** to work in **project B**, implies that your old **Django 1.6.5** is replaced by the newer version, so you are not gonna be able to use the old version of **Django** in **project A**. Wouldn't be awesome if you could work on both projects at the same time, using a different version of Django for each project? As a matter of fact you can.

A virtual environment is like a **sandbox** for each of your projects. Each environment will be isolated from the other ones, allowing you to install different versions of the same package for each environment, that do not interfere with either each other, or the main Python installation. This is particularly important for Web development, where each framework and application will have many dependencies that may interfere with each other if installed globally in your system.

.. note:: No need of **permissions** for installing new packages.
	
Another advantage of using virtual environments, is that we don't need **super user** permissions to install new packages in our new virtual environment, so we don't have to be bothering the system administrator every time we need to install some package, since we are not gonna be installing it at the system level. Each new environment automatically includes a copy of ``pip``, so that you can setup the third-party libraries and tools that we want to use in that environment.
