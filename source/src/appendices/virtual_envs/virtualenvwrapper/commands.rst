virtualenvwrapper commands
==========================
The following table contains a list with the most useful commands, most probably we're not gonna need more. 

+-----------------------------------+---------------------------------------------------+
| COMMANDS MOST USED                |                                                   |
+===================================+===================================================+
| Creating an environment [#f1]_    | ``mkvirtualenv fooenv``                           |
+-----------------------------------+---------------------------------------------------+
| Same as above and associating it  | ``mkvirtualenv fooenv -a /path/to/fooproj``       |
| with an existing project  [#f2]_  |                                                   |
+-----------------------------------+---------------------------------------------------+
| Creating a new environment and a  | ``mkproject foo``                                 |
| directory for our project [#f3]_  |                                                   |
+-----------------------------------+---------------------------------------------------+
| Associate existing project with   | ``setvirtualenvproject [venv_path project_path]`` |
| existing  environment  [#f7]_     |                                                   |
+-----------------------------------+---------------------------------------------------+
| Listing the environments          | ``lsvirtualenv`` or ``workon``                    |
+-----------------------------------+---------------------------------------------------+
| Activating an environment [#f4]_  | ``workon foobar``                                 |
+-----------------------------------+---------------------------------------------------+
| Cd to our project folder  [#f5]_  | ``cdproject``                                     |
+-----------------------------------+---------------------------------------------------+
| Deactivating an environment       | ``deactivate``                                    |
+-----------------------------------+---------------------------------------------------+
| Switching between environments    | ``workon foobar``                                 |
+-----------------------------------+---------------------------------------------------+
| Deleting an environment  [#f6]_   | ``rmvirtualenv foobar``                           |
+-----------------------------------+---------------------------------------------------+
| Cd to our environment directory   | ``cdvirtualenv``                                  |
+-----------------------------------+---------------------------------------------------+
| Cd to our packages directory      | ``cdsitepackages``                                |
+-----------------------------------+---------------------------------------------------+
| List all the environment packages | ``lssitepackages``                                |
+-----------------------------------+---------------------------------------------------+
| Wipe all the environment packages | ``wipeenv``                                       |
+-----------------------------------+---------------------------------------------------+

.. rubric:: Footnotes

.. [#f1] And we have created the environment, and activated it at the same time. 
.. [#f2] Usin the ``-a`` option, we can choose a directory for our project. The directory should exist.
.. [#f3] The new project directory will be created under ``$PROJECT_HOME``. Make sure you set this up in your ``.bash_profile``
.. [#f4] When we activate a environment, we are automatically taken to its project  directory. 
.. [#f5] Very useful, after going deep into our project directory for coming back to its root. 
.. [#f6] We have to ``deactivate`` the environment first. Do NOT forget to copy your code out of it before deleting anything!!
.. [#f7] The argument to the ``project_path`` has to be a **full path**.


Thanks to the **autocompletion** feature included in ``virtualenvwrapper``, typing all of these commands  it's faster than it looks, enough to type the first 2 or 3 letter and hit ``TAB``. For example:

* To go to our **project folder**, instead of ``cdproject`` we'll just type ``cdp[roject]`` + ``TAB``.
* To ``deactivate`` an environment, ``dea[ctivate]`` + ``<Tab>``.
