Recipes
=======
Here some use examples.

Creating a new environment and a new project
--------------------------------------------
Once both packages are installed and ``virtualenvwrapper`` is initialized, we can start using them. The most common scenario is easily accomplished with the command::

    $ mkproject foo

As a result:
    1. A new virtual environment named ``foo`` is created under ``$WORKON_HOME`` (by default ``$HOME/.virtualenvs``). Also, the new environment is automatically activated.
    2. At the same time a new empty folder is created under ``$PROJECT_HOME``, and we are placed there.

.. note::
	If we prefer using the command ``mkvirtualenv`` and we get the error that ``virtualenvwrapper`` can't locate ``virtualenv``, read the section about ``VIRTUALENVWRAPPER_VIRTUALENV`` to find out how to fix it.

Changing the project directory after creating our project
---------------------------------------------------------
If we want to change the directory for our project after we have created the environment, we still can do it, and change it as we please:

1. Go to your environment directory under `~/.virtualenvs/`.
2. Edit the ``.project`` file. It just contains a line with the path of the project directory.

If you don't find a ``.project`` file there, you can manually create one, and write the new path. 

If you don't want to do it manually, you can use a ``virtualenvwrapper`` command for it:

.. code-block:: bash
	:linenos:

	$ workon foobar
	$ cdvirtualenv
	$ setvirtualenvproject [foobar project_path] 

The 3rd line creates a ``.project`` file under the `~/.virtualenvs/foobar/` directory. 

A special situation: Creating new environment for an existing project
---------------------------------------------------------------------
Imagine you have already created a folder and have some code in it, and now you realize it's a good idea to create a new virtual environment for that project. There are 2 ways you can do this:

1. Creating the environment and linking it to the directory of the existing project. It involves 2 steps::

	$ mkvirtualenv cool-env-name
    ...
    ...
    (cool-env-name) $ setvirtualenvproject $VIRTUAL_ENV path-to-existing-project-directory

2. What the ``setvirtualenvproject`` command does is just creating a `.project` file inside your virtual environment that points to the location of your project. You can go ahead and create that file manually, and write inside the path to your project folder, the result is the same.

.. note::
    
    In both cases, we can choose a different name for the environment we are creating, it doesn't to be called like the project.

Different names for enviroment and project
------------------------------------------
Creating the environment with the ``-a`` option, allows us to create a different name for our project:

.. code-block:: bash

	$ mkvirtualenv cool-env-name -a path-to-NOT-existing-project-directory


By the way, it also creates the `.project` file we mentioned in the section above.

.. note::
    
    The project must **NOT** exist when we issue this command.

Changing the location of our virtual environments
-------------------------------------------------
By default, the location of the **virtual environments** is at ``~/.virtualenvs``. But, OPTIONALLY, we can choose another name and location for this directory. That can be accomplished setting the environment variable `WORKON_HOME` to the value of our preference, for example:

.. code-block:: bash

    export WORKON_HOME=$HOME/Documents/.my_virtualenvs

Changing the interpreter
------------------------
In case we wanted to create a new environment using an interpreter version other than the one set in ``VIRTUALENVWRAPPER_PYTHON`` we can create the environment using the ``--python`` option::

	$ mkvirtualenv --python=/usr/local/bin/python test

.. tip::

	We can also use shell expansion and write ``$ mkvirtualenv --python=`which python` test3``
	NOT REALLY. I don't know why, but with shell expansion I get: ``-bash: $: command not found``

IPython configuration files
---------------------------
When we install IPython to our virtualenv, we may want to keep a separate configuration for each environment. In order to do that we can add the following line to ``$HOME/.virtualenvs/bin/postactivate`` script::

    IPYTHONDIR=$VIRTUAL_ENV/.ipython
    export IPYTHONDIR

The ``postactivate`` script is sourced right after we activate each virtual environment. In those lines we are setting the directory for the IPython configuration files inside each virtual environment, what allows for a separate configuration.

Once we deactivate the environment we can edit the ``$HOME/.virtualenvs/bin/postdeactivate`` adding the line::

    unset IPYTHONDIR

This way the ``IPYTHONDIR`` won't be around unnecessarily.