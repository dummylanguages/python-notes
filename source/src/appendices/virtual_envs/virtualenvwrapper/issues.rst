Issues
======
Some issues I found.

Problems installing virtualenvwrapper
-------------------------------------
If you see this error when opening your shell::

	/usr/bin/python: No module named virtualenvwrapper
	virtualenvwrapper.sh: There was a problem running the initialization hooks.

	If Python could not import the module virtualenvwrapper.hook_loader,
	check that virtualenvwrapper has been installed for
	VIRTUALENVWRAPPER_PYTHON=/usr/bin/python and that PATH is set properly.

Make sure that in your shell configuration files, you have export the environment variable...?

Problems with old SSL certifications
------------------------------------
Installing **virtualenvwrapper** should have been as easy as typing:

.. code-block:: bash

	$ pip3 install virtualenvwrapper

But instead the shell threw at me several **errors** related to **SSL certifications**. To make a long story short, the thing is that OS X **El Capitan** y **Sierra** ship version **0.9.8zg** of **OpenSSL** which is old and problematic, so **Python 3.6** includes its own version **1.0.2** of **OpenSSL** and the Apple supplied OpenSSL libraries are not used at all. The problem is that the new version supplied with the Python 3.6 installer does not have trust certificates installed. 

Solutions to the bug
--------------------
When we were installing **Python 3.6** the installer provided a **README** but, seriously, who has time to read those? ;-)

.. image:: /img/readme_OpenSSL.png 
	:alt: "README"

The `issue28150 <https://bugs.python.org/issue28150>`__ provides two solutions to the problem we have described:

1. Install the **certifi package**, as the **README** clearly explained.
2. Run the script provided with the installer and located in ``/Applications/Python 3.6/Install Certificates.command``

We took this second path and after that the `virtualenvwrapper` installation run like a breeze. Almost done, but before explaining how to use this tool, we have to take care of a couple of essential steps.


After upgrading python
----------------------
One day, you open your terminal and try to enable the virtual environment you were working on but.. oops, this is the answer::

	$ workon python_notes
	dyld: Library not loaded: @executable_path/../.Python

The thing is, last nigh before going to sleep, you uninstalled an old version of Python to reinstall a new one, but the links in your virtual environment are pointing to the older version that doesn't exist anymore. The solution is easy, 2 steps:

1. Use the command ``find`` to find the symlinks inside your virtual environment, and delete them all::
   
   $ cdvirtualenv
   $ find python_notes -type l -delete

2. Create a new virtual environment with the same name of the old one::
   
   $ cd ..
   $ mkvirtualenv python_notes

Done.
