Installling virtualenvwrapper
=============================

TL;DR
-----
1) Install the packages running (Optionally, use the ``--user`` option for the **user scheme**)::

	$ pip3 install --user virtualenv virtualenvwrapper

2) Add the following lines to ``~/.bash_profile`` or ``~/.bashrc``::

    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
    export PROJECT_HOME=$HOME/code
    source /Users/javi/Library/Python/3.7/bin/virtualenvwrapper.sh

3) Source your ``.bash_profile`` or ``.barshrc`` file::

    $ source ~/.bashrc

Installing
----------
We have two options::

	$ pip3 install --user virtualenvwrapper

Or the other one::
    
    $ python3 -m pip install --user virtualenvwrapper

Initialization steps
--------------------
In order to start using ``virtualenvwrapper``, we needed to add a couple of lines to our **shell startup file** (if you use ``bash``, this file is gonna be either ``.bashrc`` or ``.bash_profile``). 

VIRTUALENVWRAPPER_PYTHON
^^^^^^^^^^^^^^^^^^^^^^^^
During startup, when we are **sourcing** ``virtualenvwrapper.sh``, it will try to run the **initialization hooks**. It will start searching for a **python interpreter** on the ``$PATH``, if we haven't installed the script next to this interpreter it will complain::

    /usr/bin/python: No module named virtualenvwrapper
    virtualenvwrapper.sh: There was a problem running the initialization hooks.
    ...

So if we have install ``virtualenvwrapper`` for other interpreter, we should specify its location using this **enviroment variable**.  By default, when we create an environment, it will be created with a copy of this python interpreter.

In our case, we installed Python3 using the **official installer**, which puts **symbolic links** to the interpreter in ``/usr/local/bin``. We can use this path when setting this variable in our shell configuration file::

    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3

.. note:: You could also use the real location of the interpreter in ``/Library/Frameworks/Python.framewor/...``, it makes no difference.

VIRTUALENVWRAPPER_VIRTUALENV
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sometimes, when creating a virtual environment you may get some error::

    $ mkproject testing
    ERROR: virtualenvwrapper could not find virtualenv in your path

It happened to me when I installed Python in **macOS** using the official **graphical installer**. This installer puts the interpreter and all the Python packages in a directory which is not in the PATH::

    /Library/Frameworks/Python.framework/Versions/3.7/bin

During the installation, the graphical installer adds this location to the PATH. Check :ref:`installing-python` to know more about it::

    PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
	export PATH

The installer also adds some **symbolic links** to the interpreter and other utilities in ``/usr/local/bin``, so I was wrongly counting on these links, and **deleted** the above lines on my PATH. That's why, ``virtualenvwrapper`` couldn't find ``virtualenv``.

For solving that, you have a couple options:

1. Restore the PATH definition created by the **graphical installer**.
2. Set up the ``VIRTUALENVWRAPPER_VIRTUALENV`` to the location of the ``virtualenv`` binary::

    export VIRTUALENVWRAPPER_VIRTUALENV=/Library/Frameworks/Python.framework/Versions/3.7/bin/virtualenv

3. Creating a **symbolic link** to ``virtualenv`` and putting it in some location in the PATH, like ``/usr/local/bin`` will also solve the problem, but this is quite hacky.

If you are gonna be installing packages to the **global site-packages**, the best option would be to rely on the lines created by the **graphical installer** on our shell configuration. That way we'll get all the Python packages we install on our PATH, ready to rock. If you choose this option, you will **not** have to set this **environment variable**.

.. warning:: If you installed ``virtualenv`` following the :ref:`user-scheme`, you have to add to the PATH, the location of the **user site-packages** scripts!

PROJECT_HOME
^^^^^^^^^^^^
The environment variable ``PROJECT_HOME`` sets a unified location for all of our **projects**. In other words, when we create a virtual environment, a directory to hold the source code of that particular project is automatically created for us.

WORKON_HOME
^^^^^^^^^^^
The variable ``WORKON_HOME`` tells ``virtualenvwrapper`` where to place your **virtual environments**. The **default** is ``$HOME/.virtualenvs``. If the directory does not exist when virtualenvwrapper is loaded, it will be created automatically.

.. note:: Bottom line, if you are happy with the default do **not** set this variable.

Location of virtualenvwrapper itself
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I had some trouble here, because according to the **virtualenvwrapper documentation**, the script is installed to ``/usr/local/bin/virtualenvwrapper.sh``. But in my case, the script was sent somewhere else. The location of this script depends on:

* How did we installed Python.
* How did we installed ``virtualenvwrapper``, to the **global site-packages** or to the **user site-packages**.

1. If you installed it **globally** you will get something like::

    /Library/Frameworks/Python.framework/Versions/3.7/bin/virtualenvwrapper.sh

2. If you installed it following the **user scheme**, you will find it in the ``userbase/bin`` directory::

    $HOME/Library/Python/3.7/bin/virtualenvwrapper.sh

In my case it was installed following the **user scheme**, so that's the path I must use to source it.

.. note:: In case of doubt, the following command shows its real location::

    $ find / -name virtualenvwrapper.sh

To make these changes effective we have to remember to source our `.bash_profile` or `.bashrc` file after changes to the configuration::

    $ source .bash_profile

My variables
^^^^^^^^^^^^
This is what I added to my shell configuration::

    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
    export PROJECT_HOME=$HOME/PythonCode
    source /Library/Frameworks/Python.framework/Versions/3.7/bin/virtualenvwrapper.sh

