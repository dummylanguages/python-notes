.. _virtualenvwrapper-label:

*****************
virtualenvwrapper
*****************

.. toctree::
    :maxdepth: 3
    :hidden:

    installing
    commands
    issues
    recipes

.. sidebar:: Who created virtualenvwrapper?
   
   **Virtualenv** was written by `Doug Hellman`_ . You can check the `project at bitbucket`_ and its documentation `here`_.

In the first section we have installed the `virtualenv`_ . That's all we need to create **virtual environments**, but we could build upon that and install another package named `virtualenvwrapper`_ .

These 2 packages work together, in the sense that ``virtualenvwrapper`` is a **wrapper** around ``virtualenv``, which makes its use way more convenient and adds some improvements and extra functionality to ``virtualenv``. For example:

* In the first section we saw is a good practice to organize **all of our environments under a single directory**, leaving our project's folder completely empty and available to hold just our project files, nothing else. If we use just ``virtualenv``, we have to create that directory manually and remember its location every time we create an enviroment. ``virtualenvwrapper`` creates that directory for us and **automatically** places every newly created enviroment there.

* With ``virtualenvwrapper``, when we create a virtual environment, this is **automatically activated**, so we can start working in it straight away. Using just ``virtualenv`` after creating an environment, we have to activate it, a 2 steps proccess.

* ``virtualenvwrapper`` provides **autocompletion**, meaning that we don't need to type the whole commands, or enviroment names. Just typing the first 3 or 4 letters and hitting the ``TAB`` key, does the job for us. So without a wrapper, working with virtual enviroments is way more verbose.

.. warning:: Do not forget that ``virtualenvwrapper`` needs ``virtualenv`` to work.

.. _`Doug Hellman`: http://doughellmann.com/
.. _`project at bitbucket`: https://bitbucket.org/dhellmann
.. _`here`: http://virtualenvwrapper.readthedocs.org/en/latest/index.html#
.. _`virtualenv`: https://github.com/pypa/virtualenv
.. _`virtualenvwrapper`: https://bitbucket.org/dhellmann/virtualenvwrapper/