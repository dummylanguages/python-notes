************************
The native way: ``venv``
************************
From **version 3.3**, the Python Standard Library includes the `venv module`_. This module allows us to create lightweight `virtual environments`_.

.. note:: These **virtual enviroments** are provided with their own **site directories**, where Python packages will be installed isolated from **system site directories**. Each virtual environment has its own **Python binary** (which matches the version of the binary that was used to create this environment).

Creating virtual environments
=============================
To create a virtual enviroment, we just have to move to the directory containing our project and execute the ``venv`` module::

    $ mkdir my_project
    $ python3 -m venv /my_project/.venv
    
.. note:: It's a common practice to name the resulting directory as ``.venv``.
    
Error in Debian/Ubuntu systems
------------------------------
If the output of the command above is::
    
    The virtual environment was not created successfully because ensurepip is not
    available.  On Debian/Ubuntu systems, you need to install the python3-venv
    package using the following command.
    
    apt-get install python3-venv
    
    You may need to use sudo with that command.  After installing the python3-venv
    package, recreate your virtual environment.
    
You may have think that the **virtual environment** has been successfully created (there is a new ``.venv`` folder), but in fact it's a crippled enviroment. To **fix** that we have to run::
    
    $ sudo apt-get install python3-venv
    
Then we have to **recreate** the virtual enviroment::
    
    $ python3 -m venv /my_project/.venv

Now we should have a perfectly functional virtual enviroment.

Activating the enviroment
=========================
To activate the enviroment we have to run::

    $ source .venv/bin/activate

We should see that our shell prompt is prefixed with ``(.venv)``, or whatever other name we gave our enviroment.

Installing packages
-------------------
Once the enviroment is activated, the Python packages we install, will be place in the **site-packages** directory of the enviroment (under ``.venv/lib/python3.X/site-packages/``), totally isolated from our system's site-packages.

A good idea may be to keep a list of the packages used in the enviroment, in case we need to share with other developers. We can easily do that with::

    (venv) $ pip freeze > requirements.txt
    
In case you are in the receiving end of the requirements file, after creating your enviroment, you can install the required packages with::
    
    (venv) $ pip install -r requirements.txt
    
.. note:: Usually people add the ``.venv`` folder to their ``.gitignore`` file, so the enviroment itself is not kept under source control. Often, it's the ``requirements.txt`` file, the one that is tracked and commited.

Accessing system packages
-------------------------
Although it's not often needed, sometimes we want to be able to install packages to our isolated enviroment while at the same time being able of accessing the **system wide** Python modules. We can do that when creating the enviroment::

    $ python3 -m venv .venv --system-site-packages

Again, once the environment is activated, the packages we install won't affect the **system site-packages**. In case we're using this option, to list the **local** packages we would run::

    $ pip3 list --local

For this type of environments, don't forget to use the ``--local`` flag with the ``freeze`` subcommand, if we want to add only the **local** packages to our ``requirements.txt`` file.

Deactivating the enviroment
===========================
Just run::
    
    (venv) $ deactivate

Deleting the enviroment
=======================
Just delete the ``.venv`` folder (or whatever name you used for the target directory when creating the enviroment).

Additional notes
================
Although in the next sections we're gonna describe other ways of creating virtual enviroments using additional tools, I feel the method explained here is usually enough for most case scenarios.

.. _`venv module`: https://docs.python.org/3/library/venv.html#module-venv
.. _`virtual environments`: https://www.python.org/dev/peps/pep-0405/
