Recipes
=======

Copying an environment
----------------------
Useful for duplicating, or renaming a virtual environment.

Requirement: User has a python virtual environment up and running; for some reason he needs to make a copy of it on same machine at different location

Go to original virtualenv and run::

	$ pip freeze > modules.txt

Go to new virtualenv, copy modules.txt into it and run::

	$ pip install -r modules.txt

``modules.txt`` file is a file that contains a list of all the python packages you want to install.

Installing packages globally
----------------------------
Most of the times, we want to install or updates packages for an specific project, but what happens when we need to do it globally? Do we have to modify back our bash configuration? 
No we don't have to. We just have to add the next function to your bashrc::

	syspip(){
	   PIP_REQUIRE_VIRTUALENV="" pip "$@"
	}

This bash function works sort of like an alias, so that when we need to install a package globally, we just have to type::

	$ syspip install fooPackage

Or if we just want to update::

	$ syspip install --upgrade fooPackage

And our *fooPackage* gets installed or upgraded globally.

.. note:: 
	If we are gonna use **pip3** (The pip version for Python3), we have to add to our ``.bashrc`` the following function::

		syspip3(){
			PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
		}

Fine tuning pip for virtual enviroments
---------------------------------------
Every time we create a virtual environment, it automatically includes a copy of **pip** so we can install additional packages according to the needs of our project. Packages installed this way, don't interfere with packages in other projects or the globally installed packages. 

Now, imagine the situation where after some hours of late night coding, we decide to switch to another project and install a new package in it, but we forget to source the environment. As a consequence, the package is installed globally. Sure we could uninstall with::

	$ pip uninstall fooPackage

But we can do better by enabling a pip's feature that makes impossible installing or upgrading a package if we don't have an activated virtual environment. Just set this variable in your bash configuration::

	export PIP_REQUIRE_VIRTUALENV=true

And reload your bash environment with::

	. ~/.bash_profile

Now, if we try to install a package in the absence of an activated virtual environment::

	$ pip install fooPackage
	Could not find an activated virtualenv (required).

About the pip.conf file
-----------------------
In MacOs the **per-user** configuration file must be placed in ``$HOME/Library/Application Support/pip``
If we want to keep such configuration so it outlives the new clean installations of our **OS**, it's handy to have a copy somewhere safe, like Dropbox or Google Drive. We can use a symbolic link running the following command::

	$ ln -s ~/Google\ Drive/dotfiles/pip $HOME/Library/Application\ Support/pip

If multiple configuration files are found by pip then they are combined in the following order:

    1. Firstly the system-wide file is read, then
    2. The per-user file is read, and finally
    3. The virtualenv-specific file is read.