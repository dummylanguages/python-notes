.. _installing Python:

*****************
Installing Python
*****************
As we already know, python is an **interpreted language**, meaning that in order to run python programs in our computer, we need to have installed the **Python interpreter**. All **Unix** operating systems include preinstalled the **Python interpreter**, so if you are lucky enough to be using Unix, you can check what version of python lives in your system. We just have to type in the terminal:

.. code-block:: bash
	:linenos:

	$ python --version
	Python 2.7

The output of that command (2nd line) shows us the current installed version. At the moment of writing this, I'm using the latest version of **Mac OS X**, 10.9 aka mavericks, which comes with Python 2.7 preinstalled. 

Since I'm interested in using mainly python 3.X, and that one is not preinstalled in my system, next we are gonna take care of it.

macOS installation
==================
There are a couple of ways of getting Python 3 installed on macOS:

1. Using the official installer.
2. Using Homebrew, a package manager for **macOS**

The official installer
----------------------
This is probably the easiest option, we just have to go to the `downloads section`_ of the `Python's official website`_ and choose the appropriate version for our **OS**, in my case is the version for **macOS**. At the time of writing there was available **Python 3.7** so that's what I chose. 

Once the installer is downloaded, it's just a matter of double-clicking to install it. If you are curious about the exact location of the interpreter::

	/Library/Frameworks/Python.framework/Versions/3.7/bin/python3

During the installation, the following line is added to our `.bash_profile` file:

	# Setting PATH for Python 3.7
	# The original version is saved in .bash_profile.pysave
	PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
	export PATH

These lines are putting in the PATH, the **scripts** of the packages we install to the **global site-packages** directory. If we are gonna be installing packages using the :ref:`user-scheme`, we can delete/comment the above lines.

.. note:: The Python interpreter is also in this location, but the installer create **symbolic links** to it in ``/usr/local/bin``, a directory which is, by default, in the ``PATH``. So no worries if you delete/comment these lines.

Using Homebrew
--------------
Another popular way to install Python in macOS is using `Homebrew`_, a command-line package manager that is gonna provides us with the latest version of Python. In the past, my experience with `Homebrew`_ and old versions of Python wasn't the best one, that's why I prefer to use the installer.

By the way,	if you don't have **Homebrew** installed, its `official site`_ offers a copy-paste one-liner to do so:: 

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Now, with Homebrew installed, to install **Python 3** we just have to run::

	$ brew install python3

When this command finishes, we can check what version of python 3.X we have installed::

	$ python3 --version
	Python 3.6.1

Homebrew’s Python also includes the latest Python package management tools: pip and Setuptools. :ref:`Here <installing-packages>` we explain how to use them for installing python packages. 

My issue with Homebrew
^^^^^^^^^^^^^^^^^^^^^^
According to pip documentation, it's possible an alternative install location that is specific to a user. This is known as the `user scheme`_. But due to a `bug`_ in the distutils.cfg written by Homebrew, which sets the package prefix, the **user scheme** doesn't work.

Getting rid of the old Python
=============================
Now you may be thinking of **deleting** the **Python 2** that comes preinstalled in your system, after all we have just installed and want to use the latest version of the **3.X** branch. Well, that may be not such a good idea, since it may break some scripts used by your system and that depend on good old **Python 2**. It's a better idea to install the new **Python 3** leaving untouched the good old **Python 2**. This way both interpreters will coexist in our system, and we will have both of them available using the commands `python` (for the system's default **python2**) and `python3` (for the new version we have just installed).

Creating some handy aliases
---------------------------
If you find yourself running **Python 3** most of the time, and after a while feel annoyed by having to type :command:`python3` instead of just :command:`python` everytime you want to start the interactive interpreter or run a script, we can create the following alias our shell configuration file::

	alias python='python3'

The :command:`python` command now refers to :command:`python3`. If you want to use the original :command:`python` (that refers to python2), you can **escape the alias** using :command:`\python`. That will launch the Python 2 interpreter bypassing the alias.

Yet another interesting option for those people who are going to launch interpreters both interpreters equally often is to create two aliases such as::

	alias py2='python2'
	alias py3='python3'

And use them accordingly to the interpreter they want to use at any given point.


.. _`official site`: https://brew.sh/
.. _`Homebrew`: https://brew.sh/
.. _`User scheme`: https://docs.python.org/2/install/index.html#alternate-installation-the-user-scheme
.. _`bug`: https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/Homebrew-and-Python.md#note-on-pip-install---user
.. _`downloads section`: https://www.python.org/downloads/
.. _`Python's official website`: https://www.python.org/