*******
IPython
*******
IPython is an alternative for the **Python interactive mode**. It offers a lot of features like:

* Tab autocompletion.
* Offers enhanced introspection.
* Additional shell syntax.
* Etc.

IPython can also be used as a system shell replacement.

Installing IPython
==================
Since I'm gonna be using **Python 3**, I'm gonna create a virtual environment which is gonna contain a Python 3.X executable:

1. Create virtual environment: ``$ mkvirtualenv -p /usr/local/bin/python3 ipython3``
2. Create folder for the project: ``mkdir ~/Python_projects/ipython3``
3. Associate both of them: ``setvirtualenvproject ipython3 ~/Python_projects/ipython3``
4. Install IPython with all the dependencies for notebook: ``pip install "ipython[notebook]"``

The IPython Notebook is a component of IPython and with the last command,  is installed alongside IPython automatically.

IPython commands
================

+----------------------+--------------------------------+
| Some useful commands |                                |
+======================+================================+
| To start ipython:    | $ ipython                      |
+----------------------+--------------------------------+
| To quit:             | ``Ctrl + D`` twice or ``exit`` |
+----------------------+--------------------------------+

.. _autoreload:

Autoreload modules
------------------
After editing some modules, these changes are not reflected in our interactive session. We have to do the tedious dance of saving the module, and reimporting it again so it reflect the changes.

With ipython we have available an **extension** for realoding modules automatically. For using it we have to load it, and call it::

    In [7]: %load_ext autoreload

    In [8]: %autoreload 2

``%autoreload 2``: Reload **all** modules (except those excluded by ``%aimport``) every time before executing the Python code typed.

.. warning:: 
    Don't forget the ``2`` option. 

.. tip:: 
    To make this configuration permanent, add this lines to your profile::

        c.InteractiveShellApp.exec_lines = []
        c.InteractiveShellApp.exec_lines.append('%load_ext autoreload')
        c.InteractiveShellApp.exec_lines.append('%autoreload 2') 



There are more options we can check in the `documentation`_.

Configuration files
===================
IPython uses *“profiles”* for configuration, and by default, all profiles will be stored in the so called *“IPython directory”*. For most users, the **configuration directory** will be ``~/.ipython`` 

.. note:: 
    Don't forget to enable your virtual environment in case you installed ipython inside one:``$ workon ipython``

To locate this configuration directory we can do::

    $ ipython locate
    /Users/your_username/.ipython

Inside this directory we can find our profiles.

Profiles
--------
A **profile** is a directory containing configuration and runtime files, such as logs, connection info for the parallel apps, and your IPython command history. The idea is that users often want to maintain a set of configuration files for different purposes. Profiles make it easy to keep a separate configuration files, logs, and histories for each of these purposes.

Creating new profile
--------------------
To generate a **default profile** and configuration files, we do in our system's shell:: 

    $ ipython profile create

and you will have a default ``ipython_config.py`` in your IPython directory under ``profile_default``.

To create a new profile, also from our system's shell::

    $ ipython profile create example

.. note:: 
    Don't forget to previously enable your virtual environment::
        
        $ workon ipython

Now under ``~/.ipython/profile_example`` we should have a file named ``ipython_config.py`` with our configuration.

Setting default profile
-----------------------
To set a default profile, we have to locate the **default** profile  we generated, and edit its ``ipython_config`` file (every profile has one). Inside this file, we'll indicate which profile we want as default:

    # The IPython profile to use.
    c.TerminalIPythonApp.profile = 'example'

.. error:: 
    This didn't work as expected. A workaround is setting an alias in my shell configuration::
        # IPYTHON
        alias javi='ipython --profile=javi'
        alias notebook='ipython notebook'

This is what I did. When I want to start session with my profile, I just write ``$ javi``.

Locating profile directories
----------------------------
To locate the defaul profile directory(or any other profile directory::

    $ ipython locate profile default

Listing profiles
----------------
To see the list of profiles we have to start ipython first::

    $ ipython
    [1] !ipython profile list

Starting a profile
------------------
We can start ipython with one of the profiles of the list::

    $  ipython --profile = <name>

Magic functions
===============
``%who`` is a magic function to show which variables are in the interactive namespace.::

    In [1]: a_num = 3.14

    In [2]: a_string = 'Bob'

    In [3]: a_list = (1, 'hello', 33)

    In [4]: %who
    a_list   a_num   a_string

Prompt customization
--------------------
Here are some prompt configurations you can try out interactively by using the ``%config`` magic::

    %config PromptManager.in_template = r'{color.LightGreen}\u@\h{color.LightBlue}[{color.LightCyan}\Y1{color.LightBlue}]{color.Green}|\#> '
    %config PromptManager.in2_template = r'{color.Green}|{color.LightGreen}\D{color.Green}> '
    %config PromptManager.out_template = r'<\#> '

We can change the prompt configuration to your liking **permanently** by editing ``ipython_config.py`` in the section named **PromptManager configuration**. Since I wanted a prompt like the one of the default python shell, this is what I added there::

    # Input prompt
    c.PromptManager.in_template = '>>> '

    # Continuation prompt
    c.PromptManager.in2_template = '... '

    # Output prompt
    c.PromptManager.out_template = ''

Bookmarks
---------
To create or reset bookmarks: ``%bookmark <bookmark_name> <dir>``
To bookmark current directory: ``%bookmark <bookmark_name>``
To list bookmarks: ``%bookmarks -ls``
To change to a bookmark: ``cd -b <bookmark_name>`` but ``cd <bookmark_name>`` is enough if  there is no directory <bookmark_name>, but a bookmark with the name exists.
To delete one bookmark: ``%bookmark -d <bookmark_name>``
To remove all bookmarks: ``%bookmark -r`` - remove all bookmarks

Our bookmarks persist through IPython sessions, but they are associated with each **profile**.

Notebook
========
Starting notebook it's as easy as typing the command::

    $ ipython notebook

Automatically opened a tab in chrome with a new notebook. The proccess it's running in a shell tab.

Create a notebook by clicking the New Notebook button.

.. note:: The notebooks are saved in the directory of my project.

Change the title of the notebook by left clicking on “Untitled0″ at the top of the page.

The notebook is composed of cells. Every cell has a unique purpose depending upon its type. By default every cell is a **code** cell. But we can change that:

#. Highlight the first cell by clicking on it. 
#. From the dropdown menu, select:
    * Code
    * Markdown
    * Raw NBconvert
    * 6 levels of headings.

.. warning:: The shortcut ``Shift+Enter`` doesn't work in Google Chrome. Once I installed the extension for chrome it's working.

Shutting down IPython : ``CTRL+C`` and confirm with ``y``, or just hitting twice ``CTRL+C``.

.. _`documentation`: http://ipython.org/ipython-doc/2/config/extensions/autoreload.html?highlight=autoreload