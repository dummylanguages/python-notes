*******
Cloning
*******
If you want to get this documentation locally just **clone** the project using::

	$ git clone https://gitlab.com/dummylanguages/bash-notes.git

If for some reason, you want to get rid of the **commit history**, you can delete the git files.

Deleting the git project
------------------------
If you want to delete the existing git project just delete the hidden ``.git`` folder::

	$ rm -rf .git

To make sure there're no more git files (``.gitignore``) or directories (some branches) you may want to run::

	$ rm -rf .git*

That way you'd get rid of all things git.

