****************
Existing project
****************
If you have already started writing your documentation, or want to continue writing this one about Bash and upload it to another remote repository, follow these steps:

1. Delete the git project as we explained in the last section
2. Create a new **local** repository
3. Create a new **remote** repository
4. Add the remote to your local
5. Push

If you just want to upload the notes to your personal remote, it's not necessary to delete the git project, and create one. You just can delete all the references to the original remote repository, and add yours. This way, you get to keep the original commit history. If you want to delete it anyways, keep reading.

1. Deleting the git project
---------------------------
If you want to delete the existing git project just delete the hidden ``.git`` folder::

	$ rm -rf .git

To make sure there're no more git files (``.gitignore``) or directories (some branches) you may want to run::

	$ rm -rf .git*

That way you'd get rid of all things git.

2. Create a new local repository
--------------------------------
Just::

    $ git init
    $ git add .
    $ git commit -m "Initial commit"

3. Create a new remote repository
---------------------------------
Use the web interface of your version control provider to create a new remote repository.

4. Add the remote to your local
-------------------------------
As easy as::

    $ git remote add origin git@gitlab.com:dummylanguages/bash-notes.git

.. note:: Replace this remote with yours!

5. Push your changes
--------------------
As usual::

    $ git push -u origin master
