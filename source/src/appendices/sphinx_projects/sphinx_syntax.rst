************
Using Sphinx
************
Here we'll go over how to use Sphinx, and the reStructuredText syntax.

The ``toctree`` directive
=========================
Since reST does not have facilities to interconnect several documents, Sphinx uses a custom directive to add relations between the single files the documentation is made of, as well as tables of contents. 

.. code-block:: rst

	.. toctree::
		:maxdepth: 3
		:numbered: 2

		file1
		file2

* A numeric ``:maxdepth:`` option may be given to indicate the depth of the tree; by default, all levels are included. 
* Numbering up to a specific depth is also possible, by giving the depth as a numeric argument to ``:numbered:``.
  
Limiting the maximum depth per file
-----------------------------------
We can do that, adding some metadata to the beginning of the file we want less depth.::

	:tocdepth: 2

The maximum depth for the contents of this file in a TOC, will be 2.

RestructuredText
================

Headers
-------
Normally, there are no heading levels assigned to certain characters as the structure is determined from the succession of headings. However, for the **Python documentation**, this convention is used which you may follow:
::

	# with overline, for parts
	* with overline, for chapters
	=, for sections
	-, for subsections
	^, for subsubsections
	", for paragraphs

Citations
---------
Standard reST citations are supported, with the additional feature that they are “global”, i.e. all citations can be referenced from all files. Citation are non-numeric **labels** are :

* *Case-insensitive single words* 
* Consisting of alphanumerics(not only numbers) plus internal hyphens, underscores, and periods
* No whitespace.

.. code-block:: rest

	[LP]_					# This reference, sends us..

	.. [LP] Learning Python.	# ... here.
				- Mark Lutz.

Sidebars
--------
We can create sidebars with this markup::

	.. sidebar:: Sidebar Title
	   :subtitle: Optional Sidebar Subtitle

	   Subsequent indented lines comprise
	   the body of the sidebar, and are
	   interpreted as body elements.


Excluding a file from being generated
=====================================
When working, sometimes we want to exclude some files of being generated. We do that for example to avoid a huge list of warnings and errors for files that are not finished.
We use the following directive in ``conf.py``::

	exclude_patterns = [
		'notes/DATA/*'
	]

Here we are excluding all the files inside the ``notes/DATA`` directory.

.. warning:: 
	Do not forget to change this line when you are ready to include these files for rendering.

If we have these files included in any toctree, sphinx will cry ``WARNING: toctree contains reference to nonexisting document``, but the documents will not be generated. 

.. todo::
	I don't know how to get rid of this warning.

Commenting out lines in the toctree
-----------------------------------
I've found out that commenting out lines in the toctree is useless. 
::

	.. toctree::
			   
		intro_to_computers
		.. about_python
		arithmetic_expressions
		objects

In the example, I commented out the entry ``about_python``, but still is generated. 
Also sphinx still keeps issuing a warning ``WARNING: toctree contains reference to nonexisting document``

Avoiding warnings for files not included the toctree
====================================================
When we have a document in our source directory that we don't include in the ``toctree``, sphinx will generate it, but will issue a warning.

.. warning::
	WARNING: document isn't included in any toctree

We can add  the field-wide metadata ``:orphan:`` at the beginning of the document. The warning will go away.

.. note::
	The document will still be generated.

Linking to other Documents
==========================
In those cases when we want to link to any document on our docs, we can use the ``:ref:`` role. It has two parts:

1. The link itself, looks something like this:
::

   :ref:`Learn how to install python here <installing-python>`

An it has two parts:

* The text for the link, in this case, ``Learn how to install python here``
* The name of the label, ``<installing-python>``
	
2. The label, looks like this:
::

	.. _installing-python:
   
.. warning::
	 Label names must be unique throughout the entire documentation.

Using ``:ref:`` is advised over standard reStructuredText links to sections (like ```Section title`_``) because it works across files, even when section headings are changed, and for all builders that support cross-references.

Cute small table of contents
============================
One think I like to do in my documentation is combine the ``.. sidebar::`` and ``.. contents::`` directives.::

	.. sidebar:: Table of contents.

		.. contents:: 
			:depth: 3
			:backlinks: top
			:local:

Enabling extensions
===================
Since many projects will need special features in their documentation, Sphinx allows to add “extensions”. Some of these extensions are bundled with sphinx.

For example, `todo <http://sphinx-doc.org/ext/todo.html>`__ is one of these `builtin extensions <http://sphinx-doc.org/extensions.html?highlight=extension#builtin-sphinx-extensions>`__, but is not enabled by default. For enable it:

1. Add the name of the extension in your ``conf.py``, under ``extensions['sphinx.ext.todo']``. 
2. Also in your ``conf.py``, add the configuration value ``todo_include_todos = True``. (The default is ``False``, this way ``.. todo::`` and ``.. todolist::`` produce nothing.)

Now we can use ``.. todo::`` as we would do with ``.. note::``. Also, ``.. todolist::`` adds a list of all the ``todo`` in our documentation.

The styling of every ``.. todo::`` item will depend on the support given by our theme.

Modifying a theme
=================
It's a 3 steps process:

1. Create a document for your styles:

	a. In the same directory where my ``conf.py`` lives, most of the times ``source``, I created a folder for my custom static files (stylesheets, javascripts). I called it ``custom``. 
	b. Inside it I created a subfolder for my stylesheets: ``source/custom/css``. 
	c. In this subfolder I'm gonna create my custom styles: ``source/custom/css/my_theme``.

2. Now we have to tell sphinx to spit this document inside ``build/_static/css``, the same directory where is the stylesheet included in the **Read The Documents** theme. We do that adding the following line to ``conf.py``:
::

	html_static_path = ['custom']		# Directory for static files.
	
Done. Now, if we build, we will have the **RTD** styles (``theme.css``), and our custom ``my_theme.css`` in the same directory, ``build/_static/css``.

3. Now we are gonna tell sphinx to use our custom ``my_theme.css``, instead of the **RTD** one. We do that adding this line in ``conf.py``:
::
 
	html_style = 'css/my_theme.css'		# Choosing my custom theme.

In our custom stylesheet, the first line should import the styles of ``theme.css`` with ``@import url("theme.css");``.

And we are ready to start overwriting styles.

----

UPDATE: THERE IS AN EVEN SIMPLER WAY.

1. Put your customizations inside ``source/_static/css/my_theme.css``. In our custom stylesheet, the first line should import the styles of ``theme.css`` with ``@import url("theme.css");``.
   
2. Add the following line in ``conf.py``::

	html_style = 'css/my_theme.css'		# Choosing my custom theme.