************
Gitlab pages
************
`Gitlab Pages`_ is a feature that allows you to publish **static websites** directly from a repository in GitLab. To use GitLab Pages, first you need to create a `project`_ in GitLab to upload your website’s files: HTML, CSS, and JavaScript. GitLab will always deploy your website from a very specific folder called ``public`` in your `repository`_.

.. note:: A **repository** is part of a **project**, which has a lot of other features.

How it works
============
Although we can use just a bunch of HTML, CSS, and JavaScript files to create our website, it pays off to use a Static Site Generator to create our website. GitLab offers the possibility of automatically generating the website for us, so every time we push our changes to the source files, we trigger a fresh build of the site. This is done using a built-in technology called `GitLab CI/CD`_.

.. note:: `CI`_ stands for Continuous Integration, whereas `CD`_ for Continuous Deployment

First thing: a project
======================
The first step, it's, obviously, to `sign in`_ to your **GitLab account**, and `create a project`_. Once we have a project, we have to create a configuration file named ``gitlab-ci.yml``, placed in the root of our repo.

How to write the configuration?
-------------------------------
To write the CI/CD configuration, aka the ``gitlab-ci.yml`` file, we have a couple of choices:

1. Use a `bundled template`_: When we click on the green and juicy **New project** button, we'll be taken to a new page with several tabs. Next to the **Blank Project** tab there's another one named **Create from template**. There we can select among 17 built-in templates, but unfortunately Sphinx wasn't in the list.

2. The other option is to fork one of the example projects from the `example pages`_. Fortunately, at the time of writing this, there was a `Sphinx example`_ there. Since I already had my documentation started, instead of forking that site, I decided to just copy the `.gitlab-ci.yml`_ file. It looked something like this::

    image: alpine

    pages:
      script:
      - apk --no-cache add py2-pip python-dev
      - pip install sphinx
      - apk --no-cache add make
      - make html
      - mv _build/html/ public/
      artifacts:
        paths:
        - public
      only:
      - master

.. note:: Be sure to check the **examples page** from time to time, they usually update the ``.gitlab-ci.yml`` with new cool tweaks.

We have two options to add the configuration file to our project:

1. We can add it on the **remote repository**: On the **main page** of our GitLab repo, right above the file list there's a button named **Set up CI/CD**, if we click it we can create the configuration. I just had to paste the file above in there, with some small modifications.

2. We can add it on our **local repository**: Just create the ``.gitlab-ci.yml`` locally in your computer and push it when your are finished. I took this second approach.

Modifications
^^^^^^^^^^^^^
First I told it to install the `sphinx_rtd_theme`_:

    pages:
      script:
        [...]
        - pip install sphinx sphinx_rtd_theme
        [...]

Then, I adjusted the **build directory** to match the value of ``BUILDDIR`` in my Sphinx ``Makefile``::

    pages:
      script:
        [...]
        - mv build/html/ public/
        [...]

.. note:: The page `Creating and Tweaking GitLab CI/CD for GitLab Pages`_ is an excellent resource.

Trigger a build
---------------
If we visit the `GitLab Docs`_ page and scroll down to where it says `Get started with GitLab CI/CD`_:

    Starting from **version 8.0**, GitLab Continuous Integration (CI) is fully integrated into GitLab itself and is **enabled by default** on all projects.

That means that every time we push changes to our remote GitLab repository, a new build is triggered. We can even check our build running if we go to **Pipelines** (in project settings, under **CI/CD**) It may be useful for debugging purposes.

.. note:: If you are on the **Pipelines** page of your project, you can also manually trigger a build by clicking on the **Run Pipeline** green button (You can get to the **Pipelines** page clicking on the **CI/CD** link on the sidebar of your project).

To check our GitLab page we can point our browser to:

    https://username.gitlab.io/project-name/

And we should see our page. Cool.

Enable shared Runners (not needed anymore)
------------------------------------------
Before **version 8.0** we had to configure our GitLab project to enable `Shared Runners`_, so that each commit or push triggers our CI pipeline. We can enable it in Project (sidebar to the left), on Settings under CI/CD, and green button **Enable shared Runners**.

.. warning: About the following info: I didn't do any of the following, and the site seems to be working fine...

Baseurl
-------
According to the Gitlab docs, we also have to configure the baseurl in our configuration file. The baseurl is the URL which points to the root of the HTML documentation, in the case of this project should be https://dummylanguages.gitlab.io/. I guess that's because if we have internal links between pages in our documentation, we want to make sure that they start with the correct baseurl, otherwise, the links will not work.
Truth is, I checked ine my conf.py and didn't find this setting, but everything seemed to be working fine...

User or group website
---------------------
We have to rename our repository to ``namespace.gitlab.io/``, where namespace equals to:

    username
    group name

In this case my username is ``dummylanguages``. We can do that in project settings under General and Advanced; make sure the the Path textbox reads: ``dummylanguages.gitlab.io``.

Also, the project should be named something like ``unixlife.gitlab.io``, but in my case creating is just as ``unixlife`` was enough.


.. _`GitLab pages`: https://docs.gitlab.com/ee/user/project/pages/
.. _`project`: https://docs.gitlab.com/ee/user/project/index.html
.. _`repository`: https://docs.gitlab.com/ee/user/project/repository/index.html
.. _`GitLab Docs`: https://docs.gitlab.com/ee/README.html
.. _`Get started with GitLab CI/CD`: https://docs.gitlab.com/ee/ci/quick_start/README.html
.. _`GitLab CI/CD`: https://docs.gitlab.com/ee/ci/README.html
.. _`CI`: https://en.wikipedia.org/wiki/Continuous_integration
.. _`CD`: https://en.wikipedia.org/wiki/Continuous_delivery
.. _`sign in`: https://gitlab.com/users/sign_in
.. _`create a project`: https://docs.gitlab.com/ee/gitlab-basics/create-project.html
.. _`bundled template`: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_bundled_template.html
.. _`Fork`: https://docs.gitlab.com/ee/user/project/pages/getting_started/fork_sample_project.html
.. _`example pages`: https://gitlab.com/pages
.. _`Sphinx example`: https://gitlab.com/pages/sphinx
.. _`.gitlab-ci.yml`: https://gitlab.com/pages/sphinx/-/blob/master/.gitlab-ci.yml
.. _`sphinx_rtd_theme`: https://sphinx-rtd-theme.readthedocs.io/en/stable/
.. _`Creating and Tweaking GitLab CI/CD for GitLab Pages`: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html
.. _`CI quick start`: https://gitlab.com/help/ci/quick_start/README
.. _`Shared Runners`: https://gitlab.com/help/ci/quick_start/README#shared-runners