.. _`sphinx tldr`:

*****
TL;DR
*****
If you want to create a nice setup to generate documentation with Sphinx, keep reading.

Install Python 3
================
This time, I used the official **graphical installer** for macOS. Once the installation is finished, we'll find that the installer has done 2 things:

* Created a copy of our ``~/.bash_profile`` named  ``~/.bash_profile.pysave``.
* Added the following lines to our ``~/.bash_profile``::

    # Setting PATH for Python 3.7
    # The original version is saved in .bash_profile.pysave
    PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
    export PATH

The purpose of that line it's to put ``python3`` in our ``$PATH`` so we can call it from our command line. But there is no need for that, since the installer creates symbolic links in ``/usr/local/bin``, and that directory should be already in our ``$PATH``. So we can safely **remove** the lines above.

.. warning:: Remove those lines from your ``~/.bash_profile``.

Install packages for creating virtual environments
==================================================
We need to install a couple of packages for **Python 3** using the ``pip3`` command::

    $ pip3 install virtualenv virtualenvwrapper

The official docs for `virtualenvwrapper`_ requires us to add 3 lines to our **shell configuration**, in macOS that would be the ``~/.bash_profile`` file::

    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/code
    source /usr/local/bin/virtualenvwrapper.sh

There's no need to add the first one, since by default that's the location for our **virtual environments**, it doesn't make any harm either to leave that line there. 

But we have to add an `additional line`_, to tell **virtualenvwrapper** where is Python installed::

    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3

After editing it, we have to reload our shell configuration::

    $ source ~/.bash_profile

Create a virtual environment for Sphinx
=======================================
Here we have 2 scenarios:

1. We have cloned this repo
2. We are starting a new Sphinx documentation project

We have cloned this repo
------------------------
If we have::

    $ git clone https://gitlab.com/dummylanguages/unixlife.git

Then we have to create a virtual environment and link it to the folder of the repo. Easily done using a virtualenvwrapper command::

    $ mkvirtualenv unixlifeenv -a ~/code/notes/unixlife/

We are starting a new Sphinx documentation project
--------------------------------------------------
Another virtualenvwrapper command::

    $ mkproject my_new_docs

It will create a folder for your project at ``PROJECT_HOME/my_new_docs``, and your new environment(``my_new_docs``) will be automatically activated.

Installing Sphinx and Readthedocs theme
=======================================
With our virtual environment enabled (``$ workon unixlifeenv``) we'll install a couple of Python packages **only for that environment** using::

    $ pip install sphinx sphinx_rtd_theme

Generating a Sphinx documentation project
=========================================
It's easy, just make sure you have activated the virtual environment, and use the Sphinx command ``sphinx-quickstart`` to generate a project::

    $ sphinx-quickstart

Once the empty project has been generated, you gotta change a setting in the project configuration settings, which is a file named ``conf.py``, which you can find at the root of the generated project. Just make sure the ``html_theme`` is setup properly::

    html_theme = "sphinx_rtd_theme"


Now you can add a couple of files and generate the documentation to see if everything works fine. Just go to the root of your project, where the ``Makefile`` is and run::
    
    $ make html

Open the generated ``index.html`` file locally in your browser.
