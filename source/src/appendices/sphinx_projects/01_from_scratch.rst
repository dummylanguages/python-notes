*******************************
Creating a project from scratch
*******************************
Assuming that you have installed Python in your system, there are a couple of ways of working with `Sphinx`_:

1. The simplest one would be just to install it for the whole system using ``pip3``.
2. The second one would be using **virtual environments**, my favourite approach for a long time. 
3. Nowadays, it's quite convenient to open a folder in a **Docker container** using VSCode.

We're not gonna explain the first approach since it will become obvious after you learn about the other two, and also because we don't use it; it's not sustainable in those cases where you need several versions of the same package.

Using virtual environments
==========================
Once you have created and activated your virtual environment, you're gonna need to use several packages:

1. Install **Sphinx** itself.
2. Install a nice theme.

.. note:: You can read our section about :ref:`virtual environments`_, where we explain the different ways to create them.

We can fullfill both steps running::

    $ pip install sphinx sphinx_rtd_theme

You may have notice that we didn't use the **user scheme**, that's because we're using a **virtual environment**.

Using the VSCode Remote Development extension
=============================================
If you use **Visual Studio Code**, you can install the **Remote Development** extension, which allows you to browse and edit files inside a **Docker container**. Once the extension is installed there are a few steps:

1. Click on the **Open a Remote Window** (lower left corner). That will open a menu, where you can select ``Remote Containers: Open folder in container``. That will open another **VS Code** window, within a running container. The first time we run it, the container will be created; we can choose one from a list.
2. After the first time, we'll choose ``Remote Containers: Attach to running containers`` to connect to a preexisting container.
3. Once we finish working in the container, we'll click on the lower left icon and choose ``Close Remote Connection``.

That's the basic workflow. Once the containter is running, we can install ``sphinx`` and the ``sphinx_rtd_theme``, and build the static site from the container. The generated site will be available in our **host**, as if we were building locally.

Generating a Sphinx documentation project
=========================================
This step is easy, just make sure you have activated the virtual environment, and use the Sphinx command ``sphinx-quickstart`` to generate a **blank project**::

	$ sphinx-quickstart

Enabling the theme
------------------
Once the empty project has been generated, you gotta change a setting in the project configuration settings, which is a file named ``conf.py``, which you can find at the root of the generated project. Just make sure the ``html_theme`` is setup properly::

    html_theme = "sphinx_rtd_theme"

Now you can add a couple of files and generate the documentation to see if everything works fine. Just go to the root of your project, where the ``Makefile`` is and run::
    
    $ make html

Open the generated ``index.html`` file locally in your browser.

Source control
==============
Needless to say, we'll want to back up our documentation efforts under source control.

Create a git project
--------------------
Navigate to the root of the documentation project and run::

	$ git init

It's a good idea to add a ``.gitignore``::

	$ touch .gitignore
	
To ignore some files you don't want in your remote repository, for example I added the lines::

	.vscode
    .devcontainer

These two **hidden folders** are created by VSCode when it creates the containers.

.. note:: A cool thing about the VSCode workflow is that the terminal window we can run our ``git`` commands as we'd do in the host, with exception of **pushing changes** to the remote, which makes sense since the container doesn't have access rights. (Something that could be changed generating **ssh keys** and  adding them in the remote)

Now create a remote repository using your browser, and add the remote to your local repository with the command::

	$ git remote add origin https://gitlab.com/username/foo.git

.. _`Sphinx`: https://www.sphinx-doc.org/en/master/#
