##################################
The Sphinx Documentation generator
##################################
.. toctree::
	:maxdepth: 3
	:numbered: 3
	:hidden:

	01_from_scratch
	02_clone
	03_existing_project
	04_gitlab_pages
	sphinx_syntax
	TLDR

`Sphinx`_ is a documentation generator written and used by the Python community. It is written in Python, but also used in other environments. Here we're gonna discuss how to create a set up to generate documentation projects using `Sphinx`_.

Needless to say, since Sphinx is written in Python, in order to use it you need to have the **Python interpreter** installed in your system. Check the appendix about :ref:`installing Python` if you need help with that.

.. warning:: If you're in a hurry check the :ref:`TL;DR section <sphinx tldr>`

.. _`Sphinx`: https://www.sphinx-doc.org/en/master/