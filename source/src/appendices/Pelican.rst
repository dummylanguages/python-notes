*******
Pelican
*******

Installing Pelican
==================
Let's start by creating a virtual environment for installing Pelican in it. 
We are gonna use **virtualenvwrapper**, for creating the environment:

.. note:: If you don't know what **virtualenvwrapper** is, check :ref:`this <virtualenvwrapper-label>`.

	$ mkproject my_blog

If everything goes ok, you should see something like::

	New python executable in my_blog/bin/python2.7
	Also creating executable in my_blog/bin/python
	Installing setuptools, pip...done.
	Creating /Users/javi/Python_projects/my_blog
	Setting project for my_blog to /Users/javi/Python_projects/my_blog

After that, we are automatically taken to our project directory, and our environment is active. Your command line starts with the name of your virtual environment inside parenthesis, in our case something like::

	(my_blog)MacBookAir:my_blog javi$

It's time. We are ready for installing the **Pelican** package in our new environment::

	$ pip install pelican

I'm gonna install **markdown** too:

	$ pip install markdown

.. note:: Remember, thanks to virtualenv and virtualenvwrapper, both packages, pelican and markdown, are installed in a separate folder: the directory of the virtual environment.

You can always check by yourself all the packages you install in your virtual environment by doing::

	$ cdvirtualenvironment

This command will take you to `$WORKON_HOME/my_blog`

Creating our blog
=================
We have all we need to start creating our blog. With our virtual environment active, we are gonna create a skeleton for our blog with the command::

	$ pelican-quickstart

After running this command we are gonna be asked a list of questions. We don't have to worry so much about the answers, because we can change all the configuration later.

This command creates the following folder structure under our **project** directory::

	my_blog/
	├── content             # Here we put the files we want to generate our website from
	│   └── (pages)
	├── output              # This is where the website will be generated
	├── develop_server.sh
	├── fabfile.py
	├── Makefile
	├── pelicanconf.py       # Main settings file
	└── publishconf.py       # Settings to use when ready to publish

You can check this skeleton with the command::

	$ cdproject

This command will take you to `$PROJECT_HOME/my_blog`

Writing content
===============
In order to start generating our website, we need some content to put in. Let's create a markdown file in the proper folder::

$ cdproject
$ cd content
$ touch my_first_post.md

Now, grab your favourite text editor and start writing some markdown in it. 

.. tip:: 
	If you don't know how to locate the markdown file you have just created, remember that you can get the path doing::

		$ cdproject
		$ cd content
		$ pwd

At the top of your markdown file, you must include a special header that at least includes the title.

Generating content
==================
Once we have some content, we can start generating HTML with the `pelican` command, specifying the path to our content and (optionally) the path to your settings file::

	$ cdproject
	$ pelican content

The above command will generate your site and save it in the `output/` folder, using the default theme to produce a simple site. 
We can also tell Pelican to watch for modifications in our content, instead of manually re-running it every time you want to see your changes. To enable this, run the pelican command with the `-r` or `--autoreload` option.
Python offers the possibility of running a simple webserver at http://localhost:8000/. We just have to switch to the `output` directory, and from there use::

	$ python -m SimpleHTTPServer

When we finish, stopping the server is easy with ``CTRL-C``.

Using the make command
======================
A Makefile is also automatically created for you when you say “yes” to the relevant question during the pelican-quickstart process. 
If you want to use make to generate your site, run::

	$ make html

If you’d prefer to have Pelican automatically regenerate your site every time a change is detected (which is handy when testing locally), use the following command instead::

	$ make regenerate

To serve the generated site so it can be previewed in your browser at http://localhost:8000/:

	$ make serve

Using make, we don't even need to be in the ``output`` directory, to run our webserver.

Normally you would need to run make regenerate and make serve in two separate terminal sessions, but you can run both at once via::

	$ make devserver

The above command will simultaneously run Pelican in regeneration mode as well as serve the output at ``http://localhost:8000``. Once you are done testing your changes, you should stop the development server via::

	$ ./develop_server.sh stop

Themes
======
By default, pelican generates our blog using a theme named **notmyidea**, where is this theme located? For finding out this and do a lot more of stuff related with themes, it's available the command `pelican-themes`.
For example, to find the path to the installed themes::

	$ pelican-themes --path
	/Users/javi/.virtualenvs/my_blog/lib/python2.7/site-packages/pelican/themes

In my case, since I am using virtual environments created with virtualenv and virtualenvwrapper, the path to the themes is inside the virtual environment I created for pelican. 

If we want to list all the themes we have, and where are they installed, we can add two options::

	$ pelican-themes --list --verbose
	/Users/javi/.virtualenvs/pelican/lib/python2.7/site-packages/pelican/themes/notmyidea
	/Users/javi/.virtualenvs/pelican/lib/python2.7/site-packages/pelican/themes/simple

Installing new themes
=====================
If we don't want to use any of the default themes (notmyidea and simple) we can install new themes with the command ``pelican-themes --install /path/to/theme``
For example, imagine we have downloaded or cloned a new theme to our desktop folder, we would install it doing::

	$ pelican-themes --install /Users/javi/Desktop/pelican-bootstrap3

The above line, will install the theme to the directory where pelican keeps all the themes. Now, if we list the themes::

	$ pelican-themes --list
	notmyidea
	pelican-bootstrap3      <== Our new installed theme
	simple

We can see the new theme on the list, right in the middle of the 2 preinstalled themes.

Using a theme
=============
To make use of a installed theme, we just have to include in our configuration file the setting ``THEME``. For example, we could add to our ``pelicanconf.py`` the following line::

	THEME = pelican-bootstrap3

The ``THEME`` setting can be a relative or absolute path to a theme folder, or the name of a default theme or a theme installed via pelican-themes.

