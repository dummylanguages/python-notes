:orphan:

ASCII
=====
was introduced in 1963 and is a 7-bit encoding scheme used to encode letters, numerals, symbols, and device control codes as fixed-length codes using integers.
ASCII was the most common character encoding on the World Wide Web until December 2007, when it was surpassed by UTF-8, which includes ASCII as a subset.

ASCII reserves the first 32 codes (numbers 0–31 decimal) for control characters: codes originally intended not to represent printable information, but rather to control devices (for example, character 10 represents the "line feed" function (which causes a printer to advance its paper)




EBCDIC
======
IBM's Extended Binary Coded Decimal Interchange Code (usually abbreviated EBCDIC) is an 8-bit encoding scheme developed in 1963.

Unicode
=======
Early computers were largely oriented toward numerical computation.
Computers were originally built to process numbers. Over the last few decades, they've become increasingly better at handling text as well. 


character numbers are usually specified in hexadecimal notation

Python
======
In Python 3.X, the normal ``str`` string handles **Unicode** text (remember that ASCII is included in Unicode); a distinct ``bytes`` string type represents raw byte values (for media and other text encodings); 