Docutils
========

Installing Docutils
-------------------
Create a virtual environment::

	$ mkvirtualenv --python=/usr/local/bin/python docgen

Creating a folder for the project and associating it with the environment::

	$ cd python_projects
	$ mkdir docs
	$ setvirtualenvproject docgen docs

Enabling the environment, and installing **doctutils** in it::

	$ workon docgen
	$ pip install doctutils

Done!

Tools
-----
Docutils includes a lot of tools for converting our reStructured text files to different formats.
Each front-end tool supports command-line options for one-off customization.

rst2html.py
^^^^^^^^^^^
The ``rst2html.py`` front end reads standalone **reStructuredText** source files and produces HTML 4 (XHTML 1) output compatible with modern browsers that support cascading stylesheets (CSS). A **stylesheet** is required for proper rendering; a simple but complete stylesheet is installed and used by **default**.

For example, to process a reStructuredText file "test.txt" into HTML::

	$ rst2html.py test.txt test.html

.. warning:: Do not forget to write the full name of the command, ``rst2html.py``. If we write just ``rst2html``, our shell will throw an error: ``-bash: rst2html: command not found``.

Now open the "test.html" file in your favorite browser to see the results.

Default stylesheet
""""""""""""""""""
This **default** stylesheet is called ``html4css1.css`` and can be usually found in the ``writers/html4css1/`` directory of the Docutils installation.

To check the exact machine-specific location, we can check the general help::

	$ rst2html.py --help

And look for the ``--stylesheet-dirs`` section. In my case, the stylesheet is in the

``~/.virtualenvs/docgen/lib/python2.7/site-packages/docutils/writers/html4css1.css``

Using a customized stylesheet
"""""""""""""""""""""""""""""
To customize the stylesheet,

1. Copy ``html4css1.css`` to the same place as your output HTML files will go.
2. Place the file where you are gonna add your custom styles, in the same directory and use the following template::

	/*
	:Author: Your Name
	:Contact: Your Email Address
	:Copyright: This stylesheet has been placed in the public domain.

	Stylesheet for use with Docutils.  [Optionally place a more
	detailed description here.]
	*/

	@import url(html4css1.css);

	/* Your customizations go here.  For example: */

	h1, h2, h3, h4, h5, h6, p.topic-title {font-family: sans-serif }

Make sure that you import ``html4css1.css``. It is important that you do not edit a copy of ``html4css1.css`` directly because html4css1.css is frequently updated with each new release of Docutils.

* ``--stylesheet`` (for a URL)
* ``--stylesheet-path`` " (for a local file)

For example::

	$ rst2html.py --stylesheet="my_styles.css"

Embedded stylesheet
"""""""""""""""""""
By default, the stylesheet will be embedded in the html file. We can change that using the option ``--link-stylesheet``. The css file will be generated

Configuration
"""""""""""""
``docgen/lib/python2.7/site-packages/doctutils/doctutils.conf``
