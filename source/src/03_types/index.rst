*****
Types
*****

.. toctree::
	:maxdepth: 3

	core_types
	objects
	numeric_types
	boolean_type

`Information Technology`_ (IT) is the use of computers to store, retrieve, transmit, and manipulate data. 
Computers store information as digital data, which is data that is represented using the **binary number system**, as opposed to analog representation. 

That means that computers represent data internally using ones (1) and zeros (0), so we need **data types** to interpret the information. For example the byte ``0b100 0001`` could be interpreted 2 ways:

    * As the character ``A``
    * As the integer ``65``

For example, let's see the byte under ``65``::

	>>> bin(65)
	'0b1000001'

And now the byte under ``A``::

	>>> ord('A')
	65
	>>> bin(ord('A'))
	'0b1000001'

Internally, the computer uses the same sequence of ones and zeroes to represent both the **integer** ``65`` and the **character** ``A``::

	>>> bin(ord('A')) == bin(65)
	True

The **type** of the value determines the set of **supported operations** (functions and methods) for that value. For example, strings support the ``len()`` function: 
::

	>>> a = 'hello'
	>>> len(a)
	5

But we couldn't use ``len()`` with numbers, they don't support that operation::

	>>> len(6)
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	TypeError: object of type 'int' has no len()


.. _`Information Technology`: https://en.wikipedia.org/wiki/Information_technology