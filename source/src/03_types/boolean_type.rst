Boolean type
============
Python has a boolean data type called ``bool``, with the only two possible values of ``True`` and ``False``. We can check this type doing::

	>>> type(True)
	<class 'bool'>
	>>> type(False)
	<class 'bool'>

Some people think of ``bool`` as a numeric type, since internally, it is a subclass of the ``int`` type, something we can check doing::

	>>> isinstance(True, int)
	True

The thing is that ``True`` and ``False`` are just customized versions of the int values ``1`` and ``0`` respectively. They behave like ``1`` and ``0``, but they are printed like ``True`` and ``False``. For example::

	>>> True + True
	2
	>>> 5 * (True + True)
	10

We can think of ``True`` and ``False`` as though they are predefined variables set to integers 1 and 0. Knowing this, we could program an infinite loop writing: ``while 1:`` what is the same that writing ``while True:``. But anyways, the ``bool`` type makes logic more explicit than using the less obvious integers ``1`` and ``0``.

Set of operations
-----------------
As we know, a type is a set of values as well as the set of operations available for those values. Since **boolean operations** are used more than mostly inside tests, we have moved the study of **boolean operators and expressions** to the :ref:`tests section <conditions>`. 