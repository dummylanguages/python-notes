.. _objects:

Data types. Types of objects
============================
In computer programming we are gonna use diferent types of data (string, numbers...) inside our programs. For every type of value, we have available a different set of operations. So a **data type** (or just **type**), is simply a set of values, and the set of available operations to process these values. 

We also mentioned before that in Python, every piece of data is an object. A string is an object, a number is an object, and so on. Every value is an object, and the **type** of the object is determined by the type of value. Let's use the interactive mode to demonstrate this::

	>>> a = 'hello'
	>>> type(a)
	<class 'str'>
	
* In line **1** we create a string variable named ``a`` by assigning it the value ``'hello'``.
* In line **2**, we use the funcion ``type()`` to check its type. 
* Line **3** contains the output of this function. ``<class 'str'>``, is telling us that the ``type`` (class) of the object ``a``, is ``str`` (string).

.. note::	
	In python 3.x, **type** and **class** are interchangeable concepts. 

Nonetheless, some programmers still prefer to use the term **type** for python's **built-in object types**, and the term **class** for those **custom objects** created using python's **class statement**.

.. note::
	Python's **built-in object types** are also known as **core types**

Some things in Python have more than one name. For example, ``a`` is an object of the type ``str``, we can also say, that ``a`` is an **instance** of the class ``str``. 

.. note::	
	Instance is synonymous to object.

Other program units such as **functions**, **modules**, and even **classes** (custom types) are objects too.

..  note::
	In python, everything is an object.

Why do we need types?
---------------------
The need for data types results from the fact that the same internal representation of data can be interpreted in various ways, for example the byte ``0100 0001`` could be interpreted 2 ways:

* As a character, ``A`` 
* As an integer, ``65``

.. todo:: 
	Relate the above with the functions ``ord()`` and ``chr()`` and ``bin()``

We need some way of indicate how some piece of data should be interpreted. 

.. todo:: 
	Relate the above with ``True`` and ``1``. ``True + 1 == 2`` but ``True is 1 == False``. And also ``type(True) != type(1)``.

We also said that t

.. note:: 
	Data types also prevent the programmer from using values inappropriately. For example::

		>>> a = 'hello'
		>>> b = 'world'
		>>> a / b
		Traceback (most recent call last):
		  File "<stdin>", line 1, in <module>
		TypeError: unsupported operand type(s) for /: 'str' and 'str'

	We can not use the ``/`` operator with strings.

Static vs. Dynamic typing
-------------------------
There are two approaches to data typing in programming languages:

1. In programming languages with **static typing** (like Java), a variable is declared as a certain type before it is used, and during all of its life can only be assigned values of that type. 
2. In languages with **dynamic typing** (like Python), the data type of a variable depends only on the type of value that the variable is currently holding. Thus, the same variable may be assigned values of different type during the execution of a program.

.. note::
	Python, uses **dynamic typing**.

For example:

.. code-block:: python
	:linenos:
	:emphasize-lines: 1,4

	>>> a = 'hello'
	>>> type(a)
	<class 'str'>
	>>> a = 3
	>>> type(a)
	<class 'int'>

In line **1** we assign ``a`` the string ``'hello'``. And in line **4** we assign ``a`` the integer ``3``. No problem, but, during the time ``a`` is a string, we only have available string operations (``len()``) and while is an integer, only the operations that the **int** type supports. That's dynamic typing.

Mixed type expressions
----------------------
A mixed-type expression is an expression containing operands of different type. The CPU can only perform operations on values with the same type. That means that in a mixed-type expressions, operands  must be converted to a common type. We have to different ways of convert operands to a different type:

Coercion
^^^^^^^^
For example, in the mixed expression:
::

	>>> 2 + 3.3 
	5.3

The **CPU** can not operate on an ``int`` and a ``float`` at the same time. Python goes ahead and automatically converts the integer 2, to the floating point number 2.0. It does it because it's **safe**, no information is lost during the proccess. 

Conversion
^^^^^^^^^^
In the above mixed-expression, it would have also be possible to convert the **float** ``3.3`` to an **int**, ``3``, but then we would have lost information. Python is smart enough to not doing that, it's **not safe**. But if in a particular situation we are ok with that, we can do an **explicit conversion** of 3.3 to an integer using the ``int()`` function:
::

	>>> 2 + int(3.3)
	5

The subexpression ``int(3.3)`` returns the **int** 3. The decimal part *.3*, is truncated.

Objects live in memory
----------------------
Objects are essentially just values taking their space in the computer memory. That position in memory is indicated by a number. The ``id()`` built-in funcion returns the object's position in memory:
::

	>>> a = 'hello'
	>>> id(a) 		
	4539924752

Python, the big picture
-----------------------
It could be useful to look at the python conceptual hierarchy to see exactly where the concept of object fits in. Let's start at the top, with the program:

1. Programs are composed of modules.
2. Modules contain statements.
3. Statements contain expressions.
4. Expressions create and process objects.

Bottom line, in Python, objects are the most fundamental notion.
:ref:`From pg. 207<computer-science>`
