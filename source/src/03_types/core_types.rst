Core Types
==========
The following table contains the `standard types`_ that are built into the Python interpreter.

+------------------------+-------------------------------------+
|      Object type       |        Some literal examples        |
+========================+=====================================+
| Numbers                | Integers, floating point values...  |
+------------------------+-------------------------------------+
| Strings                | ``'Hello world'``                   |
+------------------------+-------------------------------------+
| Lists                  | ``[1, 4, 'bob']``                   |
+------------------------+-------------------------------------+
| Dictionaries           | ``{name: 'John', age: 45}``         |
+------------------------+-------------------------------------+
| Tuples                 | ``(2, 4, 'Jane')``                  |
+------------------------+-------------------------------------+
| Files                  | ``open('notes.txt')``               |
+------------------------+-------------------------------------+
| Sets                   | ``set('abc')``, ``{'a', 'b', 'c'}`` |
+------------------------+-------------------------------------+
| Other core types       | Booleans, types, ``none``           |
+------------------------+-------------------------------------+
| Program units          | Functions, modules, classes         |
+------------------------+-------------------------------------+
| Implementation-related | Compiled code, stack tracebaks      |
+------------------------+-------------------------------------+

.. _`standard types`: https://docs.python.org/3/library/stdtypes.html