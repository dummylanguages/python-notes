Numeric types
=============
In python there are two basic types of numeric data: **integers** and **floating point** numbers.

Integer type
------------
Integer numbers include 0, all of the positive whole numbers, and all of the negative whole numbers. In python 3.X, the **int** type supports unlimited precission, they can grow to have as many digits as our memory space allows. So the set of values for the **int** type is zero and all the whole numbers our computer's memory can handle.

Integers may be coded in:

* **Decimal**, using the digits 0-9.

* **Hexadecimal**:
	* Start with a leading ``0x`` or ``0X``.
	* Followed by a string of hexadecimal digits (0–9 and A–F). Hex digits may be coded in lower or uppercase. 

* **Octal**:
	* Start with a leading ``0o`` or ``0O`` (zero and lower or uppercase letter o)
	* Followed by a string of digits (0–7)

* **Binary**:
	* Begin with a leading ``0b`` or ``0B``
	* Followed by binary digits (0–1).
  
All of these literals produce integer objects, they are just alternative syntaxes. In all of these literals a sign character (``+`` or ``-``) may be used. But since numeric literals without a provided sign character denote positive values, an explicit positive sign character is rarely used.

.. warning:: 
	If you write a number with a decimal point or exponent, Python makes it a **float**, even when the fractionary part is ``0``.::

		>>> type(3.0)
		<class 'float'>

.. warning::
	Commas are never used in numeric literals. Do **not** write ``1,500`` or ``35,457``

Floating point type
-------------------
A **real number** in mathematics, such as the value of **π** (3.1416...), consists of a whole number, a decimal point, and a fractional part. In python, real numbers are represented as floating-point numbers, also known as just **floats**.

In mathematics, real numbers have an infinite range and infinite precision, which means that the digits in the fractional part can continue forever. However, because a computer’s memory is not infinitely large, **float** values, have both a limited range and a limited precision. 

Python uses a double-precision standard format (IEEE 754) providing a range of -10 :sup:`308` to 10 :sup:`308` with 16 to 17 digits of precision. A floating-point number can be written using either ordinary **decimal notation** or **scientific notation**. Scientific notation is often useful for mentioning very large numbers. 
The following table shows some equivalent values in both notations:

+------------------+---------------------+---------------------+
| Decimal notation | Scientific notation | Meaning             |
+==================+=====================+=====================+
| 3.78             | 3.78e0              | 3.78 x 10 :sup:`0`  |
+------------------+---------------------+---------------------+
| 37.8             | 3.78e1              | 3.78 x 10 :sup:`1`  |
+------------------+---------------------+---------------------+
| 3780             | 3.78e3              | 3.78 x 10 :sup:`3`  |
+------------------+---------------------+---------------------+
| 0.378            | 3.78e-1             | 3.78 x 10 :sup:`-1` |
+------------------+---------------------+---------------------+
| 0.00378          | 3.78e-3             | 3.78 x 10 :sup:`-3` |
+------------------+---------------------+---------------------+

It is important to understand the limitations of floating-point representation. For example, the mul- tiplication of two values may result in **arithmetic overflow**, a condition that occurs when a calculated result is too large in magnitude (size) to be represented::

	>>> 1.5e200 * 2.0e210
	inf

The arithmetically correct result is 3.0e410, but the special value ``inf`` indicates that arithmetic overflow has ocurred.

Similarly, the division of two numbers may result in **arithmetic underflow**, a condition that occurs when a calculated result is too small in magnitude to be represented::

	>>> 1.0e2300 / 1.0e100
	0.0

This results in 0.0 rather than the arithmetically correct result 1.0e2400, indicating that arithmetic underflow has occurred.

Limits in representation of floats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
While arithmetic overflow and underflow are relatively easy to detect, the loss of precisition in some results, it's a bit more difficult to detect. For example::

	>>> 1 / 3
	0.3333333333333333

``1 / 3`` is equal to ``0.33333333...`` with an infinite numbers of decimal places, but the value stored in a **float** can only contain an aproximation of the true value, only up to the 16 :sup:th digit.

Another example::

	>>> 3 * (1 / 3)
	1.0

Here we would expect the result to be ``.9999999999999999``, but instead python displays a **rounded** result to keep the number of digits displayed manageable.

For every day applications, this loss of precission is not a problem, however in scientific computing and other precise applications where accurate precission is essential, the programmer must be aware of it.

Built-in ``format()`` function
------------------------------
This function may be used to produce a numeric string version of the value, with a specified number of decimal places. For example::

	>>> format(0.9812354987 , '.2f')
	'0.98'

We can even add a comma in the format specifier::

	>>> format(13402.25345, ',.2f') 
	13,402.25

We can use this same function for formating strings.

Arithmetic expressions
----------------------
A type is not only a set of values, but also a set of operations available for that type. In python, with all the numbers and their available operators, we can do arithmetic operations. The expressions that use numbers and arithmetic operators are called **arithmetic expressions**.

Operands and operators
^^^^^^^^^^^^^^^^^^^^^^
1. An **operator** is a symbol that represents an operation that may be performed on one or more **operands**. 
2. An **operand** is a value that a given operator is applied to. 

For example, in the expression: ``2 + 3``, the operands are 2 and 3, and the operator is the `+` symbol. 

Depending on how many operands we have:

1. **Binary operators** operates on two operands, as with the addition operator. Most operators in programming languages are binary operators.
2. **Unary operator** operates on only one operand, such as the negation operator in ``-12``. 

Arithmetic operators
^^^^^^^^^^^^^^^^^^^^
The following table contains the list of the arithmetic operators we can use in python:

+------------------+--------+------------+--------+
| Binary operators | Symbol | Example    | Result |
+==================+========+============+========+
| Addition         | ``+``  | 2 ``+`` 3  | 5      |
+------------------+--------+------------+--------+
| Substraction     | ``-``  | 2 ``-`` 1  | 1      |
+------------------+--------+------------+--------+
| Multiplication   | ``*``  | 3 ``*`` 3  | 9      |
+------------------+--------+------------+--------+
| Division         | ``/``  | 5 ``/`` 2  | 2.5    |
+------------------+--------+------------+--------+
| Truncating div.  | ``//`` | 5 ``//`` 2 | 2      |
+------------------+--------+------------+--------+
| Modulus          | ``%``  | 5 ``%`` 2  | 1      |
+------------------+--------+------------+--------+
| Exponentiation   | ``**`` | 2 ``**`` 4 | 16     |
+------------------+--------+------------+--------+

Operator Precedence
^^^^^^^^^^^^^^^^^^^
Each programming language has its own rules for the order that operators are applied, called **operator precedence**. This may or may not be the same as in mathematics, although it typically is. 
In python, the operator precedence table is:

+--------------------------------------+-----------------------------+---------------+
| Operator precedence                  | Symbol                      | Associativity |
+======================================+=============================+===============+
| Exponentiation                       | ``**``                      | Right to left |
+--------------------------------------+-----------------------------+---------------+
| Negation                             | ``-``                       | Left to right |
+--------------------------------------+-----------------------------+---------------+
| Addition, substration                | ``+``, ``-``                | Left to right |
+--------------------------------------+-----------------------------+---------------+
| Mult., div., truncating div., modulo | ``*``, ``/``, ``//``, ``%`` | Left to right |
+--------------------------------------+-----------------------------+---------------+

In the table, higher-priority operators are placed above lower-priority ones.

Operator Associativity
^^^^^^^^^^^^^^^^^^^^^^
The above table includes column for associativity. What happens when in some expression we have two operators with the same precedence order? Which one applies first? 

Let's see an example::

	>>> (2 + 3) + 4
	9
	>>> 2 + (3 + 4)
	9

In this case, the **associative law** applies, and the order of the operands doesn't affect the result. 
::

	>>> 2 - 3 - 4 
	-5			

Here, python operates left to right. It's like writing:
::

	>>> (2 - 3) - 4 
	-5

Division and substraction do not follow the associative law, meaning that the order of evaluation matters:

.. note::
	Operator associativity is the order in which operations are applied in case of several operators with the same level of precedence.

