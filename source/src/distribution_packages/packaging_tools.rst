.. _packaging-tools:

***************
Packaging tools
***************
The landscape of programs to create, distribute and install packages in Python is vast and confusing. It's always a good idea to periodically check the `tool recomendations`_ section of the `Python Packaging User Guide`_.

`Python Packaging Authority (PyPA)`_ doesn't list it in recommends using `pip`_ or the

Currently recommended
=====================
As of 2019 the recommended tools are:

* `setuptools`_
* `wheel`_
* `pip`_
* `twine`_

Setuptools
----------
The `setuptools`_ are recommended for:

* Defining projects (``setup.py``)
* Create **Source Distributions**

The `setuptools`_ is a collection of enhancements to the Python **distutils** that allow developers to more easily build and distribute Python packages, especially ones that have dependencies on other packages. To know more about this library check its `documentation <setuptools-doc>`_.

wheel
-----
Primarily, the `wheel`_ library adds the ``bdist_wheel`` command to the setuptools for creating a packaging format named **wheels** or **wheel files**. That's the recommended use in the packaging **tool recommendations** section of the **Python Packaging User Guide**.

Additionally, it offers its own command line utility for **creating** and **installing** wheels.

twine
-----
`twine`_ for **uploading distributions** to the `Python Package Index (PyPI)`_. In this :ref:`section <twine-label>` I go deeper into this tool.

pip
---
`pip`_, is the currently recommended tool to **install packages**. It was introduced in 2008, as an alternative to `easy_install`_. The `Python Packaging User Guide`_ contains a `comparison`_ between these two package managers. 

.. note:: `pip`_ it's also largely built on top of setuptools components. 

To know more about it you can check its `documentation <pip>`_, or a condensed version I wrote in the `pip section <pip>`_ of this site.

Deprecated tools
================
Let's start saying that technically speaking `distutils`_ is not deprecated (yet), although it's not recommended either. Also, even though `easy_install`_ is indeed `deprecated`_, we still have to use it if we have to install from **eggs**, since **pip** can't do that.

distutils
---------
The `distutils`_ package was added to he `Python Standard Library`_ back in 1998, and in 2019 is still part of it. Even though this package can still be used directly (no need to import it), its use is sort of discouraged in favor of the `setuptools`_.

It still can be used for building and installing additional modules that may be either:

* 100% **pure** Python
* **Extension modules** written in **C language**
* Or Python packages that include modules coded in both Python and C.

Many packages use `setuptools`_ while at the same time provide a **fallback import** of ``distutils``::

    try:
        from setuptools import setup
    except ImportError:
        from distutils.core import setup

Easy install
------------
`easy_install`_ is a python module bundled with **setuptools** that lets you automatically download, build, install, and manage Python packages. It was released in 2004, as part of the `setuptools`_, but now it's been **deprecated** in favor of **pip**.

Distribute
----------
`Distribute`_ was a fork of Setuptools that was merged back into Setuptools 0.7.

Distutils2
----------
`Distutils2`_ was an attempt to take the best of Distutils, Setuptools and Distribute and become the standard tool included in Python's standard library. The idea was that Distutils2 would be distributed for old Python versions, and that Distutils2 would be renamed to **packaging** for Python 3.3, which would include it in its standard library. These plans did not go as intended, however, and currently, Distutils2 development is **stopped**.

 

.. _`tool recomendations`: https://packaging.python.org/guides/tool-recommendations/
.. _`Python Packaging User Guide`: https://packaging.python.org/
.. _`Python Packaging Authority (PyPA)`: https://www.pypa.io/en/latest/
.. _`setuptools`: https://setuptools.readthedocs.io/en/latest/
.. _`pip`: https://pip.pypa.io/en/stable/
.. _`wheel`: https://pypi.org/project/wheel/
.. _`twine`: https://pypi.org/project/twine/
.. _`distutils`: https://docs.python.org/3/library/distutils.html#module-distutils
.. _`easy_install`: http://peak.telecommunity.com/DevCenter/EasyInstall
.. _`deprecated`: https://setuptools.readthedocs.io/en/latest/easy_install.html
.. _`comparison`: https://packaging.python.org/discussions/pip-vs-easy-install/
.. _`Python Standard Library`: https://docs.python.org/3/library/
.. _`Python Package Index (PyPI)`: https://pypi.python.org/pypi
.. _`Distribute`: https://pypi.org/project/distribute/
.. _`Distutils2`: https://pypi.org/project/Distutils2/
