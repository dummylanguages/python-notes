Generating the distribution package
===================================
The next step is to generate the archives that we want to upload to the Package Index and can be installed by pip.

Make sure you have the latest versions of ``setuptools`` and ``wheel`` installed::

    $ python3 -m pip install --user --upgrade setuptools wheel

Now, from the same directory where ``setup.py`` is located, we run::

    $ python3 setup.py sdist bdist_wheel

This command should output a lot of text and once completed should generate two files in the ``dist`` directory:

    dist/
    example_pkg_your_username-0.0.1-py3-none-any.whl
    example_pkg_your_username-0.0.1.tar.gz

The ``tar.gz`` file is a **source archive** whereas the ``.whl`` file is a **built distribution**. Newer pip versions preferentially install built distributions, but will fall back to source archives if needed. You should always upload a source archive and provide built archives for the platforms your project is compatible with. In this case, our example package is compatible with Python on any platform so only one built distribution is needed.


