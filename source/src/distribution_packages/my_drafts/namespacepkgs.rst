Packaging namespace packages
----------------------------
Sometimes our project contains a collection of loosely-related packages, which could be useful to break up into separate distributions that can be separately installed, used, and versioned. For example::

    project_root/
        setup.py
        mynamespace/
            __init__.py
            subpkg_a/
                __init__.py
                mod.py
            subpkg_b/
                __init__.py
                mod.py
            mod_c.py

We could break up this project into two smaller distributions, the **first** one::

        mynamespace_subpkg_a/
            setup.py
            mynamespace/
                subpkg_a/
                    __init__.py
                    mod.py

And the **second** one::

        mynamespace_subpkg_b/
            setup.py
            mynamespace/
                subpkg_a/
                    __init__.py
                    mod.py
            mod_c.py

Note how the ``mynamespace`` package doesn’t include a **dunder init** file. It is extremely important that every distribution that uses the ``namespace`` package omits the ``__init__.py``. If any distribution does not, it will cause the namespace logic to fail and the other sub-packages will not be importable.

Because ``mynamespace`` doesn’t contain an ``__init__.py``, ``find_packages()`` won’t find the sub-package. You must explicitly list all packages in your ``setup.py``. For example::

    setup(
        name='mynamespace_subpkg_a',
        ...
        packages=['mynamespace.subpackage_a']
    )

Another option would be to use the `find-namespace-packages()`_ function, 

The ``src/`` layout
-------------------
This layout consists in putting all our source code into a directory named ``src``, for example::

    foo/
        setup.py
        src/
            namespace/
                mypackage/
                    __init__.py
                    mod1.py
        tests/
            test_mod1.py

With this layout, we have to tell ``setup`` that our packages are under ``src``::

    setup(...
        package_dir={'': 'src'},
        packages=find_namespace_packages(where='src')
        ...
        )

