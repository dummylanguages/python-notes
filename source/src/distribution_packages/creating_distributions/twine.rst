.. _twine-label:

*************************
Uploading to PyPI (twine)
*************************
Once we have a distribution package ready, we may want to make it available to the Python community, or in other words, upload it to the `PyPI`_.

For that we need a package named `twine`_, which is not part of the Python Standard Library, so we'll have to install/update it::

    $ pip3 install --user --upgrade twine

Test PyPI
=========
`Test PyPI`_ is a separate instance of `PyPI`_ intended for testing and experimentation, great for trying things out before uploading to the real index. But before anything, we'll have to `create an account`_ on the site. 

Now we can run Twine to upload all of the archives under the ``dist`` folder::

    $ python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

You will be prompted for the **username** and **password** of your account in Test PyPI. After the command completes, you should see output similar to this:

    Uploading distributions to https://test.pypi.org/legacy/
    Enter your username:
    Enter your password:

Once uploaded your package should be viewable on TestPyPI, for example, at:
https://test.pypi.org/project/example-pkg-your-username

A ``.pypirc`` file
==================
The `.pypirc <pypirc>`_ file allows us to configure several repositories, for example `PyPI`_ and `Test PyPI`_ so we don't have to introduce their **url** or our **credentials** every time we upload to them.

.. note::  `pypi.org <PyPI>`_ and `test.pypi.org <Test PyPI>`_ are not integrated, so we'll need to have a separate account for each site.

This is what this file looks like::

     [distutils]
    index-servers=
        pypi
        testpypi

    [pypi]
    username: bob
    password: 1234

    [testpypi]
    repository: https://test.pypi.org/legacy/
    username: bob
    password: 4321

Note that the ``repository`` under ``[pypi]`` doesn't exist. That's because it defaults to ``https://upload.pypi.org/legacy/``, so we don't need to define it.

Securing The ``.pypirc`` file
-----------------------------
Since this file stores sensitive information (i.e. passwords) in **plain text**, it's import to set proper **permissions** so that other users on the system can not access its contents. For example::

    $ chmod 600 ~/.pypirc

The command above will ensure that only the **owner** of the file can **read** and **write** from and to this file. 

Location
--------
Put it at your $HOME directory, ``~/.pypirc``

.. _`twine`: https://twine.readthedocs.io/en/latest/
.. _`PyPI`: https://pypi.python.org/pypi
.. _`Test PyPI`: https://test.pypi.org/
.. _`create an account`: https://test.pypi.org/account/register/
.. _`pypirc`: https://docs.python.org/3/distutils/packageindex.html#the-pypirc-file