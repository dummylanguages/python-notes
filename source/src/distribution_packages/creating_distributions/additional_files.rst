Additional files
================
When building a distribution, the most important file is `setup.py`_ which exists at the root of your project directory. Along with this file, we may use some additional ones which we'll see next.

setup.cfg
---------
`Setuptools`_ allows `configuring setup() using setup.cfg files`_. In this file we can define a package’s **metadata** and other **options** that are normally supplied as **arguments** to the ``setup()`` function.

The ``setup.cfg`` is written in `ini`_ format, and unknown keys will be ignored. 

README
------
All projects should contain a ``README`` file that covers the goal of the project. This file may have several formats:

* Plain text.
* reStructuredText.
* Markdown in one of these 2 flavours:

    * `GitHub-flavoured Markdown`_
    * `CommonMark`_

We may use this file to generate the `Description`_ field in the ``PKG-INFO`` file. There is a **common pattern** used in ``setuptools.setup()`` for generating this field::

    with open("README.md", "r") as fh:
        DESCRIPTION = fh.read()

    setup(
        ...
        long_description=DESCRIPTION,
        long_description_content_type="text/markdown",
        ...
    )

We are just reading the contents of the ``README.md`` file and assign them to the ``long_description`` argument. Note that we have to specify the format used in the ``long_description_content_type`` argument:

* ``text/plain``
* ``text/x-rst``
* ``text/markdown``

This **argument** will generate the `Description-Content-Type`_ **field** in the ``PKG-INFO`` file.

MANIFEST.in
-----------
When we need to package additional files that are not automatically included in a **source distribution**, we must write a **manifest template**, called ``MANIFEST.in`` by default. This template is processed by the ``sdist`` command, which will generate a file named ``MANIFEST``, which is the exact list of files to include in your source distribution.

.. note:: `MANIFEST.in` does **not** affect **binary distributions** such as **wheels**.

For an example, see the `MANIFEST.in`_ from the `PyPA sample project`_.

LICENSE
-------
Every package should include a license file detailing the terms of distribution. If you’re unsure which license to choose, you can use resources such as GitHub’s `choose a License`_.

For an example, see the `LICENSE.txt`_ from the `PyPA sample project`_.


.. _`setup.py`: https://docs.python.org/3/distutils/setupscript.html#setup-script
.. _`Setuptools`: https://setuptools.readthedocs.io/en/latest/
.. _`configuring setup() using setup.cfg files`: https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files
.. _`ini`: https://en.wikipedia.org/wiki/INI_file
.. _`reStructuredText`: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _`GitHub-flavoured Markdown`: https://help.github.com/en/categories/writing-on-github
.. _`CommonMark`: https://commonmark.org/
.. _`Description`: https://packaging.python.org/specifications/core-metadata/#description
.. _`Description-Content-Type`: https://packaging.python.org/specifications/core-metadata/#description-content-type
.. _`MANIFEST`: https://packaging.python.org/guides/distributing-packages-using-setuptools/#manifest-in
.. _`MANIFEST.in`: https://github.com/pypa/sampleproject/blob/master/MANIFEST.in
.. _`PyPA sample project`: https://github.com/pypa/sampleproject
.. _`LICENSE.txt`: https://github.com/pypa/sampleproject/blob/master/LICENSE.txt
.. _`choose a License`: https://choosealicense.com/