*******************
A packaging example
*******************
Let's use a small example to learn about the basics of packaging in Python. We'll create a package out of a **single module** and install it in our system. This is what our project looks like::

    ex01/
        length.py
        setup.py

The ``ex01`` directory doesn't have any meaning, it's just a folder we need to hold our files, and to run our commands from.

This is what the :download:`length.py </code/distro_pkgs/ex01/length.py>` module looks like:

.. literalinclude:: /code/distro_pkgs/ex01/length.py

A ``setup.py`` file
===================
To generate the package we need a special file that must be named ``setup.py``, and we'll add it next to our module:

    ex01/
        length.py
        setup.py

We've keep the contents of :download:`setup.py </code/distro_pkgs/ex01/setup.py>` to the bare minimum:

.. literalinclude:: /code/distro_pkgs/ex01/setup.py

Packaging it
============
Now we just have to create a `distribution package`_ out of the module by running::

    $ cd ex01
    $ python setup.py sdist

That will generate a new ``.egg-info`` and ``dist`` folders::

    ex01/
        length.py
        setup.py
        dist/
            length-1.0.tar.gz
        length.egg-info/
            dependency_links.txt
            PKG-INFO
            SOURCES.txt
            top_level.txt

The ``dist`` directory contains our **source distribution** package. This is a compressed archive file (e.g., tarball on Unix, ZIP file on Windows) named ``length-1.0.tar.gz``. If we unpack this **tarball**, we'll find inside::

    length-1.0/
        length.py
        setup.py
        length.egg-info/

Our ``length.py`` and ``setup.py`` files and a copy of the ``length.egg-info`` folder.

Installing it
-------------
To install it, we have 2 options:

1. Using ``setuptools``
2. Using ``pip``

.. note:: Probably it's a good idea to install the package inside a **virtual environment** that we can erase after we finish practicing.

Using the setuptools
^^^^^^^^^^^^^^^^^^^^
To install a package this way, we have to unpack it, and from the top level directory (length-1.0) run::

    $ python setup.py install

.. note:: We can use the ``--user`` option if we prefer to install following a :ref:`user scheme <user-scheme>`. Another option would be installing inside a **virtual environment** where the user scheme it's not available.

This will copy ``length.py`` to the ``site-packages`` directory in our Python installation.

Note that the ``setup`` script is used:

* To generate the package using the ``sdist`` command, used almost exclusively by module developers.
* To install it, using the ``install`` command, used by consumers of the module (although most developers will want to install their own code occasionally)..

In both cases the ``setup`` script is the interface to both operations.

pip install
^^^^^^^^^^^
We can use ``pip``, no need to unpack the tarball::

    $ pip install dist/length-1.0.tar.gz

That will also put the module in the ``site-packages`` directory.

.. note:: Here we can also use the ``--user`` option or a **virtual environment**.

.. _`distribution package`: https://docs.python.org/3/distutils/sourcedist.html?highlight=distribution%20package