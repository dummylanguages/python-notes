*************************
Generating a distribution
*************************
The two main packages we'll be using here are gonna be the `setuptools`_ and the `wheel`_ extension. Since these are not part of the Python Standard Library, let's install/upgrade them::

    $ pip install --upgrade pip setuptools wheel


.. _`setuptools`: https://setuptools.readthedocs.io/en/latest/index.html
.. _`wheel`: https://pypi.org/project/wheel/