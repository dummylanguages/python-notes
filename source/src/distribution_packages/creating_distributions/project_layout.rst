Project layout
==============
The structure of our project depends on several factors such as:

* Its complexity: it's not the same to package a couple of module-libraries than to provide for a big project with packages/subpackages and possibly **extension modules** or **scripts**.
* How do we want to organize our **tests** around the source tree.

.. note:: On a separate section we contemplate a more elaborate example about packaging subpackages in separate distributions.

Whatever layout we choose, we have to configure our project accordingly, passing the proper **arguments** to the ``setup()`` function.

Modules distribution
--------------------
For those cases we want to distribute one or two top level modules, like this::

    foo/
        foo.py
        setup.py

.. note:: Although it’s not required, the most **common practice** is to include your Python modules and packages under a single top-level package that has the same name as your project, or something very close. Throughout all these examples, we'll use the name ``foo``.

We would use the ``py_modules`` argument, which takes a list of strings with the names of the modules::

    setup(name='foo',
        version='1.0',
        py_modules=['foo'],
        )

Package distribution
--------------------
If we want to distribute a more complex project that includes packages and subpackages we have a couple of choices to organized our source tree. For example::

    foo/
        pkg/
            __init__.py
            subpkg/
                __init__.py
                mod1.py
                mod2.py
        setup.py
        tests/

For a small tree like this, we may use the ``packages`` argument, which works the same as ``py_modules``::

    setup(name='foo',
        version='1.0',
        packages=['pkg']
        )

However, for large projects we can use the `find_packages()`_ function::

    setup(...,
        packages=find_packages(),
        ...
        )

Using this function we don't have to worry about keeping the list of packages updated after new additions. In the case of our example, we must **exclude** our test files::

    find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"])

We can use **wildcards** in our exclusion patterns:

* ``"*.tests"`` will exclude all packages whose last name part is ``tests``.
* ``"*.tests.*"`` will exclude any subpackages of packages named ``tests``.
* ``"tests"`` will exclude a top-level ``tests`` package and its children.

The ``src/`` layout
-------------------
This is a popular layout which consists in nesting our package into a top-level directory named ``src``, like this::

    foo/
        setup.py
        src/
            pkg/
                __init__.py
                mod1.py
        tests/
            test_mod1.py

With this layout, we have to tell ``setup`` that our packages are under ``src``::

    setup(...
        packages=find_packages(where='src'),
        package_dir={'': 'src'},
        ...
        )

Namespace packages
------------------
From **Python 3.3**, the ``setuptools`` provide the ``find_namespace_packages()``, which has the same function signature as ``find_packages``, but works with PEP 420 compliant **implicit namespace packages**. Here's a small example::

    foo/
        namespace/
            pkg/
                __init__.py
                mod1.py
        setup.py
        tests/
            test_mod1.py

A naive ``find_namespace_packages()`` would install both ``namespace.mypackage`` and a top-level package called ``tests``.

1. One way to avoid this problem is to use the ``include`` keyword to whitelist the packages to include, like so::

    from setuptools import setup, find_namespace_packages

    setup(name="namespace.mypackage",
          version="0.1",
          packages=find_namespace_packages(include=['namespace.*'])
    )

2. Another option is to use the **src layout**, where all package code is placed in the ``src`` directory, like so::

    foo/
        setup.py
        src/
            namespace/
                mypackage/
                    __init__.py
                    mod1.py
        tests/
            test_mod1.py

With this layout, the package directory is specified as ``src``, as such::

    setup(name="namespace.mypackage",
          version="0.1",
          package_dir={'': 'src'},
          packages=find_namespace_packages(where='src'))


.. _`Python Packaging User Guide`: https://packaging.python.org/

.. _`setuptools`: https://setuptools.readthedocs.io/en/latest/index.html
.. _`full documentation`: https://setuptools.readthedocs.io/en/latest/index.html
.. _`pip`: https://pip.pypa.io/en/latest/index.html
.. _`distutils`: https://docs.python.org/3.7/library/distutils.html#module-distutils
.. _`Python Package Index`: https://pypi.python.org/pypi
.. _`PyPI`: https://pypi.python.org/pypi
.. _`Python Package Manager`: http://docs.activestate.com/activepython/2.7/pypm.html

.. _`find_packages()`: https://setuptools.readthedocs.io/en/latest/setuptools.html#using-find-packages
.. _`find_namespace_packages()`: https://setuptools.readthedocs.io/en/latest/setuptools.html#find-namespace-packages