**********************
Creating distributions
**********************

.. toctree::
    :maxdepth: 3
    :hidden:

    project_layout
    metadata
    additional_files
    generating_distribution
    packaging_example
    twine

When creating a **distribution package** we have to take care of a few things:

* Make sure the needed tools are installed/upgraded.
* Create a project layout.
* Write a ``setup.py`` file.
* Choose an adequate format for our distribution.
* Optionally, we may upload it to PyPI.

The center piece of the puzzle is the ``setup.py`` file which serves as our **project configuration** and as **interface** for some packaging commands. Inside this file we call the ``setup()`` function, and pass it a series of **arguments** to define our project.

More useful information on:

* I based this section on one of the guides of the `Python Packaging User Guide`_, titled the `Packaging and distributing projects guide`_.
* Probably the most important resource is gonna be the `setuptools`_, so don't forget to check its `full documentation`_, in case of doubt.


.. _`Python Packaging User Guide`: https://packaging.python.org/
.. _`Packaging and distributing projects guide`: https://packaging.python.org/guides/distributing-packages-using-setuptools/
.. _`setuptools`: https://setuptools.readthedocs.io/en/latest/index.html
.. _`full documentation`: https://setuptools.readthedocs.io/en/latest/index.html