.. _pip-label:

pip
===
From Python version **2.7.9** and **3.4** (and later) `pip`_ is included by **default** with every Python installation. You can check that by running::

	$ pip -version

Installation
------------
If for some reason the command above doesn't print anything, you may need to to bootstrap ``pip`` from the standard library using `ensurepip`_::

	$ python -m ensurepip --default-pip

If that still doesn't work, we may need to **download** `get-pip.py`_ and run::

	$ python get-pip.py

.. note:: We can use python ``--prefix=/usr/local/`` to install in ``/usr/local`` which is designed for locally-installed software.

Two versions
------------
All Unix, Linux OS usually include a Python interpreter, let's call it the **system Python**. At the time of writing this, **macOS** (10.14.3 - Mojave) includes Python 2.7. as the system Python, so I decided to :ref:`installing-python`. For this reason I have 2 copies of ``pip`` available in my system:

* ``pip`` for **system Python**.
* ``pip3`` for **Python 3**.

Upgrading
=========
While `pip`_ alone is sufficient to install from **pre-built binary archives**, up to date copies of the setuptools and wheel projects are useful to ensure you can also install from **source archives**::

    $ python -m pip install --upgrade pip setuptools wheel

Or::

    $ pip install --upgrade pip setuptools wheel

Using pip
=========
To see list of installed packages::

	$ pip freeze

We can write the list to a text file, and use that text file to install the whole list::

	$ pip freeze > requirements.text
	$ pip install -r requirements.txt

Installing
----------
To install new packages::

	$ pip install package_name

By default, Python packages are installed to ...``--system-site-packages``

Uninstalling
------------
To uninstall packages::

	$ pip uninstall package_name


Upgrading packages
------------------
To see outdated packages::

	$ pip list --outdated

To upgrade packages::

	$ pip install --upgrade package_name

To upgrade pip itself::

	$ syspip install --upgrade pip

And reopen new session in terminal.


.. _`pip`: https://pip.pypa.io/en/latest/index.html
.. _`ensurepip`: https://docs.python.org/3/library/ensurepip.html
.. _`get-pip.py`: https://bootstrap.pypa.io/get-pip.py
