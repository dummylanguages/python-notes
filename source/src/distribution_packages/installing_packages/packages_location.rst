Package locations
=================
An interesting question would be, where does Python place the packages we install. To answer this question, first we must know what **installation scheme** was used to install the package. 

By default, ``pip`` will install to a **standard location**, but Python includes several **alternate schemes** that allow us to install packages to other locations:

* The `user scheme`_.
* The `home scheme`_.
* The `prefix scheme`_.
* `Custom installation`_.

Apart from these schemes, when we are working inside a **virtual environment** packages are installed to an isolated ``site-packages`` directory inside the virtual environment.

Standard location
-----------------
Using the **standard scheme**, packages are installed in a directory named ``site-packages``. The exact location of this directory depends on our OS, and the way we built/installed Python itself. We can find out this location on our system using the `site`_ module from an interactive Python session::

	$ python3
	>>> import site
	>>> print(site.getsitepackages())
	['/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages']

The function ``site.getsitepackages()`` returns the locations for **global site-packages**

The modules installed in this location are available **system wide**, for all users.

.. note:: You can get this same info running a one-liner from your shell::

	$ python3 -c "import site; print(site.getsitepackages())"

User-specific site-packages directory
-------------------------------------
Apart from installing packages system-wide, we can also install packages using what it's called the `user scheme`_. These packages are available only for a given user. The location of the **user site-packages** directory can be found running::

	$ python3 -m site --user-site
	/Users/javi/Library/Python/3.7/lib/python/site-packages

We explain this in more detail in the :ref:`user-scheme` section.

Debian and derivatives
----------------------
In Debian and other distros based on it, Python uses a directory named ``dist-packages`` for packages. If we install another Python using the **Debian package manager**, it will also install to this directory.

Now, if we install Python **manually from source**, it will use the standard ``site-packages`` directory.

The reason for that is that Debian and Ubuntu rely on the **system Python** for many system utilities. Keeping a different package directory allows us to keep the two installations separate, and reduces conflict between the system Python, and any from-source Python build you might install manually.
 

.. _`user scheme`: https://docs.python.org/3/install/index.html#alternate-installation-the-user-scheme
.. _`home scheme`: https://docs.python.org/3.7/install/#alternate-installation-the-home-scheme
.. _`prefix scheme`: https://docs.python.org/3.7/install/#alternate-installation-unix-the-prefix-scheme
.. _`Custom installation`: https://docs.python.org/3.7/install/#custom-installation
.. _`site`: https://docs.python.org/3/library/site.html#module-site