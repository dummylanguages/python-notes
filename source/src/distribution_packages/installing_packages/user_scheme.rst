.. _`user-scheme`:

***************
The user scheme
***************
From Python 2.6 and 3.0, was made available the option of installing packages to a user's local directory. This is known as the `user scheme`_, check `PEP 370`_ for more information. This scheme is designed to be the most convenient solution for users that don’t have write permission to the global **site-packages** directory or don’t want to install into it.

.. warning:: If you installed Python with **homebrew**, the **user scheme** doesn't work because of a **BUG** in **homebrew's distutils version**. They talk about it `here`_.

In pip we can take advantage of this scheme using the `--user`_ option::

	$ pip3 install --user package_name 

Every time we install a package, its files will be installed into subdirectories of ``site.USER_BASE``. We can get the location of this directory on our system by running::

	$ python3 -m site --user-base
	/Users/javi/Library/Python/3.7

The following table contains the **location** for every type of file for UNIX, including macOS:

+--------------+--------------------------------------+
| Type of file |        Installation directory        |
+==============+======================================+
| modules      | userbase/lib/pythonX.Y/site-packages |
+--------------+--------------------------------------+
| scripts      | userbase/bin                         |
+--------------+--------------------------------------+
| data         | userbase                             |
+--------------+--------------------------------------+
| C headers    | userbase/include/pythonX.Y/distname  |
+--------------+--------------------------------------+

This scheme installs pure Python modules and extension modules in the **user site-packages** directory.

User site-packages
==================
This is the location for the modules we import into our scripts. We can get the **current location** of this directory on our system using::

	$ python3 -m site --user-site
	/Users/javi/Library/Python/3.7/lib/python/site-packages

The above output is from macOS, where I installed Python using the official graphical installer. Sometimes this directory doesn't even exist (Find out with ``python3 -m site``) but it will be automatically created as soon as we install our first package using the ``--user`` option.

The **default** value of this directory, depends on how we installed Python:

1. For **macOS** framework builds (graphical installer) is ``~/Library/Python/X.Y``.
2. For UNIX and macOS **non-framework** builds, ``~/.local``.

Changing default location
-------------------------
We can change this default location by setting the `PYTHONUSERBASE`_ environment variable. We could prefer to use the ``~/.local`` directory. So in our shell configuration we would add::

	export PYTHONUSERBASE=".localpip"

That would mean that our **user packages** get installed in::

	$HOME/.local/lib/python/site-packages

Creating an alias
-----------------
If we often want to install packages to the user-specific locations, a good idea could be also setting an **alias** for the ``pip`` command, in case we forget the ``--user`` option::

	alias pipuser='pip install --user'

Adding the script folder to the PATH
====================================
Sometimes the packages we install include **scripts**, meaning executable Python files that we can invoke from our command line. In order to do that, we must put their location in the ``PATH``. 

We just saw in the table above how the scripts get installed in ``site.USER_BASE/bin``. That's the location we have to add to the PATH in our shell configuration file::

	export="${HOME}/Library/Python/3.7/bin:${PATH}"

Cleaning our PATH after the installer
-------------------------------------
When we installed Python using the **official installer**, during the installation process some lines were added to our ``~/.bash_profile``::

	export PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"

This line is putting in the PATH two things:

1. The **Python interpreter** itself, and other utilities such as **pip**.
2. The **scripts** of the packages we install to the **global site-packages** directory.

Even though that line doesn't take much space in my shell configuration, I could delete it based on 2 reasons:

1. The Python installer create **symbolic links** to the interpreter and Python utilities in ``/usr/local/bin``, a directory which is, by default, in the ``PATH``.
2. If we are gonna be installing packages using the **user scheme**, we don't need that directory in the ``PATH``.

.. note:: I experiences a small hiccup after installing `ranger`_, a text-based file manager written in Python. I couldn't launch it at first, so I doubted myself about adding the line to PATH. In turned out that there was no need for that, reinstalling ranger solved the issue.

.. _`user scheme`: https://docs.python.org/2/install/index.html#alternate-installation-the-user-scheme
.. _`here`: https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/Homebrew-and-Python.md#note-on-pip-install---user
.. _`--user`: http://pip.readthedocs.org/en/latest/user_guide.html#user-installs
.. _`PYTHONUSERBASE`: https://docs.python.org/2/using/cmdline.html#envvar-PYTHONUSERBASE
.. _`PEP 370`: https://www.python.org/dev/peps/pep-0370/
.. _`ranger`: https://github.com/ranger


