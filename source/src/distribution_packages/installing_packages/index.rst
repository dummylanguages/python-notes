.. _installing-packages:

*******************
Installing packages
*******************

.. toctree::
    :maxdepth: 3
    :hidden:

    pip
    packages_location
    home_scheme
    user_scheme

There are several points to consider when installing distribution packages:

* What's our source? Are we installing a package from an external repository such as:
	* The `Python Package Index (PyPI)`_
	* **Github** or others.
	* Or maybe we are installing a **local** package?

* What **program** are we using to install the package? One answer, :ref:`pip-label`.
* **Where** do packages get installed? This will depend on what way we install the package.

.. _`Python Package Index (PyPI)`: https://pypi.python.org/pypi

