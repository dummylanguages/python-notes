.. _distributions:

#####################
Distribution Packages
#####################

.. toctree::
    :maxdepth: 3
    :hidden:

    types_distribution_packages
    packaging_tools
    creating_distributions/index
    installing_packages/index

Your default Python installation comes packed with a lot of software:

* The `built-in functions`_ are readily available without needing to import any module.
* The `Python Standard Library`_ contains more than 200 modules, ready to use after we **import** them into our projects. 

But sometimes we need functionality not available in these places, for those cases we have a couple of options:

* Use **3rd party** software. The `Python Package Index(PyPI)`_ is the official source for Python **distribution packages** which we can download and install using ``pip``. But we can also find Python packages in `Github`_ or other repository hosting sites.
* We can also create our own distribution package, and install it on our system. We can also consider to make them publicly available at the PyPi.

The `Python Packaging User Guide`_ is an invaluable source of information when it comes to create, distribute and install Python packages. Don't forget to check it out.

.. _`built-in functions`: https://docs.python.org/3/library/functions.html#built-in-functions
.. _`Python Standard Library`: https://docs.python.org/3/library/
.. _`Python Package Index(PyPI)`: https://pypi.python.org/pypi
.. _`Github`: https://github.com
.. _`Python Packaging User Guide`: https://packaging.python.org/
