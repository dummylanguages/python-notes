******************************
Types of distribution packages
******************************
There are a lot of formats in which we can distribute our packages, but they all fall into two categories:

1. `Source Distributions`_
2. `Built Distributions`_

For a **pure** Python project, it doesn't really matter what format do we choose for our distribution package. But if our package includes `extension modules`_, these must be compiled for the specific platform where is intended to be installed.

.. note:: An **extension module** is a module written in the low-level language of the Python implementation: C/C++ for Python, Java for Jython. Currently, distutils only handles **C/C++** extensions for Python.

Source Distribution 
===================
This is a package format that provides metadata and the source files needed for:

1. Generating a **built distribution** if the package includes **extension modules**.
2. Being installed by a tool like ``pip`` if it's a **pure** Python distribution.

These packages are generated using::

    $ python setup.py sdist

A **source distribution** requires a build step when installed by pip. Even if the distribution is **pure** Python (i.e. contains no extensions), it still involves a build step to build out the installation metadata from setup.py.

The Python documentation contains information about `Creating a Source Distribution`_, in the **Legacy version** of the `Distributing Python Modules`_ section.

Built Distribution
==================
Built Distributions are **pre-built packages** containing compiled files and metadata that only need to be moved to the correct location on the target system, to be installed. There are 2 formats in this category:

Eggs
^^^^
`Eggs`_ are the old **pre-built** format for Python packages. They were installed with ``easy_install`` (now `deprecated`_) and can **not** be installed with ``pip``. Some reasons to use this format:

* Developers didn't have to include the Python source files (``.py``) in the package, including only ``.pyc`` files, as a way of obscuring the code.
* Eggs may include **compiled** C extension modules so that the end user does not need to have the necessary build tools and possible additional headers and libraries to build the extension module from scratch. 

Check the `quick guide to Python eggs`_ or the `internal structure of Python eggs`_ to learn more about them.

Wheels
^^^^^^
`Wheel`_ is the new pre-built distribution format introduced by `PEP 427`_, which is intended to replace Eggs. Wheels are installed faster than Source Distributions (sdist), especially when a distribution contains compiled extensions.

``pip`` can install from either Source Distributions (sdist) or Wheels, but if both are present on PyPI, pip will prefer a compatible wheel. If pip does not find a wheel to install, it will locally build a wheel and **cache** it for future installs, instead of rebuilding the source distribution in the future.

Build a Wheel
"""""""""""""
To **build** a Wheel we just do::

    $ python setup.py bdist_wheel

Install a Wheel
"""""""""""""""
To **install** a Wheel we use::

    $ pip install *.whl

.. _`Source Distributions`: https://packaging.python.org/glossary/#term-source-distribution-or-sdist
.. _`Built Distributions`: https://packaging.python.org/glossary/#term-built-distribution
.. _`extension modules`: https://docs.python.org/3/extending/extending.html
.. _`Wheel`: https://packaging.python.org/glossary/#term-wheel
.. _`Eggs`: https://packaging.python.org/glossary/#term-egg
.. _`quick guide to Python eggs`: http://peak.telecommunity.com/DevCenter/PythonEggs
.. _`internal structure of Python eggs`: https://setuptools.readthedocs.io/en/latest/formats.html#id1
.. _`PEP 427`: https://www.python.org/dev/peps/pep-0427/


.. _`Creating a Source Distribution`: https://docs.python.org/3/distutils/sourcedist.html
.. _`Distributing Python Modules`: https://docs.python.org/3/distutils/index.html
.. _`deprecated`: https://setuptools.readthedocs.io/en/latest/easy_install.html
