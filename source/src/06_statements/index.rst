##########
Statements
##########

.. toctree::
	:maxdepth: 3
	:hidden:
	
	types_of_statements
	assignments
	program_flow
	conditions
	selection_control_toc
	iterative_control/index

Let's revisit the **Python conceptual hierarchy** as an introduction to this chapter:

1. **Programs** are composed of modules
2. **Modules** contain statements
3. **Statements** contain expressions
4. **Expressions** create and process **objects**

A quick look to this hierarchy, shows that **expressions** are embeded in **statements**. When were talking about variables, we saw how assignment statements contain expressions to the right of the ``=`` operator. For example::

	>>> x = 2 + 2

Here we have an **assignment statement** which contains an embedded expression (``2 + 2``). The Python interpreter **first** evaluates the **expression** and then assigns the resulting value (``4``) to the variable name. We have already talked about the :ref:`assignment-statement` when we were introducing variables, but assignments are not the only type of statement that we can find in Python.

