*******************
Types of statements
*******************
We can embed expressions in any **if statements**, for example::

	if x > 3:
		print('x is greater than 3')

Here we have an **if statement** containing a **relational expression** (``x = 3``) that evaluates to ``True``. Depending on the result of the expression, the **if statement** will run the code below or not. A expression always evaluate to some value.

.. note::
	An **expression** *is* something, while a **statement** *does* something.

Besides the assigment statement, python has are a lot more of them, which we can classify in 2 groups:

Simple statements
=================
The following table contains a list of simple statements in python:

+----------------+-----------------------------+---------+
| Statement      | Role                        | Example |
+================+=============================+=========+
| Assignment     | Create references           | a = 3   |
+----------------+-----------------------------+---------+
| Function calls | To run functions or methods |         |
+----------------+-----------------------------+---------+
| ``assert``     | Debugging checks            |         |
+----------------+-----------------------------+---------+
| ``pass``       | Empty placeholder           |         |
+----------------+-----------------------------+---------+
| ``del``        | Deleting references         |         |
+----------------+-----------------------------+---------+
| ``return``     | Function results            |         |
+----------------+-----------------------------+---------+
| ``yield``      | Generator functions         |         |
+----------------+-----------------------------+---------+
| ``raise``      | Triggering exceptions       |         |
+----------------+-----------------------------+---------+
| ``break``      | Loop exit                   |         |
+----------------+-----------------------------+---------+
| ``continue``   | Loop continue               |         |
+----------------+-----------------------------+---------+
| ``import``     | Module access               |         |
+----------------+-----------------------------+---------+
| ``global``     | Namespaces                  |         |
+----------------+-----------------------------+---------+
| ``nonlocal``   | Namespaces                  |         |
+----------------+-----------------------------+---------+

Compound statements
===================
The following table contains a list of compound statements in python:

+-----------+--------------------------------+---------+
| Statement | Role                           | Example |
+===========+================================+=========+
| ``if``    | Conditional execution          |         |
+-----------+--------------------------------+---------+
| ``while`` | Repeated conditional execution |         |
+-----------+--------------------------------+---------+
| ``for``   | Iteration                      |         |
+-----------+--------------------------------+---------+
| ``try``   | Exception handlers             |         |
+-----------+--------------------------------+---------+
| ``with``  | Wraps the execution of a block |         |
+-----------+--------------------------------+---------+
| ``def``   | Function definitions           |         |
+-----------+--------------------------------+---------+
| ``class`` | Objects definitions            |         |
+-----------+--------------------------------+---------+

.. note::
	Assignments are, perhaps, the most important type of statement in any programming language.


Expressions statements
======================
In Python, you can use an expression as a statement too, that is, on a line by itself. Expressions are commonly used as statements in several situations:

	* For calls to **functions** and **methods**. Some functions and methods do their work without returning a value (**procedures**) Because they don’t return values that you might be interested in retaining, you can call these functions with expression statements. The ``print()`` function belong to this group.
	* For printing values at the interactive prompt. When using the interactive mode, python prints automatically the results of expressions you type. Technically, these are expression statements, too. We don't need to use ``print()`` explicitly.
	* Yielding expressions statements.

.. todo:: Not clear wtf is an expression statement. Dig in more.

