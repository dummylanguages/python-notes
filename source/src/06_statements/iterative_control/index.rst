*****************
Iterative control
*****************

.. toctree::
	:maxdepth: 3
	:hidden:

	while
	for
	iterations
	loop_coding_techniques
	list_comprehensions

Sometimes, our program is gonna need to **repeat** the execution of a **suite** (a block of one or more statements). For that, we use what is known in computer science as **iterative control structures**.  These include both the **suite** and the **iterative control statement** controlling their execution.

.. note:: Every repetition of the block is called a **pass** or an **iteration**, hence the name iterative control.

Python provides two main kinds of iterative control: ``while`` and ``for``. But there are other additional looping operations and concepts in Python which we'll also explore. 
