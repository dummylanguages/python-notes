***********************
The ``while`` statement
***********************
The ``while`` statement repeatedly executes a block of statements as long as the test in the header keeps evaluating to a ``True`` value. When the test becomes ``False``, control passes to the statement that follows the while block.

.. note:: The ``while`` statement is commonly known as the **while loop** because control keeps iterating (or **looping**) over the block until the test becomes ``False``. 

If the test evaluates to ``False`` from the beginning, the body never runs and the entire while statement is skipped. Let's see an example:

.. literalinclude:: /code/chap_7/while.py
    :language: python
    :linenos:
    :tab-width: 4

* Line **1** contains the **header**. In this header we have hardcoded the literal ``False`` as the **test**. 
* Line **2** contains the **suite** which never runs since the test is always ``False``. The program flow never steps into the suite.
* Lines **4** contains another call to ``print`` outside the loop. This is the only line of code that gets executed.

Infinite loops
==============
An **infinite loop** is an iterative control structure that never terminates (or eventually terminates with a system error). Infinite loops are generally the result of programming errors and can cause a program to “hang,” that is, to be unresponsive to the user. In such cases, the program must be terminated by use of some special keyboard input (such as ``Ctrl-C``) to interrupt the execution. For example::

    >>> while True:
    ...     print('Type Ctrl-C to stop me!')

In the code above, we have hardcoded a literal ``True`` which is never gonna change, thus the loop runs endlessly.

Definite loops
==============
In both of the examples above, we had **definite loops** because in both cases we can tell for how long the loop is gonna run:

    * In the first case it never runs.
    * In the infinite loop, it runs forever.

A **definite loop** is a loop in which the number of **iterations** can be determined before the loop is executed. The following it's a trivial example that keeps slicing off the first character of a string until the string is empty and hence ``False``. It’s typical to test an object directly like this instead of using the more verbose equivalent ``while x != '':`` ::

    >>> x = 'spam'
    >>> while x:
    ...     print(x)
    ...     x = x[1:]
    ...
    spam
    pam
    am
    m

In the context of **Boolean operations**, and also in **tests** used in control flow statements, the following values are interpreted as false:
    
    * ``False``
    * ``None``
    * **Numeric zero** of all types (``int``, ``float``...)
    * **Empty strings**
    * **Empty containers** (including strings, tuples, lists, dictionaries, sets and frozensets).

The next example code counts from the value of ``a`` up to, but not including, ``b`` ::

    >>> a = 0; b = 10
    >>> while a < b:    # One way to code counter loops
    ...     print(a)
    ...     a += 1      # Increase the counter
    ...
    0 1 2 3 4 5 6 7 8 9

As we will see later, **for loops** are the control statement that most easily supports definite iteration.

Indefinite Loops
================
With **indefinite loops** the number of iterations cannot be determined before the loop is executed. For example, when the evaluation of the **test** depends on the input of the user. As long as the user's input makes the test evaluate to ``True``, the loop will run, and it will stop when the input makes the test evaluates to ``False``.

.. note:: The while statement is well suited for input error checking.

Input error checking
--------------------
The while statement is well suited for **input error checking**. The program continues to prompt the user until a valid value is introduced. We have to design our test in a way that when the user enters a proper value, it evaluates to ``False``, and the loop terminates allowing the program to continue. For example:

.. literalinclude:: /code/chap_7/while_pwd.py
    :language: python
    :linenos:
    :tab-width: 4

* In line **1** we have assigned the string ``'1234'`` to the ``pwd`` variable.
* In line **2** the program ask for user's input.
* Lines **4** and **5** contain a while loop. As long as the user's doesn't type the right password the loop will run over and over again. How many times? It will depend on the users input.

break, continue, pass, and the Loop else
========================================
There are some statements used inside loops:

* ``break`` Jumps out of the closest enclosing loop (past the entire loop statement)
* ``continue`` Jumps to the top of the closest enclosing loop (to the loop’s header line)
* ``pass`` Does nothing at all: it’s a placeholder we use, when we do wan't to write any statement.
* ``else`` Yes, else clauses are optional inside loops.

General Loop Format
-------------------
Let's start nesting ``break`` and ``continue`` statements inside our loops. This is what the general format looks like::

    while test: 
        statements
            if test: break
            if test: continue 
        else:
            statements

``break`` and `continue` statements can appear anywhere inside the ``while`` or ``for`` loop’s body, but they are usually coded further nested in an **if test** to take action in response to some condition.

pass
----
The pass statement is a **do-nothing** placeholder that is used when the syntax requires a statement, but we don't want to write one. It is often used to code an empty body for a compound statement.  ``pass`` is roughly to statements as ``None`` is to objects: an explicit nothing.

For instance, if you want to code an **infinite loop** that does nothing each time through, do it with a pass::

    >>> while True: pass

This example does nothing forever, to step out of it press ``Ctrl-C``.

.. tip:: We'll see ``pass`` also used to ignore **exceptions** caught by ``try`` statements, and to define empty class objects

A pass is also used to stub out the bodies of functions temporarily::

    def func1():
        pass        # Add code later
    def func1(): 
        pass

We can’t leave the body of a function empty, without getting a syntax error, so we use ``pass`` to avoid it.

.. tip::
    In the case of compound statements, the body can be written on the same line as the header, after the colon, if the body is a single statement::

        def func1(): pass

Ellipses
--------
An **ellipsis** is a series of dots that usually indicates an intentional omission of a word, sentence, or whole section from a text. In Python, from version 3 we can use them too, and their role is the same as ``pass`` and also can be used as ``None``. 

For example::


    def func1():
        ...             # Add code later
    
    def func2(): ...    # Can also appear on same line

We can use Ellipses to initialize variable names if no specific type is required::

    >>> X = ... # Alternative to None 
    >>> X
    Ellipsis

Continue
--------
The ``continue`` statement causes an immediate jump to the top of a loop.

.. todo:: Finish it

Break
-----

.. todo:: Finish it




