*************
``for`` loops
*************
The for loop is a **generic iterator** in Python: it can step through the items in a sequence or other iterable object.

General Format
==============
The Python for loop begins with a header line that specifies an assignment target (or targets), along with the object you want to step through. The general format of a ``for`` loop is::

    for target in iterable: 
        statements

Like in any other compound statement, we have a header and a suite:

1. The **header** start with the ``for`` keyword, followed by a **target** or list of targets. A target is is usually a new variable in the scope where the for statement is coded. In the first pass, the **target** is assigned to the *first element* in the iterator, and it will automatically be set to the *next item* in the sequence when control returns to the top of the loop again. After the loop this variable normally still refers to the last item visited, which is the last item in the sequence.

2. The **suite**. The header is followed by an indented block of statements that we want to repeat.
   

Optional ``else``
-----------------
The for statement also supports an **optional** ``else`` block, which works exactly as it does in a ``while`` loop—it’s executed if the loop exits without running into a break statement. The ``break`` and ``continue`` statements also work the same in a ``for`` loop as they do in a ``while``. The for loop’s complete format can be described this way::

    for target in object: statements
        if test: break      # Exit loop now, skip else
        if test: continue   # Go to top of loop now
    else:
        statements          # If we didn't hit a 'break'

Examples
========
In our first example,  we’ll assign the name ``x`` to each of the three items in a ``list``, from left to right, and the print statement will be executed for each::

    >>> for x in ["spam", "eggs", "ham"]: 
    ...     print(x)
    ...
    spam 
    eggs 
    ham

The next example computes the sum of a series of items in a **list** ::

    >>> sum = 0
    >>> for x in [1, 2, 3, 4]:
    ...     sum += x        # sum = sum + x
    ...
    >>> sum
    10

In the next one we calculate the product of a **list** of numbers. We write the suite in the same line as the header::

    >>> prod = 1
    >>> for item in [1, 2, 3, 4]: prod *= item
    ...
    >>> prod
    24

Iterating over other data types
-------------------------------
Apart from lists, we can use other sequences as iterators. We can iterate over **strings** ::

    >>> S = 'Spam'
    >>> count = 0
    >>> for item in S: letters += 1
    ...
    >>> print(S, 'has', count, 'letters.')
    'Spam has 4 letters.'

Or over tuples::

    >>> T = ('John', 'Sue', 'Jack')
    >>> for item in T: 
    ...     if item != T[-1]:           # All except last item     
    ...         print(item, end=', ')   # No new character line        
    ... else: print(item)               # After last item, new line
    ... 
    John, Sue, Jack

``for`` loops can even work on some objects that are not sequences, like **files** and **dictionaries**.

Tuple assignment in for loops
-----------------------------
When we are iterating over a **list** of tuples, the target can be also a tuple. This is just another case of the tuple-unpacking assignment. The for loop assigns items in the sequence object to the **target**. For example ::

    >>> T = [(1, 2), (3, 4), (5, 6)]
    >>> for (a, b) in T:        # Tuple assignment at work
    ...     print(a, b) 
    ... 
    12 34 56

Here, the first time through the loop is like writing ``(a,b) = (1,2)``, the second time is like writing ``(a,b) = (3,4)``, and so on.

.. note:: This form is commonly used in conjunction with the ``zip`` call to implement parallel traversals. It's also used in  **SQL databases** in Python, where query result tables are returned as sequences of sequences like the list used here—the outer list is the database table, the nested tuples are the rows within the table, and tuple assignment extracts columns.

* To access the value in a dictionary, we can loop through the **keys**, and use them as indexes to fetch the **values** ::

    >>> D = {'a': 1, 'b': 2, 'c': 3} 
    >>> for key in D:
    ...     print(key, '=>', D[key])        # Use dict keys iterator and index
    ...
    a => 1 
    c => 3 
    b => 2

* The ``dict.items()`` **dictionary** method, returns all the items of a dictionary as a list of tuples::
  
    >>> list(D.items())                 # D.items() return each item as tuple 
    [('a', 1), ('c', 3), ('b', 2)]

Knowing that, we can use **tuples as targets**, and pair up each target iteration, with the tupled items that the ``items()`` method returns:: 

    >>> for (key, value) in D.items(): 
    ...     print(key, '=>', value)
    ...
    a => 1
    c => 3 
    b => 2

* Using tuples as targets is not our only option. We can also 
  
    >>> T
    [(1, 2), (3, 4), (5, 6)]
    >>> for both in T:
    ...     a, b = both 
    ...     print('%s - %s' % a, b) 
    ...
    1 - 2 
    3 - 4 
    5 - 6

Python 3.X extended sequence assignment in for loops
----------------------------------------------------

.. todo:: Come back after reading assignments

Nested for loops
----------------
In the next example, we are gonna use nested for loops to find some objects in a sequence::

    >>> items = ['spam', 3.14, (4, 5), 'B']
    >>> x = [(4, 5), 'C']
    >>> for i in x:
    ...     for item in items:
    ...         if i == item:
    ...             print(i, 'was found.')
    ...             break
    ... else: print(i, 'not found.')
    ...
    (4, 5) was found.
    C not found.

This example is illustrative of the use of nested loops, but we don't need the inner loop if we employ the in operator to test membership::

    >>> for i in x:
    ...     if i in items:
    ...         print(i, 'was found.')
    ...     else:
    ...         print(i, 'not found.')
    ...
    (4, 5) was found.
    C not found.

Why You Will Care: File Scanners
--------------------------------

.. todo:: Come back after reading file line iterators