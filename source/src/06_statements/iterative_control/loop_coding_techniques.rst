Loop coding techniques
======================
The ``for`` loop is generally simpler and faster to code than a ``while`` loop. Thanks to the **iteration tools** that Python offers, we shouldn't use **counters** in our ``for`` loops, like in other programming languages ``for (i = 0; i < 10; i++)``.

There are situations where we need to iterate in specific ways, like for example every second or third item in a list, or change the list along the way, or traversing more than one sequence in parallel, etc. For this and more situations, Python provides a set of built-ins that allow you to specialize the iteration in a for:

* The built-in ``range`` function.
* The built-in ``zip`` function.
* The built-in ``enumerate`` function.
* The built-in ``map`` function.

Counter Loops: ``range``
------------------------
The function ``range``, is a general tool that can be used in a variety of contexts, we can use it anywhere we need a series of integers, but most often it's used to generate indexes in a ``for`` loop.
In **Python 2.X** range creates a temporary **list**::

	>>> range(5)
	[0, 1, 2, 3, 4]

In **Python 3.X**, range is an **iterable** that generates items on demand, so in 3.X we need to wrap it in a ``list()`` call to display all of the results it produces::

	>>> list(range(5))
	[0, 1, 2, 3, 4]

With **1 argument**, range generates a list of integers from zero up to but not including the argument’s value.  With **2 arguments**, the first is taken as the lower bound::

	>>> list(range(1, 5))
	[1, 2, 3, 4]

We can pass another optional third argument to specify a **step**. If we don't specify one, the default step is **1**, that's why the results are consecutive numbers. But we can set the distance between results to whatever we want::

	>>> list(range(0, 20, 2))
	[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]

Ranges can be **descending** like for example::

	>>> list(range(10, 0, -1))
	[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

And **negatives** integers::

	>>> list(range(-5, 0))
	[-5, -4, -3, -2, -1]

Although ``range`` may be useful in different situations, it's mostly used within ``for`` loops, where they provide a simple way to repeat an action a specific number of times::

	>>> for i in range(3):
	... 	print(i, 'Pythons') 
	...
	0 Pythons
	1 Pythons
	2 Pythons

Sequence Scans: ``while`` and ``range`` Versus ``for``
------------------------------------------------------
The easiest and generally fastest way to step through a sequence exhaustively is always with a simple ``for``, as Python handles most of the details for you::

	>>> T = ('John', 'Sue', 'Jack')
	>>> for item in T:
	... 	print(item)
	...
	John
	Sue
	Jack

The example above is not our only option, we can achieve the same using:

* We can also use a ``while`` loop for doing the same, but we need to code the indexing logic ourselves::

	>>> i = 0				# Counter
	>>> while i < len(T):	# len(T) == 3
	... 	print(i)
	... 	i += 1
	...
	John
	Sue
	Jack

* Another possibility is using ``for``, with a combination of the functions ``range`` and ``len``, to create our own indexing logic::
  
	>>> T = ('John', 'Sue', 'Jack')
	>>> len(T)
	3
	>>> list(range(len(T)))		# That's our indexing logic
	[0, 1, 2]
	>>> for i in range(len(T)):
	... 	print(i)
	...
	John
	Sue
	Jack

Both solutions above work, but if you compare them with a simple ``for`` in combination with an ``in`` operator, they're both an overkill::

	>>> for item in T: print(T)
	... 
	John
	Sue
	Jack

The simpler solution is always the better, well, almost. There are some situations where we'll have to resource to the ``range`` / ``len`` combination.

Sequence Shufflers: ``range`` and ``len``
-----------------------------------------

.. todo:: Follow here

Nonexhaustive Traversals: ``range`` Versus Slices
-------------------------------------------------

Changing Lists: ``range`` Versus Comprehensions
-----------------------------------------------

Parallel Traversals: ``zip`` and ``map``
----------------------------------------

Generating Both Offsets and Items: ``enumerate``
------------------------------------------------



