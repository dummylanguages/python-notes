*****************
The if statement
*****************
The simplest form of selection is the **if statement**. Let's start right away with an :download:`example <../../../code/chap_7/confirm.py>`:

.. literalinclude:: ../../../code/chap_7/confirm.py
	:language: python
	:linenos:
	:emphasize-lines: 5-7
	:tab-width: 4

First of all, the **if statement** (as well as the rest of control statements) belong to the category of **compound statements**. In this example, lines **6** and **7** are simple statements(function calls) embedded in the **if statement**.

.. note::
	Statements that contain other statements are known as a **compound statements**.

Header
======
Line **5** is called the **header**:

.. literalinclude:: ../../../code/chap_7/confirm.py
	:language: python
	:linenos:
	:emphasize-lines: 5
	:tab-width: 4

It has 3 parts:

* It starts with the keyword ``if``
* The **test**, in this case: ``answer == 'Y'``
* It finishes with a semicolon ``:``
 
.. warning::
	Do not forget the semicolon, ``:``, at the end of headers.

The test
--------
The test or condition, is always a **boolean expression** with only two possible values (``True`` or ``False``) In this example the result of the test depends on the answer that the user inputs, and that it's stored in the variable ``answer``:

1. If the value of ``answer`` is **Y**, then the expression evaluates to ``True``, and the program will execute the 2 ``print()`` statements.

2. But if the value of ``answer`` is not **Y**, then the expression evaluates to ``False`` and the program jumps out of **if statement** to the following next statement. 

This type of control statement is also called a **one-way selection** statement, because it offers us one alternative execution path for our program, one way of changing its natural flow.

The suite
=========
Lines **6** and **7**, are called the **suite**. A suite is a group of statements controlled by the header. This block of statements are executed when the condition in the header is ``True``.  

.. literalinclude:: ../../../code/chap_7/confirm.py
	:language: python
	:linenos:
	:emphasize-lines: 6-7
	:tab-width: 4

In this case, the suite is only two lines of code, but in more complex programs, suites may contain many lines of code, that's why suites are also known as **blocks**.

.. note::
	The group of statements following a header is called a **suite**, commonly called a **block**.

In case you haven't notice, the block is indented by 4 spaces, and the end of the indentation means the end of the block. 

Indentation
-----------
In most programming languages, **blocks** are created using *curly braces*, ``{ }``, all of the statements inside the curly braces belong to the same block. Then, the blocks are indented to make them more readable to the human eye.

.. literalinclude:: ../../../code/chap_7/if_C
	:language: c
	:linenos:
	:tab-width: 4

In python, it was decided to make use of **just indentation** and not curly braces to create blocks. The rationale behind that was that blocks are gonna be indented anyways, so why add verbose characters to the mix?

.. literalinclude:: ../../../code/chap_7/if_Python
	:language: python
	:linenos:
	:emphasize-lines: 2-3
	:tab-width: 4

While in most programming languages, **indentation** has no effect on program logic (it is simply used to improve readability), in Python, however, indentation is **part of the syntax**, and when is not used properly, a **syntax error** will result. 

But don't worry, the rules of indentation are very simple:

* The statements of a given **block** must all be indented the same amount. Check that in lines **2** and **3**.

* **Headers** that are part of the same compound statement must be indented the same amount.

Python doesn’t care if you indent using **spaces** or **tabs**, or how much you indent (you may use any number of spaces or tabs), but the `PEP8 style guide <https://www.python.org/dev/peps/pep-0008/>`__ recommends the use of **4-space** indentation, and **no tabs**. 4 spaces are a good compromise between small indentation (allows greater nesting depth) and large indentation (easier to read). **Tabs** introduce confusion, and are best left out.

.. note:: For Python, `PEP8 <https://www.python.org/dev/peps/pep-0008/>`__ has emerged as the style guide that most projects adhere to; it promotes a very readable and eye-pleasing coding style.

While **four spaces** is commonly used for each level of indentation, any number of **spaces** may be used, as long as they are used **consistently**.

Syntax comparison
=================
To fully understand the different syntax that python uses, we could start comparing an **if statement** written in python, against one written in C like language, such as in C, C++, Java, JavaScript, or similar.
In a C-like language we would write:

.. literalinclude:: ../../../code/chap_7/if_C
	:language: c
	:linenos:
	:tab-width: 4

.. warning:: 
	The code above is **not** python.

Now let's write the same statement in python:

.. literalinclude:: ../../../code/chap_7/if_Python
	:language: python
	:linenos:
	:tab-width: 4

In python:

* We do add a colon ``:`` to the end of header.
* We do not use curly braces, just indentation.
* The end of the indentation means that the block has finished.   
* **Parentheses** around the tests are **optional**. We can use them though, but it is not **the python way**.
* Finishing statements with semicolons, ``;`` is not required either. In python end-of-line means the end of the statement.
