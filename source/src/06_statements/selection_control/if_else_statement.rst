The if-else statement
=====================
Sometimes, one-way selection is not enough. In these cases we can take it up a notch and use **two-way selection**. We achieve that, using **if-else** statements, resulting in two additional execution paths available for our program. For example, take a look at this :download:`script <../../../code/chap_7/pwd_control.py>`:

.. literalinclude:: ../../../code/chap_7/pwd_control.py
	:language: python
	:linenos:
	:emphasize-lines: 6-
	:tab-width: 4

* Line **3** is just a variable, ``pwd``, that stores the correct password, 
* Line **4** asks the user for her password and stores it in the variable ``login``.
* Lines **6** to **9** are the important ones here. These lines together form an **if-else statement**, all of them belong to the same compound statement. 

.. note::
	Again, a statement that contains other statements is called a **compound statement**.

Clauses
-------
Compound statements consist of one or more **clauses**. An **if-else statement** is made up of 2 clauses:

* The **if clause**
* The **else clause** 
  
A clause consists of a **header** and a **suite**.  Let's look at the first clause in our :download:`example <../../../code/chap_7/pwd_control.py>`:

.. literalinclude:: ../../../code/chap_7/pwd_control.py
	:language: python
	:linenos: 
	:emphasize-lines: 6-7
	:tab-width: 4

No big deal, the **first clause** is an **if statement**: a header and its one-liner suite. Remember, the end of the indentation in line **8**, means the end of the block and thus the end of the statement above it. After we have the **else clause**.

The ``else`` clause
-------------------
An else clause has its own **header**, that consist only of the keyword ``else``. No conditions are allowed in else clauses.

.. literalinclude:: ../../../code/chap_7/pwd_control.py
	:language: python
	:linenos:
	:emphasize-lines: 8-
	:tab-width: 4

.. note:: 
	A **header** in Python is a specific keyword followed by a colon ``:``

An ``else`` clause also includes a suite, that is executed when the condition in the ``if`` clause is not met. In our example, the suite would be the last line:

.. literalinclude:: ../../../code/chap_7/pwd_control.py
	:language: python
	:linenos:
	:emphasize-lines: 9
	:tab-width: 4

The ``else`` clause is optional, we don't have to use an **else clause** in every **if statement**, our first example is proof of that. In fact there are a lot of ocassions when they are not gonna be needed.

.. note:: ``else`` clauses are optional
