*******************
Multi-way selection
*******************
Frequently, our programs are gonna need more than only two paths of execution, in other words, **two-way selection** is not enough. **Multi-way selection** can be implemented with **if statements** in two ways:

* Nesting **if statements**. 
* With **elif statements**.

Nested if statements
====================
Let's see an example of nesting:

.. literalinclude:: ../../../code/chap_7/grades.py
	:language: python
	:linenos:
	:tab-width: 4

The nested if statements on the right result in a 5-way selection. The final else clause is executed only if all the previous conditions fail, displaying ``'Grade of F'``. This is referred to as a **catch-all case**.

The ``elif`` statements
=======================
Every **if-else statement** may contain only one else statement. Thus, if-else statements must be nested to achieve multi-way selection. 
Python, however, has another statement called ``elif`` (short for else-if) that provides multi-way selection in a single if-else statement, avoiding the deeply nested levels of indentation with the use of if-else statements.
For example, let's *refactor* (rewrite) the last program using ``elif`` statements:

.. note:: **Refactoring** means restructure code so that the design of a program is clearer.

.. literalinclude:: ../../../code/chap_7/grades_2.py
	:language: python
	:linenos:
	:tab-width: 4

In the :download:`refactored script <../../../code/chap_7/grades_2.py>`, all the headers of an **if-elif-else** statement are indented the same amount, thus avoiding the deeply nested levels of indentation with the use of if-else statements. 

A final else clause may be used for “catch-all” situations or when specifying a **test** is not necessary. (we could have also used ``elif grade < 60:``). 

The nonexistent ``switch`` statement
====================================
There is no ``switch`` or ``case`` statement in Python that selects an action based on a variable’s value. There have been a couple of proposals in  `PEP <https://www.python.org/dev/peps/>`__ (Python Enhancement Proposals), but they have been rejected (`275 <https://www.python.org/dev/peps/pep-0275/>`__, `3103 <https://www.python.org/dev/peps/pep-3103/>`__).

Instead we can use **if/elif** tests, or try different solutions that people have come up with. One of them is using dictionaries or lists. For example:

.. literalinclude:: ../../../code/chap_7/switch_dict.py
	:language: python
	:linenos:
	:tab-width: 4

Because dictionaries and lists can be built at runtime dynamically, they are sometimes more flexible than hardcoded if logic in your script. An almost equivalent but more verbose Python if statement might look like the following:

.. literalinclude:: ../../../code/chap_7/switch_elif.py
	:language: python
	:linenos:
	:tab-width: 4

the potential downside of an if like this is that, you cannot construct it at runtime as easily as a dictionary. In more dynamic programs, data structures offer added flexibility.

.. todo::
	Complete with pages 372 to 374 of LP. See also:
	`this <http://blog.simonwillison.net/post/57956755106/switch>`__
	`this <http://stackoverflow.com/questions/374239/why-doesnt-python-have-a-switch-statement?lq=1>`__
	`and this <http://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python>`__
	Python cookbook, has also some recipes.


