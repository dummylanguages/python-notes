.. _assignment-statement:

**********************
Assignments statements
**********************
A basic assignment statement is made of a **target** on the left of an equals sign, and the object to be assigned on the right::

	>>> a = 3

The target on the left may be a name or object component, and the object on the right can be an arbitrary expression that computes an object::

	>>> Bob.pay = 12000
	>>> diam = rad * 2

Assignments are straightforward, but here are a few properties to keep in mind:

* Assignments always create **references to objects** instead of copying the objects. Assignments are more like tags that we put on objects, than boxes containing objects.
* **Names are created when first assigned**. Python creates a variable name the first time you assign it a value. Once assigned, a name is replaced with the value it references whenever it appears in an expression.
* **Names must be assigned before being referenced**. Python raises an exception if you try to use a name to which you haven’t yet assigned a value.
* Some operations perform assignments implicitly. Module imports, function and class definitions, for loop variables, and function arguments are all **implicit assignments**.

Types of assignments
====================
Assignments occur in a lot of places in Python, a lot of times implicitly. In this section we are gonna take care of assignment statements in all of their forms. The following table contains a small reference:

+----------------------------------+----------------------+
| Assignments statements           |                      |
+==================================+======================+
| Basic form                       | a = 3                |
+----------------------------------+----------------------+
| Tuple assignment (positional)    | x, y = 3, z          |
+----------------------------------+----------------------+
| List assignment (positional)     | [x, y] = [3, z]      |
+----------------------------------+----------------------+
| Sequence assignment, generalized | x, y, z = None       |
+----------------------------------+----------------------+
| Extended sequence unpacking      | a, \*b = 'spam'      |
+----------------------------------+----------------------+
| Multiple target assignment       | spam = ham = 'lunch' |
+----------------------------------+----------------------+
| Augmented assignment             | a += 1               |
+----------------------------------+----------------------+

Basic assignments
-----------------
The first form in is by far the most common: binding a name (or data structure component) to a single object. We could get all your work done with this basic form alone. The other table entries represent special forms that are all optional, but that programmers often find convenient in practice.

Sequence assignments
--------------------


Advanced sequence assignment patterns
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Extended Sequence Unpacking in Python 3.X
-----------------------------------------

Multiple-Target Assignments
---------------------------

Augmented Assignments
---------------------

Variable Name Rules
-------------------