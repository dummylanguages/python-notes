*********************
The flow of a program
*********************
By default, the code in a program is executed from *top to bottom* and from *left to right*, this is called the **natural flow** of a program. In other words, a program is executed the same way as you read it, **sequentially**. This kind of programs are known as **straight-line programs**, and in practice, very few programs are as simple as that. 

Truth is, that most programs make use of **control flow statements**, statements that change the course of action of a program depending on the value of a boolean expression. 

We can control the flow of a program in three different ways:

1. **Sequential control**, the implicit form of control, in which instructions are executed in the order that they are written.
2. **Selection control** is provided by a *selection control statement* that selectively executes instructions, depending on the value of a test.
3. **Iterative control** is provided by an *iterative control statement* that repeatedly executes instructions. Allow the computer to repeat an action, depending on the value of a condition.

Collectively a set of instructions and the control statements controlling their execution are called a **control structure**.

.. note:: Control statements belong to the category of **compound statements** (statements that embed other statements).
