*****************
Selection Control
*****************
Now that we know how **conditions** (aka **tests**) work, we are ready to learn how we can control the flow of our programs. We are gonna start with **selection control statements**, those that provide alternative paths of execution to our programs, instead of the unique one provided in **straight-line** program.

Here we are gonna look at the following compound statements:

  * The **if** statement
  * The **if-else** statement
  * Multiway selection using either **if-else** or **elif** statements

.. toctree::
	:maxdepth: 3
	:hidden:

	selection_control/if_statement
	selection_control/if_else_statement
	selection_control/multiway_selection