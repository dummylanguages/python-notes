.. sidebar:: Table of contents.

	.. contents::
		:depth: 3 

.. _conditions:

**********
Conditions
**********
Before start talking about control statements, let's see what are the conditions they depend on. A **condition** or **test**, is an expression that always evaluates to a **boolean value**. **True**, or **False** are the two only possible values of the **boolean data type**.

.. note::
	Conditions (a.k.a. tests) are **boolean expressions**

Relational operators
====================
These operators are used to make comparisons between values, that's why they are also known as **comparison operators**. Those expressions that make use of **relational operators** are called **relational expressions**. Relational expressions always evaluate to a **boolean value**, but they are not the only kind of expressions that evaluate to a boolean. That's why relational expressions are considered a subtype of **boolean expression**.

.. note:: 
	A **boolean expression** is an expression that evaluates to a **boolean value**.

The following table contains the whole list of relational operators that we can use in python:

+--------------------------+--------+-------------+--------+
| Relational operators     | Symbol | Example     | Result |
+==========================+========+=============+========+
| equal to                 | ``==`` | ``2 == -2`` | False  |
+--------------------------+--------+-------------+--------+
| not equal                | ``!=`` | ``2 != -2`` | True   |
+--------------------------+--------+-------------+--------+
| less than                | ``<``  | ``3 < 2``   | False  |
+--------------------------+--------+-------------+--------+
| less than or equal to    | ``<=`` | ``3 <= 3``  | True   |
+--------------------------+--------+-------------+--------+
| greater than             | ``>``  | ``3 > 2``   | True   |
+--------------------------+--------+-------------+--------+
| greater than or equal to | ``>=`` | ``2 <= 3``  | False  |
+--------------------------+--------+-------------+--------+

The **Python's shell** comes in handy to try some of these examples::

	>>> a = 3
	>>> b = 4
	>>> a == b
	False

In the code above, we assign values to the variables ``a``, and ``b``. We use the **assignment operator**, a single equal sign, ``=``. The third line is a relational expression that compares the values of a, and b. In this case we are using the **equal to** operator, two equal signs, ``==``. A common error among newbies, is using only one equal sign (``=``), when they want to compare if two values are equal to each other.

Membership operators
====================
Python includes a couple of interesting operators to check if a particular value exists within a list of values. The resulting expression of using membership operators always evaluates to a **boolean type**. 

+------------+------------------+-------------------------+--------+
| Operators  | Meaning          | Example                 | Result |
+============+==================+=========================+========+
| ``in``     | is a member?     | ``'el' in 'Hello'``     | True   |
+------------+------------------+-------------------------+--------+
| ``not in`` | is not a member? | ``'He' not in 'Hello'`` | False  |
+------------+------------------+-------------------------+--------+

The membership operators are specially useful with **data structures** such as **lists**, **tuples** or **dictionaries**. We haven't seen any of these data types yet, but we have already seen a simple data structure that should sound very familiar to you, **strings**.

.. note::
	A string is a sequence of zero or more characters ordered linearly (hence the name, string). 

With the membership operator, we can check if some character or characters are present in some string. For example:

.. code-block:: python

	>>> full_name = 'John Smith'
	>>> 'Jo' in full_name
	True
	>>> 'Smith' not in full_name
	False

* In the first line we create the variable ``full_name``, its type is string. 
* In the second line we check if the substring ``'Jo'`` is a member of ``full_name``, and indeed it is. 
* Then we check if the substring ``'Smith'`` *is not a member* of ``full_name``, the result is **False**, because ``'Smith'`` obviously *is a member* of ``full_name``.


.. sidebar:: Boolean algebra

	Boolean algebra was created around 1850 by **George Boole**, the father of the "algebra of logic" tradition.

.. todo::
	Identity operators

Boolean operators
=================
Sometimes one **relational expression** is not enough, and we need to combine two or more of them to create a condition. We can do that thanks to the boolean operators. Again, **boolean expressions** always evaluate to a boolean type, ``True`` or ``False``.

The list of boolean operators is very short, there are only three of them:

* ``and``, only true when both of its operands are true.
* ``or``, true when any of its operands is true.
* ``not``, reverses truth values: ``not True == False`` and ``not False == True``.

Let's see some examples::

	>>> True and True
	True
	>>> True and False
	False
	>>> True or False
	True

In the code above, we just tried some boolean literals directly. ``True`` and ``False`` are never quoted, doing so would mean that python would treat them as simple strings.::

	>>> num = 4
	>>> (2 + 2 == num) and (num < 3 * 3)
	True

Here we just wrote a **boolean expression** in which we are testing two **relational expressions**. The expression evaluates to ``True``, since both of its operands are ``True``. This example shows how operator precedence works. 

+----------------------------------------------------------------+
| Operator precedence. The Higher position the higher precedence |
+================================================================+
| Arithmetic operators                                           |
+----------------------------------------------------------------+
| Relational operators, membership and indentity tests           |
+----------------------------------------------------------------+
| Boolean operators                                              |
+----------------------------------------------------------------+

Let's analyze it step by step:

1. First of all, the **arithmetic subexpressions** are evaluated:

	* ``2 + 2`` evaluates to 4.
	* ``3 * 3`` evaluates to 9. 

After the arithmetic expressions are evaluated we get this expression: ``(4 == num) and (num < 9)``

2. Then the **relational subexpressions** are evaluated:
 
	* ``(4 == num)`` evaluates to ``True``.
	* ``(num < 9)`` evaluates to ``True``.

After this we have just the boolean expression: ``True and True``

3. Finally, the above **boolean expression** evaluates to ``True``.

Lazy evaluation
===============
There are differences in how Boolean expressions are evaluated in different programming languages. In Python is used lazy evaluation, also known as **short-circuit evaluation**.
For example::

	>>> True and False and True and True
	False

The boolean expression above, is composed of 4 subexpressions. As we know, when we have an ``and`` operator, all the operands have to be ``True``. In this case, the second operand is false, so python stops right there at the second operand, what's the point of going on, if the expression is gonna evaluate to ``False`` anyways.

It works the same with the ``or`` operator, as long as only one of the operands is ``True``, the expression is always gonna evaluate to ``True``::

	>>> True or False or False or False
	True

Python stops right after evaluating the first subexpression, the result is ``True``.

.. todo::
	 Truth value testing in the `python standard library <https://docs.python.org/3.4/library/stdtypes.html>`__

