**************
Modular design
**************
`Modular design`_ allows large programs to be broken down into smaller size parts, easier to manage. Each part is known as a module, and has a clearly specified functionality. Modular software design helps with:

* **Maintenance and modification**: Separating functionality across modules makes easier to solve problems and add new features. We just have to locate the modules responsible for the feature we want to fix or modify.
* **Distributed development**: We can develop separately different functionalities of a bigger program, asigning the development of the modules that implement a given functionality to an individual or team.
* **Testing**: It allows modules to be individually developed and tested, and eventually integrated as a part of a complete system.

Python supports this **modular approach** by offering mechanisms such as **modules** and **packages**. In practice, a Python program will consist of a bunch of interconnected files, each of these files being a module. We can also organize our modules into folders, also known as packages.

.. note:: A **module** is simply a **file** containing Python code.

To interconnect the files, we use the ``import`` statement, which allows us to have easy access to code defined in other modules. Organizing our code in separate files is essential when programs start to grow in size.

The main module
===============
Consider the following script named :download:`a_script.py </code/modules/ex01/a_script.py>`:

.. literalinclude:: /code/modules/ex01/a_script.py

We are just printing a short string, using the special word `__name__`_. This is a `special attribute`_ of modules (and other object types) which evaluates to the **name of the module**, which usually is the module filename withouth the ``.py`` extension. But if we run it, we get::

    $ python a_script.py 
    I'm a script, and my module-name is __main__

In the case of **scripts** Python assigns them the special name ``__main__``, because the interpreter considers scripts as the **main module** in a program.

The ``import`` statement
========================
Let's create the following structure::

    ex02/
        another_script.py
        module_x.py

The contents of :download:`another_script.py </code/modules/ex02/another_script.py>` are as follow:

.. literalinclude:: /code/modules/ex02/another_script.py

In the first line, we are using the ``import`` statement to import a module named ``module_x``. Note that we are using the name of the module without the ``.py`` extension.

.. note:: To import a module we have to use its **filename** without the ``.py`` extension. 

This module contains the following code:

.. literalinclude:: /code/modules/ex02/module_x.py

The important lesson here is that we can put code in some file, and import it in another file, so we can use it. In this silly example we are importing a small function with 2 lines, but imagine how useful this can be to organize a bigger code base.

Types of modules
================
If we run the **script** of the section above::
    
    $ python another_script.py 
    I'm a script, and my module name is __main__
    I'm a module and my name is module_x

The special variable ``__name__`` gets two values depending on which file it is used:

1. The special name ``__main__``, which corresponds to our script, the **main module**.
2. The **name** of the **module** that we are importing in our script, in this case ``module_x``.

We can make a difference between 2 types of modules:

1. The **script** is the file that we run to launch our application. Python considers it as the **main module**, giving it the special name ``__main__``. Most of the times, scripts import any number of other modules, in order to gain access to the code they contain. But **scripts** themselves are not meant to be imported into other modules, they're meant to be run.

2. The modules we import work as **libraries**: they contain code that can be reused by the main module or by other modules. Module libraries generally don’t do anything when run directly, they just define tools intended to be imported by other modules.

.. _`__name__`: https://docs.python.org/3/reference/import.html#__name__
.. _`special attribute`: https://docs.python.org/3/library/stdtypes.html#special-attributes