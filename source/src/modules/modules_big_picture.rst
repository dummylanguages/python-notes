**************************
Modules in the big picture
**************************
It could be useful to look at the python conceptual hierarchy to see exactly where the concept of modules fits in. 

1. Programs are composed of modules.
2. Modules contain statements.
3. Statements contain expressions.
4. Expressions create and process objects.

We can see that modules are the highest-level program organization unit in Python. 
