.. _`importing-modules`:

*****************
Importing modules
*****************
Before starting getting deeper into the ``import`` statement we should explain what is a module. We've already said that a module is simply a file with Python code, but that's not the whole story.

Symbol table
============
When a module is in memory, is implemented as a **symbol table** that contains all the **names** defined in the module. The import statement brings all the definitions that this symbol table contains into the caller's namespace.

**Namespaces** are implemented using symbol tables, but symbol tables are used for more than just namespaces. For example, **functions** have their own symbol table for local variables, but those variables do not exist in any namespace (that is, it is impossible to somehow access the local variables of a function using dot-notation).

Ways to use the ``import`` statement
====================================
The ``import`` statement can be used in several different ways:

* We can import the **module** itself::

    import module

* We can import one or several of the **definitions** contained in it::

    from module import definition1, definition2

* We can create **aliases** for **modules**::

    import module as another_name

 And for **definitions**::

    from module import def1 as foo

* Or we can import **all** of the definitions it contains::

    from module import *

Depending on how we use the ``import`` statement, we'll have access to the definitions in two ways:

1. Prepending the module's name to the definition we want to access using **dot notation**.
2. Simply using the definitions.


Let's see several examples of how we can import and use the code defined in a module.

Importing the module
--------------------
Consider the following module :download:`a_mod.py </code/modules/ex05/a_mod.py>` contains:

.. literalinclude:: /code/modules/ex05/a_mod.py

Let's open an interactive session to experiment a bit:

.. code-block:: python
    :emphasize-lines: 1

    >>> import a_mod
    >>> a_mod.f1()
    I'm a function in a_mod

Here we are using the ``import`` statement in its simplest form, importing only the module's **name** into the caller's **namespace**, in this case the interactive session. The module's name works like a door that gives us access to all the definitions that the module contains by using **dot notation**.

Here we are importing the module in an interactive session, if we were to write a file, it is customary but not required to place all import statements at the **beginning of a module** (or script, for that matter).

Listing and deleting names
^^^^^^^^^^^^^^^^^^^^^^^^^^
We can use the `dir() built-in function`_ to list all the definitions in the current namespace, for example::

    >>> dir()
    ['__builtins__', '__doc__', '__name__', '__package__', 'a_mod']

The `del statement`_ can be used to **delete** a name from the namespace where it's used, for example::

    >>> del a_mod
    >>> dir()
    ['__builtins__', '__doc__', '__name__', '__package__']

.. note:: Another practical way of deleting **all** the name definitions in an **interactive session** is just to ``exit()`` the session.

Import only some definitions
----------------------------
We can also selectively import some of the **definitions** that the module contains:

.. code-block:: python
    :emphasize-lines: 1

    >>> from a_mod import f1, f2
    >>> f1()
    I'm a function in a_mod

As you can see, the name definitions we import this way are directly available in the caller's namespace, without having to use **dot notation** at all.

Aliasing
--------
Using the ``as`` keyword in an import statement allows us to:

1. Import the **module** under an **alias**.
2. Import one or several **definitions** under different aliases.

* To **alias** a module we would do:

.. code-block:: python
    :emphasize-lines: 1

    >>> import a_mod as mod_x
    >>> mod_x.f1()
    I'm a function in a_mod

Here again we can access to all the definitions that the module contains by using **dot notation**, but this time we have to use the alias name as a prefix. Using the ``as`` keyword it's like changing the door's name.

* Using **aliases** with definitions works like this:

.. code-block:: python
    :emphasize-lines: 1

    >>> from a_mod import f1 as tomato1, f2 as apple3
    >>> apple3()
    I'm another function in a_mod

In both cases trying to use the module or the definition under the original names will throw an **error**.

.. _`dir() built-in function`: https://docs.python.org/3.7/library/functions.html#dir
.. _`del statement`: https://docs.python.org/3/reference/simple_stmts.html#del