.. _modules:

#######
Modules
#######

.. toctree::
	:maxdepth: 3
	:hidden:

	the_basics
	naming_conventions
	module_search
	search_path_example
	running_modules_as_scripts
	installing_modules
	importing_modules
	wildcard_imports
	modules_big_picture


Computer programs are one of the most complex structures ever created. A web browser like Firefox, at the moment of writing this, has more than 12 million lines of codes, and has taken an estimated of almost 4,000 years of effort in man hours. Did the developers put all that source code in a **single file**?

.. _`Modular design`: https://en.wikipedia.org/wiki/Modular_design