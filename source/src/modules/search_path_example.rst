********************************
A search path precedence example
********************************
Once we have written some code that we want to reuse, we may ask ourselves where should we put the module. These are our options:

1. Next to the script it uses it.
2. To some directory defined ``PYTHONPATH``.
3. We can also **package** the module, and install it either to the **global** or to any of the alternate locations available, such as the **user** site-packages directory.

We can have the same module in the 3 locations mentioned above at the same time. In the :ref:`Module Search section<module-search-precedence>` we've seen which locations are searched first.

For example, we have the following directory structure::

    ~/ex03/
        the_script.py
        the_module.py

The contents of :download:`the_script.py </code/modules/ex03/the_script.py>` are as follow:

.. literalinclude:: /code/modules/ex03/the_script.py
   :language: python

Whereas :download:`the_module.py </code/modules/ex03/the_module.py>` contains:

.. literalinclude:: /code/modules/ex03/the_module.py
   :language: python

Put a copy in the 3 places
==========================
Let's put a copy of ``the_module.py``:

1. Next to the script location. 
2. In a directory on the ``PYTHONPATH``
3. And also install a packaged version of the module to the **user** site-packages directory. (See the :ref:`Installing modules <installing-modules>` to learn how to do that)

Script directory wins
=====================
If we run the script at this point we get::

    $ python3 the_script.py 
    I'm a script, and I'm in: /Users/javi/code/ex03
    I'm a module in: /Users/javi/code/ex03

As you can see, the other two locations for the module are shadowed by the directory of the script.

PYTHONPATH rules
================
If we delete the module next to ``the_script.py`` and we run it::

    $ python3 the_script.py 
    I'm a script, and I'm in: /Users/javi/code/ex03
    I'm a module in: /Users/javi/code/python-modules

The module in PYTHONPATH shadows the one in the site-packages.

Finally site-packages
=====================
If we delete the module in PYTHONPATH and run the script::

    $ python3 the_script.py 
    I'm a script, and I'm in: /Users/javi/code/ex03
    I'm a module in: /Users/javi/Library/Python/3.7/lib/python/site-packages

Finally we can see the module in the **user** site-packages directory.

That's how the **precedence rules** for the search path work.
