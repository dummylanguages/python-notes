*************
Modules names
*************
There are some rules when naming **modules**; the `PEP 8 - Style Guide for Python Code`_ contains all we need to know about it.

Name vs filename
================
We have seen how in practice modules are just Python files with the ``.py`` extension. But we should differentiate between:

1. The module **filename**, which should end with the ``.py`` extension. The extension is optional for **scripts**, although using the extension will make the content of our scripts more obvious.

2. The module **name**, which consists of the module filename **without** the ``.py`` extension. When we are importing a module, we must use its **module name**, meaning the ``.py`` extension should **not** be used.

The rules
=========
When a module is imported, its name enters the **namespace** of our program, the same as any other definitions such as variables or functions. For this reason, they must follow the same :ref:`naming rules <naming-variables>` about being:

* Valid identifiers
* Not reserved keywords. 

Within these limits, we can choose any name we want, although it's advisable to choose **descriptive names** that inform about the purpose of the module. Apart from these common sense rules, modules should have:

* **Short** names.
* All **lowercase**. 
* **Underscores** can be used in the module name if it improves readability.

.. _`PEP 8 - Style Guide for Python Code`: https://www.python.org/dev/peps/pep-0008/#package-and-module-names

