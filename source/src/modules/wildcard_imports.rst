.. _`wildcard-modules`:

****************
Wildcard imports
****************
First of all, let's start saying that this type of imports is `discouraged in the documentation`_. But if you have to, you can import indiscriminately **all** the names defined in a module, using an **asterisk** (``*``), which works as a `wildcard charachter`_:

.. code-block:: python
    :emphasize-lines: 1

    >>> from a_mod import *
    >>> f1()
    I'm a function in a_mod
    >>> f2()
    I'm another function in a_mod

This will directly place **all** the names defined in ``a_module`` into the caller's namespace, so they can be used without the need of dot notation.

    >>> dir()
    ['__builtins__', '__doc__', '__name__', '__package__', 'f1', 'f2']

This type of import statement is a bit dangerous in large codebases, since we may be overwriting other existing names inadvertently.

Public API
==========
Sometimes modules contain definitions that implement internal functionality not intended to be used by the final user. Since module developers can't avoid users importing their modules using **wildcard** imports, better to have some ways to control what is imported when ``import *`` is used. 

Developers can approach this issue from 2 angles:

1. They can **hide** some definitions by preceding their names with an **underscore**, ``_name``.
2. Or they can explicitely determine what definitions will be **publicly available** by listing their names under the ``__all__``  **special variable**.

Hiding definitions
------------------
For example, consider the following :download:`a_mod.py </code/modules/ex06/a_mod.py>`:

.. literalinclude:: /code/modules/ex06/a_mod.py

If we import everything, we'll have access to **all** the definitions contained in the module::

    >>> from a_mod import *
    >>> f1()
    I'm a function in a_mod

With exception of the ones whose names start with an underscore, like ``_f2``::

    >>> _f2()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name '_f2' is not defined

Public names
------------
Another possibility is defining the ``__all__`` special variable, which will contain a list of strings with the names we want to be publicly accessible after a **wildcard** import. For example, consider the following :download:`mod.py </code/modules/ex07/mod.py>`:

.. literalinclude:: /code/modules/ex07/mod.py

As you can see, we've added the ``_f2`` to the list of **public names**. This variable should be hidden, but looks what happens when we try to use it::

    >>> from mod import *
    >>> _f2()
    I'm hidden

That means that the use of ``__all__`` to list public names, trumps the use of **underscores** to hide definitions. By the way, ``f3`` is not listed::

    >>> f3()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'f3' is not defined

.. _`discouraged in the documentation`: https://docs.python.org/3/tutorial/modules.html#more-on-modules
.. _`wildcard charachter`: https://en.wikipedia.org/wiki/Wildcard_character