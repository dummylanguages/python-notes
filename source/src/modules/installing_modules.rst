.. _installing-modules:

******************
Installing modules
******************
At early stages of development, we can keep our modules:

1. In the same directory where our script is.
2. In some directory defined in PYTHONPATH.

But eventually, we'll want to install them on our system, or even create distribution packages to make them easily available so other people can install them.

In this section, we are gonna explain how to install a module on our system, and even though during the process a **distribution package** is generated, we're not gonna focus on that. In :ref:`further sections <distributions>` we'll go deeper about `distribution packages`_. 

A small example
===============
Imagine we have a module named :download:`length.py </code/modules/ex04/length.py>` with the following content:

.. literalinclude:: /code/modules/ex04/length.py
   :language: python

And we found ourselves using these functions all the time, and copying the file into all of our projects. We could totally install it on our system, to keep cleaner our project directory, and not having to deal with the hassle of copying around the file.

A top level folder
------------------
We can start by creating a top level folder to hold our module, its name doesn't matter::

    $ mkdir ex04
    $ mv length.py ex04

A ``setup.py`` file
-------------------
Inside the ``ex04`` directory we'll create a file named :download:`setup.py </code/modules/ex04/setup.py>`, with the following contents:

.. literalinclude:: /code/modules/ex04/setup.py
   :language: python

.. note:: The option ``py_modules`` is needed when distributing top level file modules, that we don't want to push down inside a package directory.

The whole structure would look like::

    ex04/
        length.py
        setup.py

Installing it
-------------
At this point we can install the module::

    $ cd ex04 && pip install .

.. note:: Consider using the ``--user`` flag if you are not working inside a **virtual environment**.

The module is installed on our system and available to be imported in any of our projects. Behind the scenes, the above command generates a `wheel`_ distribution package and installs it in our **site-packages** directory. If we check that directory we'll find:

* The ``length.py`` file, the module itself.
* A ``length-1.0.dist-info`` folder, which contains severals **metadata** files:

    * ``INSTALLER``
    * ``METADATA``
    * ``RECORD``
    * ``top_level.txt``
    * ``WHEEL``

.. warning:: If yous project has some errors and you decide to make some edits to your code, and **uninstall/install** the project again, you may found that the corrections are not reflected and you get the same errors.

The error mention above is because starting with v6.0, pip by default `cache`_ the built wheels. The default location for the cache directory depends on the Operating System:

* **Unix** in ``~/.cache/pip`` and it respects the ``XDG_CACHE_HOME`` directory.
* **macOS** in ``~/Library/Caches/pip``.
* **Windows** in ``<CSIDL_LOCAL_APPDATA>\pip\Cache``

This is disabled using the ``--no-cache-dir`` option when installing the project::

    $ pip install --no-cache-dir .

Editable installs
-----------------
Most of the time, we install **modules** because we're developing a bigger project that makes use of them. At that stage we'll need to make edits to our installed module, and reinstall it after each change, which will become cumbersome quite fast.

It would be way more practical to install our local module in a way, so that any change we make to its source code is automatically reflected in the installed version. In other words, we install once, and all the edits we make are reflected by the installed module without having to reinstall it. There's a couple of ways we can achieve this.

.. note:: In the last section, a **wheel** file was created behind the curtains and installed. In this section, another type of distribution package, known as **egg**, will be also generated for us.

setuptools develop mode
^^^^^^^^^^^^^^^^^^^^^^^
The ``setuptools`` offer a `development mode`_ using the ``develop`` command. This command does two things:

1. Generates an **egg** next to the ``setup.py`` file. This is a folder named ``length.egg-info`` which contains metadata files.
2. It doesn’t actually install the **egg**. Instead, two things happen in the ``site-packages`` directory:

    * A special ``.egg-link`` file (sort of a multiplatform symbolic link) , that links to your project’s source code. 
    * Also, the **absolute path** to our module it's added to the ``easy-install.pth`` file.

We can use this mode running::

    $ python setup.py develop

.. note:: Here also you can use the ``--user`` flag if you are not working inside a **virtual environment**.

When we are done developing and ready to create a package, we can **uninstall** this editable version running::

    $ python setup.py develop --uninstall

This command will delete the two files in the ``site-packages`` directory, but will not delete the ``length.egg-info`` folder in our project, which must be wiped **manually**.

pip editable installs
^^^^^^^^^^^^^^^^^^^^^
Using pip we can also create `editable installs`_. To install a **local project** this way we run::

    $ pip install -e .

.. note:: Consider using the ``--user`` flag if you are not working inside a **virtual environment**.

If we have our code in some **remote repository**::

    $ pip install -e git+http://repo/length.git#egg=length

The result of this command is the same than in the section above.

To **uninstall** just::

    $ pip uninstall length

The same way, that will not remove the **egg** folder, which must be removed manually.

.. _`distribution packages`: https://packaging.python.org/glossary/#term-distribution-package
.. _`wheel`: https://packaging.python.org/glossary/#term-wheel
.. _`cache`: https://pip.pypa.io/en/stable/reference/pip_install/#caching
.. _`development mode`: https://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode
.. _`editable installs`: https://pip.pypa.io/en/stable/reference/pip_install/#editable-installs