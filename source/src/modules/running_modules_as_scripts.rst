**************************
Running modules as scripts
**************************
Modules usually contain code designed to be imported by other modules or scripts. But there are some situations when we may want to add in them some executable code, for example:

* To provide a convenient user interface to the module.
* For testing purposes, when we want to add some small unit tests.

The problem with adding executable code to the module is that it will also be run when we import it. Luckily there is a way of hiding the executable code when the module is imported. We just have to add it at the end of the file, under the following line::

    if __name__ == "__main__":
        print("I'm running as a script")

When a module is imported, Python sets the special dunder variable ``__name__`` to the **name of the module**. However, if a file is run as a standalone script, ``__name__`` is set to the string ``'__main__'``.


https://docs.python.org/3/tutorial/modules.html#executing-modules-as-scripts
