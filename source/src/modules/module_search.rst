.. _module-search-precedence:

*************
Module search
*************
Consider the following ``import`` statement in some **Python file**::

    import foo

.. note:: By **Python file** we mean, either a Python **script** or a Python **module**.

When the Python interpreter finds this statement, it starts searching for a module named ``foo`` in several locations:

1. The list of **built-in modules**: If the interpreter finds it here, the search stops and the module is imported.
2. If the interpreter doesn't find it, it starts searching for it on the **search path**.

Built-in modules
================
The list of **built-in modules** is installation-dependent and can be found in the variable `sys.builtin_module_names`_, which is just a tuple of strings with the names of all modules that are compiled into a particular Python interpreter. We can check its contents from an **interactive session** by importing the ``sys`` module and then printing the variable contents::

    $ python3
    >>> import sys
    >>> print(sys.builtin_module_names)
    ('_abc', '_ast', '_codecs', '_collections', '_functools', ..., 'builtins', ...)

One of these modules is named `__builtins__`_, and it's the one that contains a lot of built-in `functions`_ (like ``print()``, or ``input()``) as well as some `constants`_. We don't have to do anything to use any of these definitions since this module is automatically imported for us.

The Search Path
===============
If a module is not found in the list of built-in modules, the interpreter keeps searching for it in the `search path`_. The search path is a list of directories contained in a variable named `sys.path`_. We can access the contents of this variables by two different ways:

1. From our system's shell (Bash, Zsh, etc).
2. Using a Python interactive session.

We are gonna examine both ways, but before doing that let's talk about ``PYTHONPATH``.

PYTHONPATH
----------
`PYTHONPATH`_ is an **environment variable** that we can set on our system, to add new directories to the **default search path** for modules. These directories would contain custom Python modules that we don't want to install to any of the standard locations (aka **site-packages**).

We can test how this variable works by setting it temporarily from our command line::

    $ mkdir ~/testing_pythonpath1 ~/testing_pythonpath2
    $ export PYTHONPATH="$HOME/testing_pythonpath1:testing_pythonpath2"
    $ echo $PYTHONPATH
    /Users/bob/testing_pythonpath1:testing_pythonpath2

.. note:: In Windows we must use ``;`` as a separator for the directories in ``PYTHONPATH``.

Examining the search path from our command line
-----------------------------------------------
The `Python Standard Library`_ includes a module named `site`_ which can be invoked using our command line interpreter:

.. code-block:: none
    :linenos:

    $ python3 -m site
    sys.path = [
        '/Users/bob',
        '/Users/bob/testing_pythonpath1',
        '/Users/bob/testing_pythonpath2',
        '/Library/Frameworks/Python.framework/Versions/3.7/lib/python37.zip',
        '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7',
        '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/lib-dynload',
        '/Users/bob/Library/Python/3.7/lib/python/site-packages',
        '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages',
    ]
    ...

The output of this command it's quite verbose, but the part we are interested in, it's the list of strings contained in the ``sys.path`` variable:

1. The first item (line **3**) is ``sys.path[0]``, which may take two values:

    * In this case is the directory from where we run the command ``python3 -m site``, in other words, Bob's home directory (`/Users/bob`).
    * It also may be a string containing the **directory of the script** that was used to invoke the Python interpreter. It doesn't matter in we run the script from its parent directory, or several directories above or below.

2. Lines **4** and **5** contain the directories we set before in the ``PYTHONPATH`` **environment variable**. This variable doesn't need to be set if the user doesn't want to, in which case these folders will not show up in ``sys.path``.

3. Lines **6** to **8** contain the **Python Standard Library**. Notice that these folders use ``/lib/`` directories. .

4. The last 2 items (lines **9** and **10**) in the **search path** list, are known as the **site-packages**:

    * The **user** ``site-packages`` directory.
    * The **global** ``site-packages`` directory.

The site-packages directories contains the modules that the user installs using ``pip``. Notice how the **user site-packages** (modules installed using the ``--user`` option) are searched before the **global site-packages**. 

.. note:: By the way, we can't install the same module, to both the **global** and **user** site-packages directory. If we try, ``pip`` would uninstall from one of them before install to the other one.

Examining the search path interactively
---------------------------------------
Let's start a Python interactive session, and from there check the contents of ``sys.path``:

.. code-block:: none
    :linenos:

    $ python3
    >>> print(sys.path)
    ['',
    '/Users/bob/testing_pythonpath1',
    '/Users/bob/testing_pythonpath2',
    '/Library/Frameworks/Python.framework/Versions/3.7/lib/python37.zip',
    '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7',
    '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/lib-dynload',
    '/Users/bob/Library/Python/3.7/lib/python/site-packages',
    '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages']

The output is almost the same, but notice how the first item in the list, at ``sys.path[0]`` it's an **empty string**. Apparently that's because Python was started interactively, and the first place Python will start the search is, by default, the **current directory** from where the interpreter was invoked.

.. note:: In my opinion it wouldn't hurt to show this directory in the list, but probably it's due to some language design choice.


.. _`sys.builtin_module_names`: https://docs.python.org/3/library/sys.html#sys.builtin_module_names
.. _`search path`: https://docs.python.org/3/tutorial/modules.html#the-module-search-path
.. _`sys.path`: https://docs.python.org/3.8/library/sys.html#sys.path
.. _`PYTHONPATH`: https://docs.python.org/3/using/cmdline.html?highlight=pythonpath#envvar-PYTHONPATH
.. _`Python Standard Library`: https://docs.python.org/3/library/index.html
.. _`site`: https://docs.python.org/3/library/site.html#module-site
.. _`functions`: https://docs.python.org/3/library/functions.html#built-in-funcs
.. _`constants`: https://docs.python.org/3/library/constants.html#built-in-consts
.. _`__builtins__`: https://docs.python.org/3/library/builtins.html