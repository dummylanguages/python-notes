#####
Files
#####

.. toctree::
	:maxdepth: 3
	:hidden:
	
	open_files
	write_files
	intro_with

So far, we have seen examples of programs that have taken input data from users at the keyboard and storing that data in variables. Once such a program terminates, that data is not available anymore. If we want that information to persist from one program execution to the next, the data must be stored in a file.

.. note::	A **file** is an object that stores data in a non-volatile memory device for a later use.

In Python, a **file** is a **built-in type** (like ``str`` or ``int``).
	
.. note:: Built-in types are also known as **core types**.

There are a lot of **types of files**, but here we are gonna make a distinction between:

	* **Binary files**, contain data designed to be read or written via a computer program (images, mp3, Excel documents) Any attempt to view the 	contents of one of these files using a text editor, will result in “garbled” characters on the screen.
	* **Text files**, contain just text, and can be directly viewed and created using a text editor. This is the kind of files we are gonna be covering here.


	