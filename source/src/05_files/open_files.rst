**********
Text Files
**********
Text files contain just characters, structured as individual **lines of text**. In addition to printable characters, text files also contain the nonprinting newline character, ``\n``, to denote the end of each text line.
Text files are used to process all sorts of text-based data, from a simple email, to an Html page. Even the source code of a python script is a text file.

.. note::	
	Another important thing regarding text, is what kind of **encoding** is used. Encoding refers to the set of characters used in the file. **Python 3.x** encodes text using the **UTF-8** standard.

When using files in Python, we have available fundamental operations such as *opening* a file, *reading* a file, *writing* to a file, and *closing* the file. Let's see how these operations work when using text files.

Opening text files
==================
This is the most important operation regarding files. All files must first be opened before they can be used. In Python, when a file is opened, a **file object** is created and we can start using the **methods** available for it.

.. note::
	**Methods** are operations provided with the object. They are like functions but they are part of the object.

To open a text file we use the **BIF** ``open()``. For example:

.. code-block:: python

	f = open("data.txt")

If the file is successfully opened, a **file object** is created and assigned to the provided identifier, in this case ``f``. 

Closing text files
==================
When the program has finished working with the file, it should be closed by calling the ``close()`` **method** on the file object. Once closed, the file may be reopened by the same, or another program. This is the whole sequence:

.. code-block:: python

	f = open("data.txt")
	# We do some stuff here with the file contents...
	f.close()

Notice than when using the ``close()`` **method**, we use the name of the object, the **dot operator**, and finally the name of the method, something like ``object.method()``. We are calling a method that is part of the object.

.. note::
	``open()`` is a **BIF**, and ``close()`` is a **method** that is part of the object.

Exceptions
==========
When opening a file, there are a few reasons why an ``IOError`` may occur. One of them happens when the **file name** does not exist, in that case, the program will terminate with a ``“no such file or directory”`` error.

Providing an alternative path
-----------------------------
When a file is opened, the interpreter looks for it in the same folder/directory from where we started the script. However, an **alternative location** can be provided as an **argument** in the call to ``open``. The **path to the file** can be:

 	* A **relative path**, relative to the location from where we are running the script. 
 	* An **absolute path**, giving the location of a file anywhere in the file system.

