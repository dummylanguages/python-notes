.. _files:

*******************************
Intro to the ``with`` statement
*******************************
We cannot stress out enough the importance of **closing** a file once we've done working with it. In order to understand it we're gonna use some example.

File descriptors leaks
======================
When you open a file, the operating system assigns a **file descriptor** to the to the open file. This file descriptor is an ``int`` number which works like a handle to the open file. 

Limited resources
-----------------
There is (usually) a limit to the number of file descriptors a process can use. On UNIX systems we can find out this limit doing::

    $ ulimit -n
    256

In my system is apparently ``256`` at the moment.

.. note:: The ``-n`` flag is for checking the **soft limit** of **open files** our system can handle. 
    * Use ``-Hn`` flag to see the **hard limits**.
    * Use the ``-a`` flag to see all the limits.

Testing the limits
------------------
Let's write a small script to see what happens when we go over these limits.

.. literalinclude:: ../../code/chap_05/leaky.py
   :language: python
   :linenos:
   :caption: leaky.py
   :tab-width: 4

When we try to run this script this happens::

    $ python3 leaky.py
    Traceback (most recent call last):
    File "leaky.py", line 4, in <module>
    OSError: [Errno 23] Too many open files in system: 'foo.txt'

Our system can handle ``256`` open files at the same time, and our script is trying to open ``1000``. 

.. note:: You could bring down the number of iterations under ``250`` see if that throws an error.

The ``with`` statement
======================
A naive solution to the problem of file descriptors leaking would be simply **close your files** when you finish working with them. But the truth is that in real systems it's difficult to make sure that the ``close()`` method is being called especially with functions that have multiple return paths, or when exceptions are raised.

Fortunately Python offers a way to deal with this problem through the use of the ``with`` statement. Let's see a basic example:

.. literalinclude:: ../../code/chap_05/basic_with.py
    :language: python
    :linenos:
    :tab-width: 4

.. note:: The ``'w'`` option will create ``foo.txt`` if it doesn't exist.

Using the ``with`` statement we don't have to worry about closing the file, it's been done for us behind the curtains.

Fixing the leak
---------------
Let's write an updated version of our ``leaky.py`` script. In this case we'll use the ``with`` statement:

.. literalinclude:: ../../code/chap_05/leak_no_more.py
    :language: python
    :linenos:
    :caption: leak_no_more.py
    :tab-width: 4

The script above opens 1000 the file ``foo.txt`` and writes the ``'Hello world!'`` string to it. We don't get any leak, because after the writing operations, the file is closed in every iteration.
