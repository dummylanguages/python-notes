***********
Write Files
***********
We can write data into files. For that we use the same ``open()`` **BIF** but using the **write mode**. For example:

.. code-block:: python
	:linenos:

	>>> s = 'First line. \nSecond line 2. \nThird line 3.'
	>>> f = open('data.txt', 'w')
	>>> f.write(s)
	>>> f.close()

	* In **line 1** we are just creating a simple string with three lines of text. 
	* In **line 2** we create new text file calling the ``open()`` **function** with the ``w`` argument. 
	* In **line 3** we write the string to the new file using the ``write()`` **method**. 
	* In the last line we call the ``close()`` method to close the file. This last step is very important, the writing operation doesn't happen until we close the file. If we don't close it, nothing happens. Now, if you check the ``data.txt`` file in your text editor, you'll notice that it contains the three lines of text.

Notice that the file ``data.txt`` didn't exist, we created it from python. The **writing mode** should be used only when we intend to create a **new file**, so we have to make sure that the name of the file (first argument) doesn't exist.

.. warning:: Be careful when using the **write mode**. If we use this mode to open an already existing file, after we close it, its previous contents will be smashed, and valuable information may be lost.

