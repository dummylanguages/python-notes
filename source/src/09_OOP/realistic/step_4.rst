Step 4: Customizing Behavior by Subclassing
===========================================
So far, we have made a class that packages together data and behavior into a single self-contained component. This is known as **encapsulation**, a distinguishing feature of **OOP**. We have seen how having the behavior defined in only one place, facilitates the location of the code for future modification and avoids redundancy. We have even done a bit of operator overloading.

What we haven't seen yet, is another major feature of OOP called **inheritance**. In a sense, we have been using inheritance from the beginning, since instances inherit methods from their classes. But inheritance goes further in allowing us to define classes that inherit from another classes, and that's the main idea behind OOP. Inheritance allow us to define a relationship superclass/subclass between parts of our code, and even customize the inherited code in the subclass. Let's see how this is done.

Coding subclasses
-----------------
We are going to define a **subclass** of ``Person`` called ``Manager`` that replaces the inherited ``giveRaise`` method with a more specialized version. Let’s assume that when a ``Manager`` gets a raise, it receives the passed-in **percentage** as usual, but also gets an **extra bonus** that defaults to 10%. 

Let's start by defining the new subclass and its method::

    class Manager(Person):
            def giveRaise(self, percent, bonus=10):

* Line **1** means that we’re defining a new class named ``Manager``, which inherits from the superclass ``Person``.
* In line **2** we are redefining the ``giveRaise()`` method inherited from the superclass. 

Now, there are two ways we might code this method customization: a good way and a bad way. Let’s start with the bad way, since it might be a bit easier to understand.
  
Augmenting Methods: The Bad Way
-------------------------------
We can just copy the code of ``giveRaise`` in Person, paste it in Manager, and modify it. Like this::

    class Manager(Person):
        def giveRaise(self, percent, bonus=10):
            self.pay = self.pay * (100 + percent + bonus) / 100

The problem here is that if we ever have to change the way raises are given (and we probably will), we’ll have to change the code in two places, not one. Every time we copy and paste code, we are essentially doubling our maintenance effort in the future. 

Augmenting Methods: The Good Way
--------------------------------
What we really want to do here is somehow **augment** the original ``giveRaise``, instead of replacing it altogether. The good way to do that in Python is by calling to the original version directly, with **augmented arguments**, like this:

.. literalinclude:: /code/chap_10/person_step_4A.py
    :language: python
    :linenos:
    :lines: 1, 18-21
    :emphasize-lines: 4

Before explaining what we've done here, let's clarify the fact that a method defined in a class can be called in 2 ways:

1. The usual way, **through an instance** of the class. The syntax for this type of calls is:
   
       ``instance.method(args...)``

For example::

    >>> Sue.lastName()
    Jones

2. We can also call the method using the **class name**. But in this case we should provide the instance ourselves:

       ``class.method(instance, args...)``

For example:: 

    >>> Person.lastName(Sue)
    Jones

The method call always needs a **subject** instance, in the first case we are providing it when we call the method on the object. In the second case we are providing it as the first argument of the method.

Let's check our augmented method again:

.. literalinclude:: /code/chap_10/person_step_4A.py
    :language: python
    :linenos:
    :lines: 1, 18-21
    :emphasize-lines: 4

In our case, we are **calling the method using the class name**, so we have to provide the name of the instance as the first argument. Since we want this method to be used by any instance, we don't specify any concrete instance, just any instance. We do that using ``self`` as the first argument.

This “good” version may seem like a small difference in code, but it can make a huge difference for future code maintenance—because the ``giveRaise`` logic lives in just one place now (Person’s method), we have only one version to change in the future as needs evolve.

This is what our whole script looks like so far:

.. literalinclude:: /code/chap_10/person_step_4A.py
    :language: python
    :linenos:
    :lines: 1-27, 32, 38, 42-45

We've added more tests at the end to check how when a manager gets a 10% raise, he's also getting a 10% bonus, so his salaray go up a 20%. We can check the output of the tests::

    $ python3 person.py
    Before the raise, Tom makes: 50000
    Giving Tom a raise of 10%...:
    Mr. Jones now makes: 60,000.00
    [Person: Tom Jones - Manager - 60000.0]

Also notice how printing Tom as a whole at the end of the test code displays the nice format defined in Person’s ``__repr__``. All ``Manager`` objects get this display method, as well as ``lastName``, and the ``__init__`` constructor methods “for free” from ``Person``, by inheritance.

.. todo:: Check the explanation of super() in this section of the book.

Polymorphism in Action
----------------------
Let's see what polymorphism is about, adding some tests to our code: 

.. literalinclude:: /code/chap_10/person_step_4B.py
    :language: python
    :linenos:
    :lines: 1, 22-28, 48-55

If we run this tests, this is the resulting output::

    $ python3 person.py
    Before the raise:
    [Person: Robert Smith - 0]
    [Person: Sue Jones - Developer - 10000]
    [Person: Tom Jones - Manager - 50000]

    Giving a raise to all the employees:
    [Person: Robert Smith - 0.0]
    [Person: Sue Jones - Developer - 11000.0]
    [Person: Tom Jones - Manager - 60000.0]

In the added tests, object is either a ``Person`` or a ``Manager``, and Python runs the appropriate ``giveRaise`` method automatically. Bob and Sue are common employees, so they get a 10% raise, but Tom is a manager, so he gets the 10% raise, plus a 10% bonus. This is what polymorphism in Python is: what ``giveRaise`` does, depends on what object is the subject of the method.

Inherit, Customize, and Extend
------------------------------
We have already seen how to inherit code from a superclass, and how to customize it in the subclass. But what we can also do is **extend** a subclass with new methods. For example, we could add a new and unique method to the subclass ``Manager``, that is not present in the ``Person`` superclass::

    class Person:
        def lastName(self): 
            ... 
        def giveRaise(self): 
            ... 
        def __repr__(self): 
            ...
    
    class Manager(Person):
        def giveRaise(self, ...):       # Customize inherited
            ...      
        def someThingElse(self, ...):   # Extend
            ...  
   
OOP: The Big Idea
-----------------
In OOP, we program by customizing what has already been done. At first glance, given the extra coding requirements of classes, it looks like an overkill, but truth is, that this programming paradigm can cut development time radically compared to other approaches.

Usually, software has to evolve over time, and the customizable hierarchies we can build with classes provide a much better solution than any other tool offered by the language.
