***************************
2. A more realistic example
***************************
Let's start coding a more realistic example, we’re going to build a set of classes for recording and processing information about people. We are gonna start creating 2 classes:

    * ``Person``, the main class. It creates and processes information about people.
    * ``Manager``, a customization of Person that modifies inherited behavior


.. toctree::
	:maxdepth: 3

	step_1
	step_2
	step_3
	step_4
	step_5
	step_6
	step_7
