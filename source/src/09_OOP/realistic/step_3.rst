Step 3: Operator overloading
============================
So far we have been fetching individual attributes and printing them with the built-in ``print()`` function. It would be nice if we could print all the attributes from an object at once. If we try with::

    >>> print(Sue)
    <__main__.Person object at 0x1078a3ba8>

The output says that ``Sue`` is an object of the class ``Person``, and its address in memory. Unfortunately, the default display format for an instance object isn’t what we are looking for. Let's see how we can change that.

Providing print displays
------------------------
We have already seen that when we create an object, the ``__init__`` method is invoked by default. We have learn how to take advantage of this situation and use it to initialize attributes for the objects. This is known as **operator overloading**.  

Everytime we **print** an object, the ``__str__`` and ``__repr__`` methods are invoked. We don't have to call them directly, but every call to the ``print()`` function does it automatically. 

What these 2 methods do, is convert an object to a string representation ready to be displayed. The problem is, we don't like what is being displayed by default. No problem, we can change that default, using the same technique we used with the ``__init__`` method: **operator overloading**

Problem is, in this case we are dealing with 2 methods, which one do we have to overload? By default, ``__str__`` defaults to ``__repr__``:

    >>> class Foo(object): pass
    ...
    >>> foo = Foo()
    >>> repr(foo)
    '<__main__.Foo object at 0x1017bb358>'
    >>> str(foo)
    '<__main__.Foo object at 0x1017bb358>'

So if we override ``__repr__``, that representation is also gonna be used for ``__str__``. For example::

    >>> class Foo(object):
    ...     def __repr__(object): return "Hi, I'm foo"
    ...
    >>> foo = Foo()
    >>> str(foo)
    "Hi, I'm foo"
    >>> repr(foo)
    "Hi, I'm foo"

.. tip:: 
    By default, ``__str__`` defaults to ``__repr__``, but not viceversa. That means that we can override both methods and have 2 different displays. Sometimes classes provide both a ``__str__`` for user-friendly displays and a ``__repr__`` with extra details for developers to view.

So the easier thing to do in our example is override ``__repr__``:

.. literalinclude:: /code/chap_10/person_step_3.py
    :language: python
    :linenos:
    :lines: 1-17
    :emphasize-lines: 15-16

Now if we print an object we get a friendly representation of all of its attributes::

    >>> print(Bob)
    [Person: Robert Smith - None - 0]
    >>> print(Sue)
    [Person: Sue Jones - Developer - 10000]

By the way, when we use the ``print()`` function with an object, we get displayed what it's defined in ``__str__``. But when we just write the name of the object in the python shell, we get what is defined in ``__repr__``. In this case both outputs look the same, because ``__str__`` defaults to ``__repr__``.

    >>> print(Sue)      # Shows Sues's __str__
    [Person: Sue Jones - Developer - 10000]
    >>> Sue             # Shows Sue's __repr__
    [Person: Sue Jones - Developer - 10000]

.. note:: 
    ``__str__`` is used for the ``print()`` operation and the ``str()`` built-in function.

    ``__repr__`` is used for **interactive echoes**, the ``repr()`` function, and nested appearances, as well as by ``print()`` and ``str()`` if no ``__str__`` is present.

The ``__repr__`` method is probably the second most used method in Python after ``__init__`` . 
