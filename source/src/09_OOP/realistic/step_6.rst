Step 6: Using Introspection Tools
=================================
Before start storing our objects in a database, we should fix a couple of issues in our classes:

1. Regarding the display of the objects, when we print *"Tom the Manager"*, the display labels him as a ``Person``. That’s not technically incorrect, since Manager is a kind of customized and specialized Person. Still, it would be more accurate to display an object with the most specific (that is, lowest) class possible: the one an object is made from.

2. The current display format shows only the attributes we include in our ``__repr__``. We can’t yet verify that tom’s job name has been set to *"Manager"* correctly by ``Manager``’s constructor, because the ``__repr__`` we coded for ``Person`` does not print this field.
Worse, if we ever expand or otherwise change the set of attributes assigned to our objects in ``__init__``, we’ll have to remember to also update ``__repr__`` for new names to be displayed, or it will become out of sync over time.
Bottom line, we still have to reduce redundancy in our code.

Special Class Attributes
------------------------
We can address both issues with Python’s **introspection tools**. Let's start exploring our class at an interactive session.

Let's start by calling the interpreter, importing the class from the module, and creating an instance::

    $ python3
    >>>  from person import Person
    >>> Bob = Person('Robert Smith')
    >>> Bob             
    [Person: Bob Smith, None, 0]
    
We've also printed out Bob as an object, using the display representation we redefined in our special ``__repr__`` method.

* Now let's start by using the special attribute ``__class__`` on the object **Bob**::

    >>> Bob.__class__
    <class 'person.Person'>

The output shows ``<class 'module_name.Class_name'>`` information about the object. Class names live in, and are imported from, modules. 

* Classes, like modules, also have a ``__name__`` attribute. We can use that to print just the **class name**, without the module's name, using ``object.__class__.__name__`` ::
  
    >>> Bob.__class__.__name__
    'Person' 

* Objects keep their **attributes** and respective **values** in a ``dictionary``, where the keys are the attributes, and the values are the values of these attributes. We can access this dictionary with::
  
    >>> Bob.__dict__
    {'job': None, 'name': 'Robert Smith', 'pay': 0}

Or we can access separately just the values or the keys::

    >>> Bob.__dict__.values()
    dict_values([None, 'Robert Smith', 0])

    >>> Bob.__dict__.keys()
    dict_keys(['job', 'name', 'pay'])

.. note:: 
    In **Python 2.x**, ``dict.keys()`` returns a **list**.
    In **Python 3.x**, ``dict.keys()`` returns a **view** and must be passed to ``list()`` in order to make it a list.

We could transform any of these outputs to a list for example, doing::

    >>> list(Bob.__dict__.keys())
    ['job', 'name', 'pay']

or also::

    >>> list(Bob.__dict__.values())
    [None, 'Robert Smith', 0]

* We can access **individual values** of attributes using the methods: ``getattr()`` and ``__getattribute__`` . For example::
  
    >>> getattr(Bob, 'name')
    'Robert Smith'

    >>> Bob.__getattribute__('name')
    'Robert Smith'

Or manually indexing the entries in the dictionary::

    >>> Bob.__dict__['name']
    'Robert Smith'

* Knowing all this, we can achieve very cool displays of the attributes of our objects, something like::
  
    >>> for key in Bob.__dict__:
    ...     print(key, '=>', Bob.__dict__[key])
    ...
    job => None
    name => Robert Smith
    pay => 0

A Generic Display Tool
----------------------
Now we are gonna create a **superclass** that displays accurate class names, and formats all attributes of an instance of any class. We are gonna save this class in an independent module named **classtools.py**.

Inside this class, we are gonna overload its ``__repr__`` method, using some of the introspection tools we have just seen in the last section. And we are gonna do it in a generic way, so any class that wishes to use this display format, only has to inherit from this superclass. An additional benefit of this, is that if we ever want to change how instances are displayed, we need only change the superclass 

.. literalinclude:: /code/chap_10/classtools.py
    :language: python
    :linenos:

Notice the **docstrings** here, because this is a general-purpose tool, we want to add some functional documentation for potential users to read. Docstrings can be placed at the top of simple functions and modules, and also at the start of classes and any of their methods. The ``help()`` function and the ``PyDoc`` tool extract and display these automatically. 

Instance Versus Class Attributes
--------------------------------
Inherited class attributes are attached to the class only, not copied down to instances. If you ever do wish to include inherited attributes too, you can climb the ``__class__`` link to the instance’s class, using the ``__bases__`` attribute to climb to even higher superclasses, repeating as necessary.


Name Considerations in Tool Classes
-----------------------------------
Since the ``AttrDisplay`` class in the ``classtools`` module is a general tool designed to be used into other arbitrary classes, we have to be aware of the potential for unintended name collisions with client classes. If a subclass innocently defines a ``gatherAttrs`` name of its own, it will likely break our class, because the lower version in the subclass will be used instead of ours.

To minimize the chances of name collisions like this, Python programmers often prefix methods not meant for external use with a single underscore: ``_gatherAttrs`` in our case. 
A better and less commonly used solution would be to use two underscores at the front of the method name only: ``__gatherAttrs`` for us. Python automatically expands such names to include the enclosing class’s name, what is called **name mangling**. This is a feature usually called **pseudoprivate** class attributes.

Our Classes’ Final Form
-----------------------
Now, to use this generic tool in our classes, all we need to do is import it from its module, mix it in by inheritance in our top-level class, and get rid of the more specific ``__repr__`` we coded before. 

.. literalinclude:: /code/chap_10/person.py
    :language: python
    :linenos:

When we run this code now, we see all the attributes of our objects, not just the ones we hardcoded in the original ``__repr__``. 
Also because ``AttrDisplay`` takes class names off the self instance directly, each object is shown with the name of its closest (lowest) class, **Tom** displays as a **Manager** now, not a Person, and we can finally verify that his job name has been correctly filled in by the Manager constructor.

This is the output::

    >>> python3 person.py
    [Person: job = Developer, name = Sue Jones, pay = 10000]
    [Person: job = None, name = Robert Smith, pay = 0]
    [Manager: job = Manager, name = Tom Jones, pay = 50000]
    Bob's last name is:  Smith
    Sue's last name is:  Jones
    Givin Sue a raise...:
    Mrs. Jones now makes: 11,000.00
    Before the raise, Tom makes: 50000
    Giving Tom a raise of 10%...:
    Mr. Jones now makes: 60,000.00












