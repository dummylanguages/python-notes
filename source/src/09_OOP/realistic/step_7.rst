Step 7 (Final): Storing Objects in a Database
=============================================
We now have a two-module system that not only implements our original design goals for representing people, but also provides a general attribute display tool we can use in other programs in the future.

Although our classes work as planned, the objects they create are not real database records. That is, if we kill Python, our instances will disappear—they’re **transient objects** in memory and are not stored in a more permanent medium like a file, so they won’t be available in future program runs.

It turns out that it’s easy to make instance objects more permanent, with a Python feature called **object persistence**, making objects live on after the program that creates them exits. As a final step in this tutorial, let’s make our objects permanent.

Pickles and Shelves
-------------------
Object persistence is implemented by three standard library modules, available in every Python:

* ``pickle``: Serializes arbitrary Python objects to and from a string of bytes.
* ``dbm`` (named anydbm in Python 2.X) Implements an access-by-key filesystem for storing strings.
* ``shelve``: Uses the other two modules to store Python objects on a file by key.
  
The pickle module
^^^^^^^^^^^^^^^^^
The pickle module can take a given object in memory and convert it to a **string of bytes**, which it can use later to reconstruct the original object in memory. The pickle module can handle almost any object you can create: lists, dictionaries, nested combinations thereof, and class instances.

The shelve module
^^^^^^^^^^^^^^^^^
The ``shelve`` module provides an extra layer of structure that allows you to store pickled objects by key. This module translates an object to its pickled string with ``pickle`` and stores that string under a key in a **dbm file**.
Later we can load this dbm file, and shelve fetches the pickled string by key and recreates the original object in memory with ``pickle``. 

To our script, a ``shelve`` of pickled objects looks just like a dictionary. We can index by key to **fetch**, assign to keys to **store**, and use dictionary tools such as ``len()``, ``in``, and ``dict.keys`` to get information. Shelves automatically map dictionary operations to objects stored in a file.

Storing Objects on a Shelve Database
------------------------------------
Let’s write a new script that throws objects of our classes onto a shelve. Let's fire up our code editor and create a file named ``makedb.py``. 
First thing we are gonna do in this file, is import our classes in order to create a few instances to store.

.. literalinclude:: /code/chap_10/makedb.py
    :language: python
    :linenos:

In lines **14-15** we are using the ``name`` attribute of our objects(Bob, Sue and Tom), as their ``key`` in the shelve dictionary. The keys in a shelve object must be **strings** and should be **unique**  inside the shelve. The ``name`` attribute of our objects are unique strings, so they're ideal as ``keys`` in the shelve.

The **values** we store under keys can be Python objects of almost any sort, built-in types like strings, lists, and dictionaries, as well as user-defined class instances. In this case, the values are the 3 objects are Bob, Sue and Tom.

If we run this script whe shouldn't get any output, we’re not printing anything, just creating and storing objects in a file-based database.::

	$ python3 makedb.py

Altough the actual number of files created can vary per platform (in OSX I only got one), we should have one or more real files in the current directory whose names all start with **“employees.db”**. These files are our database, and they were created by the function ``shelve.open('employees.db')`` in the current working directory where we run the file ``makedb.py``. We could have specified another path though.

Exploring Shelves Interactively
-------------------------------
We can check the existence of these files with **Finder** or **Windows Explorer**, but if we try to open them, they are just **binary hash files**, and most of their content makes little sense outside the context of the shelve module. 

We can access the database using another script or using the python shell::

	>>> import shelve
	>>> db = shelve.open('employees.db')		# Reopen the shelve
	>>> len(database)							# 3 items
	    3
	>>> list(db.keys())					# Listing the keys
	['Thomas Jones', 'Susan Jones', 'Robert Smith']
	>>> database['Susan Jones']			# Full names are keys
	[Person: pay = 100000, job = dev, name = Sue Jones]
	>>> db.close()						# Closing the db

Notice that we don’t have to import our ``Person`` or ``Manager`` classes here in order to load or use our stored objects. We can access their attributes as well as their methods::

	>>> Bob.name
	'Robert Smith'
	>>> Bob.lastName()
	'Smith'

This works because when Python pickles a class instance, it records its self instance attributes, along with the name of the class it was created from and the module where the class lives.  When ``Bob`` is later fetched from the shelve and unpickled, Python will automatically reimport the class and link ``Bob`` to it.

We would have to import our classes only to make **new instances**, but not to process existing ones. 

Although ``shelves`` have some limitations, for simple object storage, though, shelves and pickles are remarkably easy-to-use tools.

Updating Objects on a Shelve
----------------------------
Now, before wrapping up our *"realistic example"*, let's write a script that access our database, modify one of the objects, and updates the database to make the changes permanent: 

.. literalinclude:: /code/chap_10/updatedb.py
    :language: python
    :linenos:

If we run this script the first time, we are printing the whole database, and after the updated salary of Sue::

	$ python3 updatedb.py
	Robert Smith 	=> [Person: job = None, name = Robert Smith, pay = 0]
	Susan Jones 	=> [Person: job = dev, name = Susan Jones, pay = 10000]
	Thomas Jones 	=> [Manager: job = Manager, name = Thomas Jones, pay = 50000]

	After the raise Susan makes: 11000.0

If we decide that Sue doesn't deserve the raise, we could access the database interactively, and modify back her salary. But this time we have to add the ``writeback`` option to ``True`` ::

	>>> db = shelve.open('employees.db', writeback=True)
	>>> for key in list(db.keys()):
	...     print(db[key])
	...
	[Person: job = None, name = Robert Smith, pay = 0]
	[Person: job = dev, name = Susan Jones, pay = 11000.0]
	[Manager: job = Manager, name = Thomas Jones, pay = 50000]
	
First we have opened the database with the option ``writeback=True``, this way we can change, delete or add objects to our database. This changes will take effect after closing or synchronizing the database, with the methods ``close()`` and ``sync()`` respectively.

Now, the database is still open, let's take back that raise whe gave to Sue::

	>>> Sue = db['Susan Jones']
	>>> Sue.pay = 10000
	>>> sync()

Now, if we print again the database, Sue should have only 10000::

	>>> for key in list(db.keys()):
	...     print(db[key])
	...
	[Person: job = None, name = Robert Smith, pay = 0]
	[Person: job = dev, name = Susan Jones, pay = 10000]
	[Manager: job = Manager, name = Thomas Jones, pay = 50000]

And very important, do not forget to close the database when we are finish::

	>>> db.close()

