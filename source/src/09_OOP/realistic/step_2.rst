Step 2: Adding behaviour methods
================================
So far we have a class that stores data. Every object we create with this class, will record just 3 fields of information, one for each of its attributes. We could have achieved the same using a data structure, like for example a dictionary::

    >>> Sue = {'name' : 'Sue Jones', 'job' : 'Developer', 'pay' : 12000}
    >>> Sue['name']
    'Sue Jones'

The whole point of using classes, is to create structures that include both **data** and **behavior**, so let's add some operations to our class. We can start out by quick sketching the operations in the python shell. 

* For example, if we want to extract last names, we can use the ``string.split()`` method. This method will split a string into substrings using a given separator as an argument. If no argument is passes, the space character is the separator::

    >>> name = 'Robert Smith' 
    >>> name.split()   
    ['Robert', 'Smith']

It returns a **list** object of substrings. We can access the second element using the **index operator**::

    >>> name.split()[-1] 
    'Smith'

* We can give any of the records a pay raise of a 10%, changing one of its attributes. That would be a simple arithmetic operation::

    >>> pay = 10000
    >>> percent = 10
    >>> pay = pay * (100 + percent) / 100
    >>> print("{:,.2f}".format(pay))    # A bit of formatting
    11,000.00

To apply these operations to the Person objects created by our script, we just have to attach them to the attributes of the objects created from our class. We can do that at the end of our module, in the testing section:

.. literalinclude:: /code/chap_10/person_step_2A.py
    :language: python
    :linenos:
    :emphasize-lines: 19-20, 25

Here’s the new version’s output::

    $ python3 person.py
    Robert Smith - 0
    Sue Jones - Developer - 10000
    Bob's last name is:  Smith
    Sue's last name is:  Jones
    Givin Sue a raise of 10%:
    Mrs. Jones now makes: 11,000.00

Hardcoding operations like these, outside of the class can lead to maintenance problems in the future. If we use these operations at different places in our program, we are gonna have a lot of duplicated code. 
If we have to modify that code, we'll have multiple copies to modify. Just finding all the appearances of such code may be problematic in larger programs, they may be scattered across many files what makes hunting them down and updating every occurrence an overkill.

Coding methods
--------------
What we are gonna do, it's transforming these operations (extract last names and calculate pay rises) into methods attached to the class. These operations will be automatically available to any instance of the class, we won't have to harcode them for every object we create. This way we are removing **redundancy**, and making the task of code maintenance much easier, since there's gonna be only one copy to maintain. This is known as **factoring** code, making our code shorter and more efficient. 

This sounds more difficult than what it really is. Check how we are implemented the operations as classes:

.. literalinclude:: /code/chap_10/person_step_2B.py
    :language: python
    :linenos:
    :lines: 1-14
    :emphasize-lines: 9-10, 12-13

As you can see, the changes are minimal:

The extracting last names operation (``name.split()[-1]``) implemented as a method requires just 2 lines of code, and we can say the same about the pay raising operation.
  
.. note:: **Methods** are normal functions that are attached to classes. They are designed to process instances of those classes. The instance is the **subject** of the method call and is passed to the method’s self argument automatically. Instead of littering our code with a lot of operations on objects, each operation is gonna be coded only once as a method in the class.

We have also added some tests at the end of the class:

.. literalinclude:: /code/chap_10/person_step_2B.py
    :language: python
    :linenos:
    :emphasize-lines: 25-26, 29-31

The output of this would be::
    
    $ python3 person.py
    Robert Smith - Sysadmin - 12000
    Sue Jones - Developer - 10000
    Bob's last name is:  Smith
    Sue's last name is:  Jones
    Givin Sue a raise of 10%...:
    Mrs. Jones now makes: 11,000.00

What we are applying here is a software design concept known as **encapsulation**. Some of its benefits are:

* Each operation is coded only once, so our code avoids redundancy.
* The operation logic is wrapped up behind an **interface**. If we decide to modify the internals of the method, we are not breaking other code that may be using it.
* Code maintenance is easier.
