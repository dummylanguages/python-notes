Step 1: Making instances
========================
Classes are always put inside a module, so we have modules that serve as libraries of classes. Inside the module classes become attributes of the module object. Let's start by putting our class inside a module. So open your code editor, create a new file and save it with the name of ``person.py``. Done, the name of our module is the filename without the entension, in this case: ``person``. By convention module names are written in **lowercase**. 

Coding constructors
-------------------
The first thing we want to do with our ``Person`` class, is recording basic information about people. Each instance of this class is gonna be a *record* with 3 *fields*: name, job, salary. 

.. note:: Traditional **databases** are organized by fields, records, and files. A **field** is a single piece of information; a **record** is one complete set of fields; and a file is a collection of records. For example, a telephone book is analogous to a file. It contains a list of records, each of which consists of three fields: name, address, and telephone number.

Every field of information will correspond to an attribute of the object. 

.. literalinclude:: /code/chap_10/person_step_1A.py
     :language: python
     :linenos:
     :lines: 1-7

* In line **1** the comment contains the name of the module.
* Line **3** contains the ``class`` statement with the name of the class. By convention, class names are written with **CamelCase**.
* In line **4** we code a **constructor method**. This method is gonna be run each time an instance of the class ``Person`` is created. In the parameter lists we have:
      
      * The parameter ``self``, a reference to the new instance itself.
      * The optional parameters name, job and pay.
        
.. warning:: The ``self`` parameter is **mandatory** in every method. 

* Lines **5** to **7** may seem a bit redundant at first but they're not:

    For example, the ``job`` parameter is a variable in the local scope of the ``__init__`` function, but ``self.job`` is gonna be an attribute in a future instance of ``Person``, and it's gonna have local scope in the whole object. By assigning ``self.job = job``, we are passing the ``job`` **parameter** to the ``self.job`` **attribute**. We are initializing the attributes of the object with the values passed to the constructor function.

The important part here is the ``__init__`` method. It belongs to a special category of methods commonly known in Python as **magical methods**. Their syntax is rather peculiar: their names are always preceded by a **double underscore**, and trailed by another one, something like ``__methodName__``.

But really, there's nothing magic about ``__init__`` apart from the fact that is invoked automatically, and must contain the ``self`` reference as its first argument. It still works like a normal function, we can for example provide **defaults** for some of its arguments, so they need not be provided in cases where their values aren’t available or useful.

For example, let's make the ``job`` attribute an **optional argument**, and set ``pay`` to 0, by default :

.. literalinclude:: /code/chap_10/person_step_1B.py
     :language: python
     :linenos:
     :lines: 1-8

This code means is that we’ll need to pass in a **name** when making Persons, but **job** and **pay** are now **optional**, they’ll default to None and 0 if omitted. 

.. note:: The ``self`` argument, as usual, is filled in by Python automatically to refer to the instance object.

Constructor methods
^^^^^^^^^^^^^^^^^^^
We create objects using **constructor expressions**. Their syntax is like any other function call(we can also assign function calls to names), they contain::

    <name> = <Class([args,...])>

* The name we want to assign to the object.
* A **call to the class** we want to instantiate.
* The arguments between parentheses are optional, but if any, they are passed to the constructor method so they will become the initial attributes of the object.
  
This call sets in motion a process where 2 **constructor methods** are invoked.

.. note:: In OOP languages, a **constructor method** is a special type of subroutine called to create an object. A constructor  often accepts arguments that the new object is gonna receive, setting initial values for the attributes of the instance, what is known as **initialization**. In some languages, the creation and initialization of the object is done by the same method, the **constructor**, that's why these methods are called interchangeably constructors or initializers.

In python, there are 2 special methods involved in the creation and initialization of an object. These 2 methods are called automatically when the constructor expression is executed:

1. The ``__new__`` method is a **static method of the class**. Right after the constructor expression is run, this is the first method invoked to **create** a new instance of class. If ``__new__`` does not return an instance of class, then the new instance’s ``__init__`` method will not be invoked. It returns a new instance of the class.

If ``__new__`` returns an instance of class, then the new instance’s ``__init__`` method will be invoked like ``__init__(self[, args, ...])``, where ``self`` is the new instance and the remaining arguments are the same as were passed to ``__new__`` in the constructor expression.

2. The ``__init__`` method is an **instance method**. It's called after the instance has been created (by ``__new__``), but before the call is returned to the caller. The ``__init__`` method may receive as parameters those arguments that were passed to the class constructor expression(if any). It doesn't return anything.
  
Technically, the ``__new__`` method is the **constructor**, and the ``__init__`` method is just the **initializer**, but because both methods work together in constructing objects, both are commonly considered constructors.

For example, let's create an instance of the class ``Person`` without using initializing the object::

    >>> Will = object.__new__(Person)
    >>> type(Will)
    __main__.Person

We have created an object of the class ``Person``, but because the ``__init__`` method hasn't been invoked, this object is created without any of the attributes we defined in the class. If we try to access any of the attributes:

    >>> Will.name
    Traceback (most recent call last)
     ...
    AttributeError: 'Person' object has no attribute 'name'

The interpreter raises an ``AttributeError``, because the object we just created doesn't have that attribute, in fact doesn't have any attribute at all.

We could start adding the attibutes manually::

    >>> Will.name = 'William Pitt'
    >>> Will.job = 'Developer'
    >>> Will.pay = 230000

Or we can even call the ``__init__`` method in this object, and reinitialize its attributes::

>>> Will.__init__('William Smith', 'Actor', 1000000)

Of course creating and initializing objects like this is an overkill, and it's much easier just to use the constructor call::

    >>> Susan = Person('Susan Lennon', 'Accountant', '87000')

Testing the class
-----------------
The last one was a simple class that basically fills out the fields of a new record. But before adding more new features, it's better if we test how it works. Developing software is a matter of of **incremental prototyping**, you write some code, test it, write more code, test again, and so on. 

For testing we have 2 options here:

* Use the python shell to do interactive tests. Every time we want to run tests, we have to import the class, create new instances. If we make any modifications to the code, we also have to reload the modules. Testing in the interpreter is great for small pieces of code, but for testing classes it can get a bit tedious.

    >>> from person import Person 
    >>> Bob = Person('Robert Smith')
    >>> Bob.name
    Robert Smith
    >>> Bob.pay
    0


* Programmers often add code tests inside the module, at the bottom of the file. This method makes testing much easier, just a matter of rerun the modules(no need to import, create instances, etc), like this:

.. literalinclude:: /code/chap_10/person_step_1B.py
     :language: python
     :linenos:
     :emphasize-lines: 10-11, 13-14

To run the tests we would write::

    $ python3 person.py
    Robert Smith - 0
    Sue Jones - Developer - 10000

The output of the test shows that the attributes of **Bob** and **Sue** are different. Each instance of a class has its own set of self attributes. They are both instances of the same class, but at the same time they are different objects. Classes serve as a sort of object factory.

Using Code Two Ways
-------------------
The problem with running tests this way, is that when we just want to import the class to use it somewhere else, we’ll also see the output of its tests every time the file is imported. But that's easy to fix.

If we call the module method ``__name__`` inside any module, it returns the name of the **filename**. When we run a module as a top-level script, its name becomes ``__main__``, it doesn't matter what its filename is.

We only want to print the tests when the module it's run directly, so we have to put the tests under a simple check: ``if __name__ == '__main__':``

* When a module is directly run, its name  becomes ``__main__``, the condition evaluates ``True`` and the tests are printed.
* When the module is imported(usually because contains a library of classes), its name is its filename, not ``__main__``. The condition evaluates ``False``, and the tests don't print.
  
.. literalinclude:: /code/chap_10/person_step_1C.py
    :language: python
    :linenos:

Here's the output::

    $ python3 person.py
    Rober Smith - 0
    Sue Jones - Developer - 100000
