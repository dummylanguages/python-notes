Step 5: Customizing Constructors, Too
=====================================
So far our code is doing all right, but we could improve a small detail. When we create a ``Manager`` object, we have to provide a job name for it, when this is already implied by the class itself. It would be better if this value was filled automatically when a ``Manager`` object is made.

We want to customize the constructor method for ``Manager`` in such a way that provides a job name automatically, but at the same time we don't want to duplicate the logic contained in the ``__init__`` method of the superclass ``Person``.

.. literalinclude:: /code/chap_10/person_step_5.py
    :language: python
    :linenos:
    :lines: 1-24, 25-35
    :emphasize-lines: 19-20, 30

* In line **19** our redefinition of ``__init__`` only takes 2 arguments(not counting ``self`` ): name and pay. 
* In line **20** we are calling the constructor of the superclass, using the class name, and passing it a value for the job name.
  
Calling superclass constructors from redefinitions this way turns out to be a very common coding pattern in Python. When we instantiate an object, Python looks in the hierarchy of classes for the lowest ``__init__`` method. But in this method we can call constructors from higher classes, calling the method using this superclass name. At the same time, in this call we can pass arguments to the superclass constructor. 

In the tests, we didn't have to pass an argument for the job field when instantiating *"Tom the manager"*. The output shows that the object was initialized correctly::

    $ python3 person.py
    [Sue Jones - Developer - 10000]
    [Robert Smith - Sysadmin - 12000]
    [Tom Jones - Manager - 50000] 

OOP Is Simpler Than You May Think
---------------------------------
So far we have seen all there is to OOP in Python. Classes certainly can become larger than this, and there are some more advanced class concepts, such as **decorators** and **metaclasses**, but in terms of the basics, we have seen all the important concepts:

* Instance creation—filling out instance attributes.
* Behavior methods—encapsulating logic in a class’s methods
* Operator overloading—providing behavior for built-in operations like printing
* Customizing behavior—redefining methods in subclasses to specialize them
* Customizing constructors—adding initialization logic to superclass steps

Most of these concepts are based upon just three simple ideas:

1. The inheritance search for attributes in object trees.
2. The special self argument in methods.
3. Operator over- loading’s automatic dispatch to methods.
   
Along the way we have learned how useful are classes for factoring code to reduce redundancy, for example when we coded operations in class methods and called back to superclass methods from extensions to avoid having multiple copies of the same code.

Other Ways to Combine Classes
-----------------------------
The basic mechanics of OOP in Python are fairly simple. But apart from inheritance, programmers sometimes combine classes in other ways, too.

For example, a common coding pattern involves nesting objects inside each other to build up **composites**. 

.. todo:: Come back to finish this after learning about Composition in chapter 31.




