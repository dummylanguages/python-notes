###
OOP
###

.. toctree::
	:maxdepth: 3

	Introduction <intro>
	A more realistic example <realistic/index>
	Magic methods <magic_methods>
	Properties <properties>