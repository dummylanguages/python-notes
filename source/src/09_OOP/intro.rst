*******************
Introduction to OOP
*******************
OOP stands for **Object Oriented Programming**, and is a programming paradigm based on the concept of **objects**, hence the name "object oriented". OOP is often a more effective way of programming, but it's no silver bullet.

We have been using objects in preceding sections: string objects, integer objects, etc. The use of objects in itself, however, does not constitute object oriented programming.

OOP is supported in Python, in combination with imperative, procedural programming, that's why Python is known as a multiparadigm programming language. In Python, OOP is entirely **optional**, we don't have to use it if we don't need it.

.. note:: Procedural programming is a **programming paradigm**, based upon the concept of the procedure call. Each procedure (also known as routines, subroutines, methods, or functions) contain a list or set of instructions telling a computer what to do step by step. Every time we call the procedures, the list of instructions is executed. Procedural programming languages include C, Go, Fortran, Pascal, and BASIC.

Classes
=======
We saw :ref:`before <objects>`, that everything in Python is an object. A simple **value** (3, 'hello') is an object, and every object has a **type** (integer, string) that is determined by its value. For every type of value, we have available a different set of operations(arithmetic for integers, concatenation for strings). We can think of Python's core types, as classes already implemented in the language.

Thanks to Python's OOP capabilities, we can define our own classes (think of classes as custom types), and from these classes we can create objects. Once an object is created, we can customize it even further, adding some features that are not present in the class definition.

.. note:: Objects are instances of a class.


Defining classes, creating instances
====================================
So we are talking about a 2 step process here:

1. Define the class.
2. Create the object, an instance of the class we have previously defined.

Defining a class
----------------
For defining classes we use the ``class`` keyword. The ``class`` statement has a very simple syntax::
	
	class Name(super1, ..., superN):
	    body

As we can see, it's a compound statement that consists of 2 parts:

1. The **header**, made up of:
    
    * The keyword **class**.
    * The **name** of the class.
    * Optional list of superclass[es] between parentheses.
    * Always ended with a colon ``:``

2. The **body** of the class: An indented set of statements.

For example::

	>>> class SimplestClass:
	...     pass

That's it, we have defined a class. Pretty simple, but let's point out a couple of details:

* The **name** of the class, ``SimplestClass``. According to the `Style Guide for Python <https://www.python.org/dev/peps/pep-0008/#class-names>`__, class names are written using the **CamelCase** notation (start with a capital letter, any subsequent words should also start with a capital).

* Notice also that we didn't write any **superclasses** between parentheses, this is absolutely optional. 

* In this case, the **body** of the class is empty, that's why it only contains the keyword ``pass``. **pass** is a null operation — when it is executed, nothing happens. It is useful as a placeholder when a statement is required syntactically, but no code needs to be executed. We need to use it here because we haven't code any attributes or methods. 
 
.. note:: Classes are also objects

Remember that **everything** in Python is an object, and that every object belongs to a type. Let's check the type of ``SimplestClass`` ::

	>>> type(SimplestClass)
	<class 'type'>

So yes, even a class definition is an object in Python, of the type ``type``.

One last thing, just like the ``def`` statement, the Python ``class`` statement is an **executable statement**. Only when is reached out by the flow of the program, it generates a new class object and assigns it to the name in the class header. So in this case, when the ``class`` statement is reached out, it creates a **class object** and assigns it the name ``SimplestClass``. 

Creating instances
------------------
Continuing with the last example, let's create an instance of ``SimplestClass``::

	>>> simpleton = SimplestClass()

We have created an instance of ``SimplestClass`` and assigned it the name ``simpleton``. Let's check the class it belongs to with the the builtin ``type()`` function::

	>>> type(simpleton)
	<class '__main__.SimplestClass'>

The output is telling us that we have an instance on ``SimplestClass`` in the ``__main__`` module.

Now let's create another one, and check its type::

	>>> another_simpleton = SimplestClass()
	>>> type(another_simpleton)
	<class '__main__.SimplestClass'>

This second instance also belong to the same class. 

They are different objects though. We can use the built-in ``print()`` function  on them:: 

	>>> print(simpleton)
	<__main__.SimplestClass object at 0x1043375f8>
	>>> print(another_simpleton)
	<__main__.SimplestClass object at 0x104337630>

The output shows us again their module and class, and also see the **memory address** each object lives at. Memory addresses aren't used much in Python, but here they are useful to show that we have two different objects.

Attributes and methods
======================
So far we have learned how to define our own classes, and create instances of them. Problem is that our first class is empty, it doesn't do anything. A class like that is fine for ilustrative purposes, it even allowed us to create instances based on it, but that wasn't a real world class or object.

As we know, an object is something that has a set of **attributes** and a related set of **behaviors**. So in an object we put together:

* Data, using ordinary variables. Variables inside a class are known as **attributes**. 
* Behaviours, implemented by functions that operate on that data. Functions defined inside classes are called **methods**.
  
Attributes
----------

.. sidebar:: The cookie-cutter analogy

	A class is like a cookie-cutter, and objects are like the cookies. With one cookie-cutter we can create a lot of cookies that are gonna inherit the shape and size of the cookie-cutter.

	Like a cookie-cutter, a class definition specify the set of attributes that the objects created according to that class, are gonna inherit. 

Let's start where we left, with our empty and useless ``SimplestClass``. This class is empty, we didn't code any attributes or methods in its body, but it's not too late. We can start attaching attributes dynamically to the class, completely outside of the original class statement::

	>>> SimplestClass.x = 7
	>>> SimplestClass.y = 3

.. note:: Usually, attributes are defined inside the class definition, but the point here is demonstrate that we can also do it dynamically.

These attributes belong to the class itself. Classes are objects in their own right, the same that their instances.

Inheritance
^^^^^^^^^^^
An interesting thing to notice is a feature called **inheritance**, that means that all of the instances of a class, will inherit the attributes attached to the class. And this works even when the objects were created before adding the attributes to the class. We can check that doing::

	>>> simpleton.x
	7
	>>> another_simpleton.x
	7

.. note::
	Every time we want to make reference to an attribute, we have to use the syntax: ``<object>.<attribute>``.
	This is known as the **dot notation**. 
	To create an attribute: ``<object>.<attribute> = <value>``

If we change an attribute in an instance, though, we are changing the attribute in that object, but not in other objects or the class::

	>>> simpleton.x = 33
	>>> simpleton.x
	33
	>>> another_simpleton.x = 74
	>>> another_simpleton.x
	74
	>>> SimplestClass.x
	7

We have changed the ``x`` attribute in both objects. Their values are different to each other. The value of ``x`` in ``SimplestClass`` hasn't changed at all.

The 3 of them are independent objects, with independent attributes.

Class attributes vs. object attributes
""""""""""""""""""""""""""""""""""""""
We have already seen how it's possible to attach attributes either:

* Directly to a class(either dynamically or in the class definition) 
* Or to any instance of a class.
		
It's important to make a distinction between attributes that belong to a class and those that belong to any of its instances.

The attributes created for a class, are inherited by all of its instances. But the attributes created for a particular instance, are not inherited by the class or other instances. Inheritance only works in one direction.

For example, if we create a new attribute for an instance::

	>>> simpleton.z = 743

This won't be inherited by other instances or by the class. Trying to access an inexistent attribute will generate an ``AttributeError``.
::

	>>> another_simpleton.z
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	AttributeError: type object 'another_simpleton' has no attribute 'z'
	>>> SimplestClass.z
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	AttributeError: type object 'SimplestClass' has no attribute 'z'


Methods
-------
Now it's time to add behaviors to our classes. Methods are almost like ordinary functions, only 2 differences:

* They are defined inside classes.
* Method definitions always have to include *one special parameter at the beginning of the parameter list*. This special parameter is named by convention ``self``. 

.. note:: 
	The self parameter of every class method, is always a reference to the current instance of the class. 

For example, let's create another simple class, but this time we are gonna define a simple method in it:
::

	>>> class BePolite:
	...	    def greeting(self):
	...		    print('hello, world')
	...
	>>> polite = BePolite()

* In line **2** we defined the ``greeting()`` method, it doesn't need any parameters, but still has the self in the parameters list.
* In line **5** we create an instance of ``BePolite()`` named ``polite``.

Now, let's call the ``greeting()`` method of ``polite``:

	>>> polite.greeting()
	'hello, world'

.. note::
	To use objects methods, we also use **dot notation**: ``obj.method()``.

Here we see the self in action. When we call the method we don't have to pass any argument for the self parameter, the Python interpreter does it for us. The call to the method that the interpreter sends is ``polite.greeting(simpleton)``.
  