.. todo:: OVERRIDING METHODS

Magic methods
=============
Methods named with double underscores (``__X__``) are special hooks. Such methods are called automatically when instances appear in built-in operations. 

.. note:: Dunder: An alias for Double UNDERscore, So ``__init__`` is *"dunder init dunder"*, or just *"dunder init"*.``__``. `From wiki Python <https://wiki.python.org/moin/DunderAlias>`__

Some of these methods are:

* The ``__init__`` method, which is known as the constructor method and is called when a new instance object is created. This method is the most important  because ``__init__``, along with the ``self`` argument, turns out to be a key requirement to reading and understanding most OOP code in Python.

* The ``__add__`` method makes and returns a new instance object of its class.

* The ``__str__`` method, is run when an object is printed.

The constructor method
----------------------
The ``__init__`` constructor method is the most commonly used for initialize objects’ state. Almost every class uses this method to set initial values for instance attributes and perform other startup tasks. The special ``self`` parameter in method functions and the ``__init__`` constructor method are the two cornerstones of OOP code in Python.

For example:



The ``__init__`` is still used when an object is created, we are just taking advantage of that situation for overriding it with initial values and/or operations that new instances will receive as they are created.

The ``__str__`` and ``__repr__`` methods
----------------------------------------
In short, the goal of ``__repr__`` is to be unambiguous and ``__str__`` is to be readable. For example:

    >>> import datetime
    >>> today = datetime.datetime.now()
    >>> str(today)
    '2012-03-14 09:21:58.130922'
    >>> repr(today)
    'datetime.datetime(2012, 3, 14, 9, 21, 58, 130922)'


* By default, ``__str__`` defaults to ``__repr__`` if no ``__str__`` is implemented. 

    >>> class Foo(object): pass
    ...
    >>> foo = Foo()
    >>> repr(foo)
    '<__main__.Foo object at 0x1017bb358>'
    >>> str(foo)
    '<__main__.Foo object at 0x1017bb358>'

In both cases, we get the class and object's id.

* If you override ``__repr__``, that representation is ALSO used for ``__str__``, but not vice versa.

    >>> class Sic(object): 
    ...   def __repr__(object): return 'foo'
    ... 
    >>> print str(Sic())
    foo
    >>> print repr(Sic())
    foo
    >>> class Sic(object):
    ...   def __str__(object): return 'foo'
    ... 
    >>> print str(Sic())
    foo
    >>> print repr(Sic())
    <__main__.Sic object at 0x2617f0>

* When we use ``__str__`` on a built-in container, it uses the ``__repr__``, NOT the ``__str__``, for the items it contains.




