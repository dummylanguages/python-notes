Properties
==========
In others OOP languages, the terms **attributes** and **properties** are usually used synonymously. This is not the case in Python.

Public and private attributes
-----------------------------
A single underscore indicates that the attribute is not intended to be accessed directly, but only via the methods of the class.

Encapsulation
-------------
**Data encapsulation** is an important principle in OOP. It basically means that the attributes of a class are made private to hide and protect them from other code. Many programming languages honor this principle by using **mutator methods**. These are special methods for setting and getting the values of attributes inside the class. Let's implement this idea in Python:

.. literalinclude:: ../../code/chap_10/mutators.py
    :lines: 1-12
    :linenos:
    :emphasize-lines: 8, 11

* In line **6** the ``__init__`` method initialize the ``x`` attribute which we have declared as **private**.
* In line **8** and **11** we have created a setter and a getter methods to set and get the value of the ``x`` attribute. 

Let's import the class in an interactive session and create an instance::

    >>> from mutators import Mut
    >>> mut1 = P(3)

Now, If  we try to access the attribute ``x`` ::

    >>> mut1.x
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'Mut' object has no attribute 'x'

We get an error, because ``p1.x`` is an attribute that can't be accessed from outside, it's a **private attribute**. The only way to *get* or *set* its value, is by using respectively the ``getX()`` or ``setX()`` methods::

    >>> mut1.getX()
    3
    >>> mut1.setX(7)
    >>> mut1.getX()
    7

If in the future we want to change the implementation to make sure that the attribute ``x`` only receive values between  1 and 9, we would add this: 

.. literalinclude:: ../../code/chap_10/mutators.py
    :lines: 14-30
    :linenos:
    :emphasize-lines: 11-
    :lineno-match:

We have implemented the changes in a way that, even if we have made use of this class in other parts of our code, these parts won't be affected by the changes. That's the point of using setters and getters, they offer **encapsulation** and at the same time allow to implement changes in the software without having to change the interface.  

In Python we have the same, we just get it done another way.

The pythonic way: properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Let's see how it's done in Python. Let's start with a simplified version, with a public attribute:

.. literalinclude:: ../../code/chap_10/pythonic_way.py
    :lines: 1-7
    :linenos:
    :emphasize-lines: 3

The ``x`` attribute is now public, accesing it for getting or setting its value is much more easy::

    >>> from pythonic_way import Pyt
    >>> pyt = Pyt(9)
    >>> pyt.x
    9
    >>> pyt.x = 33
    >>> pyt.x
    33

Problem with that: there's no encapsulation at all. 

Let's implement another version of this class, this time with a **private attribute**, and the filter to assure that the value of ``x`` is in the 1-9 range:

.. literalinclude:: ../../code/chap_10/pythonic_way.py
    :lines: 5-20
    :linenos:
    :emphasize-lines: 7, 9,13

* As we can see, in line **7** now the attribute is **private**.
* In line **9** we wrote a **decorator** for the method below, which makes it a **property**.
* In line **13** we wrote another decorator for the method that functions as the setter.

Now, let's create an object and change its attribute::

    >>> pw = PytV2(99)
    >>> pw.x
    9
    >>> pw.x = -88
    >>> pw.x
    0

The attribute is now **private** but we can still use the same interface we were using when it was **public**, meaning that we are still accessing the ``x`` attribute with the expression ``px.x``. 

.. note:: For the users of a class, properties are syntactically identical to ordinary attributes.

This is good news for rapid prototyping, since we can start simple (without properties) in the early stages of development, and add properties and encapsulation later, keeping the same interface to the object.

Public or private attributes, when to use them
----------------------------------------------
When we are designing a new class, and we're not sure about how to set an attribute these are some pointers:

1. If it's not gonna be accessed by others users of the class, we'll define it as **private**.
2. If it's gonna be accessed, then we have 2 options:
    #. We will define it as a **private** attribute with the corresponding **property**, if and only if we have to do some checks or transformation of the data. (It has to be a value between 1 an 9)
    #. It will be **public**, if we just have to grant access.
   
For example, let's create a class with both public and private attributes:

.. literalinclude:: ../../code/chap_10/pythonic_way.py
    :lines: 22-
    :linenos:
    :emphasize-lines: 16-
    :lineno-match:

* Line **24**: Since **player** has to be accessible to class users, we make it **public**.
* Line **25**: We have to check that **points** stays between the range of 1-5000, so we are gonna make it **private**, and create a property and a property method to set it.

