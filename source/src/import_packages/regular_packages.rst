.. _regular-packages:

****************
Regular packages
****************
In practice, **implicit namespace packages** are used to build **dotted module names**. In other words, these packages are only used as part of the **dot notation** as a way to get to the modules. This is quite useful to create namespaces for our modules, and avoid name conflights. The problem with this approach is that users need to know the **inner structure** of our package, where the modules and their definitions are located.

We've seen how importing a **namespace package** doesn't import anything at all, and that's because, by default, the **symbol table** of one of these **packages** is empty. 

**Regular packages** are also directories that include an **initialization file**, which is a file named ``__init__.py`` that lives at the top-level of the package or subpackage. When a package is imported, the code in this file is run. That may be useful for several things:

* We can declare **globals**, that will be available to all the modules in the package.
* Or initialize a connection to a database.
* Write to some **log** files about the modules being imported.
* But must importantly, we can define what is imported, or in other words, define the symbol table of our package.

An API for our package
======================
We can use ``__init__`` files to build the symbol table of our package, so that when the user runs the ``import`` statement against the **package/subpackage** itself, he will have access to whatever modules and definitions we have defined in the initialization file. This is useful for several reasons:

* The user doesn't need to know the exact location of the modules, making our packages more **user-friendly**.
* In some cases, a module may contain some definitions that are intended for **internal use**. Using initialization files, developers can choose what modules and definitions they want to hide and which ones will be accessible to the package users. In other words, they can design a **public API** for the users of the package.

A small example
===============
Consider the following example::

    ex02/
        pkg/
            __init__.py
            internal.py
            mod1.py

            subpkg/
                __init__.py
                mod1.py

This is what the :download:`__init__.py </code/import_pkgs/ex02/pkg/__init__.py>` for ``pkg`` looks like:

.. literalinclude:: /code/import_pkgs/ex02/pkg/__init__.py

What we are doing here is choosing what names are we gonna put in the package's **symbol table**:

* In **line 1** we are importing **everything** in ``mod1``, which may be a bit careless.
* **Line 2** imports the ``subpkg``.

Importing the package
---------------------
Let's open an **interactive session** and import the package::

    >>> import pkg
    >>> pkg.foo()
    I'm in: pkg.mod1

As you can see, ``foo`` is defined in the ``pkg.mod1`` module which looks like this:

.. literalinclude:: /code/import_pkgs/ex02/pkg/mod1.py

The user doesn't need to know in what modules lives ``foo``, he's accessing ``foo`` from the package namespace using **dot notation**. If in the future we decide to rearrange our modules, we just have to modify the ``__init__.py`` file to reflect the changes, but the user's API doesn't need to change.

Hiding definitions
^^^^^^^^^^^^^^^^^^
As you can see, the package's initialization file doesn't import the :download:`internal </code/import_pkgs/ex02/pkg/internal.py>` module, which looks like this::

.. literalinclude:: /code/import_pkgs/ex02/pkg/internal.py

This module will be hidden from the user. (Unless he expressly imports it, but hey, you cannot protect users from using a package in a way not intended by the developers.) If he tries to use it::

    >>> pkg.f1()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: module 'pkg' has no attribute 'f1'

Hiding a whole module is just a matter of not importing it in the ``__init__`` file. But in some situations, a module may contain:

* A lot of definitions we want to make available.
* Just a few of them that we want to hide.

A way to hide definitions is to precede their names with an **underscore**, like what we did with ``_foo`` in ``pkg.mod1``. If the user tries to call it::

    >>> pkg._foo()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: module 'pkg' has no attribute '_foo'

The problem with this approach is that those underscore names are a bit ugly, and **wildcard** imports are kind of unsofisticated.

Selective imports
^^^^^^^^^^^^^^^^^
Using underscores in our definitions it's ok in exceptional situations, but there's a way better approach to use imports in our **initialization files**. Check the contents of :download:`/code/import_pkgs/ex02/pkg/subpkg/mod1.py`:

.. literalinclude:: /code/import_pkgs/ex02/pkg/subpkg/mod1.py

And now check what we did in the **dunder init file** of :download:`/code/import_pkgs/ex02/pkg/subpkg/__init__.py`:

.. literalinclude:: /code/import_pkgs/ex02/pkg/subpkg/__init__.py

Here, instead of using a **wildcard** import, we are **importing selectively** what we want to expose, just by importing what we're interested in and ignoring the things we want to *hide* from the API. 

.. note:: Using **selective import statements** is the best way to define our public API.

If the user tries to access any definitions not indexed in the dunder init file of the subpackage::

    >>> from pkg import subpkg
    >>> subpkg.foo()
    Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
    AttributeError: module 'pkg.subpkg' has no attribute 'foo'

Calling ``foobar`` is ok though::

    >>> subpkg.foobar()
    I'm in: pkg.subpkg.mod1