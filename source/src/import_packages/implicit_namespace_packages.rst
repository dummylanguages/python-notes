***************************
Implicit Namespace packages
***************************
These are just folders that contains modules, or other subfolders with modules, without **initialization files**. 
Check the `PEP 420`_ to know more about them.

In the section about :ref:`importing modules <importing-modules>` we saw how importing a **module name** gives us access to all the definitions it contains, this is not the same with **namespace packages**. The reason for that is that as opposed to modules, namespace packages don't have a **symbol table**. We just use their names as part of the dot notation to get to the modules.

Consider the following **example**::

    ex01/
        pkg/
            mod1.py
            subpkg/
                mod1.py

Importing a module inside a namespace package
=============================================
Let's go up above the **package** directory and start an **interactive session**::

    $ cd ex01 && python3

We can use **dot notation** to import any **module** providing the package name::

    >>> import pkg.subpkg.mod1
    >>> pkg.subpkg.mod1.foo()
    I'm in: pkg.subpkg.mod1

Note how the name of the **module** has become ``pkg.subpkg.mod1``, and we have to use this name to access the definitions in the module.

Importing all definitions
=========================
We can also use a **wildcard import** to import everything inside the **module**::

    >>> from pkg.subpkg.mod1 import *
    >>> foo()
    I'm in: pkg.subpkg.mod1

This import statement will make **all** the definitions contained in ``pkg.subpkg.mod1``, available in the caller's namespace, without having to use the verbose dotted name. The **downside** to this it's that if ``mod1`` contains a lot of definitions, we may be bringing names into the namespace that will shadow other existing names.

Selective imports
=================
A better way to not having to use the verbose dotted names is to import selectively the definitions we are interested in, for example::

    >>> from pkg.subpkg.mod1 import foo
    >>> foo()
    I'm in: pkg.subpkg.mod1

This way we are in control of what we are bringing into the namespace, but it may require a lot of import statements.

Import the package itself
=========================
We may try to import only the package name, without specifying any module::

    >>> import pkg
    >>> pgk.subpkg.mod1.foo()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'pgk1' is not defined

But as you can see, importing the **package** itself, doesn't gives us access to any of the modules, or the definitions they contain. With namespace packages we always have to import the **modules**.

Wildcard imports
================
The same happens if we try to import **all** the submodules inside a namespace package using a ``*`` wildcard::

    >>> from pkg1 import *
    >>> subpkg1.mod1.foo()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'subpkg1' is not defined

You may have been expecting that the ``import`` statement would import all the **subpackages** or even the **modules** inside the **subpackage**::

    >>> mod1.foo()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'mod1' is not defined

But that wasn't the case.

.. _`regular package`: https://docs.python.org/3/reference/import.html#regular-packages
.. _`Namespace packages`: https://docs.python.org/3/reference/import.html#namespace-packages
.. _`PEP 420`: https://www.python.org/dev/peps/pep-0420/
