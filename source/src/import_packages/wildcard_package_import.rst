************************
Wildcard package imports
************************
If we run the ``from package import *`` statement against a **namespace package** we are not bringing anything into the caller's namespace. But if we run it against a **regular package**, we are pulling in all the definitions we import in the ``__init__.py`` file.

Sometimes, **dunder init** files include definitions that have some internal functionality but are not intended for public use. Package developers have two ways of protecting the package's API from **wildcard** imports:

1. Hide modules by prepending their names with an **underscore**.
2. Define the ``__all__`` special variable to explicitely declare what modules will be imported by ``import *`` statements, if any.

A variable named ``__all__``
============================
When we define the ``__all__`` variable inside a **dunder init** file, it provides an special index of **modules** that will be imported when a **wildcard** statement is used against the package.

.. note:: ``__all__`` only affects ``from package import *`` statements.

In practice, ``__all__`` contains a list of **strings** with the **module names** that should be imported when a **wildcard** statement is encountered. 

Not using ``__all__``
---------------------
To understand how this variable works, let's start with an example that does **not** use it::

    ex04/
        pkg/
            __init__.py
            mod1.py
            mod2.py
            _internal.py

The :download:`dunder init </code/import_pkgs/ex04/pkg/__init__.py>` of ``pkg.subpkg`` contains:

.. literalinclude:: /code/import_pkgs/ex04/pkg/__init__.py

Let's change to the directory above the package and start an interactive session::

    $ cd ex04 && python3

If we use a **wildcard** import we can access the definitions of all the modules imported in the dunder init file::

    >>> from pkg import *
    >>> mod1.foo()
    I'm in pkg.mod1
    >>> mod2.bar()
    I'm in pkg.mod2

Except the modules that start with an **underscore**, like ``_internal``::

    >>> _internal.hidden()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name '_internal' is not defined

So without using ``__all__``, we can hide modules by preceding their names with an underscore.

Defining ``__all__``
--------------------
Now let's edit the :download:`dunder init </code/import_pkgs/ex05/pkg/__init__.py>` of the last example::

.. literalinclude:: /code/import_pkgs/ex05/pkg/__init__.py

Note how we have included the module ``_internal`` in the list. Let's see what happens when we try to access any of its definitions::

    >>> from pkg import *
    >>> _internal.hidden()
    I'm hidden in: pkg._internal

Yes, we can access the ``hidden`` function without any issue, even though it's in a module that should be hidden from wildcard imports. That's the effect of ``__all__``. And if we tried to access any of the definitions of a module not included in the list::

    >>> mod2.bar()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'mod2' is not defined

Empty ``__all__``
-----------------
Sometimes developers don't want to allow **wildcard** imports. The best way to do that is to define an empty ``__all__`` variable. Like what we did in this :download:`init file </code/import_pkgs/ex06/pkg/__init__.py>`:

.. literalinclude:: /code/import_pkgs/ex06/pkg/__init__.py

Let's import everything::

    >>> from pkg import *

And try to use any definition of any of the modules::

    >>> mod1.foo()
    Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
    NameError: name 'mod1' is not defined

Nothing is available when wildcard import is used. In practice it works like disabling these type of import statements.

API definition starts in the modules
====================================
Using the ``__all__`` special variable at the **package level** only allows us to expose/hide **modules**. What if we want to expose/hide only certain **definitions** inside our modules?

As we saw in the section about :ref:`modules <wildcard-modules>`, ``__all__`` can also be used inside **modules** to define the names that should be available to **wildcard** import statements.

.. note:: As a final recommendation, you should avoid to use **wildcard** imports at any level of your source code.