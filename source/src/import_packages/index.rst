.. _import-packages:

###############
Import packages
###############

.. toctree::
	:maxdepth: 3
	:hidden:

	about_packages
	implicit_namespace_packages
	regular_packages
	import_statement
	wildcard_package_import

As the code base of an application gets bigger, we need a way of organizing the growing number of modules. If we keep them all in a single location, sooner or later we'll experience name conflicts. To help with this problem, Python allows us to organize our modules in **directories**, and **subdirectories**, which are known as **packages** and **subpackages** respectively.::

    a_pkg/
        mod1.py
    b_pkg/
        mod1.py
		c_pkg/
			mod1.py

Both directories, ``a_pkg`` and ``b_pkg`` are considered by Python as `packages`_, whereas ``c_pkg`` is a **subpackage**. Note that all of them contain a module with the **same name**, but it's not a problem, since ``a_pkg.mod1``, ``b_pkg.mod1``, and ``b_pkg.c_pkg.mod1`` are all different modules. 

.. note:: A **subpackage** is just a package inside a package.

We saw how the use of **modules** saves programmers from having to worry about conflicting **definition names** (variables, functions, classes, etc) in our source code. The same way, **packages** create a new layer in the namespace that saves authors from worrying about the name of their modules conflicting with the name of other modules that live in a different package.

**Package names** are added to the **module names** using **dot notation**, and that's what we use to import them, for example::

    import a_pkg.mod1, b_pkg.mod1, c_pkg.mod1

Packages are useful not only to avoid name collisions. It allows us to organize our modules around a **hierarchical structure**, wich is essential when creating a complex system.

.. _`packages`: https://docs.python.org/3/tutorial/modules.html#packages