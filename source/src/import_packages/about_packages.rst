************************
About the term "package"
************************
In Python, the term **package** has two different meanings:

1. An `import package`_, which is just a folder to organize your modules, and optionally a **dunder init** file.
2. A `distribution package`_, is a way of distributing Python programs, hence the name. A distribution package consists of a  **single file** containing Python packages, modules and other resources, along with metadata. All of it is packed in an single `archive file`_ for easy distribution. The `PyPI`_ is the official source for Python distribution packages.

You can check the section about :ref:`distribution packages <distributions>` for more information about them. In this one we are gonna cover just **import packages**.

Types of import packages
========================
Python differentiates between two types of **import packages**:

1. A `regular package`_ is a directory that contains an **initialization file**. ``__init__.py`` file that makes them recognizable as a package by Python interpreter. In older Python versions this file was required even **empty**, to qualify a directory as a package.

2. `Namespace packages`_. **Python 3.3** introduced what is known as **Implicit Namespace Packages**, which means that **empty** ``__init__.py`` files are not required anymore, our folder will still be consider implicitly as a package.

.. note:: **Implicit namespace packages** are also known as `Native Namespace Packages`_

Packages can contain **subpackages** which are just subdirectories that have to contain an ``__init__.py`` file.

.. _`import package`: https://packaging.python.org/glossary/#term-import-package
.. _`distribution package`: https://packaging.python.org/glossary/#term-distribution-package
.. _`archive file`: https://en.wikipedia.org/wiki/Archive_file
.. _`PyPI`: https://pypi.org/

.. _`Native Namespace Packages`: https://packaging.python.org/guides/packaging-namespace-packages/#native-namespace-packages
.. _`regular package`: https://docs.python.org/3/reference/import.html#regular-packages
.. _`Namespace packages`: https://docs.python.org/3/reference/import.html#namespace-packages