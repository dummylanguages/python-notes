**************************
About the import statement
**************************
Importing packages works for the most part, the same as importing modules, although there are some small differences which we'll highlight. 

Styling
=======
According to the `PEP8 -- Style Guide for Python Code`_ ``import`` statements should be:

* Placed at the **top of the file**.
* Each one on a separate line, although we can import several modules in the same statement.
* They should be grouped in the following order:

    * Standard library imports.
    * Related third party imports.
    * Local application/library specific imports.

    You should put a **blank line** between each group of imports.

* **Absolute imports** are recommended, although **relative imports** are acceptable when dealing with complex package layouts where using absolute imports would be unnecessarily verbose.
* **Wildcard imports** (``from <module> import *``) should be avoided, as they make it unclear which names are present in the namespace.

.. _`PEP8 -- Style Guide for Python Code`: https://www.python.org/dev/peps/pep-0008/#imports

Absolute imports
================
An absolute import starts always at the **project root folder**, and specifies all the packages names using dot notation. For example::

    project/
        pgk1/
            mod1.py
        pkg2/
            mod2.py
            subpkg1/
                subpkg2/
                    mod3.py
            subpkg3/
                mod4.py
                mod5.py

Some examples of **absolute** ``import`` statements would be::

    from pgk1.mod1 import f1
    from pgk2.mod2 import f2
    from pgk2.subpkg1.subpkg2.mod3 import foo

Relative imports
================
Relative imports use **dots** to signify directory levels:

* ``.`` for the current directory.
* ``..`` for the parent directory.
* ``...`` for the grandparent directory, and so on.

This type of imports are useful when importing modules from **sibling subpackages**, for example, if we wanted to import some code from ``mod2`` into ``mod3`` we could use::

    from ...mod2 import f2

Instead of the absolute version::
    
    from pgk2.subpkg1.subpkg2.mod2 import f2

Relative imports are not so verbose, but they are brittle in projects where directory structure is likely to change.

Using it inside functions
=========================
When we use the ``import`` statement inside a function definition, it will only we executed when the function is called.