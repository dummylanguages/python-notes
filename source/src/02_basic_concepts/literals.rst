********
Literals
********
A literal is a sequence of one or more characters that represents a value, such as the number ``12`` or the string ``'hello'``. A literal is a **constant value** that appears directly in a program, not in the sense of the ``const`` keyword used in **C** language, but in the sense that they are **inmutable**, they can not be changed in place after they are created. 

A literal is a expression whose syntax generates an **object**. By simply writing the value ``12``, we are creating an **integer object** (known just as ``int``).

.. note::
	From a practical point of view, **literals** are concrete values written within the source code of a program, and meant to be interpreted literally (hence the name).

Here are some examples of literals:

+------------+----------------+
| Data types | Some literals  |
+============+================+
| Strings    | "Hello world", |
|            | 'dogs',        |
|            | '348'          |
+------------+----------------+
| Integers   | 734,           |
|            | -5             |
+------------+----------------+
| Float      | 3.14           |
+------------+----------------+

Literals by themselves don't do much in a program, if we put those literals into a script, and save the code with the name :download:`literals.py </code/chap_3/literals.py>` and run it: 

.. literalinclude:: /code/chap_3/literals.py
   :language: python
   :linenos:
   :tab-width: 4


.. code-block:: bash

	$ python3 literals.py

Nothing happens, no warnings, no errors, there's nothing wrong with the code, it's just a bunch of values, we are not doing anything with them. For a brief period of time, they create objects that take a small space in the computer's memory while the script is running, and when the script finishes, poof, they're gone.


