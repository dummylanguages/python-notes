*********
Functions
*********
The first word, ``print()``, is a **function** that tells the computer to print on the screen, whatever data is inside the parenthesis. What we put inside the parenthesis is known as the **argument** of the function. In this case the argument is the string "Hello world". 

Built-in functions
==================
So far, we have used three functions: ``print()``, ``input()`` and ``int()``. These functions are **built-in** into python, meaning that they are directly available, and for using them, we just have to write their names into our code. That is known as **calling a function**. Check this code::

	>>> print("Hello world")


Python includes a lot of **built-in functions**, to see the whole list you just have to write this::
	>>> dir(__builtins__)

Python standard library
=======================
Apart from the built-in functions, there are also available a lot of other functions available in the `Python's Standard Library`_, which is a huge bunch of functions organized into **modules**.

To use one of these functions, we can not simply call them. Before we have to import the module where the function lives in, and then they become available in our code.

For example, let's say that we want to calculate the square root of a number. There is no **BIF** to do that, but we can import a **module** that contains a lot of functions, and use the function::

	# sqrt.py

	import math

	print("This program calculates the square root of positive numbers.")
	num = input('Please, enter the number: ')
	num_float = float(num)

	result = math.sqrt(num_int)
	print("The square root of" , num , 'is' , result)

We are importing a module named **math**, that contains a lot of mathematical functions. One of them, is the ``sqrt()`` function. To use that function, we have to use the **dot notation** meaning that especify the name of the module that contains the function, followed by a dot, followed by the function itself: ``math.sqrt()``.
Since the ``sqrt()`` function only works with numbers, and the variable ``num`` contains a string, we have to do a type conversion in the variable ``num_float``. We convert the numeric string to a float value with the ``float()`` built-in function.

When you import something from a module, you either use:

1. To import a whole module: ``import some_module``
2. To import only one function: ``from some_module import some_function``
3. To import several functions we use commas to separate them: ``from some_module import some_function, another_one, and_another_one``
4. When you are certain that you want to import everything from the given module: ``from some_module import *``

We can even import modules or functions and changed their names in our scripts (or a given interactive session) on the fly. To do that we make use of the ``as`` clause. For example::

	>>> import math as foobar
	>>> foobar.sqrt(4)
	2.0

Or for the given function::

	>>> from math import sqrt as calculate_square_root_of
	>>> calculate_square_root_of(4)
	2.0

.. _`Python's Standard Library`: https://docs.python.org/3/library/