.. sidebar:: Table of contents.

	.. contents::
		:depth: 3 
		:local:

************
Basic output
************
By basic output we mean the textual information that comes out of our programs to the standard output, wich is the terminal. In python we use print operations quite often, and they offer mechanisms that allow us to: 

* Convert one or more objects to a textual representation (String).
* Some minor formatting of those strings it's also available.
* Although most of the times the result is sent to either **standard output**, we can also print to **files** or another file-like stream.

``print``, a statement or a function?
=====================================
Printing is also one of the most visible places where Python 3.X and 2.X have diverged. In fact, this divergence is usually the first reason that most 2.X code won’t run unchanged under 3.X. Specifically, the way you code print operations depends on which version of Python you use:

* In Python 3.X, printing is a ** function**, with keyword arguments for special modes.


The ``print`` statement in python 2.X
-------------------------------------
In python 2.X, ``print`` is a statement with unique and specific syntax, rather than a built-in function. 

.. tip::
	Users of recent Python 2.X releases can also import and use 3.X’s flavor of printing in their Pythons if desired, both for its extra functionality (the 2.X statement does not support separator specification at all) and to ease future migration to 3.X. 


The ``print()`` function in python 3.X
--------------------------------------
In python 3.X, ``print()`` is a **built-in function**. This BIF do its work without returning any value we care about (technically, it returns ``None``), what is known as a **procedure**, every call to ``print()`` can be considered as an **expression statement**.

.. note::
	In python 3.X, calls to ``print()`` are expression statements.

Call format
^^^^^^^^^^^
Because it is a normal function, printing in 3.X uses standard function call syntax, rather than a special statement form (like in python 2). The formal notation for this syntax form is::

	print([object, ...][, sep=' '][, end='\n'][, file=sys.stdout][, flush=False])

In this formal notation, items in square brackets are optional and may be omitted in a given call, and values after ``=`` give argument defaults.

* **object**: The textual representation of each object to be printed is obtained by passing the object to the ``str`` built-in call, this built-in returns a “user friendly” display string for any object.
* ``sep`` is a string inserted between each object’s text, which defaults to a single space if not passed.
* ``end`` is a string added at the end of the printed text, which defaults to a ``\n`` newline character if not passed.
* ``file`` specifies the file, standard stream, or other file-like object to which the text will be sent; it defaults to the sys.stdout standard output stream if not passed. Files should be already opened for output.

Let's see some examples:

.. literalinclude:: /code/chap_7/print.py
	:language: python
	:linenos:
	:tab-width: 4

Implicit and explicit line joining
----------------------------------
Sometimes a program line may be too long to fit in the Python **recommended** maximum length of **79** characters. There are two ways in Python to do deal with such situations—implicit and explicit line joining.

Implicit Line Joining:
^^^^^^^^^^^^^^^^^^^^^^
There are certain delimiting characters that allow a logical program line to span more than one physical line. This includes: 

* matching parentheses
* square brackets
* curly braces 
* triple quotes
 
For example, the following two program lines are treated as one logical line,
::

	>>> print('Doc', 'Dopey', 'Bashful', 'Grumpy', 
	...	'Sneezy', 'Sleepy', 'Happy', 'Snoop Doggy Dog')

Matching quotes (except triple quotes) must be on the same physical line. For example, the following will generate an error:
::

	>>> print('You can not break this line in an implicit way
		 File "<stdin>", line 1
		     print('You can not break this line in an implicit way
		                                                         ^
	SyntaxError: EOL while scanning string literal


Explicit Line Joining:
^^^^^^^^^^^^^^^^^^^^^^
In addition to implicit line joining, program lines may be explicitly joined by use of the backslash ``\`` character. Program lines that end with a backslash that are not part of a literal string (that is, within quotes) continue on the following line::
	
	>>> print('Doc', 'Dopey', 'Bashful', 'Grumpy', \
	>>> 'Sneezy', 'Sleepy', 'Happy', 'Snoop Doggy Dog')
	Doc Dopey Bashful Grumpy Sneezy Sleepy Happy Snoop Doggy Dog

There's a lot more to say about strings, but we are gonna do it in a later chapter. 
