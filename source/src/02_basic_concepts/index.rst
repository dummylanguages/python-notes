###############
Basics concepts
###############

.. toctree::
	:maxdepth: 3

	basic_input
	basic_output
	literals
	variables/index
	expressions
	intro_functions
	Variables and objects <intro_objects>