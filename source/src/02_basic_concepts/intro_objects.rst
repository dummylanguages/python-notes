.. sidebar:: Objects

	The idea of incorporating “objects” into a programming language came out of work in **computer simulation**. Given the prevalence of objects in the world, it was natural to provide the corresponding notion of an object within a simulation program. 

Introduction to objects
=======================
In the programming world, the term object is an **abstract concept** derived from the objects of the real world.. An object is one of the first concepts that a baby understands during its development. They understand an object as something that has a set of **attributes** (“big,” “red” ball) and a related set of **behaviors** (it rolls, it bounces). 

:ref:`From pg. 207<learning-python>`

In programming **objects** also have attributes and behavior. The **attributes** of a car, for example, include its color, number of miles driven, current location, and so on. Its **behavior** include driving the car (changing the number of miles driven attribute) and painting the car (changing its color attribute), for example.

Similarly, in the computer science world, an **object** contains a set of attributes, and a set of available operations that provide its behavior. For example::

	>>> a = 7

This assignment is creating an integer object, and at the same time a reference to the object, that is gonna allow us to manipulate the object. Its value is its main attribute. The behavior of this object is defined by the available operations for integer numbers, mostly arithmetic operations::

	>>> a * 7
	28
	>>> a / 2
	3.5
	>>> (a + 1) / 2
	4

Variables are objects references
--------------------------------
A variable is just the name we are gonna use to manipulate the object. The variable is not the object itself, just a reference to the object. For example, the following assignment statement creates an integer object and assigns it a name::

	>>> a = 7

When we create a variable, 2 entities are stored in memory:

1. The value of the object itself, known as **dereferenced value**, in this case is ``7``. Every time we use a variable reference in our code, we get the dereferenced value:
::
   
   >>> a
   7

2. The **reference** to the object. A reference is 2 things:

	* A **name**: the name we choose for the variable, in this case ``a``.
	* Every reference has a value. The **reference value** is the location in memory where the value of the object(**dereferenced value**) is stored. We can find out using the ``id()`` function, that returns the position in memory for the integer object::

		>>> id(a)
		4441029120

Regarding the names, they live in **namespaces** that python creates authomatically for each file. Names in Python spring into existence when they are first assigned values, and they must be assigned before they are used. Python uses the location of the assignment (maybe the top-level script, or a module) to associate it with a particular namespace, so the place where you assign a name in your source code determines the namespace it will live in.

For those names assigned in the interactive mode, Python creates a namespace in a module named ``__main__``

.. note:: 
	See more about **namespaces** in the :ref:`functions <functions>` and :ref:`modules <modules>` sections.

When you use a name in a program, Python creates, changes, or looks up the name in the appropriate namespace. Namespaces are managed authomatically by Python, we don't have to do anything about it.

Assigning references
--------------------
So variables are references. What happens when we assign a variable to another variable? For instance, let's create our first variable, or in other words let's assign our first name::

	>>> a = 'chi'
	>>> a
	'chi'

Now let's create a second  variable, assigning it the name of the first one::

	>>> b = a
	>>> b
	'chi'

Both of these variables are references to the exact same object, both are pointing to the same location in memory, we can check that with the ``id()`` function::

	>>> id(a)
	4443848016
	>>> id(b)
	4443848016

What happens when one of the variables is assigned another object? Let's check::

	>>> a = 'ki'
	>>> a
	'ki'
	>>> b
	'chi'
	>>> id(a)
	4443847848
	>>> id(b)
	4443848016

When one variable is assigned to another, it is the **reference value**, the location in memory what is assigned, not the dereferenced value. So even when the **dereferenced value** that ``a`` is pointing at, has changed, the value of ``b`` is still the same, because from the beginning what was assigned was the location in memory of the string object 'chi', but not the object itself.

Memory deallocation and garbage collection
------------------------------------------
Now let's see what happens when in addition to variable ``a`` being reassigned, variable ``b`` is reas- signed as well::

	>>> b = 'zen'
	>>> id(b)
	4362415776

After ``b`` is assigned to ``'zen'``, the memory location storing the string ``'chi'`` is no longer referenced by any variable, so it can be deallocated. To **deallocate** a memory location means to change its status from “currently in use” to “available for reuse.” In Python, memory deallocation is automatically performed by a process called **garbage collection**.

The garbage collector automatically is an ongoin process during the execution of a Python program, taking care for us of determining which locations in memory are no longer in use and deallocating them.




