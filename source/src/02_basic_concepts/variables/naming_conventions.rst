.. _naming-variables:

Naming conventions
==================
We can choose whatever name we want for a variable, the only requisite is that it has to be a **valid identifier**. The wise programmer selects names that inform the human reader about the purpose of the data. This, in turn, makes the program easier to maintain and troubleshoot. For example, a program that interacts with users personal information might use variable names as ``name``, ``address``, ``age``, and ``profession``.

Identifiers
-----------
An identifier is a sequence of one or more characters used to provide a name for a given program element. 

* Identifiers may contain **letters** and **digits**, but cannot begin with a digit.
* An identifier can be of any length.
* Python is **case sensitive**, thus, Line is different from line. 
* The **underscore character**, ``_``, is also allowed to aid in the readability of long identifier names. However, using the ``_`` as the first character is not a good idea, because in Python identifiers beginning with an underscore have special meaning.
* **Spaces** are **not allowed** as part of an identifier. In programming languages, spaces are used to delimit (separate) distinct syntactic entities, thus, any identifier containing a space character would be considered as two separate identifiers.
* There are certain **reserved words** which have special meanings in Python. We cannot use any of these **keywords** as a name for a variable.

Keywords
--------
A keyword is an identifier that has predefined meaning in a programming language. Therefore, keywords cannot be used as “regular” identifiers. Doing so will result in a syntax error, for example::

	>>> and = 34
	SyntaxError: invalid syntax

To see the list of keywords in python, use the ``help()`` function::

	>>> help('keywords')

	Here is a list of the Python keywords.  Enter any keyword to get more help.

	False               def                 if                  raise
	None                del                 import              return
	True                elif                in                  try
	and                 else                is                  while
	as                  except              lambda              with
	assert              finally             nonlocal            yield
	break               for                 not
	class               from                or
	continue            global              pass

There are other predefined identifiers that can be used as regular identifiers, but should not be. This includes ``float``, ``int``, ``print``, ``exit``, and ``quit``, for example. A simple way to check whether a given identifier is a keyword in Python is given below::

	>>> 'exit' in dir(__builtins__)
	True

.. todo::
	Constants, uppercase naming, like PI.

Program comments
================
A comment is a piece of program text that the interpreter ignores but that provides useful documentation to programmers. Comments begin with the ``#`` symbol and extend to the end of a line.

.. note:: Comments are not strings.
