.. _assignments:

Assignments
===========
Assignments, are **statements** which in their most common and basic form, consist on binding a name to a single value. For example::

	>>> x = 4

In the example above, we assigned a single value, ``4``, to the variable ``x``. But assignments can be more complex, that's why the general form of an **assignment statement** is the following::

	<name> = <expression>

Where ``<name>`` is any name we freely choose (within certain limits), and ``<expression>`` maybe anything that evaluates to a value. For example::

	>>> x = 2 + 2

The Python interpreter **first** evaluates the **expression on the right side** of the assignment symbol and then binds the variable name on the left side to this value. When this happens to the variable name for the first time, it is called **defining or initializing** the variable. 

.. note:: 
	The ``=`` symbol means **assignment**, not equality. 

So **assignments** create variables. Variables are **references to objects**. They do not contain the objects or even a copy of the objects. Because of that, Python variables are more like pointers than data storage areas. We can think of variables as arrows pointing to values, better than boxes containing values.

.. note::
	Unlike other languages, Python creates a variable name the first time you assign it a value, so there’s no need to predeclare names ahead of time.

After you initialize a variable, subsequent uses of the variable name in expressions are known as **variable references**. When the interpreter encounters a variable reference in any expression, it looks up the associated value and the name is replaced with the value it references. 

Undefined variables
-------------------
Names must be assigned a value before being referenced. It’s an **error** to use a name to which you haven’t yet assigned a value. If a name is not yet bound to a value when it is referenced, Python signals an error.::

	>>> a = 2
	>>> a + b
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	NameError: name 'b' is not defined

.. note::
	In other languages, unassigned variables, return some sort of ambiguous default value. If Python provided default values for unassigned names used in your program instead of treating them as errors, it would be much more difficult for you to spot name typos in your code.

Modifying variables
-------------------
A variable start its life when a name is pointed to some value, but during the execution of a program, the variable can be associated to another value. We also use the assignment statement for this. So the assignment statement has 2 uses:

* Declaring variables. 
* Changing the values of the variables.

We can change the value the variable is pointing to, whenever we want (hence the name variable). Let's see this in action:

.. literalinclude:: /code/chap_3/variables.py
   :language: python
   :linenos:

* In line 1, we define the variable ``message`` using an assignment expression. To the right of the assignment operator, the literal ``"Hello world"`` creates a **string object**. Then the object is assigned the name ``message``. 
* Line 2 use the ``print()`` function. In its argument we reference the ``message`` variable.
* In line 4, we change the value of the variable ``message``. We are creating another string object, and associating ``message`` with it.
* In line 5, the ``print()`` function references again the ``message`` variable, but this time ``message`` was associated with a different value.

You can run this :download:`script </code/chap_3/variables.py>` with::

	$ python3 variables.py
