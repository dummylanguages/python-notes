Assignments through keyboard input
==================================
The value that is assigned to a given variable does not have to be specified in the program, as demonstrated in previous examples. The value can come from the user by use of the ``input()`` function.

The ``input()`` function
------------------------
Here we are gonna learn how to make our programs more interactive, so when we run them, we can get input from the users. For example, check the next :download:`script </code/chap_3/input_1.py>`:

.. literalinclude:: /code/chap_3/input_1.py
   :language: python
   :linenos:

* In line **1** we use the ``print()`` function for asking the user's name. 
* In line **2** we use a new function named ``input()``, that stops the program execution, giving the user a chance to enters his name. The problem is, that once the user enters the name, the program doesn't do anything with what the ``input()`` function returns. This function returns the string object that the user types on the keyboard, but if we don't assign that string to a name, its value gets lost.

.. note::
	Some functions returns values. ``input()`` is one of them. 

We need to create a variable to store the user's input. Let's do that in the :download:`second version </code/chap_3/input_2.py>` of our script:

.. literalinclude:: /code/chap_3/input_2.py
   :language: python
   :linenos:

* This time in line **2**, we store the value returned by the ``input()`` function, in a variable called ``name``. The type of this value is always gonna be **string**, since this is how the ``input()`` function is designed, it doesn't matter if the user enters some number, the ``input()`` function takes the user's input, and returns it as a string. 
* Line **4** use the value stored in the variable ``name`` to print the user's name.

Not bad so far, but we could shorten this code. Notice that the parenthesis of the ``input()`` function are empty, let's change that:

.. literalinclude:: /code/chap_3/input_3.py
   :language: python
   :linenos:

Whatever text you put inside the parenthesis of the ``input()`` function, is printed before the prompt. This is pretty useful for showing messages about the information expected from the user, without having to use an additional ``print()`` function.

.. note:: Tecnically speaking, the string of text that we write between the parenthesis of ``input()`` function, is known as **argument**. Some functions take arguments, and we pass arguments to a function by writing them between parenthesis.