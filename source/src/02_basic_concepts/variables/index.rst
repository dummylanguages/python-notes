*********
Variables
*********

.. toctree::
	:maxdepth: 3
	:hidden:

	assignments
	naming_conventions
	assignments_stdin

To better understand what variables are, it's useful to compare them with literals. Literals are *inmutable objects*, you can not modify them. Remember that literals are just values, if you modify a value then you don't have that value anymore, you have a different one. 

In contrast, a **variable** is a name which we use to identify a value. For example a suitable name to identify my age could be ``my_age``. Every year ``my_age`` increases by one, today it could be 40, and same day next year 41. In other words, **literals** are values, and **variables** are the names used to manipulate those values.

In short, a **literal** evaluates to itself, whereas a **variable**, evaluates to the value the variable is pointing at a given point in time.