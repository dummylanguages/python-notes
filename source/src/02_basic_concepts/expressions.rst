***********
Expressions
***********
We have seen how simple assignments, may consists of single values written to the right of the assignment operator::

	>>> x = 33

And it's also very common to assign a variable to another variable, like in::

	>>> age = x

But they are also common those assigments that make use of expressions, which consist of a combination of operators(values or variables) and operands::

	>>> x = 33
	>>> y = 2 * (x + 1) 

.. note:: An expression is a combination of one or more symbols that evaluates to a value, a literal. 

In this context, a **subexpression** is any expression that is part of a larger expression. Subexpressions may be denoted by the use of parentheses. This expression has:

* Two **subexpressions**, ``2`` and ``(x + 1)``.
* The operator ``*``.

All expressions evaluate to a literal
-------------------------------------
An expression can be made out of a single literal value, or a combination of values and operators. In any case, as expression always evalueats to a value. So depending to the **type of value** an expression evaluates to, we can talk of different kind of expressions. For example, expressions that evaluate to a numeric type are called **arithmetic expressions**.
 
But there are also expressions that evaluate to other than a number, for example, ``3 > 2`` evaluates to ``True``. That's why it's called a **boolean expression**. So generally speaking, depending on the type of the value the expression evaluates to, we can speak of different kind of expressions:

* Arithmetic expressions
* Boolean expressions
* Literals
* Variables

are all expressions.

Expressions are a way to perform operations on values to produce other values. At the same time, literals and variables can also be considered expressions.

Boolean and relational expressions
==================================
Generally speaking, **boolean expressions** are those that always evaluate to a **boolean value** (``True`` or ``False``). **Relational expressions** are a subtype of boolean expressions, because they always evaluate to a boolean value.::
  
	>>> 3 > 2                  # This is a relational expression.
	True
	>>> (3 > 2) and False       # This is a boolean expression.
	False

