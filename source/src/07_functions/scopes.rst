Scopes and namespaces
=====================
Names in Python spring into existence when they are first assigned values, and they must be assigned before they are used. Python uses the **location of the assignment** to determine where a name can be used. A scope is a **textual region** in our code, where a name is assigned. The scope determines where that name is accessible, where it can be used. For example:

.. code-block:: python
	:linenos:

	>>> s = 'hello'
	>>> def fun():
	...     print(s)
	...     y = 'bye'
	...
	>>> fun()
	hello

* In line **1** we assigned the name ``s`` at the top-level of our interactive session.
* Then, inside the function (line **3**) we used it, it was accessible to the function because that name has **global scope**, it's accessible to the whole session.

But if we try to access the name ``y``, defined inside the function, from outside the function, the interpreter will throw an error:

.. code-block:: python
	:linenos:

	>>> print(y)
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	NameError: name 'y' is not defined

The reason is that ``y`` is in the **local scope** of the function, meaning that is only accessible from inside the function, but is not defined outside it.

.. note:: This is known as **lexical scoping**, because the scope of a variable is determined entirely by the location of the variable asignment in the source code of your program files, not by function calls.
 
So, depending where a name is assigned we have several scopes:

* Every nested function has its own scope, this is the innermost scope. For each of these scopes Python creates an independent namespace. This namespace contains **local names**, that's why this scope it's known as the **local scope**.

* **Enclosing scopes** are outer scopes containing local scopes. For example, enclosing functions contain **local names** that are *non-local* to the functions nested in. These names are also *non-global* names.

* The next-to-last scope is the current **module**’s scope. Its associated namespace contains **global names** that's why is known as the global scope.

* The outermost scope (searched last) is the namespace containing **built-in** names.

The scope of a name means where it is assigned in the source code of your program files. It also refers to its range of functionality, where it's accessible, where it can be used. 

Namespaces
----------
Python uses the scope of a name to associate it with a particular namespace. For every scope, Python generates a namespace. Namespaces are just containers for mapping names to objects. 

.. note:: 
	The place where we assign a name in our source code determines the **namespace** it will live in.

The most important thing to remember is that every namespace is isolated from the other ones, allowing the existence of identical names as long as they live in different namespaces. 

In Python we have multiple independent namespaces, that are created at different moments and have different lifetimes: 

* The statements executed by the top-level script, are considered part of a module called ``__main__``, so they have their own global namespace. The code that we type in a session at the interactive mode also lives in a ``__main__`` module.

* The builtin functions we have been calling from inside our scripts, also live in a module called ``__builtin__`` and also have their own namespace. The **builtin namespace** is created when the Python interpreter starts up, and is never deleted.

* The **global namespace** for other modules is created when the module definition is read in; normally, module namespaces also last until the interpreter quits.

* The **local namespace** for a function is created when the function is called, and deleted when the function returns or raises an exception that is not handled within the function. Every time we define a new function using the **def statement** we are also defining a new namespace. But only when we **call** that function, is when a new local scope, a namespace for the function is created. The **lifetime** of this namespace is equal to the duration of its function’s execution.

The innermost scope
-------------------
Functions are useful because allow code reuse and modularity. On top of that, functions offer another important advantage, they add an extra namespace layer to your programs to minimize the potential for collisions among variables of the same name outside those functions.

.. note:: **By default**, all names assigned inside a function exist only within that function’s namespace, and no other. 

This rule means that:

* Variables assigned inside a function, can only be seen by the code within that function. You cannot reach such names from outside the function.

* References assigned inside a function do not clash with references outside the function, even when they have identical names. They don't clash because their names live in different namespaces, they are completely different references.

Depending where a variable is assigned in relationship to a function, we can talk about 3 kinds of scope:

* A variable assigned inside a function has **local scope**, it's reachable only inside the function. 
* A variable assigned inside an enclosing function has **non-local scope** to the nested functions.
* A variable assigned outside all functions, it has **global scope** to the entire file, it's reachable by all the functions inside it. 
  
.. warning::  Given a variable with global scope named ``x``, if some function use the same name for a local variable, the local name has higher priority, and is gonna mask the name with global scope. This is known as **variable shadowing**.

The scope of a variable (range of functionality), is always determined by where it is assigned in your source code. This is called **lexical scoping** and means that a name can only be called (referenced) from within the block of code in which it is defined. 

Let's see an example:

.. literalinclude:: ../../code/chap_8/scope.py
	:language: python
	:linenos:
	:emphasize-lines: 4
	:tab-width: 4

* In lines **1** we assigned the name ``x`` to a string object, it has **global scope**. 
* In line **4**, inside the function, we have assigned the same name to another different string object, this name has **local scope**.
  
If we check the output of this script, we can see that even when we are using the same name for 2 different objects, they don't clash because they live in different namespaces.

.. literalinclude:: ../../code/chap_8/scope.output

They are 2 different variables, with different scopes. They live in different namespaces. 

Scope details
-------------
So far we know that we have multiple namespaces:

* Modules have namespaces.
* Functions have namespaces.
* Functions inside other functions have their own nested namespaces.

1. Modules: Each module is a namespace in which names assigned at the top level of the module file live. These variables have **global scope**. This global scope only affects a single file. Every module has an independent namespace, and names inside it are global in that module.

The built-in namespace contains the names of the predefined ``__builtins__`` module Python provides.

Every session in the interactive mode is also a module named ``__main__``. Variables defined in the interactive mode, have **global scope**, accessible to the entire interactive session.

2. Functions: All the names **assigned** inside a function definition are **by default** local names that live in the **local scope**. (the namespace associated with the function call.) But if we use the ``global`` or the ``nonlocal`` statements, we can, inside a function, assign names that live in the enclosing module or enclosing function respectively.

But in-place changes to objects do not classify names as locals, only actual name assignments do. For example:

.. literalinclude:: ../../code/chap_8/in_place.py
	:language: python
	:linenos:
	:emphasize-lines: 4
	:tab-width: 4

The change in place we make in line **4** doesn't mean that the list name  ``x``, lives in the namespace of the function. We changed the list, because ``x`` has global scope. Check the output:

.. literalinclude:: ../../code/chap_8/in_place.output

.. note:: Changing an object is not an assignment to a name.

Not only variables but, any type of **assignment** within a function classifies a name as local:

* Module names in ``import`` 
* Function names in ``def`` 
* The **parameter** names written in parentheses.

The other names (not assigned inside the function) but used inside a function definition are either:

* Local names from enclosing functions.
* Global names.
* Builtin names.

Name resolution: The LEGB rule
------------------------------
The place in your code where a variable is assigned usually determines its scope. When a variable is referenced, Python searches for it in this order: 

#. In the **local (L) scope**: names assigned in a function definition, and not declared **global** or **nonlocal**.

#. In the local scopes of any **enclosing (E)** functions: names in the local scope of any enclosing function, from inner to outer. Also names declared **nonlocal** in a nested function.
 
#. In the **global (G) scope**: names assigned in top-level of a module file, or declared **global** within a function definition in the file.

#. And finally in the **built-in (B) scope**: those names preassigned in the built-in module. 
   
The first occurrence wins. 

The second resolution level may include lookups in several sublevels, when you nest functions inside functions. 

.. note:: The lookup order followed in this rule only applies to **unqualified names** like ``x``. But those references that use qualified attribute names, like for example ``object.x`` follow a completely different set of lookup rules than those covered here. 

The built-in scope
------------------
The built-in scope is just a built-in module called **builtins**. To see all the names that exist in this module we can do::

>>> dir(__builtins__)

The list is very big, it contains built-in exceptions, built-in functions, special names like  ``True``, ``False``, ``None``, etc.

The ``global`` statement
------------------------
Global names are variables assigned at the top level of the enclosing module file, and they may be referenced within a function without being declared. For example:

.. literalinclude:: ../../code/chap_8/global_1.py
	:language: python
	:linenos:
	:emphasize-lines: 4
	:tab-width: 4

If inside a function definition we assign a name already in use in the module, the name in the function doesn't clash with the name in the module, they live in different namespaces:

.. literalinclude:: ../../code/chap_8/global_2.py
	:language: python
	:linenos:
	:tab-width: 4

The output of this script shows that the name ``x`` lives in 2 different namespaces, in each space each name reference a different object.

.. literalinclude:: ../../code/chap_8/global_2.output

If we want to reassign a global name from inside a funtion, we can use the ``global`` statement:

.. literalinclude:: ../../code/chap_8/global_3.py
	:language: python
	:linenos:
	:emphasize-lines: 4-5
	:tab-width: 4

In line **4** we declared ``x`` as global, an in line **5** the reassignment allowed us changing the name outside the function. The output shows how they are references to the same object:

.. literalinclude:: ../../code/chap_8/global_3.output

Program Design: Minimize Global Variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
By default, names assigned in functions are locals, so if you want to change names outside functions you have to define them using the global or the nonlocal statement. Changing globals is not advisable because they make programs difficult to understand and reuse. For example, consider this module:

.. code-block:: python

	X = 99
	
	def func1():
		global X 
		X = 88
	
	def func2(): 
		global X
		X = 77

If we have to work with this module, we can't tell what's the value of the global ``X``. For knowing its value, we have to trace the calls to ``func1()`` and ``func2()`` throughout all the program, because the value of ``X`` depends on which of these functions was called last.

On the other hand, local variables disappear when the function returns, but globals do not. When we need to retain **shared state information** (information that a function needs to remember for use the next time it is called) globals variables are most straightforward way to do it.
Nonetheless, there are other techniques to achieve that, like nested scope closures or object-oriented programming with classes

Bottom line, functions should rely on arguments and return values instead of globals.

Program Design: Minimize Cross-File Changes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The main point about modules is that by segregating variables on a per-file basis, they avoid name collisions across files, in much the same way that local variables avoid name clashes across functions.
We can access names in other files::

	# first.py
	X = 99

Now in ``second.py``, we import the first module, to access its variable ``first.X``: 

.. code-block:: python
	:linenos:

	# second.py
	import first 
	print(first.X)	# OK
	first.X = 88	# Not OK

* In line **3** we reference ``first.X`` to print its value. That's fine.
* But the assignment in line **4** is doing a cross-file change on a variable. This sets up a strong coupling between the 2 files, that makes it more difficult to maintain, specially if they live in separate directories.

If we need communication between files, it's better doing it through specific functions, passing in arguments and getting back return values. For example:

.. code-block:: python
	:linenos:

	# first.py
	X = 99

	def setX(new): 
		global X
		X = new

In line **3**, we have define an **accessor function** with the name ``setX()``. 

Then, in a second file, if we have to change ``X``, we'll use the function ``setX()``, instead of directly accessing it:

.. code-block:: python
	:linenos:

	# second.py
	import first first.setX(88)

Scopes and Nested Functions
===========================
In the LEGB lookup rule, the **E** stands for the local scopes of any enclosing functions. Enclosing scopes are sometimes also called statically nested scopes, correspond to physically and syntactically nested code structures in your program’s source code text.

Enclosing scopes
----------------
An assignment inside a function, by default creates or changes the name X in the current local scope. The 2 exceptions are:

* If X is declared **global** within the function, the assignment creates or changes the name X in the enclosing module’s scope instead. 
* If, on the other hand, X is declared **nonlocal** within the function, the assignment changes the name X *in the closest enclosing function’s local scope*.

When nested functions are present, variables in enclosing functions may be referenced:

.. code-block:: python
	:linenos:

	def f1(): 
		X = 88

		def f2(): 
			print(X)
	f2()

The ``nonlocal`` statement
--------------------------
But when we want to change a variable in an enclosing function, we need to use nonlocal declarations:

.. code-block:: python
    :linenos:

    def f1(): 
        X = 88
        def f2(): 
            nonlocal X
            X = 99      # Changing X in f1()
        
        f2()            # Calling f2() from inside f1()
        print(X)
    f1()

The output is ``99``, of course.

We have to remember a couple of things about ``nonlocal``:

#. It only exists in Python 3.X
#. It can be used only inside functions, not at the top-level of a module.
#. The ``nonlocal`` names really must have previously been assigned in an enclosing function, or else we'll get an error: ``SyntaxError: nonlocal declaration not allowed at module level``
