#########
Functions
#########

.. toctree::
	:maxdepth: 3

	functions
	scopes
	arguments
	advanced_concepts
