Anonymous Functions: lambda
===========================
Besides the ``def`` statement, Python also provides an expression form that generates function objects. 
This expression it's called ``lambda``. Like ``def``, this expression creates a function to be called later, but it returns the function instead of assigning it to a name. This is why lambdas are sometimes known as **anonymous functions**. 

.. note:: The name ``lambda`` comes from the Lisp language, which got it from lambda calculus, which is a form of symbolic logic. 

The lambda’s general form is:

	``lambda argument1, argument2,... argumentN : expression using arguments``

1. The keyword lambda.
2. Followed by one or more arguments (exactly like the arguments list you enclose in parentheses in a def header)
3. Followed by a colon ``:``.
4. Finished with an expression after the colon.

For example::

	>>> f = lambda x, y, z: x + y + z 
	>>> f(2, 3, 4)
	9

Lambdas are entirely optional, you can always use def instead. For example, we can write the lambda above like a normal ``def`` statement::

	>>> def f(x, y, z): 
	...     return x + y + z
	>>> f(2, 3, 4)
	9
	
A couple of things that make **lambdas** diferent:

* Unlike ``def``, lambdas are expressions, not statements.
* The body of a lambda is a single expression, not a block of statements.

Why use lambda?
---------------
They are simpler coding constructs in scenarios where you just need to embed small bits of executable code inline at places where you cannot use a ``def``.
