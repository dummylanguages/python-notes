.. _functions:

*********
Functions
*********
We have been using some built-in functions in earlier sections. Here we are gonna take care of how to *create our own new functions*. These custom functions will behave the same as the built-in ones: we call them, pass them values and they return results. 

Why create our own functions?
=============================
Functions are used in all programming languages, where they have be called, routines, subroutines, procedures, or as in Python, just functions. Functions are used for 2 main reasons:

* When programming, a lot of times we are gonna find ourselves writing the same set of instructions to perform some repetitive task. When that happens, programmers group the necessary instructions into blocks of code, and give them a name, so anytime they want to accomplish that task, they just have to call the routine by its name, without having to code it again. So funcions are a way of **maximizing code reuse and minimizing redundancy**.
 
* **Procedural decomposition**. Functions also allow us to implement a complex program by separating the entire proccess into independent and smaller subtasks.
  
Defining functions
==================
Like everything else in Python, functions are just **objects**. To define a function we use the ``def`` statement.

The ``def`` statement
---------------------
The def statement define a new function object and assigns it to a name. Unlike functions in compiled languages such as C, def is an **executable statement**, meaning that our function object does not exist until Python reaches and runs the def. 

.. note:: The def statement defines a function that will be created at runtime.

Its general format is as follows::

	def name(arg1, arg2,... argN): 
		statements

``def`` is a compound statement that consists of:

* A **header**:
	* Starts with the ``def`` keyword.
	* Followed by the **name** of the function.
	* A list of zero or more **parameters** in parentheses.
	* Finish with a colon ``:``

* The **body** of the function. The block of statements that Python executes each time the function is later called. 

For example::

	>>> def greeting():  print('Hello, how are you?')

.. note::  When the body it's only **one statement**, we can write it in the header line, after the colon.

As we said at the beginning, once we have created a function, we can call it like we have been doing so far with Python's built-in functions, no big deal::

	>>> greeting()
	'Hello, how are you?'

The function above doesn't receive any arguments, we just have to call it, and it prints a message to the screen.
  
The ``return`` statement
------------------------
Function bodies often contain a **return statement**. The Python ``return`` statement is **optional** when it’s not present, the function exits when the flow of the program reach the end of the body. When the return statement is reached, it ends the function call and sends a result back to the caller.  If the value is omitted, return sends back a ``None``. Technically, a function without a return statement also returns the ``None`` object automatically.::

	def name(arg1, arg2,... argN): 
			statements
			...
			return value

For example::

	>>> def simpleton(x, y): 
	...     z = x + y
	...     return z

We are gonna call ``simpleton()``, and pass it 2 integers arguments. 

	>>> simpleton(2, 7)
	9

It returns the result of the expression ``2 + 7``.  The returned object was printed here interactively, but if we needed to use it later we could instead assign it to a variable. For example::

	>>> a = simpleton(2, 7)
	>>> a
	9

Polymorphism in Python
----------------------
Let's call again the function above, this time with different arguments::

	>>> simpleton('hello ', 'world')
	'hello world'

This time, instead of passing 2 numbers, we passed 2 strings, and ``simpleton()`` returned the concatenation of these 2 strings as a result.

.. note:: Remember that the ``+`` is an example of **operator overloading**. When used on numbers means **adding**, but when used on strings **concatenation**.

So depending on the type of the arguments, our function behaves differently. This is known as **polymorphism**, meaning that the result of an operation depends on the objects being operated upon. Being Python a **dinamically typed** language, polymorphism is a Python's feature.

.. topic::   Duck typing
	:class: my-topic

	This polymorphic behavior has in recent years come to also be known as **duck typing** The essential idea being that your code is not supposed to care if an object is a duck, only that it quacks. Anything that quacks will do, duck or not.

Let's see another example::

	>>> def foo(x, y):
	...     return x - y
	...
	>>> foo(4, 1)
	3

We defined a new function named ``foo()``, and called it with 2 integers arguments. Let's try to call it with 2 strings::

	>>> foo('hello', 'world')
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	  File "<stdin>", line 2, in foo
	TypeError: unsupported operand type(s) for -: 'str' and 'str'

If we pass in objects that are not supported in the operations inside the function(what is known as the **interface** of the function), Python will automatically detect it and raise an exception.

Functions and modules
=====================

.. todo:: 
	Usually functions are coded in module files, and imported from them. Write example with 3 files as an intro to modules and scopes.