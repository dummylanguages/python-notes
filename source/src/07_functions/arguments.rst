.. _arguments:

*********
Arguments
*********
When we call a function with some arguments, we are passing arguments, sending objects as input so the function can do its work with them.

.. todo:: Talk about passing arguments by **position** and by **name**