.. Python notes documentation master file, created by
   sphinx-quickstart on Wed Dec 17 19:48:51 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###############
ABOUT THIS SITE
###############

.. toctree::
	:caption: The Python Programming Language
	:numbered: 3
	:hidden:
   
	src/01_about_python/index
	src/02_basic_concepts/index
	src/modules/index
	src/import_packages/index
	src/distribution_packages/index
	src/03_types/index
	src/04_data_structures/index
	src/05_files/index
	src/06_statements/index
	src/07_functions/index
	src/09_OOP/index
	src/context_management_protocol/index
	src/10_testing/index
.. toctree::
	:maxdepth: 3
	:caption: Appendices
	:hidden:

	src/appendices/installing_python
	src/appendices/virtual_envs/index
	src/appendices/sphinx_projects/index
	src/appendices/unicode
	src/appendices/ASCII_tables
	src/appendices/bibliography
	src/appendices/todo

The main purpose of this website is to serve as my personal notes about Python programming. After breaks, getting back to programming sometimes requires me a lot of google. Having everything written down, will save me some time in the future. So I think of these as sort of *notes for my future self*.

There are a couple of additional reasons:

* Taking notes is a fantastic way of reinforcing things as I learn them.
* Some people may find them useful. 


You may want to create the **setup** described in the following sections. It's necessary to **build locally**, instead of depending on the Gitlab pipeline.

Install Python 3
----------------
This time, I used the official **graphical installer** for macOS. Once the installation is finished, we'll find that the installer has done 2 things:

* Created a copy of our ``~/.bash_profile`` named  ``~/.bash_profile.pysave``.
* Added the following lines to our ``~/.bash_profile``::

    # Setting PATH for Python 3.7
    # The original version is saved in .bash_profile.pysave
    PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
    export PATH

Some **3rd party packages** are Python programs that we can call it from our command line, as long as their location is in our ``$PATH``. That's exactly what the lines above are doing. But this is the location of the *executables* for packages that we install to the **global site-packages**.

If we want to install to the **user site-packages**, we have to add other location to our PATH::

	export PATH="/Users/javi/Library/Python/3.7/bin:${PATH}"

.. note:: If you are sure you are not gonna install to the **global site-packages**, you can safely remove the lines added by the Python installer to your shell configuration.

Install packages for creating virtual environments
--------------------------------------------------
We need to install a couple of packages for **Python 3** using the ``pip3`` command::

    $ pip3 install --user virtualenv virtualenvwrapper

In order for ``virtualenvwrapper`` to work properly, add the following lines to your **shell configuration** file::

	export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
	export PROJECT_HOME=$HOME/code
	source /Users/javi/Library/Python/3.7/bin/virtualenvwrapper.sh

.. note:: After editing it, don't forget to restart your terminal or reload our shell configuration using the ``source`` command.

Clone this repo
---------------
Move to the directory where you want to clone this repo and run::

    $ cd ~/code/notes
    $ git clone https://gitlab.com/dummylanguages/python-notes.git

Create a virtual environment for Sphinx
---------------------------------------
Then we have to create a virtual environment and link it to the folder of the local repo. Easily done using the ``-a`` flag with a ``virtualenvwrapper`` command::

    $ mkvirtualenv python-notes-env -a ~/code/notes/python-notes

Installing Sphinx and Readthedocs theme
---------------------------------------
With our virtual environment enabled (``$ workon unixlifeenv``) we'll install a couple of Python packages **only for that environment** using::

    $ pip install sphinx sphinx_rtd_theme

Once everything is installed you can generate the documentation with::
    
    $ make html


.. _`Here`: https://gitlab.com/dummylanguages/unixlife
.. _`virtualenv`: https://gitlab.com/dummylanguages/unixlife
.. _`virtualenvwrapper`: hhttps://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file
.. _`additional line`: https://virtualenvwrapper.readthedocs.io/en/latest/install.html?highlight=VIRTUALENVWRAPPER_PYTHON#python-interpreter-virtualenv-and-path