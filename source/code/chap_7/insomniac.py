# imsomniac.py

import time

print('*' * 48 + "\nCan't sleep? Let's count 'til 100 sheep! \
\nPress <CTRL-C> if you fall asleep before 100 ;) \n" + '*' * 48)

for x in range(1, 101):
    print(x, 'sheep...')
    time.sleep(1)