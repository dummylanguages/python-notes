# File: updatedb.py

import shelve

db = shelve.open('employees.db')    # Reopen our database 

for key in sorted(db):
    print(key, '\t=>', db[key])

Sue = db['Susan Jones']     # Fetching an object by key
Sue.giveRaise(10)           # Update in memory

db['Susan Jones'] = Sue     # Update in shelve

print('\nAfter the raise Susan makes: {}'.format(db['Susan Jones'].pay))

db.close()                  # Close after making changes