class Mut:
    '''
    Implementing encapsulation with mutators
    '''
    def __init__(self,x):
        self.__x = x

    def getX(self):
        return self.__x

    def setX(self, x):
        self.__x = x

class MutV2:
    '''
    Changing the setter: the interface is the same.
    '''
    def __init__(self,x):
        self.setX(x)

    def getX(self):
        return self.__x

    def setX(self, x):
        if x < 1:
            self.__x = 0
        elif x > 9:
            self.__x = 9
        else:
            self.__x = x