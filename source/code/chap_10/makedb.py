# File: makedb.py

from person import Person, Manager  # Load our classes

# Re-create objects to be stored
Bob = Person('Robert Smith')
Sue = Person('Susan Jones', job='dev', pay=100000) 
Tom = Manager('Thomas Jones', 50000)

import shelve

db = shelve.open('employees.db')    # Filename where objects are stored 

for obj in (Bob, Sue, Tom):         # Use object's name attr as key
    db[obj.name] = obj              # Store object on shelve by key

db.close()                          # Close after making changes