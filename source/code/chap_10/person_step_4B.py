# File: person.py

class Person:
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay
    
    def lastName(self):
        return self.name.split()[-1]
    
    def giveRaise(self, percent):
        self.pay = self.pay * (100 + percent) / 100

    def __repr__(self):
        return '[Person: {} - {} - {}]'.format(self.name, self.job, self.pay)

class Manager(Person):
    def giveRaise(self, percent, bonus=10):
        Person.giveRaise(self, percent + bonus)

# Tests
if __name__ == '__main__':

    Bob = Person('Robert Smith')
    Sue = Person('Sue Jones', 'Developer', 10000)
    Tom = Manager('Tom Jones', 'Manager', 50000)

    # Printing objects
    # print(Sue)
    # print(Bob)
    # print(Tom) 

    # Last names
    # print("Bob's last name is: ", Bob.lastName())
    # print("Sue's last name is: ", Sue.lastName())

    # Raises
    #print('Givin Sue a raise...:')
    #Sue.giveRaise(10)
    #print("Mrs. {} now makes: {:,.2f}".format(Sue.lastName(), Sue.pay))
    
    # print('Before the raise, Tom makes: {}'.format(Tom.pay))
    # print('Giving Tom a raise of 10%...:')
    # Tom.giveRaise(10)
    # print("Mr. {} now makes: {:,.2f}".format(Tom.lastName(), Tom.pay))
    
    print('Before the raise:')
    for obj in (Bob, Sue, Tom):
        print(obj)

    print('\nGiving a raise to all the employees:')
    for obj in (Bob, Sue, Tom):
        obj.giveRaise(10)
        print(obj)

    
