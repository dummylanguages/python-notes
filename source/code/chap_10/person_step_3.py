# File: person.py

class Person:
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay
    
    def lastName(self):
        return self.name.split()[-1]
    
    def giveRaise(self, percent):
        self.pay = self.pay * (100 + percent) / 100

    def __repr__(self):
        return '[Person: {} - {} - {}]'.format(self.name, self.job, self.pay)

# Tests
if __name__ == '__main__':

    Bob = Person('Robert Smith')
    Sue = Person('Sue Jones', 'Developer', 10000)

    # Printing objects
    print(Bob)
    print(Sue)
    
    # Last names
    # print("Bob's last name is: ", Bob.lastName())
    # print("Sue's last name is: ", Sue.lastName())

    # Raises
    #print('Givin Sue a raise of 10%...:')
    #Sue.giveRaise(10)
    #print("Mrs. {} now makes: {:,.2f}".format(Sue.lastName(), Sue.pay))

