# File: person.py

class Person:
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay

# Tests
Bob = Person('Robert Smith')
Sue = Person('Sue Jones', 'Developer', 10000)

print('{} - {}'.format(Bob.name, Bob.pay))
print('{} - {} - {}'.format(Sue.name, Sue.job, Sue.pay))