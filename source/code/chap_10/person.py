# File: person.py (Final version)
"""
Record and process information about people. 
Run this file directly to test its classes. 
"""
from classtools import AttrDisplay

class Person(AttrDisplay):
    """
    Create and process person records
    """
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay
    
    def lastName(self):
        return self.name.split()[-1]
    
    def giveRaise(self, percent):
        self.pay = self.pay * (100 + percent) / 100

class Manager(Person):
    """
    A customized Person with special requirements 
    """
    def __init__(self, name, pay): 
        Person.__init__(self, name, 'Manager', pay)     # Job name is implied

    def giveRaise(self, percent, bonus=10):
        Person.giveRaise(self, percent + bonus)

# Tests
if __name__ == '__main__':

    Bob = Person('Robert Smith')
    Sue = Person('Sue Jones', 'Developer', 10000)
    Tom = Manager('Tom Jones', 50000)

    # Printing objects
    print(Sue)
    print(Bob)
    print(Tom)
       
    # Last names
    print("Bob's last name is: ", Bob.lastName())
    print("Sue's last name is: ", Sue.lastName())

    # Raises
    print('Givin Sue a raise...:')
    Sue.giveRaise(10)
    print("Mrs. {} now makes: {:,.2f}".format(Sue.lastName(), Sue.pay))
    print('Before the raise, Tom makes: {}'.format(Tom.pay))
    print('Giving Tom a raise of 10%...:')
    Tom.giveRaise(10)
    print("Mr. {} now makes: {:,.2f}".format(Tom.lastName(), Tom.pay))
    