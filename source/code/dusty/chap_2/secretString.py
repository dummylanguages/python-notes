class SecretString:
       '''A not-at-all secure way to store a secret message.'''

       def __init__(self, secret, password):
           self.__secret = secret
           self.__password = password

       def decrypt(self, password):
           '''Only show the message if the password is correct.'''
           if password == self.__password:
               return self.__secret
           else:
                return '' 