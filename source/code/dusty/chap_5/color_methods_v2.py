class Color:
    def __init__(self, hex_value, name):
        self._hex_value = hex_value
        self._name = name

    def set_name(self, name):
        if not name:
            raise Exception("Invalid Name")
        self._name = name
    
    def get_name(self):
        return self._name