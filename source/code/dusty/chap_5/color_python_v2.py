class Color:
    def __init__(self, hex_value, name):
        self.hex_value = hex_value
        self._name = name
    
    def _set_name(self, name):
        if not name:
            raise Exception("Invalid Name")
        self._name = name
    
    def _get_name(self):
        return self._name
        
    name = property(_get_name, _set_name)