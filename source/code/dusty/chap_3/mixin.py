from contactManager import ContactList, Contact, Supplier, Friend

class MailSender:
    def send_mail(self, message):
        print("Sending mail to " + self.email)
        # Add e-mail logic here

class EmailableContact(Contact, MailSender):
       pass

class AddressHolder:
       def __init__(self, street, city, state, code):
           self.street = street
           self.city = city
           self.state = state
           self.code = code