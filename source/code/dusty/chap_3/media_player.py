class AudioFile:
	def __init__(self, filename):
		if not filename.endswith(self.ext):
			raise Exception('Invalid File Format')

		self.filename = filename

class MP3File(AudioFile):
	ext = 'mp3'
	def play(self):
		print('Playing {} as mp3'.format(self.filename))

class WavFile(AudioFile):
	ext = 'wav'
	def play(self):
		print('Playing {} as wav'.format(self.filename))

class OggFile(AudioFile):
	ext = 'ogg'
	def play(self):
		print('Playing {} as ogg'.format(self.filename))

if __name__ == '__main__':
	print("file_1 = MP3File('imagine.mp3')")
	file_1 = MP3File('imagine.mp3')

	print("file_1.play()")
	print(file_1.play())

	print("file_2 = OggFile('even flow.mp3')")
	file_2 = OggFile('even flow.mp3')

	print("file_2.play()")
	print(file_2.play())