class Contact:

    all_contacts = []
    
    def __init__(self, name, email):
        self.name = name
        self.email = email
        Contact.all_contacts.append(self)

class Supplier(Contact):

    def order(self, order):
        print("If this were a real system we would "
        "send {} order to {}".format(order, self.name))

# Tests
if __name__ == '__main__':

    print(">>> a_contact = Contact('John Doe', 'johnny@gmail.com')")
    a_contact = Contact('John Doe', 'johnny@gmail.com')

    print(">>> a_supplier = Supplier('Nuts & Bolts', 'nutty@nuts.com')")
    a_supplier = Supplier('Nuts & Bolts', 'nutty@nuts.com')

    print('>>> print(a_contact.name, a_contact.email)')
    print(a_contact.name, a_contact.email)

    print('>>> print(a_supplier.name, a_supplier.email)')
    print(a_supplier.name, a_supplier.email)

    print('>>> a_contact.all_contacts')
    print(a_contact.all_contacts)

    print(">>> a_supplier.order('I need nuts')")
    a_supplier.order('I need nuts')          