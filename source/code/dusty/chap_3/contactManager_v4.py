class ContactList(list):
    def search(self, name):
       '''Return all contacts that contain the search value
       in their name.'''
       matching_contacts = []
       for contact in self:
           if name in contact.name:
               matching_contacts.append(contact)
       return matching_contacts

class Contact:
   all_contacts = ContactList()
   def __init__(self, name, email):
       self.name = name
       self.email = email
       Contact.all_contacts.append(self)

class AddressHolder:
       def __init__(self, street, city, state, code):
           self.street = street
           self.city = city
           self.state = state
           self.code = code

class Supplier(Contact):
    def order(self, order):
        print("If this were a real system we would send "
           "{} order to {}".format(order, self.name))

class Friend(Contact, AddressHolder):
    def __init__(self, name, email='', phone='', street='', city='', state='', code=''):
        Contact.__init__(self, name, email)
        AddressHolder.__init__(self, street, city, state, code)
        self.phone = phone 

if __name__ == '__main__':

    print(">>> friend_1 = Friend('Johnny', phone='333-4457 00')")
    friend_1 = Friend('Johnny', phone='333-4457 00') 

    print(">>> friend_2 = Friend('Sara', 'sarita@mail.ru', city='Moscow')")
    friend_2 = Friend('Sara', 'sarita@gmail.com', city='Moscow')

    print('>>> print(friend_1.name, friend_1.email, friend_2.city)')
    print(friend_1.name, friend_1.email, friend_2.city)

    print('>>> a_contact.all_contacts')
    print(friend_2.all_contacts)

                 