def funny_division(anumber):
    try:
        return 100 / anumber
    except ZeroDivisionError:
        return "Silly wabbit, you can't divide by zero!"

def funny_division_v2(anumber):
    try:
        if anumber == 13:
           raise ValueError("13 is an unlucky number")
        return 100 / anumber

    except (ZeroDivisionError, TypeError):
        return "Enter a number other than zero"

def funny_division_v3(anumber):
    try:
        if anumber == 13:
           raise ValueError("13 is an unlucky number")
        return 100 / anumber
        
    except ZeroDivisionError:
        return "Enter a number other than zero"
    except TypeError:
        return "Enter a numerical value"
    except ValueError:
        print("No, No, not 13!")
        raise