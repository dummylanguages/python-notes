def foo():
    print("I'm in: " + __name__)

def bar():
    print("I'm also in: " + __name__)

def _foo():
    print("I'm hidden")