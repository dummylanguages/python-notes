>>> print()                        # If no object is given, prints just the value in ``end``.

>>> print('spam','eggs', sep='...')            # Default separator is a white space.
spam...eggs
>>> print('spam', end=' ')                     # After the printed text, just an empty string
spam >>>                                       # The prompt appears just after ``spam``
>>> print('spam', ile=open('data.txt', 'w'))   # The output is sent to the file ``data.txt``
