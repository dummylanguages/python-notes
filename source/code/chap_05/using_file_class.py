from file_class import File

files = []

for _ in range(1000):
    with File('foo.txt', 'w') as f:
        print('Hello world!', file=f)
        files.append(f)