import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-bob",
    version="0.0.1",
    author="Bobby Wan-kenobi",
    author_email="bobbywan@example.com",
    description="Greets the whole planet.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/bobforce/skeleton_example",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)