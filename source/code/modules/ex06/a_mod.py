def f1():
    print("I'm a function in " + __name__)

def _f2():
    print("I'm hidden")

def f3():
    print("I'm another function in " + __name__)