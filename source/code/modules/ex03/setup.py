from setuptools import setup

setup(
    name = 'the_module',
    version = '1.0',
    py_modules = ['the_module']
)