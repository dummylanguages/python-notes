def km_to_miles(km):
    return km / 1.609

def miles_to_km(mi):
    return mi * 1.609