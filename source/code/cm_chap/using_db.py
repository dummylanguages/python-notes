from db_context_manager import UseDB

dbconfig = {
    'host': 'localhost',
    'user': 'bob',
    'password': '1234',
    'database': 'test'
}

with UseDB(dbconfig) as cursor:
    _SQL = '''SHOW TABLES'''
    cursor.execute(_SQL)
    data = cursor.fetchall()
    print(data)