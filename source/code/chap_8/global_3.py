x = 'mango'		# Global scope

def fun():
	global x	# We give this name global scope
	x = 'papaya'
	return x	

print('x in fun() = ', fun())
print('x in main = ', x)