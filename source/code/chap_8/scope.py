x = 'mango'		# Global scope

def fun():
	x = 'papaya'	# Local scope
	return x

print('In fun(), x = ', fun())
print('In main, x = ', x)