x = 'mango'		# Global scope

def fun():
	global x
	x = 'papaya'	# Global scope
	return x

print('In main, x = ', x)
print('In fun(), x = ', fun())
print('In main, after calling func(), x = ', x)

x = 'pinneaple'
print('\nIn main, after a reassignment x = ', x)
print('In func(), x = ', fun())