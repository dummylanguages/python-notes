x = ['a', 'b']

def func():
    x[1] = 'c'
    return x
    
print('In main, x = ', x)
print('Inside the function, x = ', func())
print('Now in main, x = ', x)