x = 'mango'		# Global scope

def fun():
	x = 'papaya'
	return x	

print('x in fun() = ', fun())
print('x in main = ', x)