# Python notes
JThis repo contains mostly a bunch of [reStructuredText][1] files used by [sphinx][2] to generate documentation in several formats, being HTML one of them. Once generated, the documentation can be browsed locally, or hosted almost anywhere, since it's just a **static site**.

This project, is **hosted for free** at: https://dummylanguages.gitlab.io/python-notes/

The **source files** can be found at: https://gitlab.com/dummylanguages/python-notes

### Automatic build
GitLab also offers the possibility of automatically generating the documentation server-side(CI/CD). Every time we push our changes, we trigger a fresh build of the site.

> CI stands for **Continuous Integration**, CD for **Continuous Deployment**

Next we'll describe what do we have to do in order to publish our site in **Gitlab Pages**. Everything is based on the [Gitlab docs][4], they explain it great.

#### First thing: a project
The **first step**, it's, obviously, to have a **project** in place. Once we have a project, Gitlab can automatically build, test, and deploy it to **Gitlab Pages**, according to a predefined **CI/CD configuration**. This configuration lives in a file named `.gitlab-ci.yml`, placed in the root of our repo. 

#### CI/CD configuration
There are several ways to get this predefined configuration:

1. When we are creating a **New project**, next to the **Blank Project** button there's another one named [Create from template][5]. There we can select among 17 **built-in** templates, but unfortunately **Sphinx** wasn't in the list.

2. The other option is to fork one of the **example projects** from [this page][6], fortunately there was one for [Sphinx][6]. Instead of forking that site, since I already had my documentation started, I decided to just copy the `.gitlab-ci.yml` file. It looked something like this::

```
image: alpine

pages:
  script:
  - apk --no-cache add py2-pip python-dev
  - pip install sphinx
  - apk --no-cache add make
  - make html
  - mv _build/html/ public/
  artifacts:
    paths:
    - public
  only:
  - master
```

On the main repo page, there's a button named **Set up CI/CD**, if we click it we can create the configuration. I did that, pasted the file above in there, with some small modification to adjust it to my my Sphinx settings (the build directory in the ``Makefile``, and the **sphinx_rtd_theme**).

> Read here about [Creating and Tweaking GitLab CI/CD for GitLab Pages][8].

### Enable shared Runners
After that we have to configure our GitLab project to use a **Runner**, so that each commit or
push triggers our CI pipeline. We can enable it in **Project** (sidebar to the left), on **Settings** under **CI/CD**, and green button **Enable shared Runners**.

#### Triggering a build
Every time we push our changes, a new build is triggered. We can even check our **build** running if we go to [Pipelines][3] (in project settings, under CI/CD) It may be useful for debugging purposes. 

> I think we can also manually trigger a build, by clicking on **Run Pipeline** and then on **Create Pipeline**.

#### Baseurl
According to the [Gitlab docs][9], we also have to configure the **baseurl** in our configuration file. The baseurl is the URL which points to the root of the HTML documentation, in the case of this project should be ``https://dummylanguages.gitlab.io/``. I guess that's because if we have **internal links** between pages in our documentation, we want to make sure that they start with the correct **baseurl**, otherwise, the links will not work.

Truth is, I checked ine my ``conf.py`` and didn't find this setting, but everything seemed to be working fine...


#### User or group website
We have to rename our repository to ``namespace.gitlab.io/``, where **namespace** equals to:
* **username** 
* **group name**

1. In this case my username is **dummylanguages**. We can do that in **project settings** under **General** and **Advanced**; make sure the the **Path** textbox reads: ``dummylanguages.gitlab.io``.

2. Also, the project should be named like ``python-notes.gitlab.io``, but in my case I just created it as ``pyton-notes``.

Truth is, I didn't do any of this, and the site seems to be working fine...


[1]: http://docutils.sourceforge.net/rst.html
[2]: http://www.sphinx-doc.org/en/master
[3]: https://gitlab.com/dummylanguages/python-notes/pipelines
[4]: https://gitlab.com/help/user/project/pages/index.md
[5]: https://gitlab.com/projects/new?nav_source=navbar
[6]: https://gitlab.com/pages
[7]: https://gitlab.com/pages/sphinx
[8]: https://gitlab.com/help/user/project/pages/getting_started_part_four.md
[9]: https://gitlab.com/help/user/project/pages/getting_started_part_two.md#urls-and-baseurls